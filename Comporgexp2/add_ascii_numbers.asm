# File:		add_ascii_numbers.asm
# Author:	K. Reek
# Contributors:	P. White, W. Carithers
#		<<Ernesto Soltero>>
#
# Updates:
#		3/2004	M. Reek, named constants
#		10/2007 W. Carithers, alignment
#		09/2009 W. Carithers, separate assembly
#
# Description:	Add two ASCII numbers and store the result in ASCII.
#
# Arguments:	a0: address of parameter block.  The block consists of
#		four words that contain (in this order):
#
#			address of first input string
#			address of second input string
#			address where result should be stored
#			length of the strings and result buffer
#
#		(There is actually other data after this in the
#		parameter block, but it is not relevant to this routine.)
#
# Returns:	The result of the addition, in the buffer specified by
#		the parameter block.
#

	.globl	add_ascii_numbers

add_ascii_numbers:
A_FRAMESIZE = 40

#
# Save registers ra and s0 - s7 on the stack.
#
	addi 	$sp, $sp, -A_FRAMESIZE
	sw 	$ra, -4+A_FRAMESIZE($sp)
	sw 	$s7, 28($sp)
	sw 	$s6, 24($sp)
	sw 	$s5, 20($sp)
	sw 	$s4, 16($sp)
	sw 	$s3, 12($sp)
	sw 	$s2, 8($sp)
	sw 	$s1, 4($sp)
	sw 	$s0, 0($sp)
	
# ##### BEGIN STUDENT CODE BLOCK 1 #####

	lw	$s0, 0($a0) #first string to add
	lw	$s1, 4($a0) #second string to add
	lw	$s2, 8($a0) #place to put the result
	lw	$s3, 12($a0) #length

	move	$s4, $zero #start carry at 0
	addi	$s3, $s3, -1 #start at n-1

loop:
	beq	$s3, $zero, first
	add	$t0, $s0, $s3 #current place of the first string
	add	$t1, $s1, $s3 #current place of the second string
	add	$t2, $s2, $s3 #current place of the result string
	lb	$t3, 0($t0) #first number
	lb	$t4, 0($t1) #second number
	add	$s5, $t3, $s4 #add carry first
	add	$s5, $s5, $t4 #add second number
	addi	$s5, $s5, -48 #subtract 
	move	$s4, $zero #reset carry
	slti	$t5, $s5, 58
	beq	$t5, $zero, carry
	sb	$s5, 0($t2)
	addi	$s3, $s3, -1
	j	loop

carry:
	addi	$s4, $s4, 1
	addi	$s5, $s5, -58
	slti	$t6, $s5, 58
	beq	$t6, $zero, carry
	addi	$s5, $s5, 48
	sb	$s5, 0($t2)
	addi	$s3, $s3, -1
	j	loop

first:
	lb	$t7, 0($s0)
	lb	$t8, 0($s1)
	add	$s5, $t7, $s4
	add	$s5, $s5, $t8
	addi	$s5, $s5, -48
	slti	$t8, $s5, 58
	beq	$t8, $zero, remove_carry
	sb	$s5, 0($s2)
	j	done

remove_carry:
	addi	$s5, $s5, -58
	slti	$t9, $s5, 58
	beq	$t9, $zero, remove_carry
	addi	$s5, $s5, 48
	sb	$s5, 0($s2)

done:
# ###### END STUDENT CODE BLOCK 1 ######

#
# Restore registers ra and s0 - s7 from the stack.
#
	lw 	$ra, -4+A_FRAMESIZE($sp)
	lw 	$s7, 28($sp)
	lw 	$s6, 24($sp)
	lw 	$s5, 20($sp)
	lw 	$s4, 16($sp)
	lw 	$s3, 12($sp)
	lw 	$s2, 8($sp)
	lw 	$s1, 4($sp)
	lw 	$s0, 0($sp)
	addi 	$sp, $sp, A_FRAMESIZE

	jr	$ra			# Return to the caller.

/**
 * Book.java
 * 
 * Versions: 
 * $Id: Book.java,v 1.4 2012-04-11 00:33:20 exs6350 Exp $
 * 
 * Revisions:
 * $Log: Book.java,v $
 * Revision 1.4  2012-04-11 00:33:20  exs6350
 * Added comments
 *
 * Revision 1.3  2012-04-10 21:32:48  exs6350
 * Fixed a couple of output errors with periods in wrong place
 *
 * Revision 1.2  2012-04-10 17:10:02  exs6350
 * Initial finish haven't tested it
 *
 * Revision 1.1  2012-04-06 05:57:05  exs6350
 * Initial
 *
 * 
 */

/**
 * Abstract class that holds book methods and other data.
 * @author Ernesto Soltero (exs6350@rit.edu)
 *
 */
public abstract class Book {
	private String authorName;
	private String bookTitle;
	int cost;
	private Media bookType;
	
	/**
	 * Constructor for the abstract book class
	 * @param title String of the title of the book
	 * @param author String of the author name.
	 * @param cost Int of the cost of the book in cents
	 * @param media Type of media
	 */
	public Book(String title, String author, int cost, Media media){
		this.bookTitle = title;
		this.authorName = author;
		this.cost = cost;
		this.bookType = media;
	}
	
	/**
	 * Outputs the object into a string in the format:
	 * BookTitle
	 * 		Author
	 * 		Media type
	 */
	public String toString(){
		String info = "\"" + bookTitle + "\"." + "\n" + "\t"
		+ authorName + ".\n" + "\t" + bookType;
		return info;
	}
	
	/**
	 * Declares is the media type is for sale or not to be 
	 * filled in by the other classes.
	 */
	public abstract boolean isForSale();
	
	/**
	 * Returns the name of the title.
	 * @return String-title name
	 */
	public String getTitle(){
		return bookTitle;
	}
	
	/**
	 * Returns the author name
	 * @return String- author name
	 */
	public String getAuthor(){
		return authorName;
	}
	
	/**
	 * Calculates the cost for the book
	 * @return Int the price of the book
	 */
	public double getCost(){
		double dollars = ((double)cost / 100) + ((double)cost%100) / 100;
		return dollars;
	}
	
	/**
	 * Returns the media type.
	 * @return String the media type
	 */
	public String getMedia(){
		return bookType.toString();
	}
}

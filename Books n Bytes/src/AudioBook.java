/**
 * AudioBook.java
 * 
 * Version:
 * $Id: AudioBook.java,v 1.3 2012-04-11 00:33:20 exs6350 Exp $
 * 
 * Revisions:
 * $Log: AudioBook.java,v $
 * Revision 1.3  2012-04-11 00:33:20  exs6350
 * Added comments
 *
 * Revision 1.2  2012-04-10 21:48:18  exs6350
 * Fixed more output errors
 *
 * Revision 1.1  2012-04-10 17:10:03  exs6350
 * Initial finish haven't tested it
 *
 */

/**
 * Represents a Audiobook class that contains methods for buying
 * an audiobook.
 * @author Ernesto Soltero (exs6350@rit.edu)
 *
 */
public class AudioBook extends Book{

	private Integer numDisc;
	
	/**
	 * Uses the superclass constructor and also initializes the number
	 * of discs.
	 * @param title The title of the book
	 * @param author The name of the author
	 * @param cost The cost of the book
	 * @param numDiscs The number of disks the media contains.
	 */
	public AudioBook(String title, String author, int cost, int numDiscs) {
		super(title, author, cost, Media.Audio);
		this.numDisc = numDiscs;
	}
	
	/**
	 * Returns false because this can only be rented.
	 */
	public boolean isForSale(){return false;}
	
	/**
	 * Uses the super.getMedia function and adds the number of disks to it.
	 */
	public String getMedia(){
		return super.getMedia() + " : "+ numDisc.toString() + " disks.";
	}

	/**
	 * Uses the super.toString method and adds the number of disks to it.
	 */
	public String toString(){
		return super.toString() + ": " + numDisc + " disks." ;
	}
}

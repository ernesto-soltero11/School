/**
 * HardcoverBook.java
 * 
 * Version: 
 * $Id: HardcoverBook.java,v 1.5 2012-04-11 00:33:20 exs6350 Exp $
 * 
 * Versions:
 * $Log: HardcoverBook.java,v $
 * Revision 1.5  2012-04-11 00:33:20  exs6350
 * Added comments
 *
 * Revision 1.4  2012-04-10 21:48:18  exs6350
 * Fixed more output errors
 *
 * Revision 1.3  2012-04-10 21:32:48  exs6350
 * Fixed a couple of output errors with periods in wrong place
 *
 * Revision 1.2  2012-04-10 21:22:29  exs6350
 * Revised store.java
 *
 * Revision 1.1  2012-04-10 17:10:03  exs6350
 * Initial finish haven't tested it
 *
 */

/**
 * Represents a hardcover book with a cloth or leather
 * cover and contains the methods to buy a hardcover book.
 * @author Ernesto Soltero (exs6350@rit.edu)
 *
 */
public class HardcoverBook extends Book{

	private String material;
	
	/**
	 * Uses the super constructor and initializes the cover material.
	 * @param title The name of the book
	 * @param author The name of the author.
	 * @param cost The cost of the book.
	 * @param coverMaterial The type of material.
	 */
	public HardcoverBook(String title, String author, int cost, String coverMaterial){
		super(title,author,cost,Media.Hardcover);
		this.material = coverMaterial;
	}
	
	/**
	 * Returns true becuase this book can be bought.
	 */
	public boolean isForSale(){return true;}
	
	/**
	 * Uses the super class getMedia and adds the type of material to it.
	 */
	public String getMedia(){return super.getMedia() + material;}
	
	/**
	 * Returns the type of material that the book currently has.
	 * @return String the type of material.
	 */
	public String getCoverMaterial(){return material;}
	
	/**
	 * Uses the super class to string method and adds the cover material to it.
	 */
	public String toString(){
		return super.toString() + " " + getCoverMaterial() + ".";
	}
}

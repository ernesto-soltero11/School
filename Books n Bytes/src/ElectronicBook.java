/**
 * ElectronicBook.java
 * 
 * Version:
 * $Id: ElectronicBook.java,v 1.4 2012-04-11 00:33:20 exs6350 Exp $
 * 
 * Revisions:
 * $Log: ElectronicBook.java,v $
 * Revision 1.4  2012-04-11 00:33:20  exs6350
 * Added comments
 *
 * Revision 1.3  2012-04-10 21:48:18  exs6350
 * Fixed more output errors
 *
 * Revision 1.2  2012-04-10 21:22:29  exs6350
 * Revised store.java
 *
 * Revision 1.1  2012-04-10 17:10:02  exs6350
 * Initial finish haven't tested it
 *
 */

/**
 * The Electronic book class represents a url book 
 * and contains methods for buying a electronicbook.
 * @author Ernesto Soltero (exs6350@rit.edu)
 *
 */
public class ElectronicBook extends Book{

	private String theURL;
	
	/**
	 * Uses the super class constructor and initializes the url.
	 * @param title The name of the book
	 * @param author The name of the author
	 * @param cost The cost of the book
	 * @param url The string representation of the url
	 */
	public ElectronicBook(String title, String author, int cost, String url){
		super(title, author, cost, Media.Electronic);
		this.theURL = url;
	}
	
	/**
	 * Uses the superclass getMedia method and adds the String url to it.
	 */
	public String getMedia(){
		return super.getMedia() + " : " + theURL;
	}
	
	/**
	 * Returns false becuase this media can only be rented.
	 */
	public boolean isForSale(){return false;}
	
	/**
	 * Uses the super class toString method and adds the url to it.
	 */
	public String toString(){
		return super.toString() + " from " + theURL;
	}
}

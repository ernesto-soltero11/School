/**
 * PaperbackBook.java
 * Versions:
 * $Id: PaperbackBook.java,v 1.3 2012-04-11 00:33:20 exs6350 Exp $
 * 
 * Revisions:
 * $Log: PaperbackBook.java,v $
 * Revision 1.3  2012-04-11 00:33:20  exs6350
 * Added comments
 *
 * Revision 1.2  2012-04-10 21:22:29  exs6350
 * Revised store.java
 *
 * Revision 1.1  2012-04-10 17:10:03  exs6350
 * Initial finish haven't tested it
 *
 * 
 */

/**
 * A PaperbackBook that inherits method from Book class.
 * @author Ernesto Soltero (exs6350@rit.edu)
 *
 */
public class PaperbackBook extends Book{
	
	/**
	 * Constructor uses the super class constructor as defualt.
	 * @param title The title of the book
	 * @param author The name of the author
	 * @param cost The cost of the book
	 */
	public PaperbackBook(String title, String author, int cost){
		super(title,author, cost,Media.Paperback);
	}
	
	/**
	 * Returns true because this can be bought.
	 */
	public boolean isForSale(){return true;}
	
	/**
	 * Uses the super class toString method and adds a period.
	 */
	public String toString(){
		return super.toString() + "."; 
	}
}

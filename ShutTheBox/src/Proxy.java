/**
 * @author Ernesto Soltero (exs6350@rit.edu)
 * 
 */
public interface Proxy
{

	/**
	 * Ends player turn
	 */
	public void done();

	/**
	 * Shuts down the session
	 */
	public void quit();
	
	/**
	 * Changes the state of the tile .
	 * @param num - The index of the tile to flip
	 * 
	 */
	public void flipTile(int num);

	/**
	 * Rolls the dice
	 * 
	 */
	public void roll();

}
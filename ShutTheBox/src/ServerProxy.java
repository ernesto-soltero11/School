import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.util.Random;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

/**
 * @author Ernesto Soltero (exs6350@rit.edu)
 *
 */
public class ServerProxy {

	private int serverPort;
	private InetAddress serverAddress;
	private DatagramSocket serverSocket;
	private Executor threadPool;
	
	public ServerProxy(int serv, InetAddress add, DatagramSocket socket){
		this.serverPort = serv;
		this.serverAddress = add;
		this.serverSocket = socket;
		this.threadPool = Executors.newCachedThreadPool();
	}
	
	/**
	 * Sends the message "done" to both clients
	 */
	synchronized public void done(ServerModelClone clone, int player) {
		String score;
		if(player == 1){
			score = "1 " + clone.getScore(0);
		}
		else{score = "2 " + clone.getScore(1);}
		String message = "score " + score;
		byte[] buffer = new byte[message.length()];
		buffer = message.getBytes();
		DatagramPacket packet = new DatagramPacket(buffer, buffer.length, clone.returnOne().returnAddress(), clone.returnOne().returnPort());
		try {
			serverSocket.send(packet);
			packet.setAddress(clone.returnTwo().returnAddress());
			packet.setPort(clone.returnTwo().returnPort());
			serverSocket.send(packet);
		} catch (IOException e) {
			System.err.println("Unable to send packet.");
		}
		
		//Reset if player two is done and send winner
		try{
				if(player == 2){
					int winner = 0;
					if(clone.getScore(0) > clone.getScore(1)){
						winner = 2;
					}
					else if(clone.getScore(0) < clone.getScore(1)){
						winner = 1;
					}
					message = "win " + winner;
					byte[] buff = new byte[message.length()];
					buff = message.getBytes();
					packet.setData(buff);
					serverSocket.send(packet);
					packet.setAddress(clone.returnOne().returnAddress());
					packet.setPort(clone.returnOne().returnPort());
					serverSocket.send(packet);
					clone.reset();
				}
		}
		catch (IOException e) {
			System.err.println("Unable to send packet.");
		}
	}

	/**
	 * Sends the message quit to both players
	 */
	synchronized public void quit(ServerModelClone clone) {
		String mes = "quit";
		byte[] buffer = new byte[mes.length()];
		buffer = mes.getBytes();
		DatagramPacket packet = new DatagramPacket(buffer, buffer.length, clone.returnOne().returnAddress(), clone.returnOne().returnPort());
		try {
			serverSocket.send(packet);
			packet.setAddress(clone.returnTwo().returnAddress());
			packet.setPort(clone.returnTwo().returnPort());
			serverSocket.send(packet);
		} catch (IOException e) {
			System.err.println("Unable to send packet.");
		}
		clone.quit();
	}

	/**
	 * Flips the tile 
	 */
	synchronized public void flipTile(int num, ServerModelClone clone) {
		String mes = "flip " + num;
		byte[] buffer = new byte[mes.length()];
		buffer = mes.getBytes();
		DatagramPacket packet = new DatagramPacket(buffer, buffer.length, clone.returnOne().returnAddress(), clone.returnOne().returnPort());
		try {
			serverSocket.send(packet);
			packet.setAddress(clone.returnTwo().returnAddress());
			packet.setPort(clone.returnTwo().returnPort());
			serverSocket.send(packet);
		} catch (IOException e) {
			System.err.println("Unable to send the packet.");
		}
	}

	/**
	 * Rolls the dice and notifies the clients of the values.
	 */
	synchronized public void roll(ServerModelClone clone) {
		Random rand = new Random();
		int x = rand.nextInt(6) + 1;
		int y = rand.nextInt(6) + 1;
		clone.setDie(0, x);
		clone.setDie(1, y);
		String mes = "dice " + x + " " + y;
		byte[] buffer = new byte[mes.length()];
		buffer = mes.getBytes();
		DatagramPacket packet = new DatagramPacket(buffer, buffer.length, clone.returnOne().returnAddress(), clone.returnOne().returnPort());
		try {
			serverSocket.send(packet);
			packet.setAddress(clone.returnTwo().returnAddress());
			packet.setPort(clone.returnTwo().returnPort());
			serverSocket.send(packet);
		} catch (IOException e) {
			System.err.println("Unable to send the packet.");
		}
	}

	/**
	 * Process the request in a new thread
	 * @param packet - The packet of received data
	 */
	public void process(DatagramPacket packet, ServerModelClone clone){
		ServerThread p = new ServerThread(packet, clone);
		threadPool.execute(p);
	}
	
	/**
	 * 
	 * @author Ernesto Soltero (exs6350@rit.edu)
	 *
	 */
	private class ServerThread implements Runnable{
		
		private DatagramPacket packet;
		private ServerModelClone model;
		
		public ServerThread(DatagramPacket packet, ServerModelClone clone){
			this.packet = packet;
			this.model = clone;
		}
		
		/**
		 * 
		 */
		public void run() {
			String[] receive = new String(packet.getData(), 0, packet.getLength()).split("");
			if(receive[0] == "flip"){
				int index = Integer.valueOf(receive[1]);
				model.flipTile(index);
				flipTile(index, model);
			}
			else if(receive[0] == "done"){
				int player = 0;
				if(packet.getPort() == model.returnOne().returnPort() && packet.getAddress() == model.returnOne().returnAddress()){
					player = 1;
				}
				else{player = 2;}
				done(model, player);
			}
			else if(receive[0] == "quit"){
				model.quit();
				quit(model);
			}
			else if(receive[0] == "roll"){
				roll(model);
			}
		}
	}
}

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.net.SocketException;

/**
 * The client proxy communicates with the server and uses the model to perform box and ui 
 * functions
 * @author Ernesto Soltero (exs6350@rit.edu)
 * 
 */
public class ClientProxy implements Proxy
{
	
//Hidden data members
	private DatagramSocket socket;
	private ClientModelClone controller;
	private static ShutTheBoxUI view;
	private InetAddress host;
	private int port;
	private int	playerNumber;
	private InetAddress clientAddress;
	private int serverPort;
	private String[] names;
	private int[] scores;
	private byte[] buffer;

	/**
	 * Construct a new client proxy.
	 * @param address - The host to connect to
	 * @param port - The port to connect to
	 *
	 */
	
	public ClientProxy(String host, int hostport, String clientAdd, int port, String player) 
	{
		try {
			this.port = port;
			this.socket = new DatagramSocket(port);
			this.host = InetAddress.getByName(host);
			this.serverPort = hostport;
			this.clientAddress = InetAddress.getByName(clientAdd);
			this.scores = new int[] { 0, 0 };
		}
		catch(Exception e){
			System.err.println("Error connecting to the socket.");
		}
	}

	/**
	 * Sets the model listener to the listener
	 * @param modelListener - The model listener to set to 
	 *   
	 */
	public void setMVC(ClientModelClone m, ShutTheBoxUI view)
	{
		this.controller = m;
		this.view = view;
		new ReaderThread().start();
	}

	/**
	 * Join the server.
	 * @param playerName - The name of the player
	 *         
	 */
	public void join(String playerName) 
	{
		String mes = "join " + playerName;
		buffer = new byte[mes.length()];
		DatagramPacket packet = new DatagramPacket(buffer, buffer.length, host, serverPort);
		try {
			socket.send(packet);
		} catch (IOException e) {
			System.out.println("Error sending the packet.");
		}
	}

	/**
	 * Changes the state of the tile
	 * @param x - The location of the tile           
	 * @param face - Flip up or down      
	 *
	 */
	public void flipTile(int x)
	{
		String mes = "flip " + x;
		buffer = new byte[mes.length()];
		DatagramPacket packet = new DatagramPacket(buffer, buffer.length, host, serverPort);
		try {
			socket.send(packet);
		} catch (IOException e) {
			System.out.println("Error sending the packet.");
		}
	}
	
	/**
	 * Rolls the dice
	 *
	 */
	public void roll()
	{
		String mes = "roll";
		buffer = new byte[mes.length()];
		DatagramPacket packet = new DatagramPacket(buffer, buffer.length, host, serverPort);
		try {
			socket.send(packet);
		} catch (IOException e) {
			System.out.println("Error sending the packet.");
		}
	}

	/**
	 * Ends the players turn
	 *
	 */
	public void done()
	{
		String mes = "done";
		buffer = new byte[mes.length()];
		DatagramPacket packet = new DatagramPacket(buffer, buffer.length, host, serverPort);
		try {
			socket.send(packet);
		} catch (IOException e) {
			System.out.println("Error sending the packet.");
		}
	}

	/**
	 * Shuts down the session
	 *
	 */
	public void quit()
	{
		String mes = "quit";
		buffer = new byte[mes.length()];
		buffer = mes.getBytes();
		DatagramPacket packet = new DatagramPacket(buffer, buffer.length, host, serverPort);
		try {
			socket.send(packet);
		} catch (IOException e) {
			System.out.println("Error sending the packet.");
		}
	}

	/**
	 * Class ReaderThread receives messages from the network, decodes them, and
	 * invokes the proper methods to process them using the model controller.
	 *
	 */
	private class ReaderThread extends Thread
	{
		public void run()
		{
			boolean running = true;
			while (running)
			{
				DatagramPacket packet = new DatagramPacket(buffer, buffer.length);
				try {
					socket.receive(packet);
				} catch (IOException e1) {
					System.out.println("Error recieveing the packet.");
				}
				//This packet is not for us!
				if(packet.getPort() != port & packet.getAddress() != clientAddress){
					continue;
				}
				String temp = new String(packet.getData(), 0, packet.getLength());
				buffer = new byte[packet.getLength()];
				String[] recieve = temp.split("");
				if (recieve != null){
					if(recieve[0] == "connected"){
						playerNumber = Integer.valueOf(recieve[3]);
						names[0] = recieve[1];
						names[1] = recieve[2];
						controller.setMessage(recieve[1] + " -- " + recieve[2]);
					}
					else if(recieve[0] == "turn"){
						if(Integer.valueOf(recieve[1]) == playerNumber){
							view.enableButtons(true);
						}
						else{
							view.enableButtons(false);
						}
					}
					else if(recieve[0] == "flip"){
						int pos = Integer.valueOf(recieve[1]);
						controller.flipTile(pos);
					}
					else if(recieve[0] == "dice"){
						int x = Integer.valueOf(recieve[1]);
						int y = Integer.valueOf(recieve[2]);
						controller.setDie(0, x);
						controller.setDie(1, y);
					}
					else if(recieve[0] == "score"){
						int player = Integer.valueOf(recieve[1]);
						int score = Integer.valueOf(recieve[2]);
						
						if(player == 1){
							scores[0] = score;
							controller.setMessage(names[0] + " " + score + " -- " + names[1]);
							view.enableButtons(false);
						}
						else{
							scores[1] = score;
							controller.setMessage(names[0] + " " + scores[0] + " -- " + names[1] + " " + scores[1]);
							view.enableButtons(true);
						}
					}
					else if(recieve[0] == "win"){
						if(recieve[1].equals("0")){
							controller.setMessage(names[0] + " " + scores[0] + " -- " + names[1] + " " + scores[1] + " Tie!");
						}
						else{
							int winner = Integer.valueOf(recieve[1]);
							controller.setMessage(names[0] + " " + scores[0] + " -- " + names[1] + " " + scores[1] + names[winner-1] + " wins!");
							}
						controller.reset();
					}
					else if(recieve[0] == "mes"){
						String message = recieve.toString();
						message = message.substring(3);
						controller.setMessage(message);
						buffer = new byte[32];
					}
					else if(recieve[0] == "quit"){
						try{
							socket.close();
						}
						catch(Exception e){
							System.err.println("Error closing the socket.");
						}
						running = false;
						controller.quit();
					}
				}

			}
		}
	}
}

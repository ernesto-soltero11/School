
/**
 * @author Ernesto Soltero (exs6350@rit.edu)
 *
 */
public class ClientModelClone implements ClientModel {

	private static ShutTheBox box;
	private static ShutTheBoxUI view;
	
	/**
	 * 
	 */
	public ClientModelClone(ShutTheBox game, ShutTheBoxUI ui) {
		this.box = game;
		this.view = ui;
	}

	/**
	 * Flips the tile on the board and game
	 */
	public void flipTile(int index) {
		box.flipTile(index);
		view.flipTile(index);
	}

	/**
	 * Resets to a new game.
	 */
	public void reset() {
		view.reset();
		box.reset();
	}

	/**
	 * Sets the die at the index to the value.
	 */
	public void setDie(int index, int val) {
		box.setDie(index, val);
		view.setDie(index, val);
	}

	
	/**
	 * Sets the message on the UI.
	 * @param m - The message to display
	 */
	public void setMessage(String m){
		view.setMessage(m);
	}

	/**
	 * Quits the client and shuts down the connection.
	 */
	public void quit() {
		view.quit();
	}
}

import java.net.InetAddress;
/**
 * @author Ernesto Soltero (exs6350@rit.edu)
 *
 */
public class Match{
	private int port;
	private InetAddress clientAddress;
	private String player;
	
	public Match(int port, InetAddress add, String player){
		this.port = port;
		this.clientAddress = add;
		this.player = player;
	}
	
	public int returnPort(){
		return this.port;
	}
	
	public InetAddress returnAddress(){
		return this.clientAddress;
	}
	
	public String returnName(){
		return this.player;
	}
}
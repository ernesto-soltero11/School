
/**
 * @author Ernesto Soltero (exs6350@rit.edu)
 *
 */
public class ServerModelClone implements ServerModel {

	private Match playerOne;
	private Match playerTwo;
	private ShutTheBox box;
	private int[] score = {0,0};
	
	/**
	 * Constructor for the servermodel.
	 */
	public ServerModelClone(Match playerone, Match playertwo, ShutTheBox game) {
		this.playerOne = playerone;
		this.playerTwo = playertwo;
		this.box = game;
	}

	/**
	 * Returns the game currently being used.
	 * @return box - The game holding the state of the tiles
	 */
	public ShutTheBox getGame(){
		return this.box;
	}
	
	/**
	 * 
	 * @param playerNum
	 * @return
	 */
	public int getScore(int playerNum){
		return score[playerNum];
	}
	
	/**
	 * @return - Returns the first player
	 */
	public Match returnOne() {
		return this.playerOne;
	}

	/**
	 * @return - Returns the second player
	 */
	public Match returnTwo() {
		return this.playerTwo;
	}

	/**
	 * Flips the tile
	 */
	public void flipTile(int index) {
		box.flipTile(index);
	}

	/**
	 * Resets the game
	 */
	public void reset() {
		box.reset();
	}

	/**
	 * Sets the dice to the specified value
	 * @param i - The index of the dice to set
	 * @param val - The value to change the dice to 
	 */
	public void setDie(int i, int val) {
		box.setDie(i, val);
	}

	/**
	 * Quits the session destroys this game when logging off the clients
	 */
	public void quit() {
		System.gc();
	}
}

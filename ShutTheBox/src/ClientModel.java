
/**
 * @author Ernesto Soltero (exs6350@rit.edu)
 *
 */
public interface ClientModel {	
	/**
	 * Sets the tile to the opposite of the current value.
	 */
	public void flipTile(int index);
	
	/**
	 * Resets the state of the game. Used when starting a new game.
	 */
	public void reset();
	
	/**
	 * Sets the dice to the new value.
	 * @param i - The index of the dice to set
	 * @param val - The new value of the dice to set to
	 */
	public void setDie(int i, int val);
	
	/**
	 * Sets the message of the current state of the game.
	 * @param mes - The message to display
	 */
	public void setMessage(String mes);
	
	/**
	 * Shuts the game down.
	 */
	public void quit();
	
}

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.Socket;
import java.util.ArrayDeque;
import java.util.HashMap;
import java.util.Map;
import java.util.Queue;
/**
 * @author Ernesto Soltero (exs6350@rit.edu)
 *
 */
public class ShutTheBoxServer{

	private DatagramSocket socket;
	private InetAddress serverAddress;
	private int port;
	private ServerProxy serverProxy;
	private Queue<Match> waitList = new ArrayDeque<Match>();
	private Map<Integer, ServerModelClone> sessions = new HashMap<Integer, ServerModelClone>(); 
	/**
	 * Sets up the server and listen to new clients connecting.
	 */
	public ShutTheBoxServer(String host, int port) {
		try {
			this.serverAddress = InetAddress.getByName(host);
			this.socket = new DatagramSocket(port, serverAddress);
			this.serverProxy = new ServerProxy(port, serverAddress, socket);
			this.port = port;
		} catch (IOException e) {
			System.out.println("Error binding the socket.");
		}
	}
	
	public void run(){
		while(true){
			byte[] buffer = new byte[32];
			DatagramPacket packet = new DatagramPacket(buffer, buffer.length);
			try {
				if(waitList.size() >= 2){
					Match playerOne = waitList.remove();
					Match playerTwo = waitList.remove();
					ServerModelClone clone = new ServerModelClone(playerOne, playerTwo, new ShutTheBox());
					String message = "joined " + clone.returnOne().returnName() + " " + clone.returnTwo().returnName();
					buffer = message.getBytes();
					DatagramPacket freshPacket = new DatagramPacket(buffer, buffer.length, clone.returnOne().returnAddress(), 
							clone.returnOne().returnPort());
					socket.send(freshPacket);
					freshPacket.setAddress(clone.returnTwo().returnAddress());
					freshPacket.setPort(clone.returnTwo().returnPort());
					socket.send(freshPacket);
					message = "turn 1";
					buffer = message.getBytes();
					freshPacket.setData(buffer);
					socket.send(freshPacket);
					freshPacket.setAddress(clone.returnOne().returnAddress());
					freshPacket.setPort(clone.returnOne().returnPort());
					socket.send(freshPacket);
					int key = (playerOne.returnPort() + playerOne.returnAddress().toString().hashCode())%65000;
					sessions.put(key, clone);
					int key2 = (playerTwo.returnPort() + playerTwo.returnAddress().toString().hashCode())%65000;
					sessions.put(key2, clone);
				}
				socket.receive(packet);
				String[] temp = new String(packet.getData(), 0, packet.getLength()).split("");
				if(temp[0] == "join"){
					System.out.println("Recieved join request from: " + packet.getSocketAddress());
					Match match = new Match(packet.getPort(), packet.getAddress(), temp[1]);
					waitList.add(match);
				}
				else{
					System.out.println("Packet data: " + packet.getData());
					int key = (packet.getPort() + packet.getAddress().toString().hashCode())%65000;
					ServerModelClone clone = sessions.get(key);
					serverProxy.process(packet, clone);
				}
			} catch (IOException e) {
				System.err.println("Error recieveing the packet.");
			}
		}
	}

	static void main(String args[]){
		if(args.length != 2){
			System.out.println("Usage: java ShutTheBoxServer <host> <port> ");
			System.exit(1);
		}
		ShutTheBoxServer server = new ShutTheBoxServer(args[0], Integer.valueOf(args[1]));
		server.run();
		}
}

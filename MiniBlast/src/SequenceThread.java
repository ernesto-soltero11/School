/**
 *
 * SequenceThread.java
 *
 * Sep 15, 2013
 *
 */

/**
 * The <b>SequenceThread</b> class is a thread that contains the 
 * sequence that it is searching through and the query and compliment
 * that it is trying to find. If the compliment or query is found it 
 * will highlight it and return the modified sequence otherwise the thread
 * just dies.
 * @author Ernesto Soltero (exs6350@rit.edu)
 */
public class SequenceThread extends Thread {

	private String name;
	private String sequence;
	private String find;
	private String compliment;
	private boolean PRINT = false;
	
	/**
	 * Constructor
	 * @param n - The name of this thread
	 * @param s - The sequence code of this thread
	 */
	public SequenceThread(String n, String s, String f) {
		this.name = n;
		this.sequence = s;
		this.find = f;
		this.compliment = findCompliment();
	}
	
	/**
	 * Runs the thread.
	 */
	public void run(){
		highlight();
	}
	
	/**
	 * Private function that finds the reverse compliment of the query 
	 * we are searching for.
	 * @return compliment - The reverse compliment of the sequence we are querying.
	 */
	private String findCompliment(){
		String backwards = new StringBuffer(find).reverse().toString();
		String compliment = "";
		for(int i = 0; i < find.length(); ++i){
			if(backwards.charAt(i) == 'a'){
				compliment = compliment.concat("t");
			}
			else if(backwards.charAt(i) == 't'){
				compliment = compliment.concat("a");
			}
			else if(backwards.charAt(i) == 'c'){
				compliment = compliment.concat("g");
			}
			else if(backwards.charAt(i) == 'g'){
				compliment = compliment.concat("c");
			}
		}
		return compliment;
	}
	
	/**
	 * Goes through the sequence searching for the query and the compliment 
	 * that we are looking for. If it finds nothing the sequence is left alone
	 * otherwise the query/compliment is capitalized and changed within the 
	 * sequence.
	 * @param findme - The query to find
	 * @return newSequence - The sequence containing the highlighted code
	 */
	public void highlight(){
		int start = 0;
		int lengthToFind = find.length();
		int temp = 0;
		String highlightedSequence = "";
		String buffer = "";
		while(start != sequence.length()){
			if((start + lengthToFind) <= sequence.length()){
				if(find.equals(sequence.substring(start, (start + lengthToFind)))){
					if(temp > start){
						highlightedSequence = highlightedSequence.concat((sequence.substring(temp, temp + 1)).toUpperCase());
						temp++;
						start++;
						continue;
					}
					else{
						buffer = find.toUpperCase();
						highlightedSequence = highlightedSequence.concat(buffer);
					}
					temp = start + lengthToFind;
					start++;
					PRINT = true;
					continue;
				}
				else if(compliment.equals(sequence.substring(start, (start + lengthToFind)))){
					if(temp > start){
						highlightedSequence = highlightedSequence.concat((sequence.substring(temp, temp + 1)).toUpperCase());
						temp++;
						start++;
						continue;
					}
					else{
						buffer = compliment.toUpperCase();
						highlightedSequence = highlightedSequence.concat(buffer);
					}
					temp = start + lengthToFind;
					start++;
					PRINT = true;
					continue;
				}
			}
			start++;
			if(temp < start){
				highlightedSequence = highlightedSequence.concat(sequence.substring(temp, temp +1));
				temp++;
			}
		}
		newSequence(highlightedSequence);
	}
	
	/**
	 * Changes the sequence to the new sequence with highlighted query.
	 * @param change - The new highlighted sequence to change to
	 */
	private void newSequence(String change){
		this.sequence = change;
	}
	
	/**
	 * Returns the result if the query/compliment was found within the sequence.
	 */
	public void getResult(){
		if(PRINT == true){
			System.out.println(name + " " + sequence);
		}
		//Thread dies after this
	}
	
}

/**
 *
 * MiniBlast.java
 *
 */
import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Scanner;


/**
 * The MiniBlast tool replicates the BLAST tool my searching sequences of 
 * data from a flat text file database. The MiniBlast program parses through 
 * the file and creates every sequence into a <code>thread</code> which is 
 * self searching and only returns if the query if found in the sequence. If 
 * the query is found it will be capitalized and the rest of the sequence will
 * be in lower case. The threads will print out in the order that they are found
 * in the database.
 * @author Ernesto Soltero (exs6350@rit.edu)
 *
 */
public class MiniBlast {
	
	private ArrayList<SequenceThread> Sequences;

	/**
	 * Constructor
	 */
	public MiniBlast() {
		this.Sequences = new ArrayList<SequenceThread>();
	}
	
	/**
	 * Opens up the file and parses it for the sequences. Creates 
	 * a thread for every sequence and inserts it into an ArrayList.
	 * The threads then start and are joined together printing out in
	 * the order that they are found in the database.
	 * @param file The file name of the sequences 
	 */
	public void fileRead(String file, String query) throws InterruptedException{
		try {
			BufferedReader input = new BufferedReader(new InputStreamReader(new FileInputStream(file)));
			String line;
			while((line = input.readLine()) != null){
				String[] Tokens = line.split(" ");
				
				//error checking
				if(Tokens.length != 2){
					System.err.println("Incorrect file format or their is a line which has no nucleotide " +
							"letters");
					System.exit(3);
				}
				
				if(!Tokens[1].matches("[ATCG]+")){
					System.err.println("Only A, C, T, or G are valid nuceotide letters.");
					System.exit(4);
					}
				//done error checking
				
				SequenceThread t = new SequenceThread(Tokens[0], (Tokens[1].toLowerCase()), query);
				t.start();
				Sequences.add(t);
			}

			Iterator<SequenceThread> iter = Sequences.iterator();
			while(iter.hasNext()){
				SequenceThread temp = iter.next();
				temp.join();
				temp.getResult();
			}
		} catch (IOException e) {
			System.err.println("Could not open the file.");
			e.printStackTrace();
			System.exit(2);
		}
	}

	/**
	 * The main entry point
	 * @param args arg0 should be the database file name arg1 should be
	 * the query we are searching for.
	 */
	public static void main(String[] args) {
		long start = System.nanoTime();
		MiniBlast blast = new MiniBlast();
		if(args.length != 2){
			System.err.println("usage: java MiniBlast <dbfile> <query>");
			System.exit(1);
		}
		try {
			blast.fileRead(((String) args[0]), ((String) args[1]).toLowerCase());
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		long end = System.nanoTime();
		long time = end - start;
		System.out.println(time);
	}
}

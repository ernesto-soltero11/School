#!/usr/local/bin/python3

"""
file: routing.py
language: python3
authors: Zack Butler, Sean Strout
description: Main program for Message Routing 2 Problem (242 Wk 2 Lecture 
    problem).
"""

import myGraph
import myStack
import myQueue

def searchDFS(source, destination):        
    """Performs a DFS search between source and destination nodes using a 
    stack.  It returns the path as a list of graph nodes (or an empty list
    if no path exists).
    """
    
    # 'prime' the dispenser with the source 
    dispenser = myStack.Stack()
    myStack.push(source, dispenser)

    # construct the parent data structure
    parents = {}
    parents[source] = None

    # loop until either the destination is found or the dispenser 
    # is empty (no path)
    while not myStack.emptyStack(dispenser):
        current = myStack.top(dispenser)
        myStack.pop(dispenser)
        if current == destination:
            break
        # loop over all neighbors of current
        for neighbor in current.neighbors:
            # process unvisited neighbors
            if neighbor not in parents:
                parents[neighbor] = current
                myStack.push(neighbor, dispenser)

    return constructPath(parents, source, destination)
       
def searchBFS(source, destination):        
    """Performs a BFS search between source and destination nodes using a 
    queue.  It returns the path as a list of graph nodes (or an empty list
    if no path exists).
    """

    # 'prime' the dispenser with the source 
    dispenser = myQueue.Queue()
    myQueue.enqueue(source, dispenser)

    # construct the parent data structure
    parents = {}
    parents[source] = None

    # loop until either the destination is found or the dispenser 
    # is empty (no path)
    while not myQueue.emptyQueue(dispenser):
        current = myQueue.front(dispenser)
        myQueue.dequeue(dispenser)
        if current == destination:
            break
        # loop over all neighbors of current
        for neighbor in current.neighbors:
            # process unvisited neighbors
            if neighbor not in parents:
                parents[neighbor] = current
                myQueue.enqueue(neighbor, dispenser)

    return constructPath(parents, source, destination)
        
def constructPath(parents, source, destination):
    """Returns the path as a list of nodes from source to destination"""
    
    # construct the path using a stack and traversing from the destination
    # node back to the source node using Node's previous
    stack = myStack.Stack()
    if destination in parents:
        tmp = destination
        while tmp != source:
            myStack.push(tmp, stack)
            tmp = parents[tmp]
        myStack.push(source, stack)
        
    path = []
    while not myStack.emptyStack(stack):
        path.append(myStack.top(stack)) 
        myStack.pop(stack)   
    return path

def main():
    # read in the data:
    graph = myGraph.loadGraph(input("Enter graph data filename: "))
    myGraph.printGraph(graph)

    # repeatedly perform either DFS (stack) or BFS (queue) on the graph
    while True:
        type = input("Enter 'D' for DFS, 'B' for BFS, or 'Q' to quit: ")
    
        if type == 'D':
            dfs = True
        elif type == 'B':
            dfs = False
        else:
            break

        # get source and dest from the user
        # get start and finish from the user
        startName = input('Enter starting node name: ')
        if startName not in graph:
            raise ValueError(startName + ' not in graph!')
        start = graph[startName]
        
        finishName =  input('Enter finish node name: ')
        if finishName not in graph:
            raise ValueError(finishName + ' not in graph!')
        finish = graph[finishName]

        # proceed only if start and finish are valid Node instances
        
        if isinstance(start, myGraph.Node) and isinstance(finish, myGraph.Node):
            if dfs:
                path = searchDFS(start, finish)
            else:
                path = searchBFS(start, finish)
        
            if len(path) == 0:
                print("No path between", start.name, "and", finish.name)
            elif len(path) == 1:
                print("You are already there!")
            else:
                for i in range(len(path)-1):
                    print(path[i].name, "to", path[i+1].name)

            print("Done.")

# # run the program
if __name__ == "__main__":
    main()

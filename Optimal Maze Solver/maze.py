"""
Author: Ernesto Soltero
File: maze.py
Created: December 2011
"""

from myGraph import *
from myNode import *
from myQueue import *
from myStack import *
from routing import searchBFS, constructPath  
      
def createMaze(file):
    """Builds the maze by adding nodes to the adjacency list when it encounters
    a '.'. It uses another counter r to account for the other rows that contain no
    nodes.
    adjacency -> dictionary"""
    
    adjacency = {}
    row = 0
    r = 0
    for line in open(file):
        col = 0
        newlist = list(line)
        newlist = [a for a in line if a != ' ']
        if newlist[1].isdigit() == True:
            maxcol = int(newlist[1])
            continue
        for cur in newlist: 
            if r % 2 == 0:
                if col == maxcol:
                    row += 1
                elif cur == '.':
                    adjacency[row,col] = [Node((row,col +1))]
                    adjacency[row,(col + 1)] = [Node((row,col))]
                    col += 1
                elif cur == '|':
                    adjacency[row,col+1] = [Node((row,col+1))]
                    adjacency[row,col] = [Node((row,col))]
                    col += 1
                
            elif r % 2 == 1:  
                if cur == '.':  
                    adjacency[(row-1),col] = [Node((row + 1,col))]
                    adjacency[(row+1),col] = [Node((row - 1,col))]
                elif cur == '-':
                    adjacency[row+1,col] = [Node((row+1,col))]
                    adjacency[row-1,col] = [Node((row-1,col))]
                    col += 1   
        r += 1
    return adjacency
            
                                       
def main():
    f = input('Enter the name of the file: ')
    datastruct = createMaze(f)
    print('The data structure is an adjacency list: ')
    for i in datastruct:
        print(i)
    startx = input('Enter the starting x coordinate: ')
    starty = input('Enter the starting y coordinate: ')
    endx = input('Enter the ending x coordinate: ')
    endy = input('Enter the ending y coordinate: ')
    start = datastruct[startx,starty]
    end = datastruct[endx,endy]
    print(searchBFS(start, end))
    again = input("Run again? y/n: ")
    if again == 'y':
        startx = input('Enter the starting x coordinate: ')
        starty = input('Enter the starting y coordinate: ')
        endx = input('Enter the ending x coordinate: ')
        endy = input('Enter the ending y coordinate: ')
        start = datastruct[startx,starty]
        end = datastruct[endx,endy]
        print(searchBFS(start, end))
    else:
        input("Hit enter to end")
    
if __name__ == "__main__":
    main()




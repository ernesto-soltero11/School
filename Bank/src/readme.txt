243 OOP: Project 02 README
===============================
(please use the RETURN key to make multiple lines; don't assume autowrap.)

0. Ernesto Soltero
---------------------

CS Username: 	exs6350		Name:  	Ernesto Soltero

1. Problem Analysis
---------

Summarize the analysis work you did. 
What new information did it reveal?  How did this help you?
How much time did you spend on problem analysis?

I spent about two hours on analyzing the problem. I was confused on how the bank
and the atm models would work but then I came up with a general solution. The bank model 
is the class that is doing most of the heavy lifting as the atm model and the other
classes rely on the bank model to organize all the accounts and keep track of the 
withdrawing and the depositing.
	

2. Design
---------

Explain the design you developed to use and why. What are the major 
components? What connects to what? How much time did you spend on design?
Make sure to clearly show how your design uses the MVC model by
separating the UI "view" code from the back-end game "model".

I used an interface to implement each class. The bank model connects to the bank gui and the 
atm model in order to update. I spent a lot of the time designing and changing the code in order
for everything to connect and work. I seperated the back end of the bank gui from the bank model
and the atm model from the atm gui. The atm model talks to the bank model in orderto keep track 
of what is happening in the atm.

3. Implementation and Testing
-------------------

Describe your implementation efforts here; this is the coding and testing.

What did you put into code first? I first started by making all the easy classes. That was the
interface and all the account classes. Then I started doing the bank model which was the most 
important so I spent some time on that.
How did you test it? I didn't test any of my code for a while until I finished the bank class
which contained the code to run the batch mode and the gui mode. After that I tested the batch mode
which workes fine and then I had a giu open while writing the code in order to see what I need to
do next for the gui.
How well does the solution work? The solution works ok but I didn't get to finish the atm gui.
The atm gui is a lot of work mostly visual components and refreshing the display but the rest of
the classes work good and they get the job done.
Does it completely solve the problem? Only the batch mode completely solves the problem. The atm gui
isn't finished so I can't test to see if it works visually but the batch mode does solve the 
problem.
Is anything missing? I didn't finish the atm gui. I first finished all the back end stuff and made 
sure that it was all working before I started on the guis. The gui's took a lot of wotk and I 
wasn't able to finish the atm gui.
How could you do it differently? I think that I could finish it if I had more time. I did start 
early but I didn't spend to much time working on the project everyday. If I had spent more time'
per day working on the project I'm sure that I could of finished it.
How could you improve it? I could probably improve the gui's. I was just making a really simple 
ugly gui that isn't to user friendly so I could of made it better.

How much total time would you estimate you worked on the project? I spent about 30 hours working 
on the project over a span of about a week.
If you had to do this again, what would you do differently? Start a lot earlier and have a partner. 
Having a partner decreases the workload and if I had a partner I'm pretty sure that I could of 
finished this.


4. Development Process
-----------------------------

Describe your development process here; this is the overall process.

How much problem analysis did you do before your initial coding effort?I spent about 2 hours
trying to figure out how I am suppose to do the project and how everything connects. Then I 
started thinking the classes that I need to make and how they would connect to each other. Overall
I actually liked this project but it was too much work for just one person. 
How much design work and thought did you do before your initial coding effort? I put in a bit of 
though and had to reread the document that explains how the project should be done a lot of times.
I also shared some ideas from other people that where working on this project alone and that was 
able to get my mind going and help me solve this problem.
How many times did you have to go back to assess the problem? I had to go back multiple times
and figure out how what I was writing right now would connect to everything else. I first wrote 
the accounts as a abstract class but then decided that a interface was better.

What did you learn about software development? A team is really useful for large projcts like this.
You get more work done and more ideas are thrown around so its easier to understand. I think that 
they should make these large projects as mandatory group projects.

	
/**
 * AtmGUI.java
 * 
 * Version:
 * $Id: AtmGUI.java,v 1.6 2012-05-18 00:22:33 exs6350 Exp $
 * 
 * Revisions:
 * $Log: AtmGUI.java,v $
 * Revision 1.6  2012-05-18 00:22:33  exs6350
 * Added more atm gui code still not working
 *
 * Revision 1.5  2012-05-17 17:50:09  exs6350
 * Added comments
 *
 * Revision 1.4  2012-05-17 02:56:57  exs6350
 * Added more atm functionality.
 *
 * Revision 1.3  2012-05-16 23:48:57  exs6350
 * Almost done with bank gui
 *
 * Revision 1.2  2012-05-14 02:44:13  exs6350
 * added some gui code and made a batch file function
 *
 * Revision 1.1  2012-05-13 01:55:28  exs6350
 * initial
 *
 * 
 */
import javax.swing.*;
import javax.swing.GroupLayout.Alignment;

import java.util.Random;
import java.awt.*;
import java.awt.event.*;
import javax.swing.event.*;
/**
 * @author Ernesto Soltero(exs6350@rit.edu)
 *
 */
public class AtmGUI{
	private JButton Withdraw = new JButton("Withdraw");
	private JButton Deposit = new JButton("Deposit");
	private JButton Balance = new JButton("Balance");
	private JButton logOff = new JButton("Log Off");
	private JButton Ok = new JButton("Ok");
	private JButton Close = new JButton("Close");
	private JButton Clear = new JButton("Clear");
	private JButton Cancel = new JButton("Cancel");
	private NButton zero = new NButton("0",0);
	private JTextField numDisplay = new JTextField();
	private JTextArea display;
	private JLabel topLabel = new JLabel();
	private JPanel centerPanel = new JPanel(new BorderLayout());
	private JPanel topPanel;
	private Account account;
	JFrame frame = new JFrame("Ernesto Soltero " + Math.abs(new Random().nextInt()));
	private AtmModel model;
	
	/**
	 * A number button that returns the number
	 * it contains.
	 *
	 */
	private class NButton extends JButton {
        private int number;
        public NButton(String name, int number) {
            super(name);
            this.number = number;
        }
        
        public int getNumber() { return number; }
	}
	
	/**
	 * Displays the welcome message that prompts you to log in.
	 */
	public void welcomeMsg(){
		display.setText("");
		display.setText("Welcome to ACME bank.\n" + "Please enter your account number.");
	}
	
	/**
	 * Displays the main menu of the atm.
	 */
	public void mainMenu(){
		JPanel southcenter = new JPanel(new GridLayout(4,0));
		southcenter.add(Withdraw);
		southcenter.add(Deposit);
		southcenter.add(Balance);
		southcenter.add(logOff);
		centerPanel.add(southcenter,BorderLayout.SOUTH);
		centerPanel.validate();
		southcenter.validate();
	}
	
	/**
	 * Prompts for you to enter your pin.
	 */
	public void pinMsg(){
		display.setText("");
		display.setText("Please enter your pin number.");
	}
	
	/**
	 * Displays the withdraw message.
	 */
	public void withdrawMsg(){
		display.setText("");
		display.setText("Enter the amount that you would like to withdraw.");
	}
	
	/**
	 * Displays the deposit message.
	 */
	public void depositMsg(){
		display.setText("");
		display.setText("Enter the amount you would like to deposit.");
	}
	
	/**
	 * Displays the balance of your account.
	 */
	public void checkBalance(){
		display.setText("");
		display.setText("Your balance is: \n" + model.checkBalance(account.getAccountName()));
	}
	
	/**
	 * The button listener class that works for all the buttons pressed.
	 *
	 */
	class ButtonListener implements ActionListener{
		public void actionPerformed(ActionEvent e){
			Object button = e.getSource();
			if(e.getSource().equals(Clear)){
				numDisplay.setText("");
			}
			else if(e.getSource().equals(Ok)){
				if(display.getText().equals("Welcome to ACME bank.\n" + "Please enter your account number.")){
					if(model.validateAccount(Integer.valueOf(numDisplay.getText()))==true){
						account = model.findAccount(Integer.valueOf(numDisplay.getText()));
						pinMsg();
						centerPanel.validate();
					}	
					else{
					topLabel.setText("Invalid account number");
					topPanel.validate();
					}
				}
				else if(display.getText().equals("Please enter your pin number.")){
					topLabel.setText("");
					if(Integer.valueOf(numDisplay.getText()).equals(account.getAccountName())){
						mainMenu();
						centerPanel.validate();
					}
					else{
						topLabel.setText("Wrong pin number");
						topPanel.validate();
					}
				}
				else if(display.getText().equals("Enter the amount that you would like to withdraw.")){
					model.Withdraw(account.getAccountName(), Integer.valueOf(numDisplay.getText()));
					topLabel.setText("You withdrew " + numDisplay.getText());
					topPanel.validate();
					
				}
				else if(display.getText().equals("Enter the amount you would like to deposit.")){
					model.Deposit(account.getAccountName(), Integer.valueOf(numDisplay.getText()));
					topLabel.setText("You deposited " + numDisplay.getText());
					topPanel.validate();
				}
			}
			else if(e.getSource().equals(Cancel)){
				mainMenu();
			}
			else if(e.getSource().equals(Close)){
				frame.dispose();
			}
			else if(e.getSource().equals(Withdraw)){
				withdrawMsg();
			}
			else if(e.getSource().equals(Deposit)){
				depositMsg();
			}
			else if(e.getSource().equals(Balance)){
				checkBalance();
			}
			else if(e.getSource().equals(logOff)){
				welcomeMsg();
			}
			else if(button instanceof JButton){
				int num = ((NButton)button).getNumber();
				numDisplay.setText(numDisplay.getText() + num);
			}
		}
	}
	
	
	/**
	 * Sets up the basics of the gui. 
	 * @param model The atm model where all the functions are. 
	 */
	public AtmGUI(AtmModel model){
		
	numDisplay.setSize(20, 50);
	numDisplay.setEditable(false);
	this.model = model;
	ButtonListener buttonlisten = new ButtonListener();
	JPanel framepanel = new JPanel(new BorderLayout());
	frame.add(framepanel);
	
	//top panel
	topPanel = new JPanel(new BorderLayout());
	topPanel.add(Close,BorderLayout.EAST);
	topPanel.add(Clear,BorderLayout.WEST);
	topPanel.add(topLabel,BorderLayout.NORTH);
	frame.add(topPanel,BorderLayout.NORTH);
	Clear.addActionListener(buttonlisten);
	Close.addActionListener(buttonlisten);
	
	//Center panel
	display = new JTextArea();
	display.setPreferredSize(new Dimension(50,300));
	display.setEnabled(true);
	display.setFont(new Font("Dialog",Font.BOLD,36));
	welcomeMsg();
	centerPanel.add(numDisplay,BorderLayout.CENTER);
	centerPanel.add(display,BorderLayout.NORTH);
	frame.add(centerPanel,BorderLayout.CENTER);
	
	//Buttons on the bottom panel
	JPanel bottompanel = new JPanel(new GridLayout(4,3));
	
	for(int i = 1; i<10;i++){
		NButton button = new NButton("" + i,i);
		button.addActionListener(buttonlisten);
		bottompanel.add(button);
	}
	bottompanel.add(Ok);
	zero.addActionListener(buttonlisten);
	bottompanel.add(zero);
	bottompanel.add(Cancel);
	frame.add(bottompanel,BorderLayout.SOUTH);
	
	frame.setLocation(500, 0);
	frame.setSize(650,500);
	frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
	frame.setVisible(true);
}
}

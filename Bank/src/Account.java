/**
 * Account.java
 * 
 * Version:
 * $Id: Account.java,v 1.4 2012-05-17 17:50:09 exs6350 Exp $
 * 
 * Revisions:
 * $Log: Account.java,v $
 * Revision 1.4  2012-05-17 17:50:09  exs6350
 * Added comments
 *
 * Revision 1.3  2012-05-17 02:56:58  exs6350
 * Added more atm functionality.
 *
 * Revision 1.2  2012-05-13 18:28:58  exs6350
 * changed account type to an interface.
 * Finished all the account classes.
 *
 * Revision 1.1  2012-05-13 01:55:29  exs6350
 * initial
 *
 * 
 */

/**
 * @author Ernesto Soltero(exs6350@rit.edu)
 *
 */
public interface Account {
	
	/**
	 * Sets the default interest rate to 0;
	 */
	double inter = 0;
	
	/**
	 * Returns the account number.
	 * @return Account number.
	 */
	public int getAccountName();
	
	/**
	 * Returns the balance.
	 * @return Returns the balance of the account.
	 */
	public double getBalance();
	
	/**
	 * Returns the pin number.
	 * @return Retunrs the pin number of the account.
	 */
	public int getPin();
	
	/**
	 * Withdraws money from the account.
	 * @param amount The amount to withdraw
	 */
	public void Withdraw(double amount);
	
	/**
	 * Deposits money into the account.
	 * @param amount The amount to withdraw
	 */
	public void Deposit(double amount);

	/**
	 * Applies interest to the account. Either applies the penalty 
	 * or the interest rate.
	 */
	public void applyInterest();
}

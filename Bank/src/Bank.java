/**
 * Bank.java
 * 
 * Version:
 * $Id: Bank.java,v 1.8 2012-05-18 00:22:34 exs6350 Exp $
 * 
 * Revisions:
 * $Log: Bank.java,v $
 * Revision 1.8  2012-05-18 00:22:34  exs6350
 * Added more atm gui code still not working
 *
 * Revision 1.7  2012-05-17 17:50:09  exs6350
 * Added comments
 *
 * Revision 1.6  2012-05-17 02:56:57  exs6350
 * Added more atm functionality.
 *
 * Revision 1.5  2012-05-16 23:48:57  exs6350
 * Almost done with bank gui
 *
 * Revision 1.4  2012-05-14 02:44:14  exs6350
 * added some gui code and made a batch file function
 *
 * Revision 1.3  2012-05-13 18:28:58  exs6350
 * changed account type to an interface.
 * Finished all the account classes.
 *
 * Revision 1.2  2012-05-13 03:26:31  exs6350
 * Added all files for my design.
 *
 * Revision 1.1  2012-05-13 01:55:28  exs6350
 * initial
 *
 * 
 */
import java.io.*;
import java.util.Scanner;

/**
 * @author Ernesto Soltero(exs6350@rit.edu)
 *
 */
public class Bank {
	private static BankModel model = new BankModel();
	private static Scanner in;
	private static File file;
	
	/**
	 * Calls the bank model to perform the action requested.
	 * @param line A line of text from the bank file or batch file.
	 */
	public static void bankFile(String[] line){
		if(line[0].equals("a")){
			System.out.println("============ Interest Report ================");
			System.out.println("Account   " + "Adjustment    " + "New Balance");
			System.out.println("-------   " + "--------------" + "-----------");
			for(Account i:model.returnAccounts()){
				i.applyInterest();
				System.out.println(i.getAccountName() + "    $    " + i.inter + "   $   "
						+ i.getBalance());
			}
			System.out.println("=============================================" + "\n");
		}
		else if(line[0].equals("c")){
			Account a = model.findAccount(Integer.valueOf(line[1]));
			model.closeAccount(Integer.parseInt(line[1]));
			if(a == null){
				System.out.println(line[1]+"    "+line[0]+"    "+"    "+"Close: " + "Failed");
				}
			else if(model.result.equals("Success")){
			System.out.println(line[1]+"    "+line[0]+"    "+"    "+"Close: " + model.result+
					"  $    "+a.getBalance());		}
			
			}
		else if(line[0].equals("d")){
			model.Deposit(Integer.parseInt(line[1]), Double.parseDouble(line[2]));
			Account a = model.findAccount(Integer.valueOf(line[1]));
			if(a != null){
			System.out.println(line[1]+"    "+line[0]+"    $    "+"    "+ line[2] + "   $    "+
					+a.getBalance());		}
			else{
				System.out.println(line[1]+"    "+line[0]+"    "+ "   $    " + "  Failed.");
				}
		}
		else if(line[0].equals("o")){
			model.openAccount(line[1],Integer.parseInt(line[2]),Integer.parseInt(line[3]),Double.parseDouble(line[4]));
			Account a = model.findAccount(Integer.valueOf(line[2]));
			if(model.result.equals("Success")){
			System.out.println(line[2]+"    "+line[0]+"    "+line[1]+"    "+"Open: " + model.result+
			"  $    " + a.getBalance());	
			}
		else{
			System.out.println(line[2]+"    "+line[0]+"    "+line[1]+"    "+"Open: " + model.result);
			}
		}
		else if(line[0].equals("w")){
			model.Withdraw(Integer.parseInt(line[1]),Double.parseDouble(line[2]));
			Account a = model.findAccount(Integer.valueOf(line[1]));
			if(a != null){
				System.out.println(line[1]+"    "+line[0]+"    $    "+"    "+ line[2] + "   $    "+
						+a.getBalance());		
			}
			else{
				System.out.println(line[1]+"    "+line[0]+"    $    "+"   Failed.");		
			}
		}
	}
	
	
	/**
	 * Runs the main program.
	 * @param args Not used
	 */
	public static void main(String[] args) throws FileNotFoundException{
		try{
			if(args.length > 2){
				System.err.println("Only two arguments are allowed.");
				System.exit(0);
			}
			//Reads the bank file and opens all the accounts requested.
			File file = new File(args[0]);
			in = new Scanner(file);
			while(in.hasNext()){
				String[] line = in.nextLine().split(" ");
				bankFile(line);
				}
			in.close();
			//Prints out the initial bank data and opens up the bank gui.
			if(args.length == 1){
				BankGUI bankgui = new BankGUI(model);
				System.out.println("========== Initial Bank data. ===============" + "\n");
				System.out.println("Account Type     " + "Account " + "Balance");
				System.out.println("------------     ------- -------");
				if(model.returnAccounts().size() != 0){
				for(Account acc:model.returnAccounts()){
					System.out.println(acc.getClass().getName() + "       " + acc.getAccountName() + "   $   " +
							acc.getBalance());
				}
				}
				System.out.println("\n");
				System.out.println("=============================================" + "\n");
			}
			if(args.length == 2){
				//Runs batch mode and prints out all the data.
				File file2 = new File(args[1]);
				Scanner second = new Scanner(file2);
				while(second.hasNext()){
					String[] line2 = second.nextLine().split(" ");
					bankFile(line2);
				}
				System.out.println("========== Final Bank data ===================" + "\n");
				System.out.println("Account Type    " + "Account  " + "Balance");
				System.out.println("----------------" + "---------" + "-------");
				if(model.returnAccounts().size() != 0){
					for(Account acc:model.returnAccounts()){
						System.out.println(acc.getClass().getName() + "       " + acc.getAccountName() + "   $   " +
								acc.getBalance());
					}
				}
				else if(model.returnClosedAccounts().size() != 0){
					for(Account acc:model.returnClosedAccounts()){
						if(acc.getAccountName() == 0){
							continue;
						}
						else{
						System.out.println(acc.getClass().getName() + "       " + acc.getAccountName() + "   $   " +
								acc.getBalance());
						}
					}
				}
			}
		}
		//Opens the bank gui if the batch file could not be found.
		catch(FileNotFoundException e){
			BankGUI gui = new BankGUI(model);
		}
		catch(IOException e){
			e.printStackTrace();
		}
	}
}

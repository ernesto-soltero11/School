/**
 * AtmModel.java
 * 
 * Version:
 * $Id: AtmModel.java,v 1.4 2012-05-18 00:22:33 exs6350 Exp $
 * 
 * Revisions:
 * $Log: AtmModel.java,v $
 * Revision 1.4  2012-05-18 00:22:33  exs6350
 * Added more atm gui code still not working
 *
 * Revision 1.3  2012-05-17 17:50:09  exs6350
 * Added comments
 *
 * Revision 1.2  2012-05-17 02:56:57  exs6350
 * Added more atm functionality.
 *
 * Revision 1.1  2012-05-16 23:48:57  exs6350
 * Almost done with bank gui
 *
 */

/**
 * @author Ernesto
 *
 */
public class AtmModel {
	private BankModel model;
	
	/**
	 * The constructor for the atm model.
	 * @param model The bank model being used.
	 */
	public AtmModel(BankModel model){
		this.model = model;
	}
	
	/**
	 * Process the deposits in the atm.
	 * @param num The account number
	 * @param amount The amount to deposit.
	 */
	public void Deposit(int num, double amount){
		model.Deposit(num, amount);
		Account a = model.findAccount(num);
		if(a != null){
		System.out.println(a.getAccountName()+"    "+"d"+"    $    "+"    "+ amount + "   $    "+
				+a.getBalance());		}
		else{
			System.out.println(a.getAccountName()+"    "+"d"+"    "+ "   $    " + "  Failed.");
			}
	}
	
	/**
	 * Process the withdraws in the atm.
	 * @param num The account number.
	 * @param amount The amount to withdraw.
	 */
	public void Withdraw(int num, double amount){
		model.Withdraw(num, amount);
		Account a = model.findAccount(num);
		if(a != null){
			System.out.println(a.getAccountName()+"    "+"w"+"    $    "+"    "+ amount + "   $    "+
					+a.getBalance());		
		}
		else{
			System.out.println(a.getAccountName()+"    "+"w"+"    $    "+"   Failed.");		
		}
	}
	
	/**
	 * Checks the balance of the account
	 * @param accountNumber The account number.
	 */
	public double checkBalance(int accountNumber){
		return model.checkBalance(accountNumber);
	}
	
	/**
	 * Validates the accounts throught the atm.
	 * @param num The account number.
	 * @return True if the account is valid false otherwise.
	 */
	public boolean validateAccount(int num){
		return model.validateAccount(num);
	}
	
	/**
	 * Finds the account in the bank if valid.
	 * @param num The account number;
	 * @return The account.
	 */
	public Account findAccount(int num){
		return model.findAccount(num);
	}

}

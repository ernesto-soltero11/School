/**
 * SavingAccount.java
 * 
 * Version:
 * $Id: SavingAccount.java,v 1.5 2012-05-17 17:50:09 exs6350 Exp $
 * 
 * Revisions:
 * $Log: SavingAccount.java,v $
 * Revision 1.5  2012-05-17 17:50:09  exs6350
 * Added comments
 *
 * Revision 1.4  2012-05-17 02:56:57  exs6350
 * Added more atm functionality.
 *
 * Revision 1.3  2012-05-14 02:44:14  exs6350
 * added some gui code and made a batch file function
 *
 * Revision 1.2  2012-05-13 18:28:58  exs6350
 * changed account type to an interface.
 * Finished all the account classes.
 *
 * Revision 1.1  2012-05-13 01:55:28  exs6350
 * initial
 *
 * 
 */

/**
 * Represents a savings account.
 * @author Ernesto Soltero(exs6350@rit.edu)
 *
 */
public class SavingAccount implements Account{
	public final double MONTHLY_INTEREST = .005/12;
	private int accountNumber;
	private final int pinNumber;
	private double accountBalance;
	double inter;
	
	/**
	 * Constructos for the saving account
	 * @param name The account number.
	 * @param pin The pin number of the account.
	 * @param balance The opening balance.
	 */
	public SavingAccount(int name, int pin, double balance){
		this.accountNumber = name;
		this.pinNumber = pin;
		this.accountBalance = balance;
	}
	
	/**
	 * Withdraws money from this account.
	 * @param amount The amount to withdraw from this account.
	 */
	public void Withdraw(double amount){
		if(accountBalance<amount){
			System.err.println("Insufficient funds.");
		}
		else{
			accountBalance -= amount;
		}
	}
	
	/**
	 * Deposits money into the account.
	 * @param amount The amount to deposit into the account.
	 */
	public void Deposit(double amount){
		accountBalance += amount;
	}
	
	/**
	 * Gets the balance of the account.
	 */
	public double getBalance(){
		return accountBalance;
		}
	
	/**
	 * Applies the interest or penalty to the account.
	 */
	public void applyInterest(){
		if(getBalance()<200){
			if(getBalance() >10){
				accountBalance -=10;
				inter = -10;
			}
			else{
				double inter = .1 * getBalance();
				Withdraw(inter);
			}
		}
		else{
		double amount = getBalance() * MONTHLY_INTEREST;
		Deposit(amount);
		}
	}
	
	/**
	 * Gets the pin number.
	 */
	public int getPin(){
		return pinNumber;
	}
	
	/**
	 * Gets the account name for this account.
	 */
	public int getAccountName(){
		return accountNumber;
	}
}

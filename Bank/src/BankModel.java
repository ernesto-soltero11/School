/**
 * BankModel.java
 * 
 * Version:
 * $Id: BankModel.java,v 1.7 2012-05-18 00:22:34 exs6350 Exp $
 * 
 * Revisions:
 * $Log: BankModel.java,v $
 * Revision 1.7  2012-05-18 00:22:34  exs6350
 * Added more atm gui code still not working
 *
 * Revision 1.6  2012-05-17 17:50:09  exs6350
 * Added comments
 *
 * Revision 1.5  2012-05-17 02:56:57  exs6350
 * Added more atm functionality.
 *
 * Revision 1.4  2012-05-16 23:48:57  exs6350
 * Almost done with bank gui
 *
 * Revision 1.3  2012-05-14 02:44:14  exs6350
 * added some gui code and made a batch file function
 *
 * Revision 1.2  2012-05-13 18:28:57  exs6350
 * changed account type to an interface.
 * Finished all the account classes.
 *
 * Revision 1.1  2012-05-13 03:26:31  exs6350
 * Added all files for my design.
 *
 */
import java.util.ArrayList;
/**
 * Represents the bank model that handles all of the open
 * and closing accounts and validating accounts.
 * @author Ernesto Soltero(exs650@rit.edu)
 *
 */
public class BankModel {
	
	private ArrayList<Account> accounts = new ArrayList<Account>();
	private ArrayList<Account> closedAccounts = new ArrayList<Account>();
	String result = "";

	/**
	 * Opens a account. Returns false if the account failed to open.
	 * @param type The account type
	 * @param num The account number
	 * @param pin The pin of the account
	 * @param balance The opening balance of the account
	 * @return True if the account opened false if it didnt.
	 */
	public boolean openAccount(String type, int num, int pin, double balance){
		if(type.equals("x")){
			accounts.add(new CheckingAccount(num,pin,balance));
			result = "Success";
			return true;
		}
		else if(type.equals("s")){
			accounts.add(new SavingAccount(num,pin,balance));
			result = "Success";
			return true;
		}
		else if(type.equals("c")){
			CDAccount acc = new CDAccount(num,pin,balance);
			accounts.add(acc);
			checkCD(acc);
			return true;
		}
		return false;
	}
	
	/**
	 * Method that specifically checks if a cd failed to open
	 * due to a minimum balance error. Closes the account if it is
	 * invalid.
	 * @param acc The cd account
	 */
	private void checkCD(CDAccount acc){
		if(acc.getAccountName() == 0){
			for(int i = 0;i<accounts.size();i++){
				if(accounts.get(i).getAccountName() == acc.getAccountName()){
					closedAccounts.add(accounts.get(i));
					accounts.remove(i);
					result = "Failed";
				}
			}
		}
	}
	
	/**
	 * Closes accounts.
	 * @param accountNumber The account number
	 * @return True if the account was closed false if it failed.
	 */
	public boolean closeAccount(int accountNumber){
		for(int i = 0;i<accounts.size();i++){
			if(accounts.get(i).getAccountName() == accountNumber){
				closedAccounts.add(accounts.get(i));
				accounts.remove(i);
				result = "Success";
				return true;
			}
		}
		return false;
	}
	
	/**
	 * Withdraws money from the account.
	 * @param num The account number.
	 * @param amount The amount to withdraw.
	 */
	public void Withdraw(int num,double amount){
		for(int i = 0;i<accounts.size();i++){
			if(accounts.get(i).getAccountName() == num){
				accounts.get(i).Withdraw(amount);
				return;
			}
		}
	}
	
	/**
	 * Finds a account.
	 * @param num The account number
	 * @return The account if found or else null.
	 */
	public Account findAccount(int num){
		for(Account i:accounts){
			if(i.getAccountName() == num){
				return i;
			}
		}
		return null;
	}
	
	/**
	 * Deposits money into the account.
	 * @param num The account number
	 * @param amount The amount to deposit.
	 */
	public void Deposit(int num, double amount){
		for(int i = 0;i<accounts.size();i++){
			if(accounts.get(i).getAccountName() == num){
				accounts.get(i).Deposit(amount);
				return;
			}
		}
	}
	
	/**
	 * Checks the balance of the account.
	 * @param accountNumber The account number.
	 * @return The balance of the account.
	 */
	public double checkBalance(int accountNumber){
		Account account = null;
		for(Account i:accounts){
			if(i.getAccountName() == accountNumber){
				account = i;
				break;
			}
		}
		return account.getBalance();
	}
	
	/**
	 * Returns all open accounts.
	 * @return ArrayList of open accounts
	 */
	public ArrayList<Account> returnAccounts(){
		return accounts;
	}
	
	/**
	 * Returns all closed accounts.
	 * @return ArrayList of closed accounts
	 */
	public ArrayList<Account> returnClosedAccounts(){
		return closedAccounts;
	}
	
	/**
	 * Checks to see if the account number entered is valid.
	 * @param accountNumber The account number
	 * @return True if a valid account false otherwise.
	 */
	public boolean validateAccount(int accountNumber){
		for(Account i:accounts){
			if(i.getAccountName() == accountNumber){
				return true;
			}
		}
		return false;
	}

}

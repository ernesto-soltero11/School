/**
 * CheckingAccount.java
 * 
 * Version:
 * $Id: CheckingAccount.java,v 1.5 2012-05-17 17:50:09 exs6350 Exp $
 * 
 * Revisions:
 * $Log: CheckingAccount.java,v $
 * Revision 1.5  2012-05-17 17:50:09  exs6350
 * Added comments
 *
 * Revision 1.4  2012-05-17 02:56:58  exs6350
 * Added more atm functionality.
 *
 * Revision 1.3  2012-05-14 02:44:15  exs6350
 * added some gui code and made a batch file function
 *
 * Revision 1.2  2012-05-13 18:28:58  exs6350
 * changed account type to an interface.
 * Finished all the account classes.
 *
 * Revision 1.1  2012-05-13 01:55:29  exs6350
 * initial
 *
 */

/**
 * Represents a checking account.
 * @author Ernesto Soltero(exs6350@rit.edu)
 *
 */
public class CheckingAccount implements Account{
	private int accountNumber;
	private final int pinNumber;
	private double accountBalance;
	double inter;
	
	/**
	 * Constructor for the checking account.
	 * @param name The account number
	 * @param pin The pin of the account
	 * @param balance The opening balance of the account.
	 */
	public CheckingAccount(int name,int pin,double balance){
		this.accountNumber = name;
		this.pinNumber = pin;
		this.accountBalance = balance;
	}
	
	/**
	 * Withdraws money from this account.
	 * @param amount The amount to withdraw.
	 */
	public void Withdraw(double amount){
		if(accountBalance<amount){
			System.err.println("Insufficient funds.");
		}
		else{
			accountBalance -= amount;
		}
	}

	/**
	 * Deposits money into this account.
	 * @param amount The amount of withdraw from the account.
	 */
	public void Deposit(double amount){
		accountBalance += amount;
	}
	
	/**
	 * The balance of the account.
	 */
	public double getBalance(){
		return accountBalance;
	}
	
	/**
	 * The account number of this account.
	 */
	public int getAccountName(){
		return accountNumber;
	}
	
	/**
	 * The pin number of the account.
	 */
	public int getPin(){
		return pinNumber;
	}

	/**
	 * Applies the interest or penalties for this class.
	 */
	public void applyInterest(){
		if(getBalance()<50){
			if(getBalance()>5){
				accountBalance -= 5;
				inter = -5;
			}
			else{
				double inter = .1 * getBalance();
				Withdraw(inter);
			}
		}
		return;
	}
	
}

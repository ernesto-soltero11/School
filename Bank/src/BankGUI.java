/**
 * BankGUI.java
 * 
 * Version:
 * $Id: BankGUI.java,v 1.6 2012-05-18 00:22:34 exs6350 Exp $
 * 
 * Revisions:
 * $Log: BankGUI.java,v $
 * Revision 1.6  2012-05-18 00:22:34  exs6350
 * Added more atm gui code still not working
 *
 * Revision 1.5  2012-05-17 17:50:09  exs6350
 * Added comments
 *
 * Revision 1.4  2012-05-17 02:56:58  exs6350
 * Added more atm functionality.
 *
 * Revision 1.3  2012-05-16 23:48:57  exs6350
 * Almost done with bank gui
 *
 * Revision 1.2  2012-05-14 02:44:15  exs6350
 * added some gui code and made a batch file function
 *
 * Revision 1.1  2012-05-13 01:55:29  exs6350
 * initial
 *
 */
import javax.swing.*;
import java.awt.*;
import javax.swing.event.*;
import java.awt.event.*;
import java.util.*;
/**
 * @author Ernesto Soltero(exs6350@rit.edu)
 *
 */
public class BankGUI {
	private JTextField accountNumber = new JTextField();
	private JTextField accountType = new JTextField();
	private JTextField balance = new JTextField();
	JList alist = null;
	//private JScrollPane scrollPane = new JScrollPane(alist);
	private static BankModel model;
	private JButton Atm = new JButton("ATM");
	private ArrayList<Account> acc;
	
	/**
	 * Contructs the main view of the bank gui.
	 * @param model The bank model being used.
	 */
	public BankGUI(BankModel model){
		this.model = model;
		this.acc = model.returnAccounts();
		
		accountNumber.setEditable(false);
		accountType.setEditable(false);
		balance.setEditable(false);
		JFrame frame = new JFrame("Ernesto");
		JPanel panel = new JPanel(new BorderLayout());
		
		JPanel centerpanel = new JPanel(new BorderLayout());
		ArrayList<Integer> temp = new ArrayList<Integer>();
		for(Account i: acc){
			temp.add(i.getAccountName());
		}
		alist = new JList(temp.toArray());
		
		//centerpanel.add(alist);
		//Southpanel
		ButtonListener buttonlisten = new ButtonListener();
		JPanel southpanel = new JPanel(new BorderLayout());
		panel.add(southpanel,BorderLayout.SOUTH);
		
		//South south atm and update buttons
		JPanel southS = new JPanel(new GridLayout(1,0,40,40));
		southS.add(Atm);
		Atm.addActionListener(buttonlisten);
		southpanel.add(southS,BorderLayout.SOUTH);
		
		//Southcenter
		JPanel southCenter = new JPanel(new GridLayout(0,1,20,20));
		JPanel southWest = new JPanel(new GridLayout(0,1,20,20));
		
		//Text fields displaying chosen account
		JLabel accNum = new JLabel("Account Number:");
		JLabel accType = new JLabel("Account Type:");
		JLabel accbal = new JLabel("Balance:");
		accountNumber.setEnabled(false);
		accountType.setEnabled(false);
		balance.setEnabled(false);
		southWest.add(accNum);
		southWest.add(accType);
		southWest.add(accbal);
		southCenter.add(accountNumber);
		southCenter.add(accountType);
		southCenter.add(balance);
		southpanel.add(southCenter,BorderLayout.CENTER);
		southpanel.add(southWest,BorderLayout.WEST);
		
		//Center
		alist.addListSelectionListener(new ListSelectionListener(){
			public void valueChanged(ListSelectionEvent e){
				int sel = alist.getSelectedIndex();
				if(sel != -1){
					accountNumber.setText(String.valueOf(acc.get(sel).getAccountName()));
					accountNumber.setEnabled(true);
					accountType.setText(acc.get(sel).getClass().getName());
					accountType.setEnabled(true);
					balance.setText(String.valueOf(acc.get(sel).getBalance()));
					balance.setEnabled(true);
				}
			}
		});
		
		
		centerpanel.add(alist,BorderLayout.CENTER);
		panel.add(centerpanel,BorderLayout.CENTER);
		
		frame.add(panel);
		frame.setLocation(500,250);
		frame.setSize(400,400);
		frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		WindowListen(frame);
		frame.setVisible(true);
	}
	
	/**
	 * Checks to see when the user closes the bank gui. When 
	 * the gui closes it prints out the final data.
	 * @param frame
	 */
	public void WindowListen(JFrame frame){
		frame.addWindowListener(new WindowListener(){
			public void windowClosing(WindowEvent e){
				System.out.println("========== Final Bank data ===================" + "\n");
				System.out.println("Account Type    " + "Account  " + "Balance");
				System.out.println("----------------" + "---------" + "-------");
				if(model.returnAccounts().size() != 0){
					for(Account acc:model.returnAccounts()){
						System.out.println(acc.getClass().getName() + "       " + acc.getAccountName() + "   $   " +
								acc.getBalance());
					}
				}
			}
			
			public void windowOpened(WindowEvent e){}
			public void windowClosed(WindowEvent e){}
			public void windowDeiconified(WindowEvent e){}
			public void windowDeactivated(WindowEvent e){}
			public void windowActivated(WindowEvent e){}
			public void windowIconified(WindowEvent e){}
			
		});
	}
		
	/**
	 * The button listener for the atm button. Opens a new atm
	 * when the button is pressed.
	 *
	 */
	private class ButtonListener implements ActionListener{
		public void actionPerformed(ActionEvent e){
			if(e.getSource().equals(Atm)){
				AtmGUI atm = new AtmGUI(new AtmModel(model));
			}
		}
	}
}


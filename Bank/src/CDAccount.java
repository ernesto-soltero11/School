/**
 * CDAccount.java
 * 
 * Version:
 * $Id: CDAccount.java,v 1.6 2012-05-17 17:50:09 exs6350 Exp $
 * 
 * Revisions:
 * $Log: CDAccount.java,v $
 * Revision 1.6  2012-05-17 17:50:09  exs6350
 * Added comments
 *
 * Revision 1.5  2012-05-17 02:56:57  exs6350
 * Added more atm functionality.
 *
 * Revision 1.4  2012-05-16 23:48:57  exs6350
 * Almost done with bank gui
 *
 * Revision 1.3  2012-05-13 18:28:57  exs6350
 * changed account type to an interface.
 * Finished all the account classes.
 *
 * Revision 1.2  2012-05-13 03:26:31  exs6350
 * Added all files for my design.
 *
 * Revision 1.1  2012-05-13 01:55:28  exs6350
 * initial
 *
 */

/**
 * Represents a cd account.
 * @author Ernesto Soltero(exs6350@rit.edu)
 *
 */
public class CDAccount implements Account{
	private final double MONTHLY_INTEREST = .05/12;
	private int accountNumber;
	private int pinNumber;
	private double accountBalance;
	double inter;
	
	/**
	 * The constructor for the cdaccount.
	 * @param name The account name.
	 * @param pin The pin for the account
	 * @param balance The opening balance
	 */
	public CDAccount(int name, int pin, double balance){
			if(balance < 500){
				//System.err.println("Can't open a CD account with less than 500$");
			}
			else{
				this.accountNumber = name;
				this.pinNumber = pin;
				this.accountBalance = balance;
			}
	}
	
	/**
	 * Applies the interest tot he cs account.
	 */
	public void applyInterest(){
		double inter = getBalance() * MONTHLY_INTEREST;
		Deposit(inter);
	}
	
	/**
	 * Returns the balance for this account.
	 */
	public double getBalance(){
		return accountBalance;
	}
	
	/**
	 * Returns the account number.
	 */
	public int getAccountName(){
		return accountNumber;
	}
	
	/**
	 * Gets the pin number of this account.
	 */
	public int getPin(){
		return pinNumber;
	}
	
	/**
	 * Not applicable for this account.
	 */
	public void Withdraw(double amount){};
	
	/**
	 * Deposits into the account.
	 * @param amount The amount to deposit.
	 */
	public void Deposit(double amount){
		accountBalance += amount;
	}
}

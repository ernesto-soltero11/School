/**
 * BinSet.java
 * 
 * Version:
 * $Id: BinSet.java,v 1.4 2012-04-24 21:46:06 exs6350 Exp $
 * 
 * Revisions:
 * $Log: BinSet.java,v $
 * Revision 1.4  2012-04-24 21:46:06  exs6350
 * Finished comments
 *
 * Revision 1.3  2012-04-24 20:48:22  exs6350
 * Made a new test for characters
 *
 * Revision 1.2  2012-04-23 22:14:25  exs6350
 * Initial finish
 *
 * Revision 1.1  2012-04-23 20:17:27  exs6350
 * Initial
 *
 * 
 */
import java.util.*;
/**
 * Constructs a BinSet out of the elements of a generic collection.
 * @author Ernesto Soltero(exs6350@rit.edu)
 *
 */

public class BinSet<E extends Comparable<? super E>> extends AbstractSet<E> {

    private ArrayList<E> s = new ArrayList<E>();

    /*
     * Creates an empty BinSet
     */
    public BinSet(){}

    /*
     * Initializes an ArrayList with all the elements from 
     * a collection.
     * @param c An instance of a collection
     */
    public BinSet(Collection<? extends E> c){
    	for(E object: c){
    		this.s.add(object);
    	}
    }
    
    /*
     * Searches if the ArrayList contains the element in the collection.
     * @param e The element to search for.
     * @return 1 if the element is in the collection and 0 if it is not. 
     */
    private int binarySearch(E e){
    	int first = 0;
    	int last = s.size()-1;
    	
    	while(last>=first){
    		int middle = first+last/2;
    		if(s.get(middle).compareTo(e) == 0){
    			return 1;
    		}
    		if(s.get(middle).compareTo(e)<0){
    			first = middle + 1; 
    		}
    		if(s.get(middle).compareTo(e)>0){
    			last = middle -1;
    		}
    	}
    	return 0;
    }

    /*
     * Adds the specified element to the ArrayList. Also keeps
     * the ArrayList in order when adding.
     * @param A generic element to add to the ArrayList.
     * @return True if the element was added false if
     * the element is already in the ArrayList.
     */
    public boolean add(E e) {
    	int index = 0;
    	if(binarySearch(e) == 0){
    		for(int i = 0;i<s.size();i++){
    			if(s.get(index).compareTo(e)>0){
    				s.add(index,e);
    				return true;
    			}
    			else{
    				index+=1;
    			}
    		}
    	}
    	return false;
    }

    /*
     * Adds all the elements in the collection to the ArrayList.
     * If the ArrayList was modified returns true.
     * @param Adds all the element from the collection to the
     * ArrayList
     * @return A boolean if at least one element was added and false
     * if no elements where added.
     */
    public boolean addAll(Collection<? extends E> c) {
    	boolean modified = false;
    	for(E thing: c){
    		if(s.add(thing) == true){
    			modified = true;
    		}
    	}
    	return modified;
    }

    /*
     * Empties out the ArrayList.
     */
    public void clear() {
    	s.clear();
    }
     
    /*
     * Returns true if the ArrayList contains this object.
     * @param An object to check if it is within the ArrayList
     * @return True if the ArrayList has the element.
     */
    @SuppressWarnings("unchecked")
    public boolean contains(Object o) {
    	if(s.contains(o)== true){
    		return true;
    	}
    	return false;
    }

    /*
     * Checks to see if the ArrayList contains all the 
     * objects in this collection.
     * @param A collection of objects.
     * @return True if all objects are within the ArrayList. False
     * if one object is not within the ArrayList.
     */
    public boolean containsAll(Collection<?> c) {
    	for(Object e: c){
    		if(contains(e) != true){
    			return false;
    		}
    	}
    return true;
    }

    /*
     * Checks to see if the ArrayList is empty.
     * @return True if the the ArrayList is empty.
     */
    public boolean isEmpty() {
    	if(s.size() == 0){
    		return true;
    	}
    	return false;
    }
          
    /*
     * Returns an iterator over the ArrayList.
     * @return An iterator over the ArrayList.
     */
    public Iterator<E> iterator() {
    	Iterator<E> iter = s.iterator();
    	return iter;
    }
    
    /*
     * Removes an object from the ArrayList returns false if the object is not 
     * in the ArrayList.
     * @param An object to remove from the ArrayList.
     * @return Returns true if the object was removed false if the object
     * is not in the ArrayList.
     */
    public boolean remove(Object o) {
    	Iterator<E> iter = iterator();
    	while(iter.hasNext()){
    		if(iter.next().equals(o)){
    			iter.remove();
    			return true;
    		}
    	}
    	return false;
    }
     
    /*
     * Retains all the elements in the collection from the
     * ArrayList.
     * @param A collection of elements
     * @return True if all the elements in the collection where 
     * retained in the ArrayList.
     */
    public boolean retainAll(Collection<?> c) {
    	boolean modified = false;
    	Iterator<?> iter = iterator();
    	while(iter.hasNext()){
    		if(c.contains(iter.next()) != true){
    			iter.remove();
    			modified = true;
    		}
    	}
    	return modified;
    }
    

    /*
     * Returns the size of the current ArrayList.
     * @return The size of the ArrayList.
     */
    public int size() {
    	return s.size();
    }
     
    /*
     * Returns the ArrayList to a generic array of object types.
     * @return The ArrayList as an array of object types.
     */
    public Object[] toArray() {
    	return s.toArray();
    }
     
    
    /*
     * Returns a new array with the specified return type out of 
     * the specified objects. 
     * @param Takes in a array with a generic type.
     * @return Returns an array with the specified type.
     */
    @SuppressWarnings("unchecked")
    public <T> T[] toArray(T[] a) {
    	T[] newarray = s.toArray(a);
    	return newarray;
    }

    /*
     * Returns a string representation of the ArrayList.
     * @return A string representation of the elements in 
     * the ArrayList.
     */
    public String toString(){
    	String stuff = "";
    	for(Object e:s){
    		stuff += e.toString();
    	}
    	return stuff;
    }

    /**
     * Displays a message followed by success or failure indicating
     * whether or not a particular test was successful.
     *
     * @param message The String form of the message.
     *
     * @param b A boolean indicating whether the test was successful or not.    
     *
     */

    private static void resultTest(String message, boolean b){
	if (b){
	    System.out.println(message + " success");
	} else {
	    System.out.println(message + " failure");
	}
    }
    
    /**
     * Runs a suite of tests to validate the implementation of BinSet
     * for Integer elements.
     *
     */

    private static void testInteger(){
	Set<Integer> set = new BinSet<Integer>(Arrays.asList(1,3));
	
	resultTest("constructor 1",
		   Arrays.equals(set.toArray(), Arrays.asList(1,3).toArray()));
	
	set.add(2);
	resultTest("add 1",
		   Arrays.equals(set.toArray(), Arrays.asList(1,2,3).toArray()));
	
	resultTest("contains 1", set.contains(1));
	resultTest("contains 2", set.contains(2));
	resultTest("contains 3", set.contains(3));
	resultTest("contains 4", !set.contains(4));
	
	resultTest("size 1", set.size() == 3);
	
	set.clear();
	resultTest("clear/size", set.size() == 0);
	resultTest("clear/isEmpty", set.isEmpty());
	
	set.addAll(Arrays.asList(1,2,3));
	resultTest("addAll 1",set.size() == 3);
	
	resultTest("containsAll 1", set.containsAll(Arrays.asList(3,2)));
	resultTest("containsAll 2", !set.containsAll(Arrays.asList(4,3)));
	
	set.remove(2);
	resultTest("remove 1", 
		   Arrays.equals(set.toArray(), Arrays.asList(1,3).toArray()));
	
	Integer[] a = {1,3};
	int j = 0;
	for (Integer i : set){
	    resultTest("iterator " + i, i.equals(a[j]));
	    j++;
	}
	
	set.retainAll(Arrays.asList(3,4));
	resultTest("retainAll 1", 
		   Arrays.equals(set.toArray(), Arrays.asList(3).toArray()));
	
	resultTest("toArray(array) 1", 
		   Arrays.equals(set.toArray(new Integer[0]), 
				 Arrays.asList(3).toArray()));
    }

    /**
     * Runs a suite of tests to validate the implementation of BinSet
     * for String elements
     */
    private static void testCharacter(){
    	Set<Character> set = new BinSet<Character>(Arrays.asList('A','C'));
    	
    	resultTest("constructor 1",
    		   Arrays.equals(set.toArray(), Arrays.asList('A','C').toArray()));
    	
    	set.add('B');
    	resultTest("add B",
    		   Arrays.equals(set.toArray(), Arrays.asList('A','B','C').toArray()));
    	
    	resultTest("contains A", set.contains('A'));
    	resultTest("contains B", set.contains('B'));
    	resultTest("contains C", set.contains('C'));
    	resultTest("contains D", !set.contains('D'));
    	
    	resultTest("size 1", set.size() == 3);
    	
    	set.clear();
    	resultTest("clear/size", set.size() == 0);
    	resultTest("clear/isEmpty", set.isEmpty());
    	
    	set.addAll(Arrays.asList('A','B','C'));
    	resultTest("addAll 1",set.size() == 3);
    	
    	resultTest("containsAll 1", set.containsAll(Arrays.asList('C','B')));
    	resultTest("containsAll 2", !set.containsAll(Arrays.asList('D','C')));
    	
    	set.remove('B');
    	resultTest("remove B", 
    		   Arrays.equals(set.toArray(), Arrays.asList('A','C').toArray()));
    	
    	Character[] a = {'A','C'};
    	int j = 0;
    	for (Character i : set){
    	    resultTest("iterator " + i, i.equals(a[j]));
    	    j++;
    	}
    	
    	set.retainAll(Arrays.asList('C','D'));
    	resultTest("retainAll 1", 
    		   Arrays.equals(set.toArray(), Arrays.asList('C').toArray()));
    	
    	resultTest("toArray(array) 1", 
    		   Arrays.equals(set.toArray(new Character[0]), 
    				 Arrays.asList('C').toArray()));
    }
    
    /**
     * The main method for BinSet.  It runs any test scaffolding methods such as testInteger.
     *
     * @param args Command line arguments are not used.
     *
     */

    public static void main(String[] args){
	testInteger();
	System.out.println("\nNext test for a set of Characters: \n");
	testCharacter();
   }

}
"""
Author: exs6350@rit.eduErnesto Soltero

tunnelling example"""

from turtle import *

def init():
    pu()
    home()
    pendown()
    
def drawCircleFromCenter(radius):
    init()
    penup()
    forward(radius)
    circle(radius)

def tunnelIt(radius,scale):
    while radius >5:
        drawCircleFromCenter(radius)
        radius = radius - (radius*scale/100)
    return
    
def tunnelRec(radius,scale):
    if radius <= 5:
        return
    else:
        drawCircleFromCenter(radius,scale)
        radius = radius - (radius*scale/100)
        tunnelRec(radius,scale)
        

def main():
    radius = int(input("Enter a value for the radius: "))
    scale = int(input("Enter a value for the scale factor: "))
    tunnelIt(radius,scale)
    input("press Enter for recursion tunnel.")
    reset()
    tunnelRec(radius,scale)
    
main()
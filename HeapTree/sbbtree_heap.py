
""" 
file: sbbtree_heap.py
language: python3
author: mtf@cs.rit.edu Matthew Fluet
author: Ernesto Soltero (exs6350@rit.edu)
description: Heap implemented with size-balanced binary trees
"""

import random
from string import ascii_letters

class TreeNode( object ):
    """
       A (non-empty) binary tree node for a size-balanced binary tree heap.
       SLOTS:
         key: Orderable
         value: Any
         size: NatNum; the size of the sub-tree rooted at this node
         parent: NoneType|TreeNode; the parent node
         lchild: NoneType|TreeNode; the node of the left sub-tree
         rchild: NoneType|TreeNode; the node of the right sub-tree
    """
    __slots__ = ( 'key', 'value', 'size', 'parent', 'lchild', 'rchild' )

    def __init__( self, key, value, parent ):
        self.key = key
        self.value = value
        self.size = 1
        self.parent = parent
        self.lchild = None
        self.rchild = None

    def __str__( self ):
        slchild = str(self.lchild)
        srchild = str(self.rchild)
        skv = str((self.key, self.value)) + " <" + str(self.size) + ">"
        pad = " " * (len(skv) + 1)
        s = ""
        for l in str(self.lchild).split('\n'):
            s += pad + l + "\n"
        s += skv + "\n"
        for l in str(self.rchild).split('\n'):
            s += pad + l + "\n"
        return s[:-1]

class SBBTreeHeap( object ):
    """
       A size-balanced binary tree heap.
       SLOTS:
         root: NoneType|TreeNode
    """
    __slots__ = ( 'root' )

    def __init__( self ):
        self.root = None

    def __str__( self ):
        return str(self.root)

def checkNode( node, parent ):
    """
       checkNode: NoneType|TreeNode NoneType|TreeNode -> Tuple(NatNum, Boolean, Boolean, Boolean, Boolean)
       Checks that the node correctly records size information,
       correctly records parent information, satisfies the
       size-balanced property, and satisfies the heap property.
    """
    if node == None:
        return 0, True, True, True, True
    else:
        lsize, lsizeOk, lparentOk, lbalanceProp, lheapProp = checkNode( node.lchild, node )
        rsize, rsizeOk, rparentOk, rbalanceProp, rheapProp = checkNode( node.rchild, node )
        nsize = lsize + 1 + rsize
        nsizeOk = node.size == nsize
        sizeOk = lsizeOk and rsizeOk and nsizeOk
        nparentOk = node.parent == parent
        parentOk = lparentOk and rparentOk and nparentOk
        nbalanceProp = abs(lsize - rsize) <= 1
        balanceProp = lbalanceProp and rbalanceProp and nbalanceProp
        nheapProp = True
        if (node.lchild != None) and (node.lchild.key < node.key):
            nheapProp = False
        if (node.rchild != None) and (node.rchild.key < node.key):
            nheapProp = False
        heapProp = lheapProp and rheapProp and nheapProp
        return nsize, sizeOk, parentOk, balanceProp, heapProp

def checkHeap( heap ):
    """
       checkHeap: SBBTreeHeap -> NoneType
       Checks that the heap is a size-balanced binary tree heap.
    """
    __, sizeOk, parentOk, balanceProp, heapProp = checkNode( heap.root, None )
    if not sizeOk:
        print("** ERROR **  Heap nodes do not correctly record size information.")
    if not parentOk:
        print("** ERROR **  Heap nodes do not correctly record parent information.")
    if not balanceProp:
        print("** Error **  Heap does not satisfy size-balanced property.")
    if not heapProp:
        print("** Error **  Heap does not satisfy heap property.")
    assert(sizeOk and parentOk and balanceProp and heapProp)
    return


def empty(heap):
    """
       empty: SBBTreeHeap -> Boolean
       Returns True if the heap is empty and False if the heap is non-empty.
       Raises TypeError if heap is not an instance of SBBTreeHeap.
       Must be an O(1) operation.
    """
    if not isinstance(heap,SBBTreeHeap):
        raise TypeError("Not a instance of SBBTreeHeap")
    else:
        return heap.root == None


def enqueue( heap, key, value ):
    """
       enqueue: SBBTreeHeap Orderable Any -> NoneType
       Adds the key/value pair to the heap.
       Raises TypeError if heap is not an instance of SBBTreeHeap.
       Must be an O(log n) operation.
    """
    if not isinstance(heap,SBBTreeHeap):
        raise TypeError("Heap is not an instance of SBBTreeHeap")
    elif empty(heap) == True:
        node = TreeNode(key,value,None)
        heap.root = node
    else:
        curnode = heap.root
        while True:
            if curnode.lchild == None:
                node = TreeNode(key,value,curnode)
                curnode.lchild = node
                curnode.size += 1
                curnode = curnode.lchild
                break
            elif curnode.rchild == None:
                node = TreeNode(key,value,curnode)
                curnode.rchild = node
                curnode.size += 1
                curnode = curnode.rchild
                break
            elif curnode.lchild.size < curnode.rchild.size:
                curnode.size += 1
                curnode = curnode.lchild
            else:
                curnode.size += 1
                curnode = curnode.rchild
        curnode = sift_up(curnode)      
    ## COMPLETE enqueue FUNCTION ##

def frontMin( heap ):
    """
       frontMin: SBBTreeHeap -> Tuple(Orderable, Any)
       Returns (and does not remove) the minimum key/value in the heap.
       Raises TypeError if heap is not an instance of SBBTreeHeap.
       Raises IndexError if heap is empty.
       Precondition: not empty(heap)
       Must be an O(1) operation.
    """
    if not isinstance(heap,SBBTreeHeap):
        raise TypeError("Not an instance of SBBTreeHeap")
    elif empty(heap) == True:
        raise IndexError("Heap is empty")
    else:
        return (heap.root.key,heap.root.value)


def dequeueMin( heap ):
    """
       dequeueMin: SBBTreeHeap -> NoneType
       Removes (and does not return) the minimum key/value in the heap.
       Raises TypeError if heap is not an instance of SBBTreeHeap.
       Raises IndexError if heap is empty.
       Precondition: not empty(heap)
       Must be an O(log n) operation.
    """
    if empty(heap) == True:
        raise IndexError("Heap is empty.")
    elif not isinstance(heap,SBBTreeHeap):
        raise TypeError("Not a instance of SBBTreeHeap")
    elif heap.root.size == 1:
        heap.root = None
    else:
        curnode = heap.root
        while True:
            if curnode.lchild == None and curnode.rchild == None:
                tempnode = curnode
                heap.root.value = tempnode.value
                heap.root.key = tempnode.key
                if curnode.parent.lchild.key == curnode.lchild.key:
                    curnode.parent.lchild = None
                else:
                    curnode.parent.rchild = None
                break
            elif curnode.rchild == None:
                curnode.size -= 1
                curnode = curnode.lchild
            elif curnode.lchild == None:
                curnode.size -= 1
                curnode = curnode.rchild
            else:
                if curnode.lchild.size < curnode.rchild.size:
                    curnode.size -= 1
                    curnode = curnode.rchild
                else:
                    curnode.size -= 1
                    curnode = curnode.lchild
        heap = sift_down(heap)
    ## COMPLETE dequeueMin FUNCTION ##


def heapsort( l ):
    """
       heapsort: ListOfOrderable -> ListOfOrderable
       Returns a list that has the same elements as l, but in ascending order.
       The implementation must a size-balanced binary tree heap to sort the elements.
       Must be an O(n log n) operation.
    """
    sortedheap = []
    index = 0
    heap = SBBTreeHeap()
    for i in l:
        enqueue(heap,i,ascii_letters[index])
        index += 1
    while len(heap) != 0:
        sortedheap.append(heap.root.key)
        dequeueMin(heap)
    return sortedheap
    ## COMPLETE heapsort FUNCTION ##

def sift_up(element):
    """Sifts the element up if it is less than its parent.
    element : node-> node"""
    while True:
        if element.parent == None:
            break
        elif element.key < element.parent.key:
            element.key,element.parent.key = element.parent.key,element.key
            element.value,element.parent.value = element.parent.value,element.value 
        else:
            break
        element = element.parent
    
def sift_down(heap):
    """Sifts the root down if one of its child is less than the root.
    root node: node -> node"""
    curnode = heap.root
    while True:
        if curnode.lchild == None and curnode.rchild == None:
            break
        elif curnode.rchild == None:
            if curnode.lchild.key < curnode.key:
                curnode.value, curnode.lchild.value = curnode.lchild.value,curnode.value
                curnode.key,curnode.lchild.key = curnode.lchild.key,curnode.key
                curnode = curnode.lchild
            else:
                curnode = curnode.rchild
        elif curnode.lchild == None:
            if curnode.rchild.key < curnode.key:
                curnode.value,curnode.rchild.value = curnode.rchild.value,curnode.value
                curnode.key,curnode.rchild.key = curnode.rchild.key,curnode.key
                curnode = curnode.rchild
            else:
                curnode = curnode.rchild
        else:
            if curnode.lchild.key < curnode.rchild.key:
                curnode.key,curnode.lchild.key = curnode.lchild.key,curnode.key
                curnode.value,curnode.lchild.value = curnode.lchild.value,curnode.value
                curnode = curnode.lchild
            elif curnode.rchild.key < curnode.key:
                curnode.value,curnode.rchild.value = curnode.rchild.value,curnode.value
                curnode.key,curnode.rchild.key = curnode.rchild.key,curnode.key
                curnode = curnode.rchild
                    
######################################################################
######################################################################

if __name__ == "__main__":
    R = random.Random()
    R.seed(0)
    print(">>> h = SBBTreeHeap()");
    h = SBBTreeHeap()
    print(h)
    checkHeap(h)
    for v in "ABCDEFGHIJKLMNOPQRSTUVWXYZ":
        k = R.randint(0,99)
        print(">>> enqueue(h," + str(k) + "," + str(v) + ")")
        enqueue(h, k, v)
        print(h)
        checkHeap(h)
    while not empty(h):
        print(">>> k, v = frontMin(h)")
        k, v = frontMin(h)
        print((k, v))
        print(">>> dequeueMin(h)")
        dequeueMin(h)
        print(h)
        checkHeap(h)
    for i in range(4):
        l = []
        for x in range(2 ** (i + 2)):
            l.append(R.randint(0,99))
        print(" l = " + str(l))
        sl = heapsort(l)
        print("sl = " + str(sl))

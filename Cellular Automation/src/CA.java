/**
 *
 * $Author$ 
 *
 * $LastChangedDate$
 *
 * $Rev$
 *
 */

import java.util.LinkedList;
import java.util.concurrent.atomic.AtomicIntegerArray;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Executors;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;


/**
 * @author Ernesto Soltero (exs6350@rit.edu)
 *
 * This class represents a CellularAutomation. Given a rule, the number of,
 * cells, and the number of steps to compute to. The transition from one step
 * to the next stage is partially done in parallel by using atomic access no threads need
 * to modify any resources because we move everything to a new atomicArray.
 */
public class CA {

	private AtomicIntegerArray currentState;
	private AtomicIntegerArray nextState;
	private int[] rule;
	private int steps;
	public int size;
	public int finalCount = 0;
	private int MAX = 0;
	private int MIN = 100000000;
	private int MIN_STEP = 1000000;
	private int MAX_STEP = 0;
	private ExecutorService pool;
	
	/**
	 * 
	 * @param r - The rule to use
	 * @param size - The number of cells we want to make
	 * @param s - The number of steps we want to compute for
	 */
	public CA(String r, int size, int s) {
		this.currentState = new AtomicIntegerArray(size);
		this.nextState = new AtomicIntegerArray(size);
		this.size = size;
		this.steps = s;
		this.rule = new int[r.length()];
		String[] temp = r.split("");
		for(int i = 0; i < temp.length; ++i){
			if(i == 0){
				continue;
			}
			this.rule[i-1] = Integer.valueOf(temp[i]);
		}
	}
	
	/**
	 * Creates all the CellularAutomationThreads that are going to change the currentState into the
	 * next state. Places them in a cached pool to be reused
	 */
	public void changeStates(){
		for(int j = 0; j < steps; ++j){
			pool = Executors.newCachedThreadPool();
			LinkedList<Future<Integer>> holder = new LinkedList<Future<Integer>>();
			for(int i = 0; i < currentState.length(); ++i){
				holder.addLast(pool.submit(new CellularAutomationThread(this, i , rule)));
			}
			try {
				pool.shutdown();
				pool.awaitTermination(30, TimeUnit.SECONDS);
				int pop = 0;
				for(Future<Integer> result:holder){
					try {
						if(result.get() == 1){
							pop += 1;
						}
					} catch (ExecutionException e) {
						e.printStackTrace();
					}
				}
				if(pop > MAX){MAX = pop; MAX_STEP = j+1;}
				if(pop < MIN){MIN = pop; MIN_STEP = j+1;}
				if(j == (steps - 1)){finalCount = pop;}
				copyAndClear(nextState);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}
	
	/**
	 * 
	 * @param index -  The index to get from the array
	 * @return - The contents of the array at index
	 */
	public int getCurrent(int index){
		return currentState.get(index);
	}
	
	/**
	 * 
	 * @param index - The index into the array
	 * @param change - The int to change the contents of location at array[index] too
	 */
	public void changeNext(int index, int change){
		nextState.set(index, change);
	}
	
	/**
	 * This function sets up the initial state of the currentState in preperation to change
	 * to the next states. (This is step 0)
	 * 
	 * @param args - The arguments to initialize the program.
	 */
	public void parse(String[] args){
		for(int i = 3; i < args.length; ++i){
			int temp = Integer.valueOf(args[i]);
			currentState.getAndIncrement(temp);
		}
		int popcount = 0;
		for(int i = 0; i < size; ++i){
			if(getCurrent(i) == 1){
				popcount += 1;
			}
		}
		if(popcount > MAX){
			MAX = popcount;
			MAX_STEP = 0;
		}
		if(popcount < MIN){
			MIN = popcount;
			MIN_STEP = 0;
		}
	}
	
	/**
	 * This copies the current state into the nextState. After all the copying is done
	 * we clear the nextState. This is done sequentially no threads access this method.
	 * @param copyFrom - The array to copy from
	 */
	private void copyAndClear(AtomicIntegerArray copyFrom){
		//copy the values over
		for(int i = 0; i < copyFrom.length(); ++i){
			currentState.set(i, copyFrom.get(i));
		}
		//clear the nextState
		for(int i = 0; i < copyFrom.length(); ++i){
			nextState.set(i, 0);
		}
	}
	
	//debugging only
	private void printState(){
		System.out.println("current state: " + currentState.toString());
	}
	
	/**
	 * @param args - The arguments to initialize the program with.
	 */
	public static void main(String[] args) {
		//Error checks
		if(args.length < 3){
			System.err.print("Usage: java CA <rule> <N> <S> <index> ...");
			System.exit(1);
		}
		if(args[0].length() != 8){
			System.err.print("error - rule must be eight characters long");
			System.exit(2);
		}
		String rule = String.valueOf(args[0]);
		for(int i = 0; i < args[0].length(); ++i){
			if(Integer.valueOf((rule.charAt(i))) < 48){
				System.err.print("error - rule must contain only 0 and 1 characters");
				System.exit(3);
			}
			else if(Integer.valueOf(rule.charAt(i)) > 49){
				System.err.print("error - rule must contain only 0 and 1 characters");
				System.exit(3);
			}
		}
		if(Integer.valueOf(args[1]) <= 0){
			System.err.print("error - N must be greater than zero");
			System.exit(4);
		}
		if(Integer.valueOf(args[2]) <= 0){
			System.err.print("error - S must be greater than zero");
			System.exit(5);
		}
		
		//End error checks
		CA CellA = new CA(args[0],Integer.valueOf(args[1]), Integer.valueOf(args[2]));
		CellA.parse(args);
		CellA.changeStates();
		System.out.println("Smallest popcount: " + CellA.MIN + " at step " + CellA.MIN_STEP);
		System.out.println("Largest popcount: " + CellA.MAX + " at step " + CellA.MAX_STEP);
		System.out.println("Final popcount: " + CellA.finalCount + " at step " + Integer.valueOf(args[2]));
	}
}

/**
 *
 * $Author$ 
 *
 * $LastChangedDate$
 *
 * $Rev$
 *
 */
import edu.rit.pj2.Loop;
import edu.rit.pj2.Task;
import edu.rit.pj2.IntVbl;
/**
 * @author Ernesto Soltero (exs6350@rit.edu)
 *
 */
public class NewCA extends Task{

	int[] rule;
	int steps;
	int[] currentState;
	int[] nextState;
	IntVbl popcount = new IntVbl(0);
	int MIN = 10000000;
	int MAX = 0;
	int MAX_STEP = 0;
	int MIN_STEP = 100000000;
	int size;
	int finalCount = 0;

	/** The main entry point for the program. Runs a parallelforLoop through
	 * the whole array changing the state of the cells and keeping track of all the
	 * popcounts at each stage. 
	 * @param args - The arguments that this program will run with.
	 */
	public void main(String[] args) throws Exception{
		long start = System.currentTimeMillis();
		checkErrors(args);
		parse(args);
		for(int i = 0; i < steps; ++i){
			popcount.item = 0;
			parallelFor(0, size -1).exec(new Loop(){
				
				IntVbl tempPop;
				int left;
				int right;
				
				public void start(){
					tempPop = threadLocal(popcount);
				}
				
				public void run(int index){
					if(index == 0){
						left = size - 1;
						right = index + 1;
					}
					else if(index == size -1){
						right = 0;
						left = index - 1;
					}
					else{
						left = index - 1;
						right = index + 1;
					}
					int changed = nextState(left, index, right);
					nextState[index] = changed;
					tempPop.item += changed;
				}
			});
			if(popcount.item > MAX){MAX = popcount.item; MAX_STEP = i+1;}
			if(popcount.item < MIN){MIN = popcount.item; MIN_STEP = i+1;}
			if(i == (steps - 1)){finalCount = popcount.item;}
			int[] temp = new int[size];
			currentState = nextState;
			nextState = temp;
		}
		long end = System.currentTimeMillis();
		System.out.println("Time taken: " + (end-start));
		System.out.println("Smallest popcount: " + MIN + " at step " + MIN_STEP);
		System.out.println("Largest popcount: " + MAX + " at step " + MAX_STEP);
		System.out.println("Final popcount: " + finalCount + " at step " + steps);
	}
	
	/**
	 * This function sets up the initial state of the currentState in preparation to change
	 * to the next states. (This is step 0)
	 * @param args - The arguments that the program will run with 
	 */
	private void parse(String[] args){
		for(int i = 3; i < args.length; ++i){
			int temp = Integer.valueOf(args[i]);
			currentState[temp] = 1;
		}
		int popcount = 0;
		for(int i = 0; i < size; ++i){
			if(currentState[i] == 1){
				popcount += 1;
			}
		}
		if(popcount > MAX){
			MAX = popcount;
			MAX_STEP = 0;
		}
		if(popcount < MIN){
			MIN = popcount;
			MIN_STEP = 0;
		}
	}
	
	/**
	 * This function checks for all the arguments entered are correctly taken in 
	 * and parsed. Also sets up all the initial values of the program.
	 * @param args - The arguments that the program will run with 
	 */
	private void checkErrors(String[] args){
		if(args.length < 3){
			System.err.print("Usage: java CA <rule> <N> <S> <index> ...");
			System.exit(1);
		}
		if(args[0].length() != 8){
			System.err.print("error - rule must be eight characters long");
			System.exit(2);
		}
		String rl = String.valueOf(args[0]);
		for(int i = 0; i < args[0].length(); ++i){
			if(Integer.valueOf((rl.charAt(i))) < 48){
				System.err.print("error - rule must contain only 0 and 1 characters");
				System.exit(3);
			}
			else if(Integer.valueOf(rl.charAt(i)) > 49){
				System.err.print("error - rule must contain only 0 and 1 characters");
				System.exit(3);
			}
		}
		if(Integer.valueOf(args[1]) <= 0){
			System.err.print("error - N must be greater than zero");
			System.exit(4);
		}
		if(Integer.valueOf(args[2]) <= 0){
			System.err.print("error - S must be greater than zero");
			System.exit(5);
		}
		
		size = Integer.valueOf(args[1]);
		steps = Integer.valueOf(args[2]);
		currentState = new int[size];
		nextState = new int[size];
		String r = args[0];
		rule = new int[r.length()];
		String[] temp = r.split("");
		for(int i = 0; i < temp.length; ++i){
			if(i == 0){
				continue;
			}
			rule[i-1] = Integer.valueOf(temp[i]);
		}
	}
	
	/**
	 * This computes the next state of the cell based on the neighbors 
	 * of the cell and the cell itself using the rule
	 * @param left - The left neighbor of the cell
	 * @param index - The cell itself
	 * @param right - The right neighbor of the cell
	 * @return The next state of the cell
	 */
	private int nextState(int left, int index, int right){
		String l = String.valueOf(currentState[left]);
		String m = String.valueOf(currentState[index]);
		String r = String.valueOf(currentState[right]);
		String s = l + m + r;
		int nextState = Integer.parseInt(s, 2);
		return rule[nextState];
	}

}

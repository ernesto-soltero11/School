/**
 *
 * $Author$ 
 *
 * $LastChangedDate$
 *
 * $Rev$
 *
 */

import java.util.concurrent.Callable;

/**
 * @author Ernesto Soltero (exs6350@rit.edu)
 *
 * The <code>CellularAutomaionThread</code> is a thread for each cell and it computes
 * the next stage of the cell. All threads run in parallel no locks are required because of 
 * the atomicArray.
 */
public class CellularAutomationThread implements Callable<Integer> {

	private int[] rule;
	private int index;
	private CA auto;
	private int changed;
	
	/**
	 * 
	 * @param automater - The CellAutomation that were modifying
	 * @param in - The index of the cell that were changing
	 * @param r - The rule to use
	 */
	public CellularAutomationThread(CA automater, int in, int[] r){
		this.index = in;
		this.rule = r;
		this.auto = automater;
	}

	/**
	 * Runs the thread and grabs the cell states from the current state.
	 * The it computes what the next stage should be for the cell and returns
	 * that stage.
	 */
	public Integer call() {
		int left; 
		int right;
		if(index == 0){
			left = auto.size - 1;
			right = index + 1;
		}
		else if(index == auto.size -1){
			right = 0;
			left = index - 1;
		}
		else{
			left = index - 1;
			right = index + 1;
		}
		changed = nextState(left, index, right);
		auto.changeNext(index, changed);
		return changed;
	}

	/**
	 * 
	 * @param l - The index of the cell's left neighbor
	 * @param m - The index of the cell itself
	 * @param r - The index of the cell's right neighbor
	 * @return The next state that this cell (m) should change to.
	 */
	public int nextState(int l, int m, int r){
		String left = String.valueOf(auto.getCurrent(l));
		String middle = String.valueOf(auto.getCurrent(m));
		String right = String.valueOf(auto.getCurrent(r));
		String s = left + middle + right;
		int nextState = Integer.parseInt(s, 2);
		return rule[nextState];
	}
}

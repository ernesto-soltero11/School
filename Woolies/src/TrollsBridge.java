/**
 * TrollsBridge.java
 * 
 * Version:
 * $Id: TrollsBridge.java,v 1.4 2012-05-16 02:17:11 exs6350 Exp $
 * 
 * Revisions:
 * $Log: TrollsBridge.java,v $
 * Revision 1.4  2012-05-16 02:17:11  exs6350
 * Remove from list is now in leave bridge and notifies all.
 *
 * Revision 1.3  2012-05-15 20:18:35  exs6350
 * Fixed the timer.
 *
 * Revision 1.2  2012-05-10 22:19:47  exs6350
 * Need to fix timer
 *
 * 
 */
import java.util.LinkedList;
/**
 * The trollbridge guard. Makes sure that there are only three
 * threads running at once.
 * @author Ernesto Soltero (exs6350@rit.edu)
 *
 */
public class TrollsBridge {
	private int capacity;
	private LinkedList<Woolie> bridge = new LinkedList<Woolie>();
	
	/**
	 * Constructor for the TrollsBridge.
	 * @param max The amount of woolies that can be on the 
	 * bridge at one time.
	 */
	public TrollsBridge(int max){
		this.capacity = max;
	}
	
	/**
	 * Woolie asks the troll to get on the bridge. Troll puts the 
	 * Woolie on a queue.
	 * @param woolie The woolie waiting to get in the bridge.
	 */
	public void enterBridgePlease(Woolie woolie){
		System.out.println("The troll scowls 'Get in line!' when " + woolie.name() 
				+ " shows up at the bridge.");
		bridge.add(woolie);
		
		synchronized(bridge){
		while(capacity ==0){
			try{
				bridge.wait();
			}
		catch(InterruptedException e){}
		}
		capacity--;
		bridge.removeFirst();
		bridge.notifyAll();
	}
	}
	
	/**
	 * A woolie invokes this notifying the troll that they have gotten
	 * off the bridge.
	 */
	public synchronized void leave(){
		capacity++;
		notifyAll();
		}
	}

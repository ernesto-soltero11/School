/**
 * Woolie.java
 * 
 * Version:
 * $Id: Woolie.java,v 1.5 2012-05-16 02:19:15 exs6350 Exp $
 * 
 * Revisions:
 * $Log: Woolie.java,v $
 * Revision 1.5  2012-05-16 02:19:15  exs6350
 * print statement was off
 *
 * Revision 1.4  2012-05-16 02:17:11  exs6350
 * Remove from list is now in leave bridge and notifies all.
 *
 * Revision 1.3  2012-05-15 20:18:35  exs6350
 * Fixed the timer.
 *
 * Revision 1.2  2012-05-10 22:19:47  exs6350
 * Need to fix timer
 *
 * 
 */

/**
 * The woolie that wants to cross the bridge.
 * @author Ernesto Soltero(exs6350@rit.edu)
 *
 */
public class Woolie extends Thread{
	private String name;
	private int time;
	private String destination;
	private TrollsBridge bridge;
	
	/**
	 * The constructor for a woolie.
	 * @param name The name of the woolie.
	 * @param crossTime The amount of time a woolie takes to cross the bridge.
	 * @param destination The destination of the woolie.
	 * @param bridgeguard The monitor for the bridge.
	 */
	public Woolie(String name, int crossTime, String destination, TrollsBridge bridgeguard){
		this.name = name;
		this.time = crossTime;
		this.destination = destination;
		this.bridge = bridgeguard;
	}
	
	/**
	 * Runs the code for the woolie to cross the bridge.
	 * Prints out the time that the woolie is currently at.
	 */
	public void run(){
		bridge.enterBridgePlease(this);
		int sleepTimer = 0;
		System.out.println(name + " is starting to cross.");
		while(sleepTimer< (time * 1000)-1000){
			try{
				System.out.println( name + " is at " + ((sleepTimer/1000)+1) + " seconds.");
				this.sleep(1000);
				sleepTimer+= 1000;
				
			}
			catch(InterruptedException e){e.printStackTrace();}
	}
		System.out.println(name + " leaves at " + destination);
		bridge.leave();
}
	
	/**
	 * Gets the name of the woolie.
	 * @return The string representation of the woolie name.
	 */
	public String name(){
		return name;
	}
}

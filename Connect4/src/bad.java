/**
 * bad.java
 * 
 * Version:
 * $Id: bad.java,v 1.4 2012-04-18 00:40:21 exs6350 Exp $
 * 
 * Revisions:
 * $Log: bad.java,v $
 * Revision 1.4  2012-04-18 00:40:21  exs6350
 * Finished basic algorithms
 *
 * Revision 1.3  2012-04-17 00:32:06  exs6350
 * Finished implementing the board.
 * Need to fix the error messages output.
 * Also need to finish the move algorithms for
 * the bad and good player classes.
 *
 * Revision 1.2  2012-04-16 02:28:22  exs6350
 * Added more code. Changed player abstract class to an interface. Easier to declare types and figure out when a player is human.
 *
 * Revision 1.1  2012-04-15 05:08:31  exs6350
 * Initial
 *
 */
import java.util.ArrayList;
/**
 * Represents the bad computer algorithm.
 * @author Ernesto Soltero
 *
 */
public class bad implements Player {
	
	private String piece;
	/**
	 * Constructor for the bad class.
	 * @param Piece The piece for the bad object
	 */
	public bad( String Piece) {
		this.piece = Piece;
	}

	/**
	 * Returns the piece for this object.
	 */
	public String returnPiece(){
		return piece;
	}
	
	/**
	 * Returns the first col that is empty.
	 */
	public int makeMove(ArrayList<Integer> valid){
		int col = valid.get(0);
		return col;
	}
	
	/**
	 * Helper function for the makeMove function. Makes
	 * an ArrayList of all empty columns.
	 */
	public ArrayList<Integer> moveHelper(Board board){
		String [][] game = board.getBoard();
		ArrayList<Integer> validmoves = new ArrayList<Integer>(board.getcolLength());
		for(Integer i = 0;i<game[0].length;i++){
			if(game[0][i].equals("0")){
				validmoves.add(i);
			}
		}
		return validmoves;
	}
	
	/**
	 * Returns the move message when this object is
	 * making a move.
	 */
	public String moveMessage(){
		String i = "bad player " + piece + " moving... ";
		return i;
	}
	
	/**
	 * Not applicable to this class.
	 */
	public String promptForPiece(){
		return "";
	}
}

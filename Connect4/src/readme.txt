4003-243 Project 01: README
===============================


0. Author Information
---------------------

CS Username: 	exs6350	Name:Ernesto Soltero

-------------------
1. Problem Analysis
-------------------

Summarize the analysis work you did. 
What new information did it reveal?  How did this help you?
How much time did you spend on problem analysis?

The most of amount of time that I spent on was the board class. I was thinking
of what I should take in as parameters and how to implement the endgame. I ran
into a problem where I needed to know the state of the board in order to implement
my ai algorithms. I had to solve this by returning the board and making it mutable. 
In order to offset this problem I created a undomove which would return the board
back to the original state when the good class would run its algorithms.

---------
2. Design
---------

Explain the design you developed to use and why. 
What are the major components? What connects to what?
How much time did you spend on design?

I designed the board in its own class and implemented some checking methods. The endgame
method is used to check is someone one by checking vertically, horizantally, and diagonally.
I also implemented a validmove function but this later became obselte when I implemented the
validity checking into the player classes. I used an arraylist to hold all the possible moves
that the ai classes could make and choose from that arraylist the integer of the col that they 
should move to. The promptforpiece method was mainly used to identify if the object was a human or not.
I ran two sets of code twice one for player1 and the other for player2. If the class was a player a 
different set of code would run so it would prompt for the col to enter the piece.

-----------------------------
3. Implementation and Testing
-----------------------------

Describe your implementation efforts here; this is the coding and testing.

What did you put into code first?
How did you test it?
How well does the solution work? 
Does it completely solve the problem?
Is anything missing?
How could you do it differently?
How could you improve it?
What design changes are needed for the next iteration?

How much total time would you estimate you worked on the project? 
If you had to do this again, what would you do differently?

I worked on this project for about 3 days. The only ai class that I made was the random
class because it was the easiest and then I used that to test my board. I made sure
that the board was working correctly before I even started implementing all the other
player classes. I then tested all the possible error messages, different arguments some
valid some invalid, and the default board size. The solution works pretty decently. I would
probably restructure my ai classes to work more smoothly and use a minmax algorithm for 
the good player class to at least the third level.

----------------------
4. Development Process
----------------------

Describe your development process here; this is the overall process.

How much problem analysis did you do before your initial coding effort?
How much design work and thought did you do before your initial coding effort?
How many times did you have to go back to assess the problem?

What did you learn about software development?

I only put in a little thought into the board class. I put in little thought
for the player classes because I figured that they wouldn't be hard to implement
once I knew what my board had to take in from the classes. I had to address
the board class many times as it had issues but once I started coding the player 
classes I only had to address the board class twice. Once for getting the state
of the board and another for fixing the endgame function because it was originally
designed for only the defualt board.
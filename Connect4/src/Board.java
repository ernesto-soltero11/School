/**
 * Board.java
 * 
 * Version;
 * $Id: Board.java,v 1.6 2012-04-18 02:26:37 exs6350 Exp $
 * 
 * Revisions:
 * $Log: Board.java,v $
 * Revision 1.6  2012-04-18 02:26:37  exs6350
 * Added text files.
 *
 * Revision 1.5  2012-04-18 01:53:01  exs6350
 * Finished the error message output.
 * Fixed good player class. Added more
 * comments.
 *
 * Revision 1.4  2012-04-17 00:32:06  exs6350
 * Finished implementing the board.
 * Need to fix the error messages output.
 * Also need to finish the move algorithms for
 * the bad and good player classes.
 *
 * Revision 1.3  2012-04-16 02:28:23  exs6350
 * Added more code. Changed player abstract class to an interface. Easier to declare types and figure out when a player is human.
 *
 * Revision 1.2  2012-04-15 20:23:07  exs6350
 * Added more functions to Board class. Need to do computer algorithms.
 *
 * Revision 1.1  2012-04-15 05:08:32  exs6350
 * Initial
 *
 */
import java.util.ArrayList;
/**
 * Represents the board.
 * @author Ernesto Soltero(exs6350@rit.edu)
 *
 */
public class Board {
	private String[][] gameboard;
	private String winresult = "";
	
	/**
	 * Constructor for the board
	 * @param row Int for row size
	 * @param column Int for col size
	 */
	public Board(int row, int column){
		this.gameboard = new String[row][column];
		for(int i = 0;i<row;i++)
			for(int j = 0;j<column;j++){
				gameboard[i][j] = "0";
			}
	}
	
	/**
	 * Gets the col length of the array
	 * @return int col length
	 */
	public int getcolLength(){
		return gameboard[0].length;
	}
	
	/**
	 * Returns the board. Allows the computer algorithms to make their
	 * move based on the current state of the board.
	 * @return Two dimensional array
	 */
	public String[][] getBoard(){
		return gameboard;
	}
	
	/**
	 * Checks to see if someone won and records the way they won
	 * in a string. Some of this code was borrowed from wiki.acse.net
	 * credit goes to the association of computer studies educators.
	 * @return True or false if someone won or game is tied.
	 */
	public boolean endGame(){
		// Checks for a horizontal win
		if(gameboard[0].length>=4){
			for(int row = 0;row<gameboard.length;row++)
				for(int col = 0;col<gameboard[0].length-3;col++){
					if(gameboard[row][col] != "0" &&
					   gameboard[row][col].equals(gameboard[row][col+1]) &&
					   gameboard[row][col].equals(gameboard[row][col+2]) &&
					   gameboard[row][col].equals(gameboard[row][col+3])){
						winresult = " won in row " + row;
						return true;
					}
				}
			}
		//check for a vertical win
		if(gameboard.length>=4){
			for(int col = 0;col<gameboard[0].length;col++)
				for(int row = 0;row<gameboard.length-3;row++){
					if(gameboard[row][col] != "0" &&
					   gameboard[row][col].equals(gameboard[row+1][col]) &&
					   gameboard[row][col].equals(gameboard[row+2][col]) &&
					   gameboard[row][col].equals(gameboard[row+3][col])){
						winresult = " won in column " + col;
						return true;
					}
				}
			}
		//check for a diagonal win(negative slope)
		if(gameboard.length>=4 && gameboard[0].length>=4){
			for(int row = 0;row<gameboard.length-3;row++)
				for(int col = 0;col<gameboard[0].length-3;col++){
					if(gameboard[row][col] != "0" &&
					   gameboard[row][col].equals(gameboard[row+1][col+1]) &&
					   gameboard[row][col].equals(gameboard[row+2][col+2]) &&
					   gameboard[row][col].equals(gameboard[row+3][col+3])){
						winresult = " won on a diagonal";
						return true;
				}
			}
		}
		//check for a diagonal win(positive slope)
		if(gameboard[0].length>=4 && gameboard.length>=4){
			for(int row = gameboard.length-1;row>3;row--)
				for(int col = 0;col<gameboard[0].length-3;col++){
					if(gameboard[row][col] != "0" &&
					   gameboard[row][col].equals(gameboard[row-1][col+1]) &&
					   gameboard[row][col].equals(gameboard[row-2][col+2]) &&
					   gameboard[row][col].equals(gameboard[row-3][col+3])){
						winresult = " won on a diagonal";
						return true;
				}
			}
		}
		ArrayList<Boolean> nowin = new ArrayList<Boolean>();
		//Checks to see if all top rows of the board are filled.
		for(int k = 0;k<gameboard[0].length;k++){
			if(!gameboard[0][k].equals("0")){
				nowin.add(true);
			}
		}
		//If size of the array and the columns of board are equal means all
		//spaces are filled thus a tie occured.
		if(nowin.size() == gameboard[0].length){
			winresult = "Its a tie, no one wins";
			return true;
		}
		return false;
	}
		
	/**
	 * Prints the current state of the board.
	 */
	public void printBoard(){
		for(int i = 0;i<gameboard.length;i++){
			for(int k = 0;k < gameboard[0].length; k++){
				String contents = gameboard[i][k];
				if(contents.equals("0")){
					System.out.print("| ");
				}
				else{
					System.out.print("|" + contents);
				}
				
			}
			System.out.print("|" + "\n");
		}
		for(int i = 0; i<=gameboard[0].length-1;i++){
			System.out.print("+-");
		}
		System.out.print("+");
	}
	
	/**
	 * Updates the board with the move made.
	 * @param colum the column of where the piece was placed
	 * @param piece the player peice
	 */
	public void getMove(int colum, String piece){
		for(int i = 0; i<gameboard[0].length+1;i++){
			if(i>gameboard.length-1){
				gameboard[gameboard.length-1][colum] = piece;
				break;
			}
		    else if(gameboard[i][colum].equals("0")){
				continue;
			}
			else if(!(gameboard[i][colum].equals("0"))){
				gameboard[i-1][colum] = piece;
				break;
			}
		}
	}
	
	/**
	 * Returns a string with the move that the player made.
	 * @param column the column where the move was made.
	 * @param piece the piece of the player.
	 * @return String where the player placed their move.
	 */
	public String afterMove(int column,String piece){
		String out = "Player drops an " + piece + " into column: " + column;
		return out;
	}
	
	/**
	 * Prints how a player won or if it is a tie.
	 * @return String of game results.
	 */
	public String getresults(){
		return winresult;
	}
}
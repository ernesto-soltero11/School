/**
 * Player.java
 * 
 * Version:
 * $Id: Player.java,v 1.5 2012-04-18 01:53:00 exs6350 Exp $
 * 
 * Revisions:
 * $Log: Player.java,v $
 * Revision 1.5  2012-04-18 01:53:00  exs6350
 * Finished the error message output.
 * Fixed good player class. Added more
 * comments.
 *
 * Revision 1.4  2012-04-17 00:32:06  exs6350
 * Finished implementing the board.
 * Need to fix the error messages output.
 * Also need to finish the move algorithms for
 * the bad and good player classes.
 *
 * Revision 1.3  2012-04-16 02:28:22  exs6350
 * Added more code. Changed player abstract class to an interface. Easier to declare types and figure out when a player is human.
 *
 */
import java.util.ArrayList;
/**
 * Player interface that contains methods for other 
 * player classes to fill.
 * @author Ernesto Soltero(exs6350@rit.edu)
 *
 */
public interface Player {
		
		/**
		 * Returns the best spot to move to.
		 */
		public int makeMove(ArrayList<Integer> valid);
		
		/**
		 * Returns the piece of the class
		 * @return String
		 */
		public String returnPiece();
		
		/**
		 * Returns the move message
		 * @return String
		 */
		public String moveMessage();
		
		/**
		 * Algorithm that returns a list of the best spots to move to.
		 * @param board Current Board
		 * @return ArrayList
		 */
		public ArrayList<Integer> moveHelper(Board board);
		
		/**
		 * Prompts for a piece.(human class only). This 
		 * is also used to differentiate if this is a human class
		 * or computer class.
		 * @return
		 */
		public String promptForPiece();
}

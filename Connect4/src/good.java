import java.util.ArrayList;

/**
 * good.java
 * 
 * Version:
 * $Id: good.java,v 1.5 2012-04-18 01:53:00 exs6350 Exp $
 * 
 * Revisions:
 * $Log: good.java,v $
 * Revision 1.5  2012-04-18 01:53:00  exs6350
 * Finished the error message output.
 * Fixed good player class. Added more
 * comments.
 *
 * Revision 1.4  2012-04-18 00:40:21  exs6350
 * Finished basic algorithms
 *
 * Revision 1.3  2012-04-17 00:32:06  exs6350
 * Finished implementing the board.
 * Need to fix the error messages output.
 * Also need to finish the move algorithms for
 * the bad and good player classes.
 *
 * Revision 1.2  2012-04-16 02:28:22  exs6350
 * Added more code. Changed player abstract class to an interface. Easier to declare types and figure out when a player is human.
 *
 * Revision 1.1  2012-04-15 05:08:29  exs6350
 * Initial
 *
 * 
 */

/**
 * Represents the good computer algrothm.
 * @author Ernesto Soltero(exs6350@rit.edu)
 *
 */
public class good implements Player{
	
	private String piece;
	/**
	 * Constructor for the good class.
	 * @param Piece
	 */
	public good( String Piece) {
		this.piece = Piece;
	}

	/**
	 * Returns the first best move.
	 */
	public int makeMove(ArrayList<Integer> valid){
		return valid.get(0);
	}
	
	/**
	 * Performs a best move algorithm. If it can win it will
	 * return the column where it will win. If the oppenent can 
	 * win it will return the column where it can block. Otherwise
	 * it builds up where its other pieces are.
	 */
	public ArrayList<Integer> moveHelper(Board board){
		ArrayList<Integer> moves = new ArrayList<Integer>();
		String[][] game = board.getBoard();
		
		//Figures out if the move results in a win
		//if it does adds the column to a list of 
		//possible moves
		for(int i = 0; i<game[0].length;i++){
			if(game[0][i].equals("0")){
				board.getMove(i,piece);
				if(board.endGame() == true){
					undoMove(i,board);
					moves.add(i);
					continue;
				}
				undoMove(i,board);
			}
		}
		
		//Places an opponent piece into the board. If
		//it is a winning piece it adds it to the possible moves
		//meaning the win will be blocked.
		for(int k = 0; k<game[0].length;k++){
			if(game[0][k].equals("0")){
				if(piece.equals("X")){
					board.getMove(k, "O");
				}
				else{
					board.getMove(k, "X");
				}
				if(board.endGame() == true){
					undoMove(k,board);
					moves.add(k);
					continue;
				}
				undoMove(k,board);
			}
		}
		
		//Else place a piece at the next column
		//that contains the next empty column.
		for(int h =0;h<game[0].length;h++){
			if(game[0][h].equals("0")){
				moves.add(h);
			}
		}
		return moves;
	}
	
	/**
	 * Helper function that undoes a move that
	 * move helper makes because the board is modified in
	 * place.
	 * @param col where the last move was made
	 * @param board board class
	 * @return the board to its original state
	 */
	private Board undoMove(int col,Board board){
		String[][] gameboard = board.getBoard();
		for(int i = 0; i<gameboard[0].length+1;i++){
		    if(gameboard[i][col].equals("0")){
				continue;
			}
			else if(!(gameboard[i][col].equals("0"))){
				gameboard[i][col] = "0";
				break;
			}
		}
		return board;
	}
	
	/**
	 * Returns the piece of this class.
	 */
	public String returnPiece(){
		return piece;
	}
	
	/**
	 * Returns the move message for this class.
	 */
	public String moveMessage(){
		String i = "good player " + piece + " moving... ";
		return i;
	}
	
	/**
	 * Not applicable to this class.
	 */
	public String promptForPiece(){
		return "";
	}
}

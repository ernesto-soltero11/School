/**
 * Connect4.java
 * 
 * Version:
 * $Id: Connect4.java,v 1.9 2012-04-18 02:49:52 exs6350 Exp $
 * 
 * Revisions:
 * $Log: Connect4.java,v $
 * Revision 1.9  2012-04-18 02:49:52  exs6350
 * More fixes
 *
 * Revision 1.8  2012-04-18 02:41:29  exs6350
 * Fixed a invalid column error
 *
 * Revision 1.7  2012-04-18 01:57:04  exs6350
 * Human player 2 wasn't casted right so
 * it took the piece of player 1.
 *
 * Revision 1.6  2012-04-18 01:53:00  exs6350
 * Finished the error message output.
 * Fixed good player class. Added more
 * comments.
 *
 * Revision 1.5  2012-04-18 00:40:21  exs6350
 * Finished basic algorithms
 *
 * Revision 1.4  2012-04-17 00:32:06  exs6350
 * Finished implementing the board.
 * Need to fix the error messages output.
 * Also need to finish the move algorithms for
 * the bad and good player classes.
 *
 * Revision 1.3  2012-04-16 02:28:22  exs6350
 * Added more code. Changed player abstract class to an interface. Easier to declare types and figure out when a player is human.
 *
 * Revision 1.2  2012-04-15 20:23:06  exs6350
 * Added more functions to Board class. Need to do computer algorithms.
 *
 * Revision 1.1  2012-04-15 05:08:30  exs6350
 * Initial
 *
 */
import java.util.Scanner;
import java.util.ArrayList;
/**
 * Runs the main connect 4 program.
 * @author Ernesto Soltero(exs6350@rt.edu)
 *
 */
public class Connect4 {
	private static Board gameboard;
	private static Player player1;
	private static Player player2;
	private static Scanner input;
	
	
	/**
	 * Runs the main program.
	 * @param args
	 */
	public static void main(String[] args) {
		input = new Scanner(System.in);
		final ArrayList<String> player= new ArrayList<String>();
		player.add("human");
		player.add("good");
		player.add("bad");
		player.add("random");
		
		//When the args is not 4 or 2
		if(args.length!=4 && args.length != 2){
			System.err.println("Usage: java Connect4 player-X player-O [#rows #cols]");
			System.err.println("where player-X and player-O are one of: human bad good random");
			System.err.println("[#rows #cols] are optional, if provided their values must be"); 
			System.err.println("in the ranges: #rows from 1 to 6 and #cols from 1 to 7");
			System.exit(0);
		}
		//When a invalid row or col is given
		if(args.length == 4 && Integer.parseInt(args[2])>6 | Integer.parseInt(args[2])<1 |
				Integer.parseInt(args[3])>7 | Integer.parseInt(args[3])<1){
			System.err.println("Usage: java Connect4 player-X player-O [#rows #cols]");
			System.err.println("where player-X and player-O are one of: human bad good random");
			System.err.println("[#rows #cols] are optional, if provided their values must be"); 
			System.err.println("in the ranges: #rows from 1 to 6 and #cols from 1 to 7");
			System.exit(0);
		}
		//When a player that is not of human,good,bad, or random type is declared.
		if(!player.contains(args[0]) | !player.contains(args[1])){
			System.err.println("Usage: java Connect4 player-X player-O [#rows #cols]");
			System.err.println("where player-X and player-O are one of: human bad good random");
			System.err.println("[#rows #cols] are optional, if provided their values must be"); 
			System.err.println("in the ranges: #rows from 1 to 6 and #cols from 1 to 7");
			System.exit(0);
		}
	
		if(args.length == 4){ //If it has more args then it means a board size was declared
			gameboard = new Board(Integer.valueOf(args[2]),Integer.valueOf(args[3]));
		}
		else{
			gameboard = new Board(4,4); //default board size
		}
		
		if(args[0].equals("human")){
			player1 = new human("X");
		}
		if(args[1].equals("human")){
			player2 = new human("O");
		}
		if(args[0].equals("random")){
			player1 = new random("X");
		}
		if(args[1].equals("random")){
			player2 = new random("O");
		}
		if(args[0].equals("bad")){
			player1 = new bad("X");
		}
		if(args[1].equals("bad")){
			player2 = new bad("O");
		}
		if(args[0].equals("good")){
			player1 = new good("X");
		}
		if(args[1].equals("good")){
			player2 = new good("O");
		}
		
		while(true){//keeps running while no one has won
			System.out.println("\n");
			gameboard.printBoard();
			
			//Code that runs all the functions for the first player.
			if(!(player1.promptForPiece()).equals("")){//figures out if it is a human
				human humanplayer = (human) player1;
				System.out.println("\n"+ "\n"+ humanplayer.moveMessage());
				System.out.print(humanplayer.promptForPiece());
				int col = input.nextInt();
				
				//Code that prompts and checks to see of the player quits or enters a
				//number bigger than the column length of the board.
				if(col == -1){
					System.out.print(humanplayer.returnPiece() + " quits the game");
					System.exit(0);
				}
				while(col>gameboard.getcolLength()-1){//if col entered is greater than column
					//length keeps prompting for another col
					System.out.println("invalid column: " + col);
					System.out.print(player1.promptForPiece());
					col = input.nextInt();
					if(col == -1){
						System.out.print(humanplayer.returnPiece() + " quits the game");
						System.exit(0);
					}
				}
				
				
				gameboard.getMove(col, humanplayer.returnPiece());
				System.out.println(gameboard.afterMove(col, humanplayer.returnPiece()));
				if(gameboard.endGame() == true){//checks to see if it was the winning move
					//and if it is prints how the player won
					if(gameboard.getresults().equals("Its a tie, no one wins")){
						System.out.println(gameboard.getresults());
						System.exit(0);
					}
					else{
						System.out.println(humanplayer.returnPiece() + gameboard.getresults()+ "\n");
						gameboard.printBoard();
						System.exit(0);
				}
				}
				System.out.println("\n");
				gameboard.printBoard();
			}
			else{//else it runs the computer algorithms
				System.out.println("\n");
				System.out.println(player1.moveMessage());
				int move = player1.makeMove(player1.moveHelper(gameboard));
				gameboard.getMove(move, player1.returnPiece());
				System.out.println(gameboard.afterMove(move, player1.returnPiece()));
				if(gameboard.endGame() == true){
					if(gameboard.getresults().equals("Its a tie, no one wins")){
						System.out.println(gameboard.getresults()+"\n");
						gameboard.printBoard();
						System.exit(0);
					}
					else{
						System.out.println(player1.returnPiece() + gameboard.getresults()+ "\n");
						gameboard.printBoard();
						System.exit(0);
				}	
			}
				System.out.println("\n");
				gameboard.printBoard();
			}
			
			
			//This is the same exact code for the first player but it runs all the functions
			//for the second player
			if(!(player2.promptForPiece()).equals("")){
				human humanplayer1 = (human) player2;
				System.out.println("\n"+ "\n"+humanplayer1.moveMessage());
				System.out.print(humanplayer1.promptForPiece());
				int col = input.nextInt();
				
				//Code that prompts and checks to see of the player quits or enters a
				//number bigger than the column length of the board.
				if(col == -1){
					System.out.print(humanplayer1.returnPiece() + " quits the game");
					System.exit(0);
				}
				while(col>gameboard.getcolLength()-1){
					System.out.println("invalid column: " + col);
					System.out.print(player1.promptForPiece());
					col = input.nextInt();
					if(col == -1){
						System.out.print(humanplayer1.returnPiece() + " quits the game");
						System.exit(0);
					}
				}
				
				gameboard.getMove(col, humanplayer1.returnPiece());
				System.out.println(gameboard.afterMove(col,humanplayer1.returnPiece()));
				if(gameboard.endGame() == true){
					if(gameboard.getresults().equals("Its a tie, no one wins")){
						System.out.println(gameboard.getresults()+"\n");
						gameboard.printBoard();
						System.exit(0);
					}
					else{
						System.out.println(humanplayer1.returnPiece() + gameboard.getresults()+ "\n");
						gameboard.printBoard();
						System.exit(0);
				}
					
			}
			gameboard.printBoard();	
			}
			else{
				System.out.println("\n");
				System.out.println(player2.moveMessage());
				int move = player2.makeMove(player2.moveHelper(gameboard));
				gameboard.getMove(move, player2.returnPiece());
				System.out.println(gameboard.afterMove(move, player2.returnPiece()));
				if(gameboard.endGame() == true){
					if(gameboard.getresults().equals("Its a tie, no one wins")){
						System.out.println(gameboard.getresults()+ "\n");
						gameboard.printBoard();
						System.exit(0);
					}
					else{
						System.out.println(player2.returnPiece() + gameboard.getresults()+ "\n");
						gameboard.printBoard();
						System.exit(0);
				}
			}
		}		
	}
}
}
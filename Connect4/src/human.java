/**
 * human.java
 * 
 * Version:
 * $Id: human.java,v 1.4 2012-04-17 00:32:07 exs6350 Exp $
 * 
 * Revisions:
 * $Log: human.java,v $
 * Revision 1.4  2012-04-17 00:32:07  exs6350
 * Finished implementing the board.
 * Need to fix the error messages output.
 * Also need to finish the move algorithms for
 * the bad and good player classes.
 *
 * Revision 1.3  2012-04-16 02:28:23  exs6350
 * Added more code. Changed player abstract class to an interface. Easier to declare types and figure out when a player is human.
 *
 * Revision 1.2  2012-04-15 20:23:07  exs6350
 * Added more functions to Board class. Need to do computer algorithms.
 *
 * Revision 1.1  2012-04-15 05:08:32  exs6350
 * Initial
 *
 */
import java.util.ArrayList;

 /**
 * Human player class.
 * @author Ernesto Soltero(exs6350@rit.edu)
 */
public class human implements Player {
	private String piece;
	
	/**
	 * Constructor for the human object.
	 * @param Piece String representing the piece of the human player.
	 */
	public human( String Piece) {
		this.piece = Piece;
	}

	/**
	 * Returns the piece of this object.
	 */
	public String returnPiece(){
		return piece;
	}
	
	/**
	 * Prompts for the human player of where to place their move.
	 */
	public String promptForPiece(){
		String i = "Player " + piece + ": Enter the column to drop your piece (-1 to quit): ";
		return i;
	}
	
	/**
	 * Not applicable for the human class.
	 */
	public int makeMove(ArrayList<Integer> valid){
		return 0;
	}
	
	/**
	 * Not applicable for the human class.
	 */
	public ArrayList<Integer> moveHelper(Board board){
		return null;
	}
	
	/**
	 * Prints out when the human player is making a move.
	 */
	public String moveMessage(){
		String i = "human player " + piece + " moving... ";
		return i;
	}
}

/**
 * random.java
 * 
 * Version: 
 * $Id: random.java,v 1.3 2012-04-17 00:32:07 exs6350 Exp $
 * 
 * Versions:
 * $Log: random.java,v $
 * Revision 1.3  2012-04-17 00:32:07  exs6350
 * Finished implementing the board.
 * Need to fix the error messages output.
 * Also need to finish the move algorithms for
 * the bad and good player classes.
 *
 * Revision 1.2  2012-04-16 02:28:23  exs6350
 * Added more code. Changed player abstract class to an interface. Easier to declare types and figure out when a player is human.
 *
 * Revision 1.1  2012-04-15 05:08:34  exs6350
 * Initial
 *
 * 
 */
import java.util.ArrayList;
import java.util.Random;
/**
 * Represents the random computer algorithm.
 * @author Ernesto Soltero
 *
 */
public class random implements Player {
	
	private String piece;
	/**
	 * Constructor for the random object.
	 * @param Piece The piece of this object
	 */
	public random(String Piece) {
		this.piece = Piece;
	}
	
	/**
	 * Returns the piece of this object.
	 */
	public String returnPiece(){
		return piece;
	}

	/**
	 * Places a piece in a random position based on the open spots.
	 */
	public int makeMove(ArrayList<Integer> valids) {
		Random rand = new Random();
		if(valids.size() == 1){
			return valids.get(0);
		}
		int random = rand.nextInt(valids.size());
		int col = valids.get(random);
		return col;
	}

	/**
	 * Gets all the empty positions of the board. Helper function.
	 */
	public ArrayList<Integer> moveHelper(Board board){
		String [][] game = board.getBoard();
		ArrayList<Integer> validmoves = new ArrayList<Integer>(board.getcolLength());
		for(Integer i = 0;i<game[0].length;i++){
			if(game[0][i].equals("0")){
				validmoves.add(i);
			}
		}
		return validmoves;
	}
	
	/**
	 * Outputs when the object is making a move.
	 */
	public String moveMessage(){
		String i = "random player " + piece + " moving... ";
		return i;
	}
	
	/**
	 * Not applicable for this object.
	 */
	public String promptForPiece(){
		return "";
	}
}

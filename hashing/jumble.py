"""
Author: Ernesto Soltero (exs6350@rit.edu)
Created: January 2012
File: jumble.py
"""
from hashtable import *

def buildTable(dim,file):
    """Builds a hash table and fills it.
    dim: int-> int
    file: txt file->txt file"""
    htable = HashTable(dim)
    for words in open(file):
        key = sort_words(words).strip()
        put(htable,key,words)
    return htable

def sort_words(word):
    """Sorts the word in alphabetical order and the sorted version is used as the key.
    words: str-> str"""
    word = list(word)
    word.sort()
    return "".join(word)     
       
def main():
    """Runs the main program. Returns a hash table that contains all the keys and values.
    If the hash function has a load greater than 75% then the table get rehashed by the length
    of the table multiplied by 2 plus 1.
    return a filled hashtable"""
    dim = int(input("Enter the size of the hash table: "))
    file = input("Enter the name of the file: ")
    table = buildTable(dim,file)
    print("The imbalance of the table is: ", imbalance(table))
    word = input("Enter a sequence of letters without spaces: ")
    while word != '':
        key = sort_words(word).strip()
        if contains(table,key) == True:
            contain = get(table,key)
            for i in contain:
                print(i)
        else:
            print("There are no words that can be made from these letters.")
        word = input("Enter a sequence of letters without spaces: ")
        
if __name__ == "__main__":
    main()
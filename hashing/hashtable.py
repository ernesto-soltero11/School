""" 
file: hashtable.py
language: python3
author: sps@cs.rit.edu Sean Strout 
author: jeh@cs.rit.edu James Heliotis 
author: anh@cs.rit.edu Arthur Nunes-Harwitt
author: Ernesto Soltero (exs6350@rit.edu)
description: open addressing Hash Table for CS 242 Lecture
"""

from string import ascii_lowercase, ascii_uppercase

class HashTable( object ):
    """
       The HashTable data structure contains a collection of values
       where each value is located by a hashable key.
       No two values may have the same key, but more than one
       key may have the same value.
    """

    __slots__ = ( "table", "size" )

    def __init__( self, capacity=100 ):
        """
           Create a hash table.
           The capacity parameter determines its initial size.
        """
        self.table = [None] * capacity
        self.size = 0

    def __str__( self ):
        """
           Return the entire contents of this hash table,
           one chain of entries per line.
        """
        result = ""
        for i in range( len( self.table ) ):
            result += str( i ) + ": "
            result += str( self.table[i] ) + "\n"
        return result

    class _Entry( object ):
        """
           A nested class used to hold key/value pairs.
        """

        __slots__ = ( "key", "value" )

        def __init__( self, entryKey, entryValue ):
            self.key = entryKey
            self.value = entryValue

        def __str__( self ):
            return "(" + str( self.key ) + ", " + str( self.value ) + ")"

def hash_function(val, n):
    """
       Compute a hash of the val string that is in [0 ... n).
    """
    count = 1
    alphabet = []
    punctuation = ["'","&",".","\\"]
    for i in ascii_lowercase:
        alphabet.append(i)
    for i in ascii_uppercase:
        alphabet.append(i)
    for i in punctuation:
        alphabet.append(i)
    for i in range(10):
        alphabet.append(str(i))
        alphabet.append(int(i))
    lst = list(val)
    for i in lst:
        count += count * int(alphabet.index(i))
    return count % n

def keys( hTable ):
    """
       Return a list of keys in the given hashTable.
    """
    result = []
    for entry in hTable.table:
        if entry != None:
            for i in range(len(entry)):
                result.append(i.key)
    return result

def contains( hTable, key ):
    """
       Return True if hTable has an entry with the given key.
    """
    index = hash_function( key, len( hTable.table ) )
    if hTable.table[index] != None:
        for i in hTable.table[index]:
            if i .key == key:
                return True
    return False

def put( hTable, key, data ):
    """
       Using the given hash table, set the given key to the
       given value. If the key already exists, the given ralue
       will replace the previous one already in the table.
       If the table is full, an Exception is raised.
    """
    if checkload(hTable) == True:
        rehash(hTable)
    index = hash_function( key, len( hTable.table ) )
    if hTable.table[index] == None:
        hTable.table[index] = [HashTable._Entry(key,data)]
        hTable.size += 1
    elif hTable.table[index] != None:
        hTable.table[index].append(HashTable._Entry(key,data))
        hTable.size += 1
    return True

def get( hTable, key):
    """
       Return the value associated with the given key in
       the given hash table.
       Precondition: contains(hTable, key)
    """
    val = []
    index = hash_function( key, len( hTable.table ) )
    if hTable.table[index] == None:
        raise Exception( "Hash table does not contain key." )
    else:
        for i in hTable.table[index]:
            if i.key == key:
                val.append(i.value)
    return val

def imbalance(hTable):
    """Checks the ratio of the numbers of elements in the table to the size of the table.
    hTable: hashtable -> hashtable"""
    count = 0
    for i in hTable.table:
        if i == None:
            continue
        else:
            count += 1          
    return (hTable.size/count) - 1

def checkload(htable):
    """
    Checks the ratio of table. If its more than 75% returns True.
    htable: hashtable -> hashtable
    """
    return (htable.size/len(htable.table)) >= .75

def rehash(hTable):
    """Creates a new hash table multiplied by 2 plus 1.
    hTable: hashtable -> new hash table
    """
    size = (len(hTable.table) * 2) + 1
    newtable = HashTable(size)
    for index in hTable.table:
        if index == None:
            continue
        else:
            for i in index:
                put(newtable,i.key, i.value)
    hTable.table = newtable.table
    print(len(hTable.table))
    return hTable       
"""
File: skyscraper.py (Student version)
Author: Sean Strout <sps@cs.rit.edu>
Author: Miriam Barnett (mxb3429@rit.edu)
Purpose:  A backtracking solver for the Skyscraper puzzle.
Language: Python 3
"""

from copy import deepcopy
from sys import argv

# STUDENTS ARE FREE TO ADD ADDITIONAL SLOTS TO THIS CLASS, BUT DO NOT CHANGE
# ANY OF THE EXISTING SLOTS!!!!
class SkyscraperConfig:
    """
    A class that represents a skyscraper configuration.
        DIM - square board DIMension (int)
        lookNS - north to south looking values, left to right 
            (list of DIM int's)
        lookEW - east to west looking values, top to bottom 
            (list of DIM int's)
        lookSN - south to north looking values, left to right 
            (list of DIM int's)
        lookWE - west to east looking values, top to bottom 
            (list of DIM int's)
        board - square grid of values 
            (list of list of int's, size DIM*DIM)
    """
    __slots__ = ('DIM', 'lookNS', 'lookEW', 'lookSN', 'lookWE', 'board')
    
    """The empty cell value"""
    EMPTY = 0   # can be referenced anywhere as: SkyscraperConfig.EMPTY
    
    def __init__(self, dim, lookNS, lookEW, lookSN, lookWE, board):
        """
        Constructor.
        """
        
        self.DIM = dim
        self.lookNS = lookNS
        self.lookEW = lookEW
        self.lookSN = lookSN
        self.lookWE = lookWE
        self.board = board
        
        # YOUR ARE FREE TO ADD ADDITIONAL FUNCTIONALITY HERE...
                    
    def __str__(self):
        """
        Return a string representation of the config.
        """
        
        # top row
        result = '  '
        for val in self.lookNS:
            result += str(val) + ' '
        result += '\n  ' + '-' * (self.DIM*2-1) + '\n'
            
        # board rows
        for row in range(self.DIM):
            result += str(self.lookWE[row]) + '|'
            for col in range(self.DIM):
                if self.board[row][col] == SkyscraperConfig.EMPTY:
                    result += '.'
                else:
                    result += str(str(self.board[row][col]))
                if col != self.DIM-1: result += ' '
            result += '|' + str(self.lookEW[row]) + '\n'
            
        # bottom row
        result += '  ' + '-' * (self.DIM*2-1) + '\n'
        result += '  '
        for val in self.lookSN:
            result += str(val) + ' '  
        result += '\n'
                  
        return result

def readBoard(filename):
    """
    Read the board file.  It is organized as follows:
        DIM     # square DIMension of board (1-9)
        lookNS   # DIM values (1-DIM) left to right
        lookEW   # DIM values (1-DIM) top to bottom
        lookSN   # DIM values (1-DIM) left to right
        lookWE   # DIM values (1-DIM) top to bottom
        row 1 values    # 0 for empty, (1-DIM) otherwise
        row 2 values    # 0 for empty, (1-DIM) otherwise
        ...
    
        filename: The file name (string)
    Returns: A config (SkyscraperConfig) containing the board info from file
    """
    
    f = open(filename)
    DIM = int(f.readline().strip())
    lookNS = [int(val) for val in f.readline().split()]
    lookEW = [int(val) for val in f.readline().split()]
    lookSN = [int(val) for val in f.readline().split()]
    lookWE = [int(val) for val in f.readline().split()]
    board = list()
    for _ in range(DIM):
        line = [int(val) for val in f.readline().split()]
        board.append(line)
    f.close()
    return SkyscraperConfig(DIM, lookNS, lookEW, lookSN, lookWE, board)
    
def isGoal(config):
    """
    Checks whether a config is a goal or not
        config: The config (SkyscraperConfig)
    Returns: True if config is a goal, False otherwise
    """
    for i in config.board:
        if i.count(0) != 0:
            return False
 
    return True
    
def getSuccessors(config):
    """
    Get the successors of config
        config: The config (SkyscraperConfig)
    Returns: A list of successor configs (list of SkyscraperConfig)
    """
    coord = ""
    successors = []
 
    for i in range(config.DIM):
        if coord != "":
            break
        for j in range(config.DIM):
            if config.board[i][j] == 0:
                coord = (i, j)
                break
                
    for k in range(1, config.DIM +1):
        c_copy = deepcopy(config)
        c_copy.board[coord[0]][coord[1]] = k 
        successors.append(c_copy)
                          
    return successors
        
def isValid(config):
    """
    Checks the config to see if it is valid
        config: The config (SkyscraperConfig)
    Returns: True if the config is valid, False otherwise
    """
    
    """Check for duplicates in columns (vertical)"""
    for c in range(config.DIM):
        vert = []
        for r in range(config.DIM):
            vert.append(config.board[r][c])
        for num in range(1,config.DIM+1):
            if vert.count(num) > 1:
                return False
        
    """Checking EW, WE, and duplicates in rows
     is all in one loop so its easier to understand"""
              
    for i in config.board:
        
        """Checks for duplicates in rows""" 
        for k in range(1,config.DIM+1):
            if i.count(k) > 1:
                return False
            
        """If there's zero's in the row we can't really
        tell if its valid or not. So just wait until the whole
        row is filled up"""    
        if i.count(0) > 0:
            continue
        
        """Checks WE"""
        max = i[0]
        count = 1
        for west in i:
            if west > max:
                max = west
                count += 1
        if count != config.lookWE[config.board.index(i)]:
            return False
        
        """Checks EW"""
        max = i[config.DIM-1]
        count = 1
        for x in range(config.DIM-1,-1,-1):
            if i[x] > max:
                max = i[x]
                count += 1
        if count != config.lookEW[config.board.index(i)]:
            return False
        
        
    """Now check NS and SN"""
    
    """Make the list like we do for EW and WE"""
    for col in range(config.DIM):
        lst = []
        
        """Check NS"""
        for row in range(config.DIM):
            lst.append(config.board[r][c])
        
        if lst.count(0) > 0:
            continue
        
        max = lst[0]
        count = 1
        for north in lst:
            if north > max:
                max = north
                count += 1
        if count != config.lookNS[col]:
            return False
        
        
        max = i[config.DIM-1]
        count += 1
        
        for south in range(len(lst)-1,-1,-1):
            if lst[south] > max:
                max = lst[south]
                count += 1
        if count != config.lookSN[col]:
            return False        
            
    
    
            
    
            
    return True
        
def solve(config, debug):
    """
    Generic backtracking solver.
        config: the current config (SkyscraperConfig)
        debug: print debug output? (Bool)
    Returns:  A config (SkyscraperConfig), if valid, None otherwise
    """
    
    if isGoal(config):
        return config
    else:
        if debug: print('Current:\n' + str(config))
        for successor in getSuccessors(config):
            if isValid(successor):
                if debug: print('Valid Successor:\n' + str(successor))
                solution = solve(successor, debug)
                if solution != None:
                    return solution
    
def main():
    """
    The main program.
        Usage: python3 skyscraper.py [filename debug]
            filename: The name of the board file
            debug: True or False for debug output
    """
    
    # if no command line arguments specified, prompt for the filename
    # and set debug output to False
    if len(argv) == 1:
        filename = input('Enter board file: ')
        debug = eval(input("Debug output (True or False): "))
    # otherwise, use the command line arguments
    elif len(argv) == 3:
        filename = argv[1]
        debug = eval(argv[2])
    # incorrect number of command line arguments
    else:
        print("Usage: python3 skyscraper.py [filename debug]")
        print("optional command line arguments:" )
        print("  filename: The name of the board file")
        print("  debug: True or False for debug output")
        return -1
        
    # read and display the initial board
    initConfig = readBoard(filename)
    print('Initial Config:\n' + str(initConfig))
    
    # solve the puzzle
    solution = solve(initConfig, debug)
    
    # display the solution, if one exists
    if solution != None:
        print('Solution:\n' + str(solution))
    else:
        print('No solution.')
    
if __name__ == '__main__':
    main()

# File: $Id: tents.asm,v 1.5 2013/02/12 18:33:21 exs6350 Exp exs6350 $
# Author: Ernesto Soltero
# 
# Description: The main program file that solves the 
#              tent puzzle. This reads in the file and 
#              prints out the initial puzzle and the 
#              final solved puzzle.
# Revisions: $Log: tents.asm,v $
# Revisions: Revision 1.5  2013/02/12 18:33:21  exs6350
# Revisions: added more comments.
# Revisions:
# Revisions: Revision 1.4  2013/02/10 06:17:28  exs6350
# Revisions: File input works correctlty tested.
# Revisions:
# Revisions: Revision 1.3  2013/02/08 05:25:10  exs6350
# Revisions: added error messages
# Revisions:
# Revisions: Revision 1.1  2013/02/08 04:21:45  exs6350
# Revisions: Initial revision
# Revisions:

#constants
READ_INT = 5
PRINT_STRING = 4
EXIT = 10

	.data
	.align 2

size_of_board:
	.word 0

tree_limit_col:    
	.space 48

tree_limit_row:
	.space 48

board:
	.space	144

num_trees:
	.word 0


	.text
	.align 2
	.globl main
	.globl print_banner
	.globl tree
	.globl print_board
	.globl print_error
	.globl dot
	.globl final_puzzle
	.globl newline
	.globl solve
	.globl impossible
	.globl initial_puzz
	
main:
	addi	$sp, $sp, -28
	sw	$s0, 0($sp)
	sw	$s1, 4($sp)
	sw	$s2, 8($sp)
	sw	$s3, 12($sp)
	sw	$s4, 16($sp)
	sw	$s5, 20($sp)
	sw	$ra, 24($sp)
	jal	print_banner
	jal	read
	jal	initial_puzz
	la	$a0, tree_limit_row
	la	$a3, tree_limit_col
	la	$a1, board
	la	$a2, size_of_board
	jal	print_board
	la	$t5, tree_limit_row
	la	$t6, tree_limit_col
	lw	$t2, 0($a2)
	li	$t3, 0
	li	$t4, 0
	

#If the limits are 0 then this is
#already a final solution so just
#return.
#############################################
check_board1:
	beq	$t2, $t3, check_board2		#check this
	lw	$t7, 0($t5)
	bne	$t7, $zero, continue
	addi	$t3, $t3, 1
	addi	$t5, $t5, 4
	j	check_board1

check_board2:
	beq	$t2, $t4, solve_first
	lw	$t7, 0($t6)
	bne	$t7, $zero, continue
	addi	$t4, $t4, 1
	addi	$t6, $t6, 4
	j	check_board2

solve_first:
	li	$v0, PRINT_STRING
	la	$a0, final_puzzle
	syscall
	la	$a0, tree_limit_row
	la	$a1, board
	la	$a2, size_of_board
	la	$a3, tree_limit_col
	jal	print_board
	li	$v0, EXIT
	syscall
############################################




#part of main if final board isnt initial board
############################################
continue:
#	jal	solve
	jal	impossible
	li	$v0, EXIT
	syscall

#############################################



#Reads in the board and initializes it
#
###########################################
read:
	la	$s0, board
	la	$s1, size_of_board
	la	$s2, tree_limit_col
	la	$s3, tree_limit_row
	li	$t2, 0
	li	$t3, 0
	li	$t4, 0
	li	$t5, 0
	li	$t6, 0
	li	$t7, 0
	li	$t8, 0
	li	$t9, 0

readfile:

	beq	$t2, $zero, make_size
	beq	$t3, $s1, initialize_board
	li	$v0, READ_INT
	syscall
	move 	$t5, $v0
	sw	$t5, 0($s2)
	slti	$t6, $t5, 0
	bne	$t6, $zero, error_2
	sgt	$t6, $t5, $s1
	bne	$t6, $zero, error_2
	addi	$s2, $s2, 4
	addi	$t3, $t3, 1
	j	readfile
	
make_size:

	li	$v0, READ_INT
	syscall
	slti	$t6, $v0, 2
	bne	$t6, $zero, error_1
	slti	$t6, $v0, 12
	beq	$t6, $zero, error_1
	sw	$v0, 0($s1)
	move	$t2, $v0
	move	$s1, $t2
	j	readfile

initialize_board:
	beq	$s1, $t9, col_limit
	beq	$s1, $t8, next_col
	la	$t6, dot
	lb	$t7, 0($t6)
	sb	$t7, 0($s0)
	addi	$s0, $s0, 1
	addi	$t8, $t8, 1
	j	initialize_board

next_col:
	li	$t8, 0
	addi	$t9, $t9, 1
	j	initialize_board

col_limit:

	beq	$s1, $t4, num_tree
	li	$v0, READ_INT
	syscall
	move	$t5, $v0
	sw	$t5, 0($s3)
	slti	$t6, $t5, 0
	bne	$t6, $zero, error_2
	sgt	$t6, $t5, $s1
	bne	$t6, $zero, error_2
	addi	$t4, $t4, 1
	addi	$s3, $s3, 4
	j	col_limit

num_tree:
	li	$v0, READ_INT
	syscall
	move	$t2, $zero
	la	$t2, num_trees
	slt	$t6, $v0, $zero
	bgtz	$t6, error_3
	sw	$v0, 0($t2)
	move	$t5, $v0
	move	$t3, $zero
	move	$t2, $zero
	move	$t4, $zero
	addi	$sp, $sp, -4
	sw	$ra, 0($sp)
	
read_tree:
	beq	$t5, $t3, done_read
	li	$v0, READ_INT
	syscall
	move	$a0, $v0
	slti	$t6, $a0, 0
	bne	$t6, $zero, error_4
	sge	$t6, $a0, $s1
	bne	$t6, $zero, error_4
	li	$v0, READ_INT
	syscall
	move	$a1, $v0
	slti	$t6, $a1, 0
	bne	$t6, $zero, error_4
	sge	$t6, $a1, $s1
	bne	$t6, $zero, error_4
	jal	formula
	la	$t8, tree
	lb	$t9, 0($t8)
	la	$t1, board
	add	$t1, $t1, $v0
	sb	$t9, 0($t1)
	addi	$t3, $t3, 1
	j	read_tree

done_read:
	lw	$ra, 0($sp)
	addi	$sp, $sp, 4
	jr	$ra		#retore the caller

################################################


#calculates the position to put the trees. This
#is also used to convert from row/col to an offset.
# a0 - row
# a1 - col
# v0 - result
################################################
formula:

	beq	$a0, $zero, return_col
	mult	$a0, $s1
	mflo	$t7
	add	$v0, $t7, $a1
	jr	$ra

return_col:
	move	$v0, $a1
	jr	$ra
#################################################

#invalid board size
error_1:
	li	$a0, 1
	j	print_error

#invalid tree limit 
error_2:
	li	$a0, 2
	j	print_error

#invalid num of trees 
error_3:
	li	$a0, 3
	j	print_error

#Illegal tree location
error_4:
	li	$a0, 4
	j	print_error

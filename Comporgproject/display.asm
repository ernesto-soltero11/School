# File: $Id: display.asm,v 1.3 2013/02/10 17:30:08 exs6350 Exp exs6350 $
# Author: Ernesto Soltero
#
# Description: All the functions used to print the board
#              and for debugging while creating the project.
#
#
# Revisions: $Log: display.asm,v $
# Revisions: Revision 1.3  2013/02/10 17:30:08  exs6350
# Revisions: Printing board works!
# Revisions:
# Revisions: Revision 1.2  2013/02/07 00:02:48  exs6350
# Revisions: Initial finish of printing board haven't tested.
# Revisions: No saved registers used need to add more comments and test.
# Revisions:
# Revisions: Revision 1.1  2013/01/26 21:01:22  exs6350
# Revisions: Initial revision
# Revisions:
# Revisions: Revision 1.1  2013/01/23 05:05:30  exs6350
# Revisions: Initial revision
# Revisions:

# syscall #constants 
PRINT_STRING = 4
PRINT_INT = 1
EXIT = 10
	.data

# the error messages
error1:
	.asciiz "Invalid board size, Tents terminating\n"

error2:
	.asciiz "Illegal sum value, Tents terminating\n"

error3:
	.asciiz "Illegal number of trees, Tents terminating\n"

error4:
	.asciiz "Illegal tree location, Tents terminating\n"

banner_line:
	.asciiz "******************\n"

middle_banner:
	.asciiz "**     TENTS    **\n"

initial_puzzle:
	.asciiz "Initial Puzzle\n"

final_puzzle:
	.asciiz "Final Puzzle\n\n"

unsolveable:
	.asciiz "Impossible Puzzle\n"

plus:
	.asciiz "+"

dash:
	.asciiz "-"

wall:
	.asciiz "| "

tree:
	.asciiz "T "

tent:
	.asciiz "A "

dot:
	.asciiz ". "

space:
	.asciiz " "
newline:
	.asciiz "\n"

	.text 
	.align 2

# Print the board in its current state. 
# a0 = The col limiti
# a1 = The address of the board
# a2 = The size of the board
# a3 = The row limit
print_board:	

	addi	$sp, $sp, -12
	sw	$s0, 0($sp)
	sw	$s1, 4($sp)
	sw	$ra, 8($sp)
	lw	$t9, 0($a2)
	move	$s1, $t9            #s1 size of board
	move	$s0, $a0
	li	$v0, PRINT_STRING
	la	$a0, plus	    #plus sign top
	syscall

	la	$a0, dash  	    #dash top
	li	$t9, 0		    #number of dashes
	li	$t8, 0		    #making make_middle 
	li	$t7, 0		    #making middle
	
make_dash_top:
	
	beq	$t9, $s1, last_top #Made all the dashes now print +
	syscall 
	syscall
	addi	$t9, $t9, 1
	j	make_dash_top

last_top:
	la	$a0, dash	   #The last odd dash
	syscall			   

	la	$a0, plus	   #Last plus
	syscall
	la	$a0, newline	   
	syscall
	move	$t9, $zero

make_middle:
	
	beq	$t7, $s1, bottom
	la	$a0, wall
	syscall
	move	$t6, $zero
	jal	middle
	la	$a0, wall
	syscall	
	li	$v0, PRINT_INT
	lw	$t0, 0($a3)
	move	$a0, $t0
	syscall
	addi	$a3, $a3, 4
	la	$a0, newline
	li	$v0, PRINT_STRING
	syscall
	addi	$t7, $t7, 1
	li	$t8, 0
	j	make_middle

middle:
	
	beq	$t8, $s1, done
	lb	$t5, 0($a1)
	la	$t4, dot
	lb	$t0, 0($t4)
	beq	$t5, $t0 , print_dot
	la	$t4,tent
	lb	$t0, 0($t4)
	beq	$t5, $t0, print_tent
	la	$t4, tree
	lb	$t0, 0($t4)
	beq	$t0, $t5, print_tree

done:
	jr	$ra

print_dot:
	la	$a0, dot
	syscall
	addi	$t8, $t8, 1
	addi	$a1, $a1, 1
	j	middle

print_tent:
	la	$a0, tent
	syscall
	addi	$t8, $t8, 1
	addi	$a1, $a1, 1
	j	middle

print_tree:
	la	$a0, tree
	syscall
	addi	$t8, $t8, 1
	addi	$a1, $a1, 1
	j	middle

bottom:
	la	$a0, plus
	move	$t9, $zero
	syscall

loop_bottom:
	beq	$t9, $s1, almost_done
	la 	$a0, dash
	syscall
	syscall
	addi	$t9, $t9, 1
	j	loop_bottom

almost_done:
	la	$a0, dash
	syscall
	la	$a0, plus
	syscall
	la	$a0, newline
	syscall
	move	$t8, $zero
	la	$a0, space
	syscall
	syscall
	li	$v0, PRINT_INT

last_line:
	beq	$t8, $s1, completetly_done
	lw	$t1, 0($s0)
	move	$a0, $t1
	syscall
	la	$a0, space
	li	$v0, PRINT_STRING
	syscall
	li	$v0, PRINT_INT
	addi	$s0, $s0, 4
	addi	$t8, $t8, 1
	j	last_line

completetly_done:
	la	$a0, newline
	li	$v0, PRINT_STRING
	syscall
	syscall
	lw	$s0, 0($sp)
	lw	$s1, 4($sp)
	lw	$ra, 8($sp)
	addi	$sp, $sp, 12
	jr	$ra

# Prints out at the start of the puzzle
print_banner:
	
	li	$v0, PRINT_STRING
	la	$a0, newline
	syscall
	la	$a0, banner_line
	syscall		                 #The first asterisk banner line

	la	$a0, middle_banner       #The banner text "Tents"
	syscall

	la	$a0, banner_line 
	syscall		                 #The second asterisk line
	la	$a0, newline
	li	$v0, PRINT_STRING
	syscall
	jr	$ra

initial_puzz:
	li	$v0, PRINT_STRING
	la	$a0, initial_puzzle
	syscall	
	la	$a0, newline
	syscall
	jr	$ra

# This function is called if the puzzle is not solvable
impossible:
	li	$v0, PRINT_STRING
	la	$a0, unsolveable
	syscall
	la	$a0, newline
	syscall
	li	$v0, EXIT
	syscall

# Print out error messages.
# a0- The value of the error message to print.
print_error:
	li	$t0, 1
	beq	$t0, $a0, print1
	li	$t0, 2
	beq	$t0, $a0, print2
	li	$t0, 3
	beq	$t0, $a0, print3
	li	$t0, 4
	beq	$t0, $a0, print4

# error message one
print1:
	la	$a0, error1
	li	$v0, PRINT_STRING
	syscall
	li	$v0, EXIT
	syscall

# error message two
print2:
	la	$a0, error2
	li	$v0, PRINT_STRING
	syscall
	li	$v0, EXIT
	syscall

# error message three
print3:
	la	$a0, error3
	li	$v0, PRINT_STRING
	syscall
	li	$v0, EXIT
	syscall

# error message four
print4:
	la	$a0, error4
	li	$v0, PRINT_STRING
	syscall
	li	$v0, EXIT
	syscall


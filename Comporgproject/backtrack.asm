# File: $Id: backtrack.asm,v 1.3 2013/02/13 04:03:43 exs6350 Exp exs6350 $
# Author: Ernesto Soltero
#
#
# Description: The backtracking algorithm that recursively
#              solves the board. Contains all the valid 
#              functions, solve algorithm, and make neighbors
#              functions.
#
# Revisions: $Log: backtrack.asm,v $
# Revisions: Revision 1.3  2013/02/13 04:03:43  exs6350
# Revisions: Finished with validation. Added more comments.
# Revisions: Still having trouble with successors.
# Revisions:
# Revisions: Revision 1.2  2013/02/12 18:33:41  exs6350
# Revisions: Redoing successors still need to do valid.
# Revisions:
# Revisions: Revision 1.1  2013/02/11 18:25:49  exs6350
# Revisions: Initial revision
# Revisions:

#constants
PRINT_STRING = 4
EXIT = 10

	.data
	.align 2

current_pos:
	.word  100:80

placed_pos:
	.word  100:576


	.text
	.align 2
	.globl	print_board
	.globl	tent
	.globl 	num_trees
	.globl  dot
	.globl	tree
	.globl	formula
	.globl	impossible
	.globl	final_puzzle

# a0- tree_limit_row<F9>
# a1 - board
# a2 - size of board
# a3 - tree_limit_col
#############################################
solve:
	addi	$sp, $sp, -36
	sw	$ra, 0($sp)
	sw	$s0, 4($sp)     #row of changed
	sw	$s1, 8($sp)     #col of changed
	sw	$s2, 12($sp)    
	sw	$s3, 16($sp)    
	sw	$s4, 20($sp)    
	sw	$s5, 24($sp)    
	sw	$s6, 28($sp)	
	sw	$s7, 32($sp)

	la	$t0, num_trees
	lw	$s2, 0($t0)
	move	$s4, $a1	#address of board
	lw	$s5, 0($a2)	#size of board
	move	$s6, $a0	#row limit
	move	$s7, $a3	#col limit
	mul	$s5, $s5
	mflo	$s3


	li	$t0, 0
	move	$t3, $s4
	addi	$sp, $sp, -4
	sw	$ra, 0($sp)
	jal	goal		#check if solution
	lw	$ra, 0($sp)
	addi	$sp, $sp, 4

	la	$t0, current_pos
	lw	$t1, 0($t0)	#start from last place checked
	addi	$t1, $t1, 1	#start from the next pos
	move	$t2, $s4
	add	$t2, $t2, $t1	#the starting pos in board
	li	$t0 , 0
	add	$t0, $t0, $t1	#our counter

#Puts down a tent at a valid spot around a
#tree. Saves were the tent was placed.
# t2 - the board
# t0 - the counter
############################################
successors:
	beq	$s5, $t0, no	#if we hit the last byte no solution
	la	$t3, tree
	lb	$t6, 0($t3)	#tree
	lb	$t5, 0($t2)	#whats at this pos
	beq	$t5, $t6, place_tent #No tree so place a tent
	addi	$t0, $t0, 1	#place a tent somewhere
	addi	$t2, $t2, 1
	j	successors

place_tent:
	div	$t0, $s5	#To find our current row/col
	mflo	$s0		#row of the tree we found
	mfhi	$s1		#col of the tree we found
	bne	$zero, $s0, top
	bne	$zero, $s1, left
	move	$t3, $s0
	addi	$t3, $t3, -1
	bne	$t3, $s1, right
	bne	$t3, $s0, bot
	
top:
	move	$t8, $s0	
	move	$t9, $s1
	addi	$t8, $t8, -1	#place to try tent
	addi	$sp, $sp, -4
	sw	$ra, 0($sp)
	li	$t1, 0
	li	$t4, 100
	la	$t3, placed_pos
	jal	already_visited	#see if we already visited
	lw	$ra, 0($sp)
	addi	$sp, $sp, 4
	bne	$v0, $zero, left
	move	$s0, $t8
	move	$s1, $t9
	addi	$sp, $sp, -4
	sw	$ra, 0($sp)
	jal	validate	#validate pos
	lw	$ra, 0($sp)
	addi	$sp, $sp, 4
	beq	$v0, $zero, next_recurse
	move	$s1, $t9
	addi	$t8, $t8, 1
	move	$s0, $t8
	j	left

left:
	beq	$s1, $zero, right
	move	$t8, $s0
	move	$t9, $s1
	addi	$t9, $t9, -1

right:
	move	$t3, $s5
	addi	$t3, $t3, -1
	beq	$t3, $s1, bot

bot:
	move	$t3, $s5
	addi	$t3, $t3, -1
	beq	$t3, $s1, place_tent

next_recurse:
	jal solve
	lw	$ra, 0($sp)
	lw	$s0, 4($sp)
	lw	$s1, 8($sp)
	lw	$s2, 12($sp)
	lw	$s3, 16($sp)
	lw	$s4, 20($sp)
	lw	$s5, 24($sp)
	lw	$s6, 28($sp)
	lw	$s7, 32($sp)
	addi	$sp, $sp, 36
	jr	$ra

#Checks to see if we have already tried a 
#tree here
#$t8 - row
#$t9 - col
already_visited:
	lw	$t5, 0($t3)
	beq	$t5, $t4, good 

good:
	jr	$ra

############################################


#Checks to see if it is a goal or solved 
#puzzle if not it returns and continues
############################################
goal:
	beq	$s2, $t0, solved
	beq	$s3, $t4, return_goal
	la	$t2, tent
	lb	$t5, 0($t2)		#t5 = tent
	lb	$t1, 0($t3)		#t3 is board
	beq	$t1, $t5, increment	#t1 is whats at that loc
	addi	$t4, $t4, 1
	addi	$t3, $t3, 1
	j	goal

increment:
	addi	$t0, $t0, 1
	addi	$t3, $t3, 1
	addi	$t4, $t4, 1
	j	goal

return_goal:
	jr	$ra

solved:
	li	$v0, PRINT_STRING
	la	$a0, final_puzzle
	syscall
	move	$a0, $s6
	move	$a1, $s4
	move	$a2, $s5
	move	$a3, $s7

	jal	print_board
	li	$v0, EXIT
	syscall
########################################


#Validates a tents position 
#$s0 - row of placed tent
#$s1 - col of placed tent
#$v0 - returns a 0 if valid
########################################

validate:
	move	$t8, $s4	#board
	mult	$s0, $s5	#check the row
	mflo	$t9
	add	$t8, $t8, $t9	#the start of the row
	li	$t7, 0
	li	$t1, 0		#num of trees
	move	$t6, $s6
	li	$t9, 4
	mult	$s0, $t9
	mflo	$t9
	add	$t6, $t6, $t9
	lw	$t5, 0($t6)	#the row limit

check_row:
	beq	$s5, $t7, check_col_setup
	la	$t9, tent
	lb	$t4, 0($t9)
	lb	$t5, 0($t8)
	beq	$t4, $t5, increment_row
	addi	$t7, $t7, 1
	addi	$t6, $t6, 1
	j	check_row

increment_row:
	addi	$t7, $t7, 1
	addi	$t6, $t6, 1
	addi	$t1, $t1, 1
	move	$t4, $t5
	addi	$t4, $t4, 1
	slt	$t3, $t1, $t4
	beq	$t3, $zero, not_valid
	j	check_row

check_col_setup:
	move	$t8, $s4
	li	$t9, 4
	mult	$s1, $t9
	mflo	$t9
	move	$t7, $s7	
	add	$t7, $t7, $t9
	lw	$t5, 0($t7)	#col limit
	li	$t1, 0
	li	$t3, 0 
	add	$t8, $t8, $s1	#start of col

check_col:
	beq	$s5, $t3, check_around_setup
	la	$t9, tent
	lb	$t7, 0($t9)	#tent
	lb	$t6, 0($t8)	
	beq	$t7, $t6, increment_col
	addi	$t1, $t1, 1
	addi	$t8, $t8, 1
	j	check_col

increment_col:
	addi	$t3, $t3, 1
	addi	$t1, $t1, 1
	addi	$t8, $t8, 1
	move	$t4, $t5
	addi	$t4, $t4, 1
	slt	$t6, $t3, $t4
	beq	$t6, $zero, not_valid
	j	check_col

check_around_setup:
	move	$a0, $s0
	move	$a1, $s1
	la	$t2, tent
	lb	$t3, 0($t2)
	addi	$sp, $sp, -4
	sw	$ra, 0($sp)
	jal	formula
	lw	$ra, 0($sp)
	addi	$sp, $sp, 4
	move	$t1, $v0
	beq	$t1, $zero, validate_topleft_corner
	move	$t2, $s5
	addi	$t2, $t2, -1
	beq	$t1, $t2, validate_topright_corner
	beq	$t1, $s3, validate_botright_corner 
	move	$t2, $s3
	sub	$t2, $t2, $s5
	addi	$t2, $t2, 1
	beq	$t1, $t2, validate_botleft_corner
	beq	$s0, $zero, validate_top_row
	beq	$s1, $zero, validate_left_col
	move	$t2, $s5
	addi	$t2, $t2, -1
	beq	$s0, $t2, validate_bot_row
	beq	$s1, $t2, validate_right_col
	j	validate_four

#col = size of board -1
validate_right_col:
	move	$t8, $s4
	add	$t8, $t8, $t1
	add	$t8, $t8, $s5
	lb	$t5, 0($t8)
	beq	$t5, $t3, not_valid
	addi	$t8, $t8, -1
	lb	$t5, 0($t8)
	beq	$t5, $t3, not_valid
	sub	$t8, $t8, $s5
	lb	$t5, 0($t8)
	beq	$t5, $t3, not_valid
	sub	$t8, $t8, $s5
	lb	$t5, 0($t8)
	beq	$t5, $t3, not_valid
	addi	$t8, $t8, 1
	lb	$t5, 0($t8)
	beq	$t5, $t3, not_valid
	j	valid

#row = size of board -1
validate_bot_row:
	move	$t8, $s4
	add	$t8, $t8, $t1
	addi	$t8, $t8, -1
	lb	$t5, 0($t8)
	beq	$t5, $t3, not_valid
	sub	$t8, $t8, $s5
	lb	$t5, 0($t8)
	beq	$t5, $t3, not_valid
	addi	$t8, $t8, 1
	lb	$t5, 0($t8)
	beq	$t5, $t3, not_valid
	addi	$t8, $t8, 1
	lb	$t5, 0($t8)
	beq	$t5, $t3, not_valid
	add	$t8, $t8, $s5
	lb	$t5, 0($t8)
	beq	$t5, $t3, not_valid
	j	valid

#col == 0
validate_left_col:
	move	$t8, $s4
	sub	$t8, $t8, $s5
	lb	$t5, 0($t8)
	beq	$t5, $t3, not_valid
	addi	$t8, $t8, 1
	lb	$t5, 0($t8)
	beq	$t5, $t3, not_valid
	add	$t8, $t8, $s5
	lb	$t5, 0($t8)
	beq	$t5, $t3, not_valid
	add	$t8, $t8, $s5
	lb	$t5, 0($t8)
	beq	$t5, $t3, not_valid
	addi	$t8, $t8, -1
	lb	$t5, 0($t8)
	beq	$t5, $t3, not_valid
	j	valid

#row == 0
validate_top_row:
	move	$t8, $s4
	add	$t8, $t8, $s1
	addi	$t8, $t8, -1
	lb	$t5, 0($t8)
	beq	$t5, $t3, not_valid
	addi	$t8, $t8, 2
	lb	$t5, 0($t8)
	beq	$t5, $t3, not_valid
	add	$t8, $t8, $s5
	lb	$t5, 0($t8)
	beq	$t5, $t3, not_valid
	addi	$t8, $t8, -1
	lb	$t5, 0($t8)
	beq	$t5, $t3, not_valid
	addi	$t8, $t8, -1
	lb	$t5, 0($t8)
	beq	$t5, $t3, not_valid
	j	valid

#(0,0)
validate_topleft_corner:
	move	$t8, $s4
	addi	$t8, $t8, 1	#right side
	lb	$t5, 0($t8)
	beq	$t5, $t3, not_valid
	move	$t8, $s4
	add	$t8, $t8, $s5	#bottom
	lb	$t5, 0($sp)
	beq	$t5, $t3, not_valid
	j	valid

#(0, size of board-1)
validate_topright_corner:
	move	$t8, $s4
	move	$t4, $s5
	addi	$t4, $t4, -2	#left of top right
	add	$t8, $t8, $t4
	lb	$t5, 0($t8)
	beq	$t5, $t3, not_valid
	add	$t4, $s5, $s1	#bottom of top right
	lb	$t5, 0($t8)
	beq	$t5, $t3, not_valid
	j	valid

#(size of board -1, 0)
validate_botleft_corner:
	move	$t8, $s4
	add	$t8, $t8, $s3
	sub	$t8, $t8, $s5
	addi	$t8, $t8, 2	#right of bot left
	lb	$t5, 0($t8)
	beq	$t5, $t3, not_valid
	sub	$t8, $t8, $s5
	addi	$t8, $t8, -1
	lb	$t5, 0($t8)
	beq	$t5, $t3, not_valid
	j	valid

#(size of board -1, size of board -1)
validate_botright_corner:
	move	$t8, $s4
	add	$t8, $t8, $s3	#top of bot right
	sub	$t8, $t8, $s5
	lb	$t5, 0($t8)
	beq	$t5, $t3, not_valid
	move	$t8, $s4
	add	$t8, $t8, $s3
	addi	$t8, $t8, -1
	lb	$t5, 0($t8)
	beq	$t5, $t3, not_valid
	j	valid
	
#validates 8 squares around pos	
validate_four:

	#checking top
	move	$t8, $s4
	move	$a0, $s0
	move	$a1, $s1
	addi	$a0, $a0, -1	#to get (row-1, col)
	addi	$sp, $sp, -4
	sw	$ra, 0($sp)
	jal	formula	
	lw	$ra, 0($sp)
	addi	$sp, $sp, 4
	add	$t8, $t8, $v0
	lb	$t5, 0($t8)
	beq	$t5, $t3, not_valid

	#checking bottom
	move	$t8, $s4
	move	$a0, $s0
	move	$a1, $s1
	addi	$a0, $a0, 1
	addi	$sp, $sp, -4
	sw	$ra, 0($sp)
	jal	formula
	lw	$ra, 0($sp)
	addi	$sp, $sp, 4
	add	$t8, $t8, $v0
	lb	$t5, 0($t8)
	beq	$t5, $t3, not_valid

	#checking left
	move	$t8, $s4
	move	$a0, $s0
	move	$a1, $s1
	addi	$a1, $a1, -1
	addi	$sp, $sp, -4
	sw	$ra, 0($sp)
	jal	formula	
	lw	$ra, 0($sp)
	addi	$sp, $sp, 4
	add	$t8, $t8, $v0
	lb	$t5, 0($t8)
	beq	$t5, $t3, not_valid

	#check right
	move	$t8, $s4
	move	$a0, $s0
	move	$a1, $s1
	addi	$a1, $a1, 1
	addi	$sp, $sp, -4
	sw	$ra, 0($sp)
	jal	formula
	lw	$ra, 0($sp)
	addi	$sp, $sp, 4
	add	$t8, $t8, $v0
	lb	$t5, 0($t8)
	beq	$t5, $t3, not_valid

	#diagonal topright
	move	$t8, $s4
	move	$a0, $s0
	move	$a1, $s1
	addi	$a0, $a0, -1
	addi	$a1, $a1, 1
	addi	$sp, $sp, -4
	sw	$ra, 0($sp)
	jal	formula
	lw	$ra, 0($sp)
	addi	$sp, $sp, 4
	add	$t8, $t8, $v0
	lb	$t5, 0($t8)
	beq	$t5, $t3, not_valid

	#diagonal top left
	move	$t8, $s4
	move	$a0, $s0
	move	$a1, $s1
	addi	$a1, $a1, -1
	addi	$a0, $a0, -1
	addi	$sp, $sp, -4
	sw	$ra, 0($sp)
	jal	formula	
	lw	$ra, 0($sp)
	addi	$sp, $sp, 4
	add	$t8, $t8, $v0
	lb	$t5, 0($t8)
	beq	$t5, $t3, not_valid

	#diagonal bot left
	move	$t8, $s4
	move	$a0, $s0
	move	$a1, $s1
	addi	$a0, $a0, 1
	addi	$a1, $a1, -1
	addi	$sp, $sp, -4
	sw	$ra, 0($sp)
	jal	formula
	lw	$ra, 0($sp)
	addi	$sp, $sp, 4
	add	$t8, $t8, $v0
	lb	$t5, 0($t8)
	beq	$t5, $t3, not_valid

	#diagonal bot right
	move	$t8, $s4
	move	$a0, $s0
	move	$a1, $s1
	addi	$a0, $a0, 1
	addi	$a1, $a1, 1
	addi	$sp, $sp, -4
	sw	$ra, 0($sp)
	jal	formula
	lw	$ra, 0($sp)
	addi	$sp, $sp, 4
	add	$t8, $t8, $v0
	lb	$t5, 0($t8)
	beq	$t5, $t3, not_valid
	
	j	valid

valid:
	li	$v0, 0
	jr $ra

not_valid:
	li	$v0, 1
	jr	$ra
########################################
no:
	j	impossible
	



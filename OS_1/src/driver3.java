// File:	driver3.java
// Author:	W. R. Carithers
// SCCS id:	%W%        %G%
// Contributors:
// Description:	Driver program for project 1
//
//	30 baboons, each takes 2 trips across the canyon
//

import java.util.Random;

public class driver3 {

    /**
     * Prevent construction
     */
    private driver3() { }

    /**
     * Some controlling constants
     */

    static int N_BABOONS = 30;		// how many baboons to start?
    static int N_TRIPS = 2;		// how many trips will each make?
    static int DELAY_MAX = 2000;	// maximum delay before trips (usec)
    static int TRIP_TIME = 500;		// trip time (usec)

    /**
     * Direction information
     */

    public enum Direction {
        East, West
    }

    /**
     * Shared globals
     */

    static Random random;
    static RopeController rope;

    /**
     * A Runnable object that simulates the behavior of one baboon.
     *
     * NOTE:  all baboons are fundamentally the same.  The only
     * differences between two baboons are the direction of
     * travel and the various delays between trips.
     *
     * Each baboon will attempt to cross the canyon several times,
     * in alternating directions.  Before each crossing, the baboon
     * will delay for a random length of time.
     *
     * Each crossing takes TRIP_TIME usec.
     */

    private static class Baboon implements Runnable {
        Direction dir;
        int n;
        int num;

        public Baboon( int i ) {
	    num = i;
	    // initial direction:  odd, E->W; even, W->E
	    if( num % 2 == 0 ) {
	        dir = Direction.East;
	    } else {
	        dir = Direction.West;
	    }
	}


	public void run() {

	    // start crossing
	    while( n < N_TRIPS ) {
	        int delay;

	        // delay for a random length of time before crossing
		try {
		    Thread.sleep( random.nextInt(DELAY_MAX-1) + 1 );
		}
		catch( InterruptedException ecx ) {
		}

		// attempt to cross
		if( dir == Direction.East ) {
		    rope.crossEast( num );
		    try {
		        Thread.sleep( TRIP_TIME );
		    }
		    catch( InterruptedException ecx ) {
		    }
		    rope.doneEast( num );
		    dir = Direction.West;
		} else {
		    rope.crossWest( num );
		    try {
		        Thread.sleep( TRIP_TIME );
		    }
		    catch( InterruptedException ecx ) {
		    }
		    rope.doneWest( num );
		    dir = Direction.East;
		}

	        ++n;	// finished one more crossing
	    }

	} 

    }


    /**
     * Main program.
     */

    public static void main(String[] args ) throws Throwable {
	Thread[] ids = new Thread [ N_BABOONS ];
	int i;
	long seed;

	rope = new RopeController();

	// determine the seed for the random number generator
	if( args.length != 0 ) {
	    seed = Integer.parseInt( args[0] );
	} else {
	    seed = System.currentTimeMillis() / 1000;
	}

	// create the generator and seed it
	random = new Random( seed );

	// fire up the threads
	for( i = 0; i < N_BABOONS; ++i ) {
		ids[i] = new Thread( new Baboon(i) );
		ids[i].start();
	}

	// wait for the threads to terminate before exiting
	for( i = 0; i < N_BABOONS; ++i ) {
		ids[i].join();
	}

    }

}

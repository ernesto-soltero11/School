These files contain the following things:

	header.mak-*	for use with 'gmakemake' to create a Makefile
			(language-specific for C and C++)

	driver*.c	C driver programs for your solution
	driver*.cpp	C++ driver programs for your solution
	driver*.java	Java driver programs for your solution

	out.1.41	example output from driver1 with seed 41

I recommend that you delete the driver* source files for the languages
you are not using.

The various driver programs differ only in the number of baboon threads
the start and how many trips across the canyon each baboon makes.
The current settings are:

	driver1		20 baboons, 5 trips
	driver2		15 baboons, 3 trips
	driver3		30 baboons, 2 trips
	driver4		6 baboons, 25 trips

Depending on the number of baboons and the number of trips each makes,
these driver programs will run for varying lengths of time; for example,
'driver1' runs for approximately 50 seconds each time it is executed.

Each driver program can be invoked with an integer on the command line,
which will be used as the seed for the random number generator used by
the threads to calculate their delay between trips.  If no integer is
given, the current system time will be used as the seed.

NOTE: you may expect to see differences in your output when you compare
it to the sample output.  This is due to variations in the order in
which blocked threads are awakened, as semaphore blocked queues are not
guaranteed to be FIFO in their operation.

For those working in Java, remember that your cross*() and done*()
routines must be part of a class named RopeController.

For those working in C or C++, use the appropriate provided header.mak-*
file to generate a Makefile.  This will ensure that your code is compiled
with the proper options and linked against the POSIX wrapper library.

If you are working in C, either copy the "header.mak-c" file to
"header.mak" before running 'gmakemake > Makefile', or run 'gmakemake'
as 'gmakemake -header header.mak-c > Makefile'

If you are working in C++, either copy the "header.mak-cpp" file to
"header.mak" before running 'gmakemake > Makefile', or run 'gmakemake'
as 'gmakemake -header header.mak-cpp > Makefile'

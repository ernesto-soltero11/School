/**
 * @author Ernesto Soltero (exs6350@rit.edu)
 *
 */
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.Semaphore;
import java.util.concurrent.atomic.AtomicInteger;

public class RopeController {
	private static final int MAX_CROSS = 3;
	private final static Semaphore guard = new Semaphore(MAX_CROSS, true);
	private final static Semaphore oneDirection = new Semaphore(1, true);
	private ConcurrentLinkedQueue<Baboon_Wrapper> crossQueue = new ConcurrentLinkedQueue();
	private enum Direction{EAST, WEST};
	private Direction currentDir;
	private AtomicInteger East = new AtomicInteger();
	private AtomicInteger West = new AtomicInteger();
	private AtomicInteger onRope = new AtomicInteger();
	public RopeController(){}
	
	/**
	 * Just a wrapper class that helps determine the 
	 * direction of the baboons in the queue
	 * @author Ernesto Soltero (exs6350@rit.edu)
	 *
	 */
	private class Baboon_Wrapper{
		protected Direction direction;
		protected int n;
		
		/**
		 * 
		 * @param n - The id of the baboon
		 * @param dir - The direction of the baboon
		 */
		public Baboon_Wrapper(int n, Direction dir){
			this.direction = dir;
			this.n = n;
		}
	}
	
	/**
	 * A baboon calls this when he wants to cross East
	 * @param n - The id of the baboon
	 */
	public void crossEast(int n){
		crossQueue.add(new Baboon_Wrapper(n, Direction.EAST));
		East.getAndIncrement();
		if((guard.availablePermits() == 0 || currentDir != Direction.EAST) && currentDir != null){
			System.out.format("Baboon %d waiting - %d baboon(s) waiting to cross East %n", n, East.get());
			while(guard.availablePermits() == 0){}
		}
		try {
			guard.acquire();
		} catch (InterruptedException e1) {}
		Baboon_Wrapper baboon = crossQueue.remove();
		if(currentDir == baboon.direction){
			East.getAndDecrement();
			onRope.getAndIncrement();
			System.out.format("Baboon %d now crossing East %n", n);
		}else{
			try {
				oneDirection.acquire();
			} catch (InterruptedException e) {}
			currentDir = Direction.EAST;
			while(onRope.get() != 0){}
			System.out.format("Baboon %d now crossing East %n", n);
			East.getAndDecrement();
			onRope.getAndIncrement();
			oneDirection.release();
		}
	}
	
	/**
	 * A baboon calls this when he is done crossing.
	 * @param n - The id of the baboon
	 */
	public void doneEast(int n){
		System.out.format("Baboon %d finished crossing East %n", n);
		onRope.getAndDecrement();
		guard.release();
	}
	
	/**
	 * A baboon calls this when he wants to cross west
	 * @param n - The id of the baboon
	 */
	public void crossWest(int n) {
		crossQueue.add(new Baboon_Wrapper(n, Direction.WEST));
		West.getAndIncrement();
		if((guard.availablePermits() == 0 || currentDir != Direction.WEST) && currentDir != null){
			System.out.format("Baboon %d waiting - %d baboon(s) waiting to cross West %n", n, West.get());
			while(guard.availablePermits() == 0){}
		}
		try {
			guard.acquire();
		} catch (InterruptedException e1) {}
		Baboon_Wrapper baboon = crossQueue.remove();
		if(currentDir == baboon.direction){
			West.getAndDecrement();
			onRope.getAndIncrement();
			System.out.format("Baboon %d now crossing West %n", n);
		}else{
			try {
				oneDirection.acquire();
			} catch (InterruptedException e) {}
			currentDir = Direction.WEST;
			while(onRope.get() != 0){}
			System.out.format("Baboon %d now crossing West %n", n);
			West.getAndDecrement();
			onRope.getAndIncrement();
			oneDirection.release();
		}
	}
	
	/**
	 * A baboon calls this when he is done crossing west
	 * @param n - The id of the baboon
	 */
	public void doneWest(int n){
		System.out.format("Baboon %d finished crossing West %n", n);
		onRope.getAndDecrement();
		guard.release();
	}
}

"""
Author; Jasmine Phan
Created: January 2012
"""

def see(index,seesay):
    count = 0
    if index == 0:
        return seesay
    else:
        see = seesay[0]
        seesay = list(seesay)
        for i in seesay:
            if i == see:
                count += 1
            elif i != see:
                break
        seesay = "".join(seesay)
        seesay = str(count) + str(see)
        return seesay + see(index-1, seesay[count+1:])

def play_seeNsay():
    index = input("Enter a index: ")
    print("--------------------------")
    print("Index                Value")
    print("--------------------------")
    for i in range((int(index)+1)):
        print(i,"                   ",see(i,'0'))
      
play_seeNsay()
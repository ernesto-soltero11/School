import java.util.Collection;


/**
 * The MultiKeyMap map interface that allows other classes to extend this
 * interface to implement a multikey collection of items.
 * @author Ernesto Soltero (exs6350@rit.edu)
 * @param <K1> - 
 * @param <K2> - 
 * @param <V> - 
 */
public interface MultiKeyMap<K1, K2, V> {

	/**
	 * Removes all the mappings from this map.
	 */
	public void clear();
	
	/**
	 * Returns true if the map contains the key(s) specified.
	 * @param key1 - The key to search for.
	 * @param key2 - The key to search for.
	 * @return Returns true if the mapping contains both key(s) 
	 */
	public boolean containsKey(K1 key1, K2 key2);
	
	/**
	 * Returns true if the map contains the value.
	 * @param value - The value to search for in the mapping.
	 * @return Returns true if 
	 */
	public boolean containsValue(Object value);
	
	/**
	 * Compares the object to this map for equality.
	 * @param o - The object to compare the map to.
	 * @return Returns true if this object is equal to the map.
	 */
	public boolean equals(Object o);
	
	/**
	 * Returns the value stored in the mapped to these two keys. Keys can be 
	 * entered in any order.
	 * @param key1 - One of the keys mapped to the value
	 * @param key2 - The other key mapped to the value
	 * @return The value v mapped to these keys
	 */
	public V get(K1 key1, K2 key2);
	
	/**
	 * Returns the hash code value for this multikeymap.
	 * @return The hash code value
	 */
	public int hashCode();
	
	/**
	 * Returns true if this Multikeymap is empty.
	 * @return True if this map is empty.
	 */
	public boolean isEmpty();
	
	/**
	 * Inserts the value V into the Multikeymap associating the value V with the 
	 * two keys.
	 * @param key1 - The first key to associate the value with.
	 * @param key2 - The second key to associate the value with.
	 * @param value - The value to insert into the map
	 * @return The value that was inserted into the map.
	 */
	public V put(K1 key1, K2 key2, V value);
	
	/**
	 * Copies all the mappings from the inputed map into this map.
	 * @param map - The map to copy the values from.
	 */
	public void putAll(MultiKeyMap<? extends K1, ? extends K2, ? extends V> map);
	
	/**
	 * Removes the object from this map
	 * @param key1 - The first key to look for.
	 * @param key2 - The second key to look for.
	 * @return The value V that is removed from the mapping.
	 */
	public V remove(Object key1, Object key2);
	
	/**
	 * Returns the key-value mappings in the map.
	 * @return The number of key value mappings in the map.
	 */
	public int size();
	
	/**
	 * Returns a <code>Collection</code> view of the values contained in this map.
	 * @return
	 */
	public Collection<V> values();
	
	/**
	 * 
	 * @author Ernesto Soltero (exs6350@rit.edu)
	 *
	 * @param <K1>
	 * @param <K2>
	 * @param <V>
	 */
	static interface MutliKeyEntry<K1, K2, V>{
		
		/**
		 * 
		 * @param o
		 * @return
		 */
		public boolean equals(Object o);
		
		/**
		 * 
		 * @return
		 */
		public K1 getKeys();
		
		/**
		 * 
		 * @return
		 */
		public V getValue();
		
		/**
		 * 
		 * @return
		 */
		public int hashCode();
		
		/**
		 * 
		 * @param value
		 * @return
		 */
		public V setValue(V value);
	}
}

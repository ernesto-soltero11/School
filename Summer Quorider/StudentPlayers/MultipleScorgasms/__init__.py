"""
Quoridor player
 
Author: Adam Oest
        Ernesto Soltero (exs6350@rit.edu)
Date: June, 2013
"""

# If you get this error checked for in the code that follows
# this comment, either run quoridor.py or
# create a new file in the same directory as quoridor.py,
# in which you import your module and call your own functions
# for testing purposes
if __name__ == "__main__":
    import sys
    sys.stderr.write("You cannot run this file on its own.")
    sys.exit()

# Imports the player move class, board size constant, and utility classes
from Model.interface import PlayerMove, BOARD_DIM
from .playerData import PlayerData
from .boardTile import boardTile
from .Stack import *
from .myQueue import *

def init(logger, playerId, numWalls, playerHomes):
    """
        Part 1 - 4
    
        The engine calls this function once at the beginning of the game.
        The student player module uses this function to initialize its data
        structures to the initial game state.

        Parameters
            logger: a reference to the logger object. The player model uses
                logger.write(msg) and logger.error(msg) to log diagnostic
                information.
                
            playerId: this player's number, from 1 to 4
        
            numWalls: the number of walls each player is given initially
            
            playerHomes: An ordered tuple of player locations
                         A location is a 2-element tuple of (row,column).
                         If any player has already been eliminated from
                         the game (rare), there will be a bool False in
                         that player's spot in the tuple instead of a
                         location.
                    
        returns:
            a PlayerData object containing all of this player module's data
    """
    board = []
    for x in range(BOARD_DIM):
        for y in range(BOARD_DIM):
            board.append(boardTile(x,y))
            
    #make the graph connected with all the neighbors        
    for node in board:
        index = index_form(node.Coord[0], node.Coord[1])
        #Connects the four corner neighbors
        if node.Coord == (0,0):
            node.Neighbors.append(board[index + 1])
            node.Neighbors.append(board[index + 9])
        elif node.Coord == (0,8):
            node.Neighbors.append(board[index - 1])
            node.Neighbors.append(board[index + 9])
        elif node.Coord == (8,0):
            node.Neighbors.append(board[index - 9])
            node.Neighbors.append(board[index + 1])
        elif node.Coord == (8,8):
            node.Neighbors.append(board[index - 9])
            node.Neighbors.append(board[index - 1])
        
        #connects the four sides
        elif node.Coord[0] == 0:
            node.Neighbors.append(board[index - 1])
            node.Neighbors.append(board[index + 1])
            node.Neighbors.append(board[index + 9])
        elif node.Coord[0] == 8:
            node.Neighbors.append(board[index - 1])
            node.Neighbors.append(board[index + 1])
            node.Neighbors.append(board[index - 9])
        elif node.Coord[1] == 0:
            node.Neighbors.append(board[index - 9])
            node.Neighbors.append(board[index + 9])
            node.Neighbors.append(board[index + 1])
        elif node.Coord[1] == 8:
            node.Neighbors.append(board[index - 9])
            node.Neighbors.append(board[index + 9])
            node.Neighbors.append(board[index - 1])
            
        #else the tile has four neighbors
        else:
            node.Neighbors.append(board[index + 1])
            node.Neighbors.append(board[index - 1])
            node.Neighbors.append(board[index + 9])
            node.Neighbors.append(board[index - 9])
            
    playerData = PlayerData(logger, playerId, list(playerHomes), board, numWalls)
    
    return playerData

def index_form(row, col):
    if row == 0:
        return col
    else:
        return (row * 8) + col 

def last_move(playerData, move):
    """
        Parts 1 - 4
    
        The engine calls this function after any player module, including this one,
        makes a valid move in the game.
        
        The engine also calls this function repeatedly at the start of the game if
        there have been some moves specified in the configuration file's PRE_MOVE
        variable.

        The student player module updates its data structure with the information
        about the move.

        Parameters
            playerData: this player's data, originally built by this
                        module in init()
        
            move: the instance of PlayerMove that describes the move just made
        
        returns:
            this player module's updated (playerData) data structure
    """
    
    # Update your playerData object with the latest move.
    # Remember that this code is called even for your own moves.
    
    if move.playerId == playerData.playerId:  #this is us
        if move.move == False: #we put down a wall
            playerData.numWalls -= 1
    else:
        if move.move == False:
            pass
        else:
            playerData.playerLocations.remove((move.r1, move.c1))
            playerData.playerLocations.append((move.r2, move.c2))
    
    return playerData

def get_neighbors(playerData, r, c):
    """
        Part 1
    
        This function is used only in part 1 mode. The engine calls it after
        all PRE_MOVEs have been made. (See the config.cfg file.)

        Parameters
            playerData: this player's data, originally built by this
                        module in init()
            r: row coordinate of starting position for this player's piece
            c: column coordinate of starting position for this player's piece
        
        returns:
            a list of coordinate pairs (a list of lists, e.g. [[0,0], [0,2]],
            not a list of tuples) denoting all the reachable neighboring squares
            from the given coordinate. "Neighboring" means exactly one move
            away.
    """  
    lst = []
    
    for node in playerData.board[index_form(r, c)].Neighbors:
        lst.append(node.Coord)
        
    return lst

def get_shortest_path(playerData, r1, c1, r2, c2):
    """
        Part 1
    
        This function is only called in part 1 mode. The engine calls it when
        a shortest path is requested by the user via the graphical interface.

        Parameters
            playerData: this player's data, originally built by this
                        module in init()
            r1: row coordinate of starting position
            c1: column coordinate of starting position
            r2: row coordinate of destination position
            c2: column coordinate of destination position
        
        returns:
            a sequence of coordinate tuples that form the shortest path
            from the starting position to the destination, inclusive.
            The format is a tuple or list of coordinate pairs (row,column)
            ordered from starting position to destination position.
            An example would be [(0,0), (0,1), (1,1)].
            If there is no path, an empty list, [], should be returned.
    """
    queue = Queue()
    source = (r1, c1)
    destination = (r2, c2)
    enqueue(source, queue)
    
    pre = {}
    pre[source] = None
    
    while not emptyQueue(queue):
        current = front(queue)
        dequeue(queue)
        if current == destination:
            break
        for neighbor in get_neighbors(playerData, current[0], current[1]):
            if neighbor not in pre:
                pre[neighbor] = current
                enqueue(neighbor, queue)
    
    stack = Stack()
    if destination in pre:
        tmp = destination
        while tmp != source:
            push(tmp, stack)
            tmp = pre[tmp]
        push(source, stack)
        
    path = []
    while not emptyStack(stack):
        path.append(top(Stack))
        pop(stack)
    return path

def move(playerData):
    """
        Parts 2 - 4
    
        The engine calls this function at each moment when it is this
        player's turn to make a move. This function decides what kind of
        move, wall placement or piece move, to make.
        
        Parameters
            playerData: this player's data, originally built by this
                        module in init()
        
        returns:
            the move chosen, in the form of an instance of PlayerMove
    """
    
    # This function is called when it's your turn to move
        
    # Here you'll figure out what kind of move to make and then return that
    # move. We recommend that you don't update your data structures here,
    # but rather in last_move. If you do it here, you'll need a special case
    # check in last_move to make sure you don't update your data structures
    # twice.
        
    # In part 3, any legal move is acceptable. In part 4, you will want to
    # implement a strategy
    
    # Placeholder, fill in these values yourself
    move = PlayerMove(-1, False, -1, -1, -1, -1)
    
    return move

def player_invalidated(playerData, playerId):
    """
        Part 3 - 4
    
        The engine calls this function when another player has made
        an invalid move or has raised an exception ("crashed").
        
        Parameters
            playerData: this player's data, originally built by this
                        module in init()
            playerId: the ID of the player being invalidated
        
        returns:
            this player's updated playerData
    """
    
    # Update your player data to reflect the invalidation.
    # FYI, the player's piece is removed from the board,
    # but not its walls.
    
    # When you are working on part 4, there is a small chance you'd
    # want to change your strategy when a player is kicked out.
    
    return playerData


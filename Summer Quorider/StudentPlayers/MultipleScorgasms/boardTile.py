"""
This is a node that represents the board tiles in 
the game graph.

Author: Ernesto Soltero (exs6350@rit.edu)
"""

class boardTile():
    """
        This class represents one node in the graph.
        Coord - Represents the coordinate position of 
        the node in the grid.
        Neighbors - The neighboring nodes that this node
        is connected to.
    """
    __slots__ = ('Coord', 'Neighbors')
    
    def init(self, x, y):
        self.Coord = [x, y]
        self.Neighbors = []
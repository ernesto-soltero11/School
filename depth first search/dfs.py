class Node(object):
    __slots__ = ('name', 'neighbors')
   
    def __init__(self, name):
        self.name = name
        self.neighbors = []
    
    def __str__(self):
        """name: neighbor1, neighbor2, ...."""
        result = self.name + ": "
        for neighbor in self.neighbors:
            result += neighbor.name + ", "
        return result
        
"""
def mkNode(name):
    node = Node()
    node.name = name
    node.neighbors = []
    return node
"""

def loadGraph(filename):
    graph = {}
    for line in open(filename):
        contents = line.split()
        if contents[0] not in graph:
            n1 = Node(contents[0])
            graph[contents[0]] = n1
        else:
            n1 = graph[contents[0]]

        if contents[1] not in graph:
            n2 = Node(contents[1])
            graph[contents[1]] = n2
        else:
            n2 = graph[contents[1]]

        if n2 not in n1.neighbors:
            n1.neighbors.append( n2 )
        if n1 not in n2.neighbors:
            n2.neighbors.append( n1 )
    return graph

def printGraph(graph):
    for name in graph:
        print(graph[name])
        
def visitDFS(node, visited, finish):
    if node == finish:
        return
    else:
        for neighbor in node.neighbors:
            if neighbor not in visited:
                visited.append(neighbor)
                visitDFS(neighbor, visited, finish)
        
def canReachDFS(start, finish):
    visited = [start]
    visitDFS(start, visited, finish)
    return finish in visited

def buildPathDFS(node, visited, finish):
    if node == finish:
        return [node]
    else:
        for neighbor in node.neighbors:
            if neighbor not in visited:
                visited.append(neighbor)
                path = buildPathDFS(neighbor, visited, finish)
                if path != None:
                    return [node] + path
        
def printPathDFS(start, finish):
    visited = [start]
    path = buildPathDFS(start, visited, finish)
    if path == None:
        print("No path found!")
    else:
        for node in path:
            print(node.name + " ")

def main():
    file = input("Enter graph file: ")
    graph = loadGraph(file)
    printGraph(graph)
    
    # get start and finish from the user
    startName = input('Enter starting node name: ')
    if startName not in graph:
        raise ValueError(startName + ' not in graph!')
    start = graph[startName]
   
    finishName = input('Enter finish node name: ')
    if finishName not in graph:
        raise ValueError(finishName + ' not in graph!')
    finish = graph[finishName]
    
    found = canReachDFS(start, finish)
    print(found)
    
    printPathDFS(start, finish)

      
"""
    nodeA = Node('A')  
    nodeB = Node('B')
    nodeC = Node('C')
    nodeA.neighbors.append(nodeB)
    nodeB.neighbors.append(nodeA)
    nodeA.neighbors.append(nodeC)
    nodeC.neighbors.append(nodeA)
    #print(nodeA)
    #print(nodeB)
    #print(nodeC)
    
    graph = {}
    graph["A"] = nodeA
    graph["B"] = nodeB
    graph["C"] = nodeC
    printGraph(graph)
    
    nodeA = mkNode('A')
    nodeB = mkNode('B')
"""

main()
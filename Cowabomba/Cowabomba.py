"""Ernesto Soltero
   CS2 SI class
   File: Cowabomba.py
   Description: Explode the bombs to get the max amount of cows
   bombs can explode other bombs too dfs search."""
   
from math import sqrt,pow   

class Cow():
    __slots__ = ("name","x","y")
       
    def __init__(self, name, x , y):
        self.name = name
        self.x = x
        self.y = y
            
class Bomb():
    __slots__ = ("name","x","y","radius","neighbors","max")
        
    def __init__(self, name, x, y, radius):
        self.name = name
        self.x = x
        self.y = y
        self.radius = radius
        self.neighbors = dict()
        self.max = 0
            
    def __str__(self):
        for key in self.neighbors:
            print(self.name + ": " + self.neighbors[key].name)
                
def makeField(file):
    """Makes the graph for the cow and bomb field"""
    lst = []
    for line in open(file):
        line = line.split()
        if line[0] == "cow":
            lst.append(Cow(line[1],int(line[2]),int(line[3])))
        else:
            lst.append(Bomb(line[1],int(line[2]),int(line[3]),int(line[4])))
    for start in range(len(lst)):
        if isinstance(lst[start],Cow): continue
        for cur in lst:
            if lst[start].name == cur.name: continue
            elif sqrt( pow((cur.x - (lst[start].x) ), 2) + pow((cur.y - (lst[start].y)),2 )) <= lst[start].radius:
                lst[start].neighbors[cur.name] = cur
    return lst
   
   
def search(bomb, visited,max):
    for key in bomb.neighbors:
        if key not in visited:
            visited.append(key)
            if isinstance(bomb.neighbors[key], Cow):
                print(bomb.name + " bomb paints " + key)
                max +=1
                continue
            elif isinstance(bomb.neighbors[key], Bomb):
                print(bomb.name + " bomb sets off " + key +" bomb")
                return search(bomb.neighbors[key],visited,max)
    return max    
            
    
def main():
    max = 0
    name = ""
    file = input("Enter the file name: ")
    for i in makeField(file):
        visited = []
        if isinstance(i, Cow):
            continue
        print("Triggering " + i.name + " bomb...")
        curNum = search(i,visited,0)
        if curNum > max:
            max = curNum
            name = i.name 
    print("Triggering the " + name + " bomb paints the largest number of cows: " + str(max))

if __name__ == "__main__":
        main()  
/// File: $Id: SymbolTable.cpp,v 1.2 2012/10/09 01:03:29 exs6350 Exp exs6350 $
/// Author: Ernesto Soltero
/// Description: Saves the declared keywords with its associated
/// value of int. A keyword is a string and a value is an int.

/// Revisions: $Log: SymbolTable.cpp,v $
/// Revisions: Revision 1.2  2012/10/09 01:03:29  exs6350
/// Revisions: Finished works correctly
/// Revisions:
/// Revisions: Revision 1.1  2012/10/08 02:08:33  exs6350
/// Revisions: Initial revision
/// Revisions:

#include "SymbolTable.h"
#include <string>
#include <iostream>
#include <map>

using namespace RITCS4;
using namespace std;

/// Creates an instance of a SymbolTable whose size is 0.
SymbolTable::SymbolTable():tab(){
}

/// Inserts only the name of the variable into the map and initializes it 
/// to 0.
void SymbolTable::declare(string name, int init){
	
	if(contains(name) == true){
	cout << "The element already exists!" << endl;
	}
	else{
		tab[name] = init;;
	}
}

/// Changes the value of the keyword.
void SymbolTable::set(string name, int val){
	if(contains(name) == false){
		cout << "The key is not in the map!" << endl;
	}
	else{
		tab[name] = val;
	}
}

/// Gets the value of the varaible.
int SymbolTable::get(string name) const{
	if(contains(name) == false){
		cout << "That variable is not in the map" << endl;
		return 0;
	}
	else if(tab.find(name)->second == 0){
	cout << "The variable was not initialized to anything." << endl;
	}
	return tab.find(name)->second;
}

/// Returns the size of the current size of the map
unsigned int SymbolTable::size()const{
	return tab.size();
}

/// Searches the map to see if the value is contained in the map
bool SymbolTable::contains(string name)const{
	if(tab.count(name) == 0){
		return true;
	}
	
	return false;
}

/// Displays the keys and values that map is currently holding.
void SymbolTable::dump(ostream &out)const {
	for (map<string,int>::const_iterator iter = tab.begin();iter != tab.end();iter++){
		out << iter->first << " : " << iter->second << endl;
	}
}

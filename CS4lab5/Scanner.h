// Scanner.h
// Author: James Heliotis
// Contributors: {}

// Description: Scans *and*interprets* tiny language programs

#ifndef RITCS4_SCANNER_H
#define RITCS4_SCANNER_H

#include <iostream>
#include <cstdlib>
#include "SymbolTable.h"

namespace RITCS4 {

/// Scanner reads and interprets TL 'Tiny Language' programs.
///
class Scanner {

public: 

	/// constructor
	///
	/// Initialize the scanner to an "empty" state.
	///
	Scanner();

	/// readAndInterpret
	///
	/// Read a Tiny Language (TL) program from the given file
	/// and execute it.
	/// Arguments:	inFile:	the source program
	/// Pre:	inFile is open (not checkable for all types of streams)
	///
	void readAndInterpret( std::istream &inFile );

	/// postMortem
	///
	/// Dump out the final values of all the variables in
	/// this format:
	///	<empty line>
	///	Post-Mortem:
	///	============
	///	<content of a SymbolTable dump>
	/// Pre:	readAndInterpret has been called
	///
	void postMortem();

private: // storage

	/// the table holds the symbols of the interpreted program.
	SymbolTable table;

	/// class constant keyword to declare a variable
	static const std::string Declare;

	/// class constant keyword to assign value to a variable
	static const std::string Set;

	/// class constant keyword to print a variable
	static const std::string Print;

	/// class constant keyword signalling the end of the program
	static const std::string End;

}; // Scanner

} // namespace

#endif

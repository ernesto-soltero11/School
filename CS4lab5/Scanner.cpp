// $Id: Scanner.cpp,v 1.1 2012/10/08 02:08:33 exs6350 Exp exs6350 $
// Author: James Heliotis
// Contributors: Ernesto Soltero

// Description: Scans *and*interprets* tiny language programs

// Revisions:
//	$Log: Scanner.cpp,v $
//	Revision 1.1  2012/10/08 02:08:33  exs6350
//	Initial revision
//
//	Revision 1.2  2012/09/24 02:19:39  bks
//	code cleanup (docs repeating .h file removed)
//
//	Revision 1.1  2012/09/24 01:50:45  bks
//	Initial revision
//
//

#include <iostream>
#include "Scanner.h"
#include <ctype.h>
#include <cstdlib>
#include <map>
#include "SymbolTable.h"

using namespace std;

namespace RITCS4 {

// class constant definitions

const string Scanner::Declare( "declare" );
const string Scanner::Set( "set" );
const string Scanner::Print( "print" );
const string Scanner::End( "end" );

// constructor
//
Scanner::Scanner(): table() {
}

// readAndInterpret
//
void Scanner::readAndInterpret( istream &inFile ) {

	string token;
	string variable;
	int number;

	inFile >> token;
	while( !inFile.eof() && token != End ) {
		if ( token == Declare ) {
			inFile >> variable;
			table.declare( variable );
		}
		else if ( token == Set ) {
			inFile >> variable;
			inFile >> number;
			if(isalpha(number) == true){
				cerr << "Illegal integer value." << endl;
				cerr << "Exiting." << endl;
				exit(1);
			}
			if(table.contains(variable) == false){
				cerr << "Variable " << variable <<  " has not been declared." << endl;
			}
			table.set( variable, number );
		}
		else if(token == Print){
			inFile >> variable;
			if(table.contains(variable) == false){
			cerr << "Variable " << variable << " has not been declared." << endl;
			continue;
			}
			cout << table.get(variable) << endl;
		}
		else if(token == End){
			postMortem();
			exit(0);
		}
		else {
			if(table.contains(token) == false && token != End || token != Print || token != Set || token != Declare){
				cerr << "Illegal token: " << token << endl;
			}
		} 

		inFile >> token;
		if(token == "\n"){
			cerr << "Premature ending of file." << endl;
			postMortem();
		}
	}
}

// postMortem
//
void Scanner::postMortem() {
	cout << endl;
	cout << "Post-Mortem:";
	cout << endl;
	cout << "============";
	cout << endl;
	cout << endl;
	table.dump( cout );
}

} // namespace

// $Id: SymbolTable.h,v 1.2 2012/10/09 01:03:29 exs6350 Exp exs6350 $
// Author: James Heliotis
// Contributors: {ben k steele}
// Description: A symbol table whose symbols are integer variables

// Revisions:
//	$Log: SymbolTable.h,v $
//	Revision 1.2  2012/10/09 01:03:29  exs6350
//	Finished works correctly
//
//	Revision 1.1  2012/10/08 02:08:33  exs6350
//	Initial revision
//
//	Revision 1.2  2012/09/24 02:07:32  bks
//	format revisions
//
//	Revision 1.1  2012/09/24 01:50:45  bks
//	Initial revision
//
//
//

#ifndef RITCS4_SYMBOLTABLE
#define RITCS4_SYMBOLTABLE

#include <iostream>
#include <string>
#include <map>

namespace RITCS4 {

/// SymbolTable implements the variable data storage for a TL 'Tiny Language'
/// program.
///
class SymbolTable {

public:

	/// constructor
	///
	/// Description:	creates an empty symbol table
	/// Post:	size() is 0
	///
	SymbolTable();

public: // element operations

	/// declare
	///
	
	/// Arguments:	name: variable name
	///		init: initial integer value
	/// Pre:		!contains( name )
	/// Post:	contains( name )
	/// Post:	variable named by 'name' has the value of 'init'
	///
	void declare( std::string name, int init = 0 );

	/// set
	///
	/// Description:	Change the value of a variable in the table
	/// Arguments:	name: variable name
	/// 		val: new integer value
	/// Pre:		contains( name )
	/// Post:	contains( name )
	/// Post:	variable named by 'name' has the value of 'val'
	///
	void set( std::string name, int val );

	/// get
	///
	/// Returns:	The value of the variable
	/// Argument:	name: variable name
	/// Pre:		contains( name )
	/// Post:	table is unchanged
	///
	int get( std::string name ) const;

public: // table queries

	/// size
	///
	/// Returns:	the number of variables currently in the table
	/// Post:	returned value >= 0
	///
	unsigned int size() const;

	/// contains
	///
	/// Returns:	true iff a variable with given name is in the table
	/// Argument:	name: name of variable being sought
	/// Post:	table is unchanged
	///
	bool contains( std::string name ) const;

	/// dump
	///
	/// Description: display the contents of the table on the given
	///		output stream. Each line contains one variable
	///		listing of the form "<variable>: <value>"
	///		The listing is sorted by variable name.
	/// Arguments:	out:	where the dump will be written
	/// Post:	table is unchanged
	void dump( std::ostream &out )const ;

private: // storage
	
	/// I choose to use the map data structure because it is the easiest 
	/// to use because of its mapping of a value to a key. Since you are
	/// declaring a keyword (key) that will only correspond to one value
	/// (value) it makes sense to use map as a data structure.

	std::map<std::string,int> tab;

}; // SymbolTable

} // RITCS4 namespace

#endif

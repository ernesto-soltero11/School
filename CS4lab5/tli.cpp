// $Id: tli.cpp,v 1.2 2012/09/24 02:19:39 bks Exp $
// tli.cpp
// Author: James Heliotis

#include <cstdlib>
#include "Scanner.h"

using namespace RITCS4;

/// Description: Execute programs written in the TL "Tiny Language".
///
int main() {
	Scanner scanner;
	scanner.readAndInterpret( std::cin );
	scanner.postMortem();

	return EXIT_SUCCESS;
}


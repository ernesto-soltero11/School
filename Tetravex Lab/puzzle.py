"""
Author: Ernesto Soltero(exs6350@rit.edu)
File: puzzle.py
Created: January 2012
"""
from copy import deepcopy

class tile(object):
    """Tile class that holds each configuration.
    Precondition: File has been split.
    Postcondition: Returns filled tile class."""
    __slots__ = ('nums','row','col')
    
    def __init__(self,nums,row,col):
    
        self.nums = nums
        self.row = row
        self.col = col
        
    def __str__(self):
        result = str(self.nums) #+ ' pos: ' + str(self.row) + str(self.col) 
        return result
    
class config(object):
    """Holds the data for the grid.
    Precondition: None
    Postcondition: Initializes the config class."""
    
    __slots__ = ('placement', 'tiles', 'current', 'emptypos','board')
    
    def __init__(self):
        
        self.board = [[0 for row in range(3)]for col in range(3)]
        self.placement = []
        self.tiles = []
        self.current = 0
        self.emptypos = [(0,0),(0,1),(0,2),(1,0),(1,1),(1,2),(2,0),(2,1),(2,2)]
        
    def __str__(self):
        result = '<' + str(self.placement) + str(self.tiles) + str(self.current) + str(self.emptypos) + '>'
        return result
    
def printConfig(config):
    if config == False:
        print ("No solution")
    else:
        plc = config.board
        for row in range(3):
            rowText = ' '
            for col in range(3):
                if plc(row,col):
                    rowText = rowText + '[][][][]'
                else:
                    rowText = rowText + '[]' + north(plc(row,col)) + '[][]'
                print(rowText)
                rowText = ' '
                for col in range(3):
                    if plc(row,col) is None:
                        rowText = rowText + '[][][][]'
                    else:
                        rowText = rowText + west(plc(row,col)) + '[]' + east(plc(row,col)) + '[]'
                print(rowText)
                rowText = ' '
                for col in range(3):
                    if plc(row,col) is None:
                        rowText = rowText + '[][][][]'
                    else:
                        rowText = rowText + '[]' + south(plc(row,col)) + '[][]'
                    print(rowText)
                    
def isGoal(config):
    """Checks to see if the board is complete and valid.
    Precondition: Takes in a initialized config.
    Postcondition: Returns a boolean."""
    print(config)
    if len(config.emptypos) == 0:
        return True
    else:
        return False

def successors(config):
    """Creates a list of successors whether valid or not.
    Precondition: Config file is updated with tiles.
    Postcondition: Returns a list of possible tile placements."""
    neighbors = []
    if config.current >= len(config.tiles):
        return neighbors
    else:
        for row in range(3):
            for col in range(3):
                tile = config.tiles[config.current]
                boardcopy = deepcopy(config.board)
                tile.row = row
                tile.col = col
                if boardcopy[row][col] == 0:
                    boardcopy[row][col] = tile
                if isvalid(boardcopy,tile) == True:
                    neighbors.append(boardcopy)
        config.current += 1
        config.emptypos.remove((tile.row,tile.col))
        config.placement = boardcopy
    return neighbors
    
                
    
def isvalid(boardcopy,tile):
    """Checks to see if the configurations created are valid. If no it takes them out of the list.
    Precondition: List filled with configurations.
    Postcondition: Returns list with only valid configurations."""
    valid = [] 
    if tile.row - 1 >= 0:
        if boardcopy[tile.row-1][tile.col] ==  0 or boardcopy[tile.row-1][tile.col].nums[0] == tile.nums[1]:
            valid += "True"
        else:
            valid += "False"
    if tile.row + 1 <= 2:      
        if boardcopy[tile.row + 1][tile.col] == 0 :
            valid += "True"
        else:
            if boardcopy[tile.row + 1][tile.col].nums[1] == tile.nums[0]:
                valid+="True"
            else:
                valid += "False"
    if tile.col + 1 <= 2:
        if boardcopy[tile.row][tile.col + 1] == 0 or boardcopy[tile.row][tile.col + 1].nums[3] == tile.nums[2]:
            valid += "True"
        else:
            valid += "False"
    if tile.col - 1 >= 0:
        if boardcopy[tile.row][tile.col -1] == 0 or boardcopy[tile.row][tile.col -1].nums[2] == tile.nums[3]:
            valid += "True"
        else:
            valid += "False"
    if "False" in valid == True:
        return False
    else:
        return True
                    
def solve(config):
    """Solver for the board.
    Precondition: Takes a initialized config.
    Postcondition: Returns a solved board."""
    
    if isGoal(config):
        return config
    else:
        for child in successors(config):
            solution = solve(child)
            if solution != False:
                return solution 
        return False

def readfile(config,file):
    """Splits up the number into seperate tile classes.
    Precondition: Takes a file.
    Postcondition: Returns a list of tiles."""
    result = []
    for line in open(file):
        line = line.split()
        result.append(tile(line,0,0))
    config.tiles = result
    return config
             
def main():
    """Runs the main program and solves the board.
    Precondition: None
    Postcondition: Returns a solved board."""
    configuration = config()
    configuration = readfile(configuration,(input('Enter the file name: ')))
    configuration.tiles = tuple(configuration.tiles)
    print("The tile configurations are: ")
#    for i in configuration.tiles:
#        print (i)
    solved = solve(configuration)
    printConfig(solved)
        
main()
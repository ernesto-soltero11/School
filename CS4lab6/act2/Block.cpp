/// File: $Id: Block.cpp,v 1.1 2012/10/11 12:41:00 exs6350 Exp exs6350 $
/// Author: Ernesto Soltero\
/// 
/// Revisions: $Log: Block.cpp,v $
/// Revisions: Revision 1.1  2012/10/11 12:41:00  exs6350
/// Revisions: Initial revision
/// Revisions:
/// Description: Creates a block at a certain position

#include "Block.h"
#include <string>
#include <iostream>

using namespace RITCSFigures;
using namespace std;

/// Constructor for the block uses base class constructor also
Block::Block(int r, int c, Display &d, int h, int w):Figure(r,c,d){
	height = h;
	width = w;
}

/// Returns the name of the class
string Block:: what_am_i() const{
	return "Block";
}

/// Draws the object on the screen.
void Block::display(char ch){
	int index,index2;

	for(index = 0; index < height; index++){
		get_canvas().set_cur_pos((get_row() + index),
		(get_column()));

		for(index2 = 0; index2 < width; index2++){
			get_canvas().put(ch);
		}
	}
}

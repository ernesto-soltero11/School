// File:          $Id: Student.cpp,v 1.1 2010/08/18 16:08:40 cs4 Exp $
// Author:        Jeremiah D. Brazeau
// Contributors:
// Description:   
// Revisions:     $Log: Student.cpp,v $
// Revisions:     Revision 1.1  2010/08/18 16:08:40  cs4
// Revisions:     Initial revision
// Revisions:
// Revisions:     Revision 1.1  2003/04/03 17:49:40  cs4
// Revisions:     Initial revision
// Revisions:

#include <cassert>

#include "Student.h"

using namespace std;

Student::Student( int howTall ):
  name( "someName" ), height( howTall ){

  assert( height >= 0 );

}


Student::Student() :
  name("noName"), height( -1 ) {	// dummy invalid values
}


Student::~Student(){
}


string Student::getName() const {
  return name;
}

int Student::getHeight() const {
  return height;
}

Student& Student::operator=(const Student &other) {
	if (this != &other) {
		name = other.name;
		height = other.height;
	}
}

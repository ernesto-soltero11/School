"""
A Heap implemented by 
mapping a tree onto an array (Python list) of the same size.
file: array_heapsort.py
language: python3
author: Matthew Fluet
author: James Heliotis
author: Arthur Nunes-Harwitt

new language feature: passing (and storing) functions as arguments.
"""

# Utility functions to map tree relations to array indices ###
from math import sqrt
from time import clock

def parent(i):
    """
       Return index of parent of node at index i.
    """
    return (i - 1)//2  

def lChild(i):
    """
       Return index of left child of node at index i.
    """
    return 2*i + 1

def rChild(i):
    """
       Return index of right child of node at index i.
    """
    return 2*i + 2

############################################################################

class Heap(object):
    """
       A heap inside an array that may be bigger than the
       heapified section of said array
       SLOTS:
           array: the Python list object used to store the heap
           size: the number of array elements currently in the
                 heap. (size-1) is the index of the last element.
           compareFunc: A function to compare values in the heap.
                  For example, if compareFunc performs less-than,
                  then the heap will be a min-heap.
    """
    __slots__ = ('array', 'size', 'compareFunc')

    def __init__(self, maxSize, compareFunc):
        """
           Create an empty heap with capacity maxSize
           and comparison function compareFunc.
        """
        self.array = [None] * maxSize
        self.size = 0
        self.compareFunc = compareFunc

    def __str__(self):
        return str(self.size) + ": " + str(self.array)

def displayHeap(heap, startIndex=0, indent=""):
    """
       displayHeap : Heap * NatNum * String -> NoneType 
       Display the heap as a tree with each child value indented
       from its parent value. Traverse the tree in preorder.
    """
    if startIndex < heap.size:
        print(indent + str(heap.array[startIndex]))
        displayHeap(heap, lChild(startIndex), indent + '    ')
        displayHeap(heap, rChild(startIndex), indent + '    ')

def siftUp(heap, startIndex):
    """
       siftUp : Heap * NatNum -> NoneType 
       Move the value at startIndex up to its proper spot in
       the given heap. Assume that the value does not have
       to move down.
    """
    i = startIndex
    a = heap.array
    while i > 0 and not heap.compareFunc(a[parent(i)], a[i]):
        (a[parent(i)], a[i]) = (a[i], a[parent(i)])     # swap
        i = parent(i)

def _first_of_3(heap, index):
    """
    _first_of_3 : Heap * NatNum -> NatNum 
    _first_of_3 is a private, utility function.
       Look at the values at:
       - index
       - the left child position of index, if in the heap
       - the right child position of index, if in the heap
       and return the index of the value that should come
       first, according to heap.compareFunc().
    """
    lt = lChild(index)
    rt = rChild(index)
    thisVal = heap.array[index]
    if rt < heap.size:        # If there are both left and right children
        lVal = heap.array[lt]
        rVal = heap.array[rt]
        if heap.compareFunc(lVal, thisVal)    \
        or heap.compareFunc(rVal, thisVal):
            if heap.compareFunc(lVal, rVal):
                return lt # The left child goes first
            else:
                return rt # The right child goes first
        else:
                return index # This one goes first
    elif lt < heap.size: # If there is only a left child
        lVal = heap.array[lt]
        if heap.compareFunc(lVal, thisVal):
            return lt # The left child goes first
        else:
            return index # This one goes first
    else: # There are no children
        return index

def siftDown(heap, startIndex):
    """
       siftDown : Heap * NatNum -> NoneType 
       Move the value at startIndex down to its proper spot in
       the given heap. Assume that the value does not have
       to move up.
    """
    curIndex = startIndex
    a = heap.array
    swapIndex = _first_of_3(heap, curIndex)
    while (swapIndex != curIndex):
        (a[swapIndex], a[curIndex]) = (a[curIndex], a[swapIndex]) # swap
        curIndex = swapIndex
        swapIndex = _first_of_3(heap, curIndex)

def add(heap, newValue):
    """
       add : Heap * Comparable -> NoneType
       add inserts the element at the correct position in the heap.
    """
    if heap.size == len(heap.array):
        heap.array = heap.array + ([None] * len(heap.array))
    heap.array[heap.size] = newValue
    siftUp(heap, heap.size)
    heap.size = heap.size + 1

def removeMin(heap):
    """
       removeMin : Heap -> Comparable
       removeMin removes and returns the minimum element in the heap.
    """
    res = heap.array[0]
    heap.size = heap.size - 1
    heap.array[0] = heap.array[heap.size]
    heap.array[heap.size] = None
    siftDown(heap, 0)
    return res

def updateValue(heap, index, newValue):
    """
       Fix the heap after changing the value in one of its nodes.
    """
    oldValue = heap.array[index]
    heap.array[index] = newValue
    if heap.compareFunc(newValue, oldValue):
        siftUp(heap, index)
    else:
        siftDown(heap, index)

def less(a, b):
    """
       less : Comparable * Comparable -> Boolean
       This ordering function returns True if the first value is smaller.
    """
    return a.distance <= b.distance

def greater(a, b):
    """
       greater : Comparable * Comparable -> Boolean
       This ordering function returns True if the first value is larger.
    """
    return a.distance >= b.distance

############################################################################

class balloon():
    __slots__ = ("color", "x", "y", "z")
    def __init__(self, color, x, y, z):
        self.color = color
        self.x = x
        self.y = y
        self.z = z

class distance():
    __slots__ = ("distance", "ball1", "ball2")
    def __init__(self, distance, ball1, ball2):
        self.distance = distance
        self.ball1 = ball1
        self.ball2 = ball2

def readFile(file): # Read the file and make the distances in one.
    heap = ""
    List = []      
    for line in open(file):
        line = line.split()
        if len(line) == 1:
            heap = Heap(int(line[0]), less)
        else:
            fill = balloon(str(line[0]), int(line[1]), int(line[2]), int(line[3]))
            List.append(fill)
    for i in List:                  
        save = distance(10000,i,None)   # You only want to make distances for the first balloon to pop so we keep a max value
        for j in List:                  # and replace it when the distance is less
            if i.color == j.color:
                continue
            dist = int(sqrt(abs(i.x-j.x)**2 + abs(i.y-j.y)**2 + abs(i.z-j.z)**2))
            if dist < save.distance:    # Replace the second balloon when the distance is less
                save.distance = dist
                save.ball2 = j
        add(heap, save)
    return heap

#def makeDistances(List, Heap):
#    for i in List:
#        for j in List:
#            if i.color == j.color:
#                continue
#            else:
#                dist = sqrt((i.x - j.x)**2 + (i.y-j.y)**2 + (i.z - j.z)**2)
#                save = distance(int(dist), i, j)
#                add(Heap, save)
#    print(Heap.size)
#    return Heap


def main():
    file = input("Enter a file:  ")
    print("Finding the magicalz balloon.")
    t = clock() #This just tell us how long it takes to run just to make sure its not n^3
    heap = readFile(file)
    popped = {}

    for sec in range(1000):
        if heap.size == 0 or heap.size == 1:
            break
        while(True): # While the distance is the same as sec keep popping balloons
            if heap.size == 0:
                break
            if heap.array[0].distance != sec:
                    break
            elif heap.array[0].distance == sec:
                take = removeMin(heap)
                if take.ball1.color not in popped and take.ball2.color not in popped:
                    popped[take.ball1.color] = take.ball1.color
                    popped[take.ball2.color] = take.ball2.color
                    continue
                if take.ball1.color not in popped and take.ball2.color in popped:
                    popped[take.ball1.color] = take.ball1.color
                    continue
                elif take.ball2.color not in popped and take.ball1.color in popped:
                    popped[take.ball2.color] = take.ball2.color
                    continue
    if heap.size == 0:
        print("NO balloons left!")
    elif heap.array[0].ball1 not in popped:
        print(heap.array[0].ball1.color + " is left.")
    else:
        print(heap.array[0].ball2.color + " is left.")
    time = str(clock() - t)
    print("This took " + time + " seconds.")

if __name__ == "__main__":
    main()
              
            
            
            
            
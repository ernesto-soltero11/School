"""
Ernesto Soleftero
Steele SI
Turightle and functions
"""

from turtle import *

def init():
    #speed("slowest")
    penup()
    home()
    
def X():
    setheading(270)
    pendown()
    left(45)
    forward(40)
    backward(80)
    penup()
    left(45)
    forward(56)
    pendown()
    right(135)
    forward(80)
    backward(40)
    setheading(270)
    penup()
    
def O():
    setheading(270)
    forward(40)
    left(90)
    pendown()
    circle(40)
    setheading(270)
    penup()
    
def Board():
    pendown()
    backward(100)
    forward(300)
    penup()
    setposition(-100,-100)
    pendown()
    forward(300)
    penup()
    setposition(100,100)
    setheading(270)
    pendown()
    forward(300)
    penup()
    setposition(0,100)
    pendown()
    forward(300)
    
def main():
    #speed("slowest")
    Board()
    init()
    setposition(-50,-150)
    X()
    setposition(-50,50)
    O()
    setposition(50,50)
    X()
    setposition(150,-50)
    O()
    setposition(50,-50)
    X()
    setposition(150,-150)
    O()
    setposition(50,-150)
    X()
    hideturtle()
    input("Press Enter to end the program")
    bye()
    
main()
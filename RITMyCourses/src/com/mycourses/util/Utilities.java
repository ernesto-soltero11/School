package com.mycourses.util;

import java.io.IOException;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

/**
 * @author Ernesto Soltero (exs6350@rit.edu)
 *
 */
public class Utilities {
	
	/**
	 * 
	 * @param url - The url to get.
	 * @return - A document of the url
	 */
	public static Document getDoc(String url){
		try {
			return Jsoup.connect(url).get();
		} catch (IOException e) {
			// TODO Log the errors
			
		}
		return null;
	}
}

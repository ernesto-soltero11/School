package com.mycourses.util;

import java.io.IOException;
import java.util.logging.*;

/**
 * @author Ernesto Soltero (exs6350@rit.edu)
 *
 */
public class ErrorLogger {
	
	private static Logger log = null;
	private static FileHandler file;
	private static SimpleFormatter format;
	
	/**
	 * Singleton pattern this class is the global logger.
	 */
	private ErrorLogger(){
		try {
			file = new FileHandler("Errorlog.txt");
			format = new SimpleFormatter();
			file.setFormatter(format);
			log.addHandler(file);
		} catch (IOException e) {
			//error making file
		}
		
	}
	
	/**
	 * 
	 * @return A single instance of the logger
	 */
	public static synchronized Logger getInstance(){
		if(log == null){
			log = Logger.getLogger(Logger.GLOBAL_LOGGER_NAME);
		}
		return log;
	}
}

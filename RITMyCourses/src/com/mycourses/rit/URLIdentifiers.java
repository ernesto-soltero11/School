package com.mycourses.rit;

/**
 * @author Ernesto Soltero (exs6350@rit.edu)
 * Global class that contains the URL's to the different sections in MyCourses.
 */
public class URLIdentifiers {
	
	private static URLIdentifiers urlId = null;
	
	public static String BASE = "https://mycourses.rit.edu";
	public static String HOME = "https://mycourses.rit.edu/d2l/home";
	public static String CONTENT;
	public static String CALENDAR;
	public static String CLASSLIST;
	public static String GROUPS;
	public static String DROPBOX;
	public static String DISCUSSIONS;
	public static String CHAT;
	public static String SURVEYS;
	public static String QUIZZES;
	public static String GRADES;
	
	/**
	 * Singleton pattern this class is only meant to hold the url's as they are used globally.
	 */
	private URLIdentifiers(){}
	
	public static synchronized URLIdentifiers getInstance(){
		if(urlId == null){
			urlId = new URLIdentifiers();
		}
		return urlId;
	}
	
	public void setContent(String url){CONTENT = BASE+url;}
	
	public void setCalendar(String url){CALENDAR = BASE+url;}
	
	public void setClasslist(String url){CLASSLIST = BASE+url;}
	
	public void setGroups(String url){GROUPS = BASE+url;}
	
	public void setDropbox(String url){DROPBOX = BASE+url;}
	
	public void setDiscussions(String url){DISCUSSIONS = BASE+url;}
	
	public void setChat(String url){CHAT = BASE+url;}
	
	public void setSurveys(String url){SURVEYS = BASE+url;}
	
	public void setQuizzesr(String url){QUIZZES = BASE+url;}
	
	public void setGrades(String url){GRADES = BASE+url;}
}

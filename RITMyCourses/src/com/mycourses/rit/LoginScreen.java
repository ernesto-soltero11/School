package com.mycourses.rit;

import java.io.IOException;
import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;

import com.mycourses.util.Utilities;

/**
 * 
 * @author Ernesto Soltero (exs6350@rit.edu)
 * Main login screen
 */
public class LoginScreen extends Activity {

	private EditText username;
	private EditText password;
	private Button login_button;
	
	private Document doc;
	
	/**
	 * 
	 */
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.login);
	}
	
	
	/*
	 * Attempts to authenticate the user
	 */
	private void Login(View view){
		doc = Utilities.getDoc(URLIdentifiers.BASE);
		Element user = doc.select("input[type=text]").first();
		Element pw = doc.select("input[type=password]").first();
	}

}

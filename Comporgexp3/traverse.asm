# File:		traverse_tree.asm
# Author:	K. Reek
# Contributors:	P. White,
#		W. Carithers,
#		<<<Ernesto Soltero>>>
#
# Description:	Binary tree traversal functions.
#
# Revisions:	$Log$


# CONSTANTS
#

# traversal codes
PRE_ORDER  = 0
IN_ORDER   = 1
POST_ORDER = 2

	.text			# this is program code
	.align 2		# instructions must be on word boundaries

#***** BEGIN STUDENT CODE BLOCK 3 *****************************

traverse_tree:

	addi	$sp, $sp, -20
	sw	$ra, 16($sp) #store caller variables
	sw	$a0, 12($sp)
	sw	$s0, 8($sp)
	sw	$s1, 4($sp)
	sw	$s2, 0($sp)

	beq	$a0, $zero, done
	move	$s0, $a0 #the node and pointers
	move	$s1, $a1 #the address of the function
	move	$s2, $a2 #traversal type
	li	$t0, PRE_ORDER
	beq	$t0, $s2, preorder
	li	$t0, IN_ORDER
	beq	$t0, $s2, inorder
	li	$t0, POST_ORDER
	beq	$t0, $s2, postorder

preorder:

	jalr	$s1	#node
	lw	$a0, 4($s0)
	jal	traverse_tree	#left
	lw	$a0, 8($s0)
	jal	traverse_tree	#right
	jal done

inorder:
	lw	$a0, 4($s0)
	jal	traverse_tree	#left
	move	$a0, $s0
	jalr	$s1	#node
	lw	$a0, 8($s0)
	jal 	traverse_tree	#right
	jal	done

postorder:
	lw	$a0, 4($s0)	#left
	jal	traverse_tree
	lw	$a0, 8($s0)	#right
	jal	traverse_tree
	move	$a0, $s0
	jalr	$s1	#node
	jal	done

done:
	lw	$ra, 16($sp)	#restore caller
	lw	$a0, 12($sp)
	lw	$s0, 8($sp)
	lw	$s1, 4($sp)
	lw	$s2, 0($sp)
	addi	$sp, $sp, 20
	jr	$ra
#***** END STUDENT CODE BLOCK 3 *****************************

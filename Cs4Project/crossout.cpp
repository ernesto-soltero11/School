///File: $Id: crossout.cpp,v 1.1 2012/11/04 21:33:13 exs6350 Exp $
///
///Author: Ernesto Soltero
/// Description: Crossout is a game that can take away two numbers from a game 
/// board at one time but they must be less than the limit. The person to 
/// cross out the last number wins.
/// Revisions: $Log: crossout.cpp,v $
/// Revisions: Revision 1.1  2012/11/04 21:33:13  exs6350
/// Revisions: Initial revision
/// Revisions:

#include "Framework.h"
#include <iostream>
#include <set>
#include <cstdlib>
#include <vector>

using namespace std;

///Reperesents a move of one or two integers
class Take{

///Constructors    
public:
	///Default
	Take(): value1(0), value2(0){}

    ///Constructor
    Take (int val1){
	value1 = val1;
        value2 = 0;
    }
	
    ///Overloaded constructor
    Take(int first, int second){
        value1 = first;
	value2 = second;
    }

	///Destructor
	~Take(){}

///Accessors    
public:
   ///Return value1
   int getFirst() const{
        return value1;
    } 

    ///Return value2
    int getSecond() const{
         return value2;
    }

///private variables
private:
    int value1;
    int value2;
};

///The move made by a player
typedef Take MOVE;

///The board of crossout
typedef set<int> BOARD;

///Implements the crossout game
class Crossout : public Framework<BOARD,MOVE>{

public:  
	///Constructor 
	///@param board: The initial state of the board (set)
	///@param max: The upper bound
	Crossout(BOARD& board, int max) : MAXIMUM(max), Framework<BOARD,MOVE>::Framework(board){}

	///destructor
	virtual ~Crossout(){}

///accessors
public:
	
	///Checks to see if it is the end condition
	///@param board: The current state of the board
	///@return bool: If true game is over false if not
	virtual bool isEndCondition(const BOARD& board) const{
		if(board.empty()){
			return true;
		}
		else{
			set<int>::iterator iter = board.begin();
			if((*iter) > MAXIMUM){
				return true;
			}
		}
		return false;
	}

	///Updates the board with the move made
	///@param board:The current state of the board
	///@param move: The move made
	///@return board: The new state of the board
	virtual BOARD getValue(const BOARD& value, const MOVE& move) const{
		BOARD newBoard(value);
		if(move.getSecond() == 0){
			newBoard.erase(move.getFirst());	
		}
		else{
			newBoard.erase(move.getFirst());
			newBoard.erase(move.getSecond());
		}
		return newBoard;
	}
	
	///Return all the possible moves whether good or not
	///@param board: The current state of the board
	///@return choices: A vector holding all the possible moves
	virtual vector<MOVE> getMoves(const BOARD& board) const{
		vector<MOVE> choices;

		///For possible one int moves 
		for(set<int>::iterator single = board.begin(); single != board.end(); single++){
			if((*single) < MAXIMUM){
				choices.push_back(Take((*single)));		
			}
		}

		///For possible two int moves
		for(set<int>::iterator total = board.begin(); total != board.end(); total++){
			set<int>::iterator current(total);
			for(current; current != board.end(); current++){ 
				choices.push_back(Take((*total),(*current)));
			}
		}
		return choices;
	}

	///Checks to see if the move is a good move or not for the player
	///@param The current state of the board
	///@param player: The player doing the move
	///@param result: The result type defined in framework
	virtual result isGoodValue(const BOARD& board, bool player) const{
		
		///If the board is empty means your oppenent won  
		if(isEndCondition(board)){
			if(player){
				return BAD;
			}
			else{
				return NEITHER;
			}
		}

		///Best values to take is the maximum amount of smallest integers
		///that can equal the max
		set<int>::const_iterator iter = board.begin();
		while(true){
			int firstval = (*iter);
			iter++;
			int secondval = (*iter);
			if((firstval + secondval) == MAXIMUM){
				return GOOD;
			}
			else if((firstval + secondval) == (MAXIMUM -1)){
				return GOOD;
			}
			iter++; 
		}
		return NEITHER;;
	}

	///Prints the values
	virtual void printValues() const{
		BOARD cur = currentValue();
		set<int>::iterator iter = cur.begin();
		cout << "The values left are: ";
		for(iter; iter != cur.end(); iter++){
			cout << (*iter) << ", ";
		}
		cout << endl;
	}

	///Checks to see if the human move is a valid move and then
	///make the move and update the board
	///@param input: The choice made by the player in a string
	///@param choice: The choice of the player in a move object
	///@return bool: True if a valid move.
	virtual bool inputMove(string& input, MOVE& choice) const{
		int val1 = input[0];
		int val2 = 0;
		if(input.size() == 3){
			val2 = input[2];
		}
		else if(input.size() == 2){
			val2 = input[1];
		}

		///Not valid if greater than the upper bound
		if(val1 + val2 > MAXIMUM){
			return false;
		}
		return true;
	}

	///Prints all the moves that you can make
	virtual void printMoves() const{
		vector<MOVE> choices(getMoves(currentValue()));
		cout << "Choices: ";
		for(int i = 0; i < choices.size(); i++){
			cout << "\n\t"; 
			printSingleMove(choices[i]);
		}
		cout << "\n";
	}

	///Prints a single move
	virtual void printSingleMove(const MOVE& choice) const{
		if(choice.getSecond() == 0){
			cout << "(" << choice.getFirst() << ")";
		}
		else{
			cout << "(" << choice.getFirst() << " " << 
			choice.getSecond() << ")";
		}
	}
	
///Private variables
private:
	
	///The upper bound
	const int MAXIMUM;

};

int main(int argc, char *argv[]){
	int max = 0;
	int num = 0;
	bool p1 = false;
	bool p2 = false;
	if(argc == 4 && string(argv[1]) == "play"){
		max = atoi(argv[3]);
		num = atoi(argv[2]);
		p1 = true;
	}
	else if(argc == 3){
		max = atoi(argv[2]);
		num = atoi(argv[1]);
	}
	else{
		cerr << "Usage: crossout [play] max_num max_sum" << endl;
		return 1;
	}
	///Now set up the board and initialize the framework
	set<int> board;
	for(int i = 1; i <=num; i++){
		board.insert(i);
	}
	Crossout game(board,max);
	game.play(p1,p2);
}

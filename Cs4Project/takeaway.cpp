// File:         $Id: takeaway.cpp,v 1.1 2012/11/04 21:33:49 exs6350 Exp $
// Author:       Joe Cuffney
// Author:       Ernesto Soltero
// Description:  
// Revisions:    
//               $Log: takeaway.cpp,v $
//               Revision 1.1  2012/11/04 21:33:49  exs6350
//               Initial revision
//
//               Revision 1.1  2012/10/18 03:36:11  p334-01k
//               Initial revision
//

#include <iostream>
#include <vector>
#include "Framework.h"

///value for takeaway in an int representing coins
typedef int VALUE;

///each move in takeaway is represented by an integer
typedef int Move;

///Takeaway inherits from the abstract framework class
class Takeaway: public Framework<VALUE, Move> {

public:
	///Constructor & destructor

	///constructor
	Takeaway(VALUE& state) :
			Framework<VALUE, Move>::Framework(state) {
	}

	///destructor
	virtual ~Takeaway() {
	}

public:
	///accessors

	///checks if the value is and the end condition for takeaway
	///@param value the value to check
	///@return bool indicator
	virtual bool isEndCondition(const VALUE& value) const {
		if (!value) {
			return true;
		} else {
			return false;
		}
	}

	///gets the value o a potential move based on takeaway rules
	///@param value - the number of pennies
	///@param move - the move to be applied to the pennies
	///@return newValue - the value with the move applied
	virtual VALUE getValue(const VALUE& value, const Move& move) const {
		int newValue = value - move;
		if (newValue < 0) {
			newValue = 0;
		}
		return newValue;
	}

	///gets the moves possible for a takeaway game
	///@param value - the starting value
	///@return a set of Move types
	virtual std::vector<Move> getMoves(const VALUE& value) const {
		std::vector<Move> moves;
		if (value == 1) {
			moves.push_back(1);
		} else if (value == 2) {
			moves.push_back(1);
			moves.push_back(2);
		} else {
			moves.push_back(1);
			moves.push_back(2);
			moves.push_back(3);
		}
		return moves;
	}

	///determines if a move is good for a player
	///@param the value to check
	///@param the player perspective
	///@return the return type defined in Framework.h
	virtual result isGoodValue(const VALUE& value, bool player) const {
		if (!value) {
			if (player) {
				return GOOD;
			} else {
				return NEITHER;
			}
		}
		if (((value - 1) % 4) == 0) {
			if (player) {
				return BAD;
			} else {
				return GOOD;
			}
		} else {
			return NEITHER;
		}
	}
	///prints the number of pennies left
	virtual void printValues() const {
		VALUE pennies = currentValue();
		if (pennies > 1) {
			std::cout << "There are  " << currentValue();
			std::cout << " pennies still left." << std::endl;
		} else if (pennies == 1) {
			std::cout << "There is one penny left.";
		} else {
			std::cout << "There are no more pennies.";
		}
	}

	///inputMove is for human players and asserting that the input is
	///		valid and then making it a MOVE type
	///@param input (human input)
	///@param move the move to be returned (ensures type security)
	///@return true if a valid input, false otherwise
	virtual bool inputMove(std::string& input, Move& move) const {
		move = atoi(input.c_str());

		if (move > -1) {

			VALUE value = currentValue();

			if (move <= value && move <= 3) {

				return true;
			}
		}

		return false;

	}

	///print Moves will print the choices available (testing purposes)
	virtual void printMoves() const {

		std::vector<Move> choices(getMoves (currentValue()));
		std::cout << "Choices:";
		for (unsigned int i = 0; i < choices.size(); i++) {
			std::cout << "\n\t" << choices[i];
		}
		std::cout << std::endl;
	}

		///prints a single move as a stirng (to be called from printMoves
		///@param Move to be printed
		virtual void printSingleMove(const Move& choice) const {
			std::cout << choice;
		}

	};

///takeaway main function
///@params arguement #
///@params arguements[]
///@return int main
int main(int argc, char *argv[]) {
	int pennies = 0;
	bool p1 = false;
	bool p2 = false;
	if (argc == 3 && std::string(argv[1]) == "play") {
		pennies = atoi(argv[2]);
		p1 = true;

	} else if (argc == 2) {
		pennies = atoi(argv[1]);
	} else {
		std::cerr << "Usage: takeaway [play] num_pennies" << std::endl;
		return 1;
	}
	//create the framework and play the game
	Takeaway framework(pennies);
	framework.play(p1, p2);

	return 0;
}

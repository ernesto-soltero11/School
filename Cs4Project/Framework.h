// File:         $Id: Framework.h,v 1.1 2012/11/08 01:22:45 exs6350 Exp $
// Author:       Joe Cuffney
// Author:       Ernesto Soltero
// Description:  
// Revisions:    
//               $Log: Framework.h,v $
//               Revision 1.1  2012/11/08 01:22:45  exs6350
//               Initial revision
//
//               Revision 1.1  2012/10/18 03:36:11  p334-01k
//               Initial revision
//

#ifndef FRAMEWORK_H_
#define FRAMEWORK_H_

///imports
#include <iostream>
#include <cstdlib>
#include <vector>
#include <map>

typedef int result;

const int GOOD = 1;

const int BAD = -1;

const int NEITHER = 0;

///the Framework Class is of template type VALUE and
///choice which is defined in the game being passed in
template<typename VALUE, typename MOVE>
class Framework {

public:
	///constructor & destructor

	///constructs a framework object with an intial value
	///@param value - the inital value of the game
	Framework(VALUE& value) :
			value_(value) {
	}

	///destroys the object
	virtual ~Framework() {
	}

public:

	///checks if the current value is equal to the endValue
	///@param value - the value being checked
	///@return bool - indicator
	virtual bool isEndCondition(const VALUE& value) const = 0;

	///gets the value given the passed move
	///@param value value to be modified
	///@param move to be made
	///@return newValue
	virtual VALUE getValue(const VALUE& value, const MOVE& move) const = 0;

	///gets the set of moves
	///@param value to get moves from
	///@return vector of MOVE types
	virtual std::vector<MOVE> getMoves(const VALUE& value) const = 0;

	///returns if this is a good value to be at
	///@param value (current value)
	///@param player (human/computer)
	///@return result type (defined above) as an integer
	virtual result isGoodValue(const VALUE& value, bool player) const = 0;

	///prints the current value of the game to the console
	virtual void printValues() const = 0;

	///converts a string to the passed in choice
	///@param input - string to be converted
	///@param choice - passed in because choice type could vary
	///@return bool - success?
	virtual bool inputMove(std::string& input, MOVE& move) const = 0;
	
	///prints all the valid moves
	virtual void printMoves() const = 0;

	///prints a single move as a stirng (to be called from printMoves
	///@param Move to be printed
	virtual void printSingleMove(const MOVE& move) const = 0;

public:

	///gets the current value (dosen't change state)
	///@param returns the value
	VALUE currentValue() const {
		return value_;
	}

	///plays the game between players p1 and p2
	///@param p1, player 1
	///@param p2, player 2
	void play(bool p1, bool p2) {
		const VALUE initialState = currentValue();
		Player* p1_ = 0;
		Player* p2_ = 0;
		if (p1) {
			p1_ = new Player(*this, 1);
		} else {
			p1_ = new Computer(*this, 1);
		}

		if (p2) {
			p2_ = new Player(*this, 2);
		} else {
			p2_ = new Computer(*this, 2);
		}
		int p1Score = 0;
		int p2Score = 0;
		//while (true) {
		while (true) {
			VALUE current = currentValue();
			if (isEndCondition(current)) {
				if (isGoodValue(currentValue(), true)) {
					p1Score++;
					std::cout << "\nPlayer 1 has won the game" << std::endl;
				} else {
					p2Score++;
					std::cout << "\nPlayer 2 has won the game" << std::endl;
				}
				break;
			}
			std::cout << std::endl;
			printValues();
			MOVE move = p1_->getMove(current);
			updateValue(move);
			current = currentValue();
			if (isEndCondition(current)) {
				if (isGoodValue(currentValue(), true)) {
					p2Score++;
					std::cout << "\nPlayer 2 wins!" << std::endl;
				} else {
					p1Score++;
					std::cout << "\nPlayer 1 wins!" << std::endl;
				}
				break;
			}
			std::cout << std::endl;
			printValues();
			MOVE moveTwo = p2_->getMove(current);
			updateValue(moveTwo);
		}
		std::cout << std::endl << "Current Scores:" << std::endl;
		std::cout << "\tPlayer 1: " << p1Score << std::endl;
		std::cout << "\tPlayer 2: " << p2Score << std::endl;
		std::cout << std::endl;
//            while (true) {
//                std::cout << "Play again? ( y / n ): ";
//                char again;
//                std::cin >> again;
//                if (again == 'n') {
//                    std::cout << std::endl <<
//                            "Final Scores:" << std::endl;
//                    std::cout << "\tPlayer 1: " <<
//                            p1Score << std::endl;
//                    std::cout << "\tPlayer 2: " <<
//                            p2Score << std::endl;
//                    std::cout << std::endl;
//                    return;
//
//                } else if (again == 'y') {
//                    resetState(initialState);
//                    break;
//                } else {
//                    std::cout << "Incorrect character... try again!\n";
//                }
//            }
//        }
	}

private:
	///frameworks private classes

	///players (computer subclass overrides player) (human would play as a player)
	class Player {

	public:

		///constructs a player object
		///@param game (game being played)
		///@param num (playerNumber)
		Player(const Framework& game, int num) :
				game_(&game), number_(num) {
		}

		///destructor
		virtual ~Player() {
		}

	public:
		///accessors

		///prompts user for a move
		///@pram value - current value
		///@return Move the move selected
		virtual MOVE getMove(VALUE value) {
			MOVE choice;
			while (true) {
				std::cout << "\nPlayer " << number_ << " - your turn.\n";
				std::cout << "Enter move (type HELP for available choices): ";
				std::string input;
				std::getline(std::cin, input, '\n');
				if (input == "HELP" || input == "help" || input == "Help") {
					std::cout << std::endl;
					game_->printValues();
					std::cout << std::endl;
					game_->printMoves();
				} else {
					if (game_->inputMove(input, choice)) {
						break;
					} else {
						std::cout << "Invalid choice..." << std::endl;
					}
				}
			}
			return choice;

		}

	protected:
		//instance variables

		///the game sent into the framework
		const Framework* game_;

		///the
		const int number_;

	};
	///this is the computer type of player (minus the prompts)
	class Computer: public Player {

	public:
		//constructor & destructor

		///constructs a computer player
		Computer(const Framework& game, int playerNumber) :
				Player(game, playerNumber) {
			if (Player::number_ == 1) {
				solver(Player::game_->currentValue(), true);
			} else {
				solver(Player::game_->currentValue(), false);
			}
		}

		///destructor
		~Computer() {
			memoization.clear();
		}

	public:
		///accessors

		///returns the move based on the value
		///@param value the value
		///@param Move the move chosen to be best
		MOVE getMove(VALUE value) {
			MOVE* choice(memoization[value]);
			MOVE returnMove;
			if (choice == 0) {
				std::cout << std::endl << "No best choice...";
				returnMove = Player::game_->getMoves(
						Player::game_->currentValue())[0];
			} else {
				returnMove = *choice;
			}
			std::cout << std::endl;
			std::cout << "Computer " << Player::number_ << " chose: ";
			Player::game_->printSingleMove(returnMove);
			std::cout << std::endl;
			return returnMove;

		}

	private:
		/// private methods

		///the evaluate position method
		int solver(const VALUE& value, bool player) {
			//base case
			if (Player::game_->isEndCondition(value)) {
				return Player::game_->isGoodValue(value, player);
			}

			int idealScore = 0;
			MOVE idealMove;
			int valueScore = 0;
			std::vector<MOVE> choices(Player::game_->getMoves(value));
			for (unsigned int i = 0; i < choices.size(); i++) {
				MOVE choice = choices[i];
				result choiceScore(Player::game_->isGoodValue(value, player));
				VALUE nextState = Player::game_->getValue(value, choice);
				choiceScore += solver(nextState, !player);
				if (choiceScore > idealScore) {
					idealScore = choiceScore;
					idealMove = choice;
				}
				valueScore += choiceScore;
			}

			if (player) {
				if (idealScore == 0) {
					MOVE* nullPointer = 0;
					memoization.insert(std::make_pair(value, nullPointer));
				} else {
					memoization.insert(
							std::make_pair(value, new MOVE(idealMove)));
				}
			}
			return valueScore;
		}
	private:
		//private instance variable

		// The possible states and the best choice for those states.
		std::map<VALUE, MOVE*> memoization;

	};

private:
	///private methods

	///update the value
	///@param choice
	void updateValue(const MOVE& choice) {
		value_ = getValue(value_, choice);
	}

	///resets the value (for future games?)
	///@param value
	void resetValue(const VALUE& value) {
		value_ = value;
	}

private:
	///instance variables

	///arbitary value
	VALUE value_;

	///set of moves
	std::vector<MOVE> moves_;
};

#endif

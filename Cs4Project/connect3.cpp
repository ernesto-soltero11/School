//Log: $Id: connect3.cpp,v 1.2 2012/10/27 18:00:50 p334-01k Exp p334-01k $
//Author: Joe Cuffney
//Author: Ernesto Soltero
//Revisions: $Log: connect3.cpp,v $
//Revisions: Revision 1.2  2012/10/27 18:00:50  p334-01k
//Revisions: connected connect3 to Framework.h
//Revisions: added skeleton
//Revisions:
//Revisions: Revision 1.1  2012/10/27 17:33:07  p334-01k
//Revisions: Initial revision
//Revisions:
//Revisions: Revision 1.2  2012/10/25 03:01:32  jwc2790
//Revisions: worked on i/o (still working on reading in board)
//Revisions:
//Revisions: Revision 1.1  2012/10/24 00:34:38  jwc2790
//Revisions: Initial revision
//Revisions:

#include<iostream>
#include<string>
#include<fstream>
#include<vector>
#include "Framework.h"

using namespace std;

///A board memeber class which will represent each
///piece that goes into the board. 
class BoardMember {

	public: ///constructor/Destructor

	    ///makes a board memeber based on a character
	    ///@param char c the gamepiece value
	    BoardMember(char& c) {
		if (c == '.') {
		    value_ = '.';
		} else if (c == 'X') {
		    value_ = 'X';
		} else if (c == 'O') {
		    value_ = 'O';
		} else {
		    cerr << "file written improporly" << endl;
		}
		//cout << "value: " << value_ << endl;
	    }

	    ///default constructor (empty piece)
	    BoardMember() :
		    value_('.'),col(0) {
	    }

	    ///destructor
	    ~BoardMember() {
	    }
	 
	public: ///mutators

	    ///sets the value of the board member
 	    ///@param char c the value of the gamepiece
	    void setValue(char c) {
			value_ = c;
	    }

		///Sets the column where this move is placed at.
		///@ param i: The col to place this move.
		void setCol(int i){
			col = i;
		}

	public: ///accessors

	    ///gets the value of the board member
	    ///@return the current value of the piece
	    char getValue() const {
			return value_;
	    }

		//Returns the col this move is placed at.
		int getCol() const{
			return col;
		}

	private: ///members
		int col;
	    char value_;

};//end board member

/////////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////

///The board class represents the connect3 board
class Board {

	public: ///constructors/destructor

	    ///cosntructs a board
	    ///@param vector<vector<BoardMember> > board the board repres.
	    ///@param int height the height of the board
	    ///@param int width the width of the board
	    Board(vector<vector<BoardMember> >& board , int height, int width) :
		    board_(board), width_(width), height_(height) {
	    }

	    ///destructor
	    ~Board() {
	    }

	    ///Returns the height of the board
 	    ///@return height_
	    int getHeight() const{
			return height_;
	    }

	    ///Returns the width of the board
	    ///@return width_
	    int getWidth() const{
			return width_;
	    }

		///Inserts the move into the column
		///@param move: The move to be made
		///@param column: The col to place the move in.
		typedef BoardMember MOVE;
		void insert(MOVE move){
			int last = -1;
			int col = move.getCol();
			for(int i =0; i < height_; i++){
				if(board_[i][col].getValue() != '.'){
					break;
				}
				last++;
			}
			board_[last][col] = move;
		}

	    ///Gets the current state of the board.
	    ///@return board_ the board
	    vector<vector<BoardMember> > getBoard() const{
	    	return board_;
	    }


	private: ///members

	    vector<vector<BoardMember> > board_;
	    int height_;
	    int width_;

};///end board class

///////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////

///defines the VALUE of this game to be a BoardMember
typedef Board VALUE;

///defines a MOVE to be of type BoardMember
typedef BoardMember MOVE;

class Connect3 : public Framework<VALUE , MOVE> {

	public:
	    Connect3(VALUE& value) :
		Framework<VALUE, MOVE>::Framework(value) {
	    }

	    ~Connect3() {
	    }

	public: //accessors
	    
	    // @param value - The value of the current game.
	    // @return vector: The possible moves that you can make.
	    // Gets the possible moves that you can make.
	    virtual vector<MOVE> getMoves(const VALUE& value) const {
		 vector<MOVE> moves;
		 char ch = 'O';
		 ///if the top is a '.' then you can make a move there
		 int width = value.getWidth();
		 vector< vector<MOVE> > val = value.getBoard();
			for(int r = 0; r < width; r++){
		 		if(val[r][0].getValue() == '.'){
					MOVE mov(ch);
					mov.setCol(r);
					moves.push_back(mov);
				}
		 	}
			return moves;
	    }

	    ///Makes the move given and then updates the board
	    ///@param value value to be modified
	    ///@param move to be made
	    ///@return newValue
	    virtual VALUE getValue(const VALUE& value, const MOVE& move)const{
		VALUE newVal = currentValue();
		
		newVal.insert(move);
		return newVal;

	    }

	    ///determines if a move is good for a player
	    ///@param the value to check
	    ///@param the player perspective
	    ///@return the return type defined in Framework.h
	    virtual result isGoodValue(const VALUE& value, bool player) const {
		   
			int height = value.getHeight();
			int width = value.getWidth();
			///Check horizontally
			for(int row = 0; row < height; row++){
				for(int col = 0; col <= width -2; col++){
					if((value.getBoard())[row][col].getValue()  == 
					(value.getBoard())[row][col+1].getValue()  && 
					(value.getBoard())[row][col].getValue() != '.'){
						if(player){
							return GOOD;
						}
						else{
							return BAD;
						}
					}
				}
			}

			///Check vertically
			for(int col = 0; col < width; col++){
				for(int row = 0; row <= height -2; row++){
					if((value.getBoard())[row][col].getValue() == 
					(value.getBoard())[row+1][col].getValue() &&
					(value.getBoard())[row][col].getValue() != '.'){
						if(player){
							return GOOD;
						}
						else{
							return BAD;
						}
					}
				}
			}

			///Check diagonally up
			for(int col = 0; col <= width -2;col++){
				for(int row = height -1; row >= 1; row--){
					if((value.getBoard())[row][col].getValue() ==
					(value.getBoard())[row-1][col+1].getValue() &&
					(value.getBoard())[row][col].getValue() != '.'){
						if(player){
							return GOOD;
						}
						else{
							return BAD;
						}
					}
				}
			}

			///Check diagonally down
			for(int col = 0; col <= width -2; col++){
				for(int row = 0; row <= height -2;row++){
					if((value.getBoard())[row][col].getValue() ==
					(value.getBoard())[row+1][col+1].getValue() &&
					(value.getBoard())[row][col].getValue() != '.'){
						if(player){
							return GOOD;
						}
						else{
							return BAD;
						}
					}
				}
			}

			return NEITHER;

		}

	    ///prints the state of the board
	    virtual void printValues() const {
			vector<vector<MOVE> > value = currentValue().getBoard();
			int width = currentValue().getWidth();
			int height = currentValue().getHeight();

			for(int r = 0; r < width; r++){
				cout << "|";
				for(int h = 0; h < height; h++){
					cout << value[r][h].getValue() << "|";
				}
			cout << endl;
			}
	    }

	public: //void/input methods

	    ///inputMove is for human players and asserting that the input is
	    ///		valid and then making it a MOVE type
	    ///@param input (human input)
	    ///@param move the move to be returned (ensures type security)
	    ///@return true if a valid input, false otherwise
	    virtual bool inputMove(std::string& input, MOVE& move) const {
	    	///if the input is less than 0 or greater than the width
			//then it is not a valid move

			int width = currentValue().getWidth();
			vector<vector<MOVE> > val = currentValue().getBoard();

			if(atoi(input.c_str()) < 0 || atoi(input.c_str()) > width){
		    	return false;
			}

			//if the input column is full then not a valid move

			if(val[0][atoi(input.c_str())].getValue() != '.'){
		    	return false;
			}
			return true;
	    }

	    ///print Moves will print the moves available (testing purposes)
	    virtual void printMoves() const {
	    	cout << "You can move in these spaces..." << endl;
			///call the getMoves and get a vector<MOVE> then get these as
			///then call the printSingleMove for each value in that vector
			VALUE val = currentValue();
			vector<MOVE> moves = getMoves(val);
			vector<MOVE>::iterator iter = moves.begin();
			for(iter; iter != moves.end(); iter++){
				printSingleMove((*iter));
			}

	    }

	    ///prints a single move as a string (to be called from printMoves
	    ///@param Move to be printed
	    virtual void printSingleMove(const MOVE& move) const {
	    	cout << "You can place at: " << 
			move.getCol() << endl;
	    }



	public: //indicator methods

	    ///checks if the current value is equal to the endValue
	    ///@param value - the value being checked
	    ///@return bool - indicator
	    virtual bool isEndCondition(const VALUE& value) const{
	    	///if top is full then no one wins game over
			int width = value.getWidth();
			int height = value.getHeight();
			vector< vector<MOVE> > val = value.getBoard();

			for(int i = 0; i <= width - 3; i++){
				if(val[0][i].getValue() != '.' && val[0][i+1].getValue() 
				!= '.' && val[0][i+2].getValue() != '.'){
					return true;
				}

			}

			///Check horizontally for a win
			for(int r = 0; r < height; r++){
				for(int h = 0; h <= width - 3; h++){
					if(val[r][h].getValue() == val[r][h+1].getValue()
					== val[r][h+2].getValue()){
						return true;
					}
				}
			}
		
			///Check vertically for a win
			for(int h = 0; h < width; h++){
				for(int r = 0; r <= height -3; r++){
					if(val[r][h].getValue() == val[r+1][h].getValue() 
					== val[r+2][h].getValue()){
						return true;
					}
				}
			}

			///Check diagonally up for a win
			for(int col = 0; col <= width - 3; col++){
				for(int row = height - 1; row >= 2; row--){
					if(val[row][col].getValue() == 
					val[row-1][col+1].getValue() == 
					val[row-2][col+2].getValue()){
						return true;
					}
				}
			}

			///Check diagonally down for a win
			for(int row = 0; row <= height - 3; row++){
				for(int col = 0; col <= width -3;col++){
					if(val[row][col].getValue() == 
					val[row+1][col+1].getValue() ==
					val[row+2][col+2].getValue()){
						return true;
					}
				}
			}

			return false;

	    }


	private:

};

int main(int argc, char *argv[]) {
    int flag = 1;

    bool p1 = false;
    bool p2 = false;

    int width;
    int height;

    string file;
    string line;
	

    if (argc > 1 && argc < 4) {
        if (string(argv[1]) == "play") {
            flag = 2;
            p1 = true;
        }


        // Make istream pointer instead of declaring it an instance
        // of a inheriting class helped me out 
        istream* in = 0;

        // Create a input file stream to test file validity
        ifstream fileInput;

        if (argv[flag][0] != '-' && argv[flag][1]) {

            // Try opening the file, if it dosen't work, print error,
            // otherwise, fileInput is all good
            fileInput.open(argv[flag], ios_base::in);
            if (fileInput.fail()) {
                cerr << "Error opening file " << argv[flag] << endl;
                fileInput.close();
                return -1;
            } else {
                // Wset in to file input
                in = &fileInput;
            }
        } else {
            // Else, if '-', use cin
            in = &cin;
            fileInput.close();
        }

        if (in->good()) { // We only need to ask if it's good once.

            cout << "Enter width: ";
            if ( in->bad() ) {
		// In case of bad input
                cout << "Input error!"; 

            } else {
                (*in) >> width; 
            }

            cout << "Enter height: ";
            if ( in->bad() ) { 
		// In case of bad input
                cout << "Input error!";
            } else {
                (*in) >> height;
            }

            cout << "width: " << width << " height: " << height << endl;

           vector< vector<MOVE> > board (width ,vector<BoardMember> (height));

            char temp;
	    ///temp row to add to the board 
	    vector<BoardMember> v;
	    int count = 0;
            while ( ! in->fail() ) { // *** Referencing again ***
                (*in) >> temp; // *** One last time ***
                //cout << temp << endl;
		if(count != width){
			count++;
			///add to row list
			v.push_back(*(new BoardMember(temp)));	
		}else{
                	board.push_back(v);
			v.clear();
			///add to the board and empty the row
			v.push_back(*(new BoardMember(temp)));
			count = 0;
		}
            }

	    //done reading in the board...
	    ///create the board object
	    VALUE bo(board , height, width);
	    ///print the intial state
	    Connect3 game(bo);
	    //game.play(p1,p2);
        }

	

    } else {
        ///invalid arguements
        cerr << "usage: connect3 [play] - " << endl;
        return -1;
    }
}

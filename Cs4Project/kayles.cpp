// File:         $Id: kayles.cpp,v 1.1 2012/11/08 01:22:45 exs6350 Exp $
// Author:       Joe Cuffney
// Author:       Ernesto Soltero
// Description:  
// Revisions:    
//               $Log: kayles.cpp,v $
//               Revision 1.1  2012/11/08 01:22:45  exs6350
//               Initial revision
//
//               Revision 1.1  2012/10/18 03:36:11  p334-01k
//               Initial revision
//

#include <iostream>
#include <vector>
#include "Framework.h"


///This class represents the action of "bowling" and knocking down pins

class Bowl {

public: ///constructor & destructor

	///constructor
	Bowl() :
			pins_(0), twoPins_(0) {
	}
	///destructor
	Bowl(int pin, bool twoPins) :
			pins_(pin), twoPins_(twoPins) {

	}

public:  ///ostream
	
	///overloads the operator
	friend std::ostream& operator <<(std::ostream& stream, Bowl& bowl) {
		int numPins;
		if (bowl.twoPins_) {
			numPins = 2;
		} else {
			numPins = 1;
		}
		stream << "Knocking down pin " << bowl.pins_;
		if (bowl.twoPins_) {
			stream << " and pin " << (bowl.pins_ + 1);
		}
		return stream;
	}

public: ///accesors

	///gets the number of pins
	///@return the number of pins
	int getPin() const {
		return pins_;
	}

	int numPins() const {
		if (twoPins_) {
			return 2;
		} else {
			return 1;
		}
	}

private:
	// line to knock down
	int pins_;
	// knock down 2 pins? y/n
	bool twoPins_;

};

///The class Pins represnts some pins in a Kayles game
class Pins {

public: ///constructor
	
	///constructs a pins object
	Pins() :
		numPins_(0) {
	}

public: ///operators

	///overloads the operator
	friend std::ostream& operator <<(std::ostream& stream, Pins& pins) {
		stream << "Standing pins (By pin index):\n";
		int currentPin(1);
		for (unsigned int line = 0; line < pins.lines_.size(); line++) {
			for (int pin = 0; pin < pins.lines_[line]; pin++) {
				stream << " " << currentPin << " ";
				currentPin++;
			}
			stream << "\n";
		}
		return stream;
	}

	///overloads the operator
	bool operator<(const Pins& p) const {
		if (numPins_ != p.numPins_) {
			return numPins_ < p.numPins_;

		} else if (size() != p.size()) {
			return size() < p.size();

		} else {
			for (int i = 0; i < size(); i++) {
				if ((this->operator [](i)) != p[i]) {
					return (this->operator [](i)) < p[i];
				}
			}
			return false;
		}
	}
	
	///overloads the operator
	int operator [](const int& line) const {
		return lines_[line];
	}

public:
	///gets the size of a line
	///@return the size of the line
	int size() const {
		return lines_.size();
	}

	///gets the number of pins
	///@return the number of pins
	int getNumPins() const {
		return numPins_;
	}

public: 
	/// do methods
	///@param line the line to be inserted
	void insertLine(const int& line) {
		lines_.push_back(line);
		numPins_ += line;
	}

private: ///instance variables

	///vector of the lines of pins
	std::vector<int> lines_;
	///number of pins
	int numPins_;

};


///the VALUE of kayles in in a Pin
typedef Pins VALUE;
///the MOVE of kayles in called bowl and in bowling
typedef Bowl MOVE;
///the kayles class is of the template type Framework
class Kayles: public Framework<VALUE, MOVE> {

public:
	///constructor & destructor

	///constructs a Kayles object
	Kayles(VALUE& value) :
			Framework<VALUE, MOVE>::Framework(value) {
	}

	///destructor
	virtual ~Kayles() {
	}

public:
	///accesors

	///checks if the value is an end condition
	///@param value to be checked
	///@return boolean indicator
	virtual bool isEndCondition(const VALUE& value) const {
		if (value.size() == 0) {
			return true;
		} else {
			return false;
		}
	}

	///gets the value after the given move
	///@param value - the starting value
	///@param move - the move to be applied
	///@return the updated value
	virtual VALUE getValue(const VALUE& value, const MOVE& move) const {
		VALUE nextValue;
		int pin(move.getPin());
		int numLines(value.size());
		int curPin = 1;
		for (int i = 0; i < numLines; i++) {

			if (curPin == pin || (curPin + value[i] - 1) == pin) {
				int line(value[i] - move.numPins());
				if (line > 0) {
					nextValue.insertLine(line);
				}

			} else if (curPin < pin && (curPin + value[i] - 1) > pin) {
				int lineLeft(pin - curPin);
				if (lineLeft > 0) {
					nextValue.insertLine(lineLeft);
				}
				int lineRight(value[i] - lineLeft - move.numPins());
				if (lineRight > 0) {
					nextValue.insertLine(lineRight);
				}

			} else {
				nextValue.insertLine(value[i]);
			}
			curPin += value[i];
		}
		return nextValue;

	}

	///gets possible moves
	///@param value to start from
	///@return a set of MOVE types of possible moves
	virtual std::vector<MOVE> getMoves(const VALUE& value) const {
		std::vector<MOVE> choices;
		int currentPin = 0;
		for (int line = 0; line < value.size(); line++) {
			int lineSize = value[line];
			for (int pin = 1; pin < lineSize; pin++) {
				choices.push_back(Bowl(pin + currentPin, false));
				choices.push_back(Bowl(pin + currentPin, true));
			}
			choices.push_back(Bowl(currentPin + lineSize, false));
			currentPin += lineSize;
		}
		return choices;

	}

	///checks if the value is good for the player
	///@param value the current value
	///@param the player doing the move
	///@return result type as defined in Framework.h
	virtual result isGoodValue(const VALUE& value, bool player) const {
		if (value.size() == 0) {
			if (player) {
				return NEITHER;
			} else {
				return GOOD;
			}
		} else if (value.getNumPins() % 2 == 0) {
			int winStates = 0;

			for (int i = 0; i < value.size(); i++) {
				if (value[i] == 1 || value[i] == 2 || value[i] == 3
						|| value[i] == 4) {
					winStates++;
				}
			}
			if (winStates % 2) {
				if (player) {
					return GOOD;
				} else {
					return BAD;
				}
			} else {
				if (player) {
					return BAD;
				} else {
					return GOOD;
				}
			}
		} else {
			return NEITHER;
		}
	}

	///prints the values
	virtual void printValues() const {
		VALUE state = currentValue();
		std::cout << state;

	}

	///checks if the human players input is a valid move and then
	/// 	make it of the MOVE type
	///@param input - string input from human
	///@param choice - the choice of type MOVE
	///@return bool indicator of success
	virtual bool inputMove(std::string& input, MOVE& choice) const {
		int space(input.find(' '));
		int totalPins(currentValue().getNumPins());
		if (space > 0) {
			int pinOne(atoi(input.substr(0, space).c_str()));
			if (pinOne < 1 || pinOne > totalPins) {
				return false;
			}
			int pinTwo(atoi(input.substr
				(space, input.size() - 1).c_str()));
			if (pinTwo < 1 || pinTwo > totalPins) {
				return false;
			}
			if ((pinOne - pinTwo) == 1 || (pinOne - pinTwo) == -1) {
				VALUE state = currentValue();
				if (pinOne < pinTwo) {
					int curPin = 0;
					for (int line = 0; 
						line < state.size(); line++) {
						curPin += state[line];
						if (pinOne == curPin) {
							return false;
						}
					}
					choice = MOVE(pinOne, true);
					return true;
				} else {
					int curPin = 0;
					for (int line = 0; line < state.size(); 
						line++) {
						curPin += state[line];
						if (pinTwo == state[line]) {
							return false;
						}
					}
					choice = MOVE(pinTwo, true);
					return true;
				}

			} else {
				return false;
			}
		} else {
			int pinOne(atoi(input.c_str()));
			if (pinOne < 1 || pinOne > totalPins) {
				return false;
			}
			choice = MOVE(pinOne, false);
			return true;
		}
	}

	///prints the possible moves (testing)
	virtual void printMoves() const {
		std::cout << "You can knock down any two "
				"pins that are next to one another." << std::endl;

	}

	///prints a specific Move
	///@param move a MOVE
	virtual void printSingleMove(const MOVE& move) const {

		MOVE choiceToPrint(move);
		std::cout << choiceToPrint;

	}

};


///main function for a game of kayles
///@params arguement #
///@params arguements[]
///@return int main
int main(int argc, char *argv[]) {
	Pins pins;

	bool player1 = false;
	bool player2 = false;
	if (argc >= 3 && std::string(argv[1]) == "play") {
		player1 = true;
		for (int i = 2; i < argc; i++) {
			int arg = atoi(argv[i]);
			if (arg) {
				pins.insertLine(arg);
			} else {
				std::cerr << "Usage: kayles [play] num_pins_1 num_pins_2 ..."
						<< std::endl;
				return 1;
			}
		}
	} else if (argc >= 2) {
		for (int i = 1; i < argc; i++) {
			int arg = atoi(argv[i]);
			if (arg) {
				pins.insertLine(arg);
			} else {
				std::cerr << "Usage: kayles [play] num_pins_1 num_pins_2 ..."
						<< std::endl;
				return 1;
			}
		}

	} else {
		std::cerr << "Usage: kayles [play] num_pins_1 num_pins_2 ..."
				<< std::endl;
		return 1;
	}

	Kayles framework(pins);
	framework.play(player1, player2);

	return 0;
}

"""
Cable Car: Student Computer Player

A sample class you may use to hold your state data
Author: Adam Oest (amo9149@rit.edu)
Author: Ernesto Soltero (exs6350)
Author: Obaleski Idemudia(oxi8927@rit.edu)
"""
class PlayerData(object):
    """A sample class for your player data"""
    
    # Add other slots as needed
    __slots__ = ('logger', 'playerId', 'currentTile', 'numPlayers', 'board', 'score','stations','stations','ownedstations')
    
    def __init__(self, logger, playerId, currentTile, numPlayers, score):
        """
        __init__: PlayerData * Engine.Logger * int * NoneType * int -> None
        Constructs and returns an instance of PlayerData.
            self - new instance
            logger - the engine logger
            playerId - my player ID (0-5)
            currentTile - my current hand tile (initially None)
            numPlayers - number of players in game (1-6)
            stations - Number of stations that the player owns
        """
        
        self.logger = logger
        self.playerId = playerId
        self.currentTile = currentTile
        self.numPlayers = numPlayers
        self.board = [[0 for row in range(8)] for col in range(8)]
        self.score = score
        self.ownedstations = dict()
        # initialize any other slots you require here
        
        if numPlayers == 1:
            self.ownedstations[1] = [1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32]
    
        elif numPlayers == 2:
            self.ownedstations[1] = [1,3,5,7,9,11,13,15,17,19,21,23,25,27,29,31]
            self.ownedstations[2] = [2,4,6,8,10,12,14,16,18,20,22,24,26,28,30,32]
            
        elif numPlayers == 3:
            self.ownedstations[1] = [1,4,6,11,15,20,23,25,28,31]
            self.ownedstations[2] = [2,7,9,12,14,19,22,27,29,32]
            self.ownedstations[3] = [3,5,8,10,13,18,21,24,26,30]
            
        elif numPlayers == 4:
            self.ownedstations[1] = [4,7,11,16,20,23,27,32]
            self.ownedstations[2] = [3,8,12,15,19,24,28,31]
            self.ownedstations[3] = [1,6,10,13,18,21,25,30]
            self.ownedstations[4] = [2,5,9,14,17,22,26,29]
            
        elif numPlayers == 5:
            self.ownedstations[1] = [1,5,10,14,22,28]
            self.ownedstations[2] = [6,12,18,23,27,32]
            self.ownedstations[3] = [3,7,15,19,25,29]
            self.ownedstations[4] = [2,9,13,21,26,30]
            self.ownedstations[5] = [4,8,11,20,24,31]
            
        elif numPlayers == 6:
            self.ownedstations[1] = [1,5,10,19,27]
            self.ownedstations[2] = [2,11,18,25,29]
            self.ownedstations[3] = [4,8,14,21,26]
            self.ownedstations[4] = [6,15,20,24,31]
            self.ownedstations[5] = [3,9,13,23,30]
            self.ownedstations[6] = [7,12,22,28,32]
        
    def __str__(self):
        """
        __str__: PlayerData -> string
        Returns a string representation of the PlayerData object.
            self - the PlayerData object
        """
        result = "PlayerData= " \
                    + "playerId: " + str(self.playerId) \
                    + ", currentTile: " + str(self.currentTile) \
                    + ", numPlayers:" + str(self.numPlayers) \
                    + ", score: " + str(self.score)
                
        # add any more string concatenation for your other slots here
                
        return result
    
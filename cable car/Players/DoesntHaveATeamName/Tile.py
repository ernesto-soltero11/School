from Station import Station

"""
Author: Ernesto Soltero (exs6350@rit.edu)
"""

class Tile(object):
    """Creates an instance that represents a tile object.
    tileId -> is a str of a ,b ,c ,d , e, f, g, h, j, or i
    rotation -> int that returns rotation of the tile
    Nodes -> contains a list of nodes that represent the eight points on a tile. (This is for later use)"""
        
    __slots__ = ('tileId', 'rotation','tracktiles','position','boardlocation','station')
        
    def __init__(self, tileId, rotation, position, boardlocation):
            
        self.tileId = tileId
        self.rotation = rotation
        self.tracktiles = {} 
        self.position = position
        self.boardlocation = boardlocation
        self.station = Station(None,None)
        
        
        self.tracktiles['a'] = [[0,1],[1,0],[2,7],[3,6],[4,5],[5,4],[6,3],[7,2]]
        self.tracktiles['b'] = [[0,3],[1,4],[2,7],[3,1],[4,1],[5,6],[6,5],[7,2]]
        self.tracktiles['c'] = [[0,3],[1,4],[2,5],[3,0],[4,1],[5,2],[6,7],[7,6]]
        self.tracktiles['d'] = [[0,1],[1,0],[2,7],[3,4],[4,3],[5,6],[6,5],[7,2]]
        self.tracktiles['e'] = [[0,1],[1,0],[2,3],[3,2],[4,7],[5,6],[6,5],[7,4]]
        self.tracktiles['f'] = [[0,5],[1,4],[2,7],[3,6],[4,1],[5,0],[6,3],[7,2]]
        self.tracktiles['g'] = [[0,1],[1,0],[2,3],[3,2],[4,5],[5,4],[6,7],[7,6]]
        self.tracktiles['h'] = [[0,7],[1,2],[2,1],[3,4],[4,3],[5,6],[6,5],[7,0]]
        self.tracktiles['i'] = [[0,3],[1,6],[2,5],[3,0],[4,7],[5,2],[6,1],[7,4]]
        self.tracktiles['j'] = [[0,7],[1,6],[2,5],[3,4],[4,3],[5,2],[6,1],[7,0]]
        
        if self.rotation == 1:
            self.tracktiles['a'] = [[0,5],[1,4],[2,3],[3,2],[4,1],[5,0],[6,7],[7,6]]
            self.tracktiles['b'] = [[0,7],[1,4],[2,5],[3,6],[4,1],[5,2],[6,3],[7,0]]
            self.tracktiles['c'] = [[0,1],[1,0],[2,5],[3,6],[4,7],[5,2],[6,3],[7,4]]
            self.tracktiles['d'] = [[0,7],[1,4],[2,3],[3,2],[4,1],[5,6],[6,5],[7,0]]
            self.tracktiles['e'] = [[0,7],[1,6],[2,3],[3,2],[4,5],[5,4],[6,1],[7,0]]
            self.tracktiles['f'] = [[0,5],[1,4],[2,7],[3,6],[4,1],[5,0],[6,3],[7,2]]
            self.tracktiles['g'] = [[0,1],[1,0],[2,3],[3,2],[4,5],[5,4],[6,7],[7,6]]
            self.tracktiles['h'] = [[0,7],[1,2],[2,1],[3,4],[4,3],[5,6],[6,5],[7,0]]
            self.tracktiles['i'] = [[0,3],[1,6],[2,5],[3,0],[4,7],[5,2],[6,1],[7,4]]
            self.tracktiles['j'] = [[0,3],[1,2],[2,1],[3,1],[4,7],[5,6],[6,5],[7,4]]
            
        elif self.rotation == 2:
            self.tracktiles['a'] = [[0,1],[1,0],[2,7],[3,6],[4,5],[5,4],[6,3],[7,2]]
            self.tracktiles['b'] = [[0,5],[1,2],[2,1],[3,6],[4,7],[5,0],[6,3],[7,4]]
            self.tracktiles['c'] = [[0,5],[1,6],[2,3],[3,2],[4,7],[5,0],[6,1],[7,4]]
            self.tracktiles['d'] = [[0,7],[1,2],[2,1],[3,6],[4,5],[5,4],[6,3],[7,0]]
            self.tracktiles['e'] = [[0,3],[1,2],[2,1],[3,0],[4,5],[5,4],[6,7],[7,6]]
            self.tracktiles['f'] = [[0,5],[1,4],[2,7],[3,6],[4,1],[5,0],[6,3],[7,2]]
            self.tracktiles['g'] = [[0,1],[1,0],[2,3],[3,2],[4,5],[5,4],[6,7],[7,6]]
            self.tracktiles['h'] = [[0,7],[1,2],[2,1],[3,4],[4,3],[5,6],[6,5],[7,0]]
            self.tracktiles['i'] = [[0,3],[1,6],[2,5],[3,0],[4,7],[5,2],[6,1],[7,4]]
            self.tracktiles['j'] = [[0,7],[1,6],[2,5],[3,4],[4,3],[5,2],[6,1],[7,0]]
            
        elif self.rotation == 3:
            self.tracktiles['a'] = [[0,5],[1,4],[2,3],[3,2],[4,1],[5,0],[6,7],[7,6]]
            self.tracktiles['b'] = [[0,5],[1,6],[2,7],[3,4],[4,3],[5,0],[6,1],[7,2]]
            self.tracktiles['c'] = [[0,3],[1,6],[2,7],[3,0],[4,5],[5,4],[6,1],[7,2]]
            self.tracktiles['d'] = [[0,5],[1,2],[2,1],[3,4],[4,3],[5,0],[6,7],[7,6]]
            self.tracktiles['e'] = [[0,1],[1,0],[2,5],[3,4],[4,3],[5,2],[6,7],[7,6]]
            self.tracktiles['f'] = [[0,5],[1,4],[2,7],[3,6],[4,1],[5,0],[6,3],[7,2]]
            self.tracktiles['g'] = [[0,1],[1,0],[2,3],[3,2],[4,5],[5,4],[6,7],[7,6]]
            self.tracktiles['h'] = [[0,7],[1,2],[2,1],[3,4],[4,3],[5,6],[6,5],[7,0]]
            self.tracktiles['i'] = [[0,3],[1,6],[2,5],[3,0],[4,7],[5,2],[6,1],[7,4]]
            self.tracktiles['j'] = [[0,3],[1,2],[2,1],[3,0],[4,7],[5,6],[6,5],[7,4]]
            
    def __str__(self):
        result = "Position: " + str(self.position) 
        + " TileId: " + self.tileId 
        + " rotation: " + str(self.rotation) 
        return result
    

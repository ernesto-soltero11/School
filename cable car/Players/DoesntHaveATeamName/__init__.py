from Model.interface import PlayerMove
from playerData import PlayerData
from Tile import *
from Station import *
from random import randrange
"""
Cable Car: Student Computer Player

Complete these function stubs in order to implement your AI.
Author: Adam Oest (amo9149@rit.edu)
Author: Ernesto Soltero(exs6350@rit.edu)
Author: Obaleski Idemudia(oxi8927@rit.edu)
"""

def init(playerId, numPlayers, startTile, logger, arg = "None"):
    """The engine calls this function at the start of the game in order to:
        -tell you your player id (0 through 5)
        -tell you how many players there are (1 through 6)
        -tell you what your start tile is (a letter a through i)
        -give you an instance of the logger (use logger.write("str") 
            to log a message) (use of this is optional)
        -inform you of an additional argument passed 
            via the config file (use of this is optional)
        
    Parameters:
        playerId - your player id (0-5)
        numPlayers - the number of players in the game (1-6)
        startTile - the letter of your start tile (a-j)
        logger - and instance of the logger object
        arg - an extra argument as specified via the config file (optional)

    You return:
        playerData - your player data, which is any data structure
                     that contains whatever you need to keep track of.
                     Consider this your permanent state.
    """
    
    # Put your data in here.  
    # This will be permanently accessible by you in all functions.
    # It can be an object, list, or dictionary
    
    playerData = PlayerData(logger, playerId, startTile, numPlayers, 0)
    
    # initializes the power station 
    playerData.board[4][4] = Tile('ps', 0,None,(4,4))
    playerData.board[4][3] = Tile('ps', 0,None,(4,3))
    playerData.board[3][4] = Tile('ps', 0,None,(3,4))
    playerData.board[3][3] = Tile('ps', 0,None,(3,3))
    
    # This is how you write data to the log file
    playerData.logger.write("Player %s starting up" % playerId)
    
    # This is how you print out your data to standard output (not logged)
    print(playerData)
    
    return playerData

def move(playerData):  
    """The engine calls this function when it wants you to make a move.
    
    Parameters:
        playerData - your player data, 
            which contains whatever you need to keep track of
        
    You return:
        playerData - your player data, 
            which contains whatever you need to keep track of
        playerMove - your next move
    """
    
    playerData.logger.write("move() called")
    # Populate these values
    emptyspot = []
    for r in range(len(playerData.board)):
        for c in range(len(playerData.board)):
            if tile_info_at_coordinates(playerData,r,c) == False:
                emptyspot.append((r,c))
    validspots = isvalid(playerData,emptyspot)
    move = block_and_extend(playerData,validspots,emptyspot)
    if move == False:
        move = edge_block_strategy(playerData,validspots)
    if move == False:
        move = extend_strategy(playerData,validspots)
    if move == False:
        move = random_strategy(playerData,validspots,emptyspot)
    playerData.board[move.position[0]][move.position[1]] = Tile(move.tileName,move.rotation,None,move.position)
    return playerData, move

def scan_board(playerData,stations):
    """Updates the current route info leading out of that station. Returns a list of the route of
    the enemy player. The list tuple in the list is the next spot leading out of the route and is 
    empty.
    playerData -> playerData"""
    routes = dict()
    for es in stations:
        route_complete(playerData,es)
        if es >= 0 and es <= 8:
            if playerData.board[0][es-1]  == 0:
                routes[es] = 0
                continue
            else:
                routes[es] = playerData.board[0][es -1].station.routepos
        elif es >=9 and es <= 16:
            if playerData.board[es-9][7] == 0:
                routes[es] = 0
                continue
            else:
                routes[es] = playerData.board[es-9][7].station.routepos
        elif es >= 17 and es <= 24:
            if playerData.board[7][abs(es-24)] == 0:
                routes[es] = 0
                continue
            else:
                routes[es] = playerData.board[7][abs(es-24)].station.routepos
        elif es >= 25 and es <= 32:
            if playerData.board[abs(es-32)][0] == 0:
                routes[es] = 0
                continue
            else:
                routes[es] = playerData.board[abs(es-32)][0].station.routepos
    return routes

def check_around(playerData,r,c):
    """Checks the adjacent tiles to make sure that it is a valid placement. If not near the powerstation 
    then it checks all 4 possible adjacent tiles. Doesnt check the edges.
    playerData -> playerData
    r-> row
    c-> col
    return a boolean"""
    valid = True
    if (r,c) == (3,2) or (r,c) == (4,2):
        if tile_info_at_coordinates(playerData,r,c-1) == False and tile_info_at_coordinates(playerData,r-1,c) == False \
            and tile_info_at_coordinates(playerData,r+1,c) == False:
            valid = False
    elif (r,c) == (5,3) or (r,c) == (5,4):
        if tile_info_at_coordinates(playerData,r,c+1) == False and tile_info_at_coordinates(playerData,r,c-1) == False \
            and tile_info_at_coordinates(playerData,r+1,c) == False:
            valid = False
    elif (r,c) == (3,5) or (r,c) == (4,5):
        if tile_info_at_coordinates(playerData,r,c+1) == False and tile_info_at_coordinates(playerData,r+1,c) == False \
            and tile_info_at_coordinates(playerData,r-1,c) == False:
            valid = False
    elif (r,c) == (2,3) or (r,c) == (2,4):
        if tile_info_at_coordinates(playerData,r,c+1) == False and tile_info_at_coordinates(playerData,r,c-1) == False \
            and tile_info_at_coordinates(playerData,r-1,c) == False:
            valid = False
    if r != 0 and r != 7 and c != 0 and c != 7:
        if tile_info_at_coordinates(playerData,r,c+1) == False and tile_info_at_coordinates(playerData,r,c-1) == False \
            and tile_info_at_coordinates(playerData,r+1,c) == False and tile_info_at_coordinates(playerData,r-1,c) == False:
            valid = False
    return valid
            
def isvalid(playerData, emptyspots):
    """Checks the validity of the tile at every empty spot and returns a list of valid tile
    placements.
    playerData -> PlayerData
    emptyspots -> list of empty positions"""
    validspots = []
    curtile = playerData.currentTile
    for empty in emptyspots:
        if curtile == 'a':
            if empty == (0,0) or empty == (7,0) or empty == (0,7) or empty == (7,7):
                continue
            elif empty[0] == 0 or empty[0] == 7:
                validspots.append([(empty[0],empty[1]),curtile,(1,3)])
            else:
                if check_around(playerData,empty[0],empty[1]) == True:
                    validspots.append([(empty[0],empty[1]),curtile,(0,1,2,3)])
        elif curtile == 'b':
            if empty == (0,7) or empty == (7,0):
                validspots.append([(empty[0],empty[1]),curtile,(1,3)])
            elif empty == (0,0) or empty == (0,7):
                validspots.append([(empty[0],empty[1]),curtile,(0,2)])
            else:
                if check_around(playerData,empty[0],empty[1]) == True:
                    validspots.append([(empty[0],empty[1]),curtile,(0,1,2,3)])
        elif curtile == 'c':
            if empty == (0,0) or empty == (0,7) or empty == (7,0) or empty == (7,7):
                continue
            elif empty[1] == 0:
                validspots.append([(empty[0],empty[1]),curtile,(1,2,3)])
            else:
                if check_around(playerData,empty[0],empty[1]) == True:
                    validspots.append([(empty[0],empty[1]),curtile,(0,1,2,3)])
        elif curtile == 'd':
            if empty == (0,0) or empty == (0,7) or empty == (7,0) or empty == (7,7):
                continue
            elif empty[0] == 0:
                validspots.append([(empty[0],empty[1]),curtile,(1,2,3)])
            else:
                if check_around(playerData,empty[0],empty[1]) == True:
                    validspots.append([(empty[0],empty[1]),curtile,(0,1,2,3)])
        elif curtile == 'e':
            if empty == (0,0) or empty == (0,7) or empty == (7,0) or empty == (7,7):
                continue
            elif empty[0] == 0:
                validspots.append([(empty[0],empty[1]),curtile,(1,2)])
            elif empty[1] == 7:
                validspots.append([(empty[0],empty[1]),curtile,(2,3)])
            else:
                if check_around(playerData,empty[0],empty[1]) == True:
                    validspots.append([(empty[0],empty[1]),curtile,(0,1,2,3)])
        elif curtile == 'f':
            if check_around(playerData,empty[0],empty[1]) == True:
                validspots.append([(empty[0],empty[1]),curtile,(0,1,2,3)])
        elif curtile == 'g':
            if empty[0] == 0 or empty[0] == 7 or empty[1] == 0 or empty[1] == 7:
                continue
            else:
                if check_around(playerData,empty[0],empty[1]) == True:
                    validspots.append([(empty[0],empty[1]),curtile,(0,1,2,3)])
        elif curtile == 'h':
            if empty == (0,0) or empty == (0,7) or empty == (7,0) or empty == (7,7):
                continue
            else:
                if check_around(playerData,empty[0],empty[1]) == True:
                    validspots.append([(empty[0],empty[1]),curtile,(0,1,2,3)])
        elif curtile == 'i':
            if empty == (0,0) or empty == (0,7) or empty == (7,0) or empty == (7,7):
                continue
            else:
                if check_around(playerData,empty[0],empty[1]) == True:
                    validspots.append([(empty[0],empty[1]),curtile,(0,1,2,3)])
        elif curtile == 'j':
            if empty == (0,0) or empty == (7,7):
                validspots.append([(empty[0],empty[1]),curtile,(1,3)])
            elif empty == (0,7) or empty == (7,0):
                validspots.append([(empty[0],empty[1]),curtile,(0,2)])
            else:
                if check_around(playerData,empty[0],empty[1]) == True:
                    validspots.append([(empty[0],empty[1]),curtile,(0,1,2,3)])
    return validspots

def checksides(playerData,r,c):
    """Function that checks the sides of a tile to see if they are empty and returns 
    which sides are empty. 
    playerData -> playerData
    r -> current row
    c -> current col"""
    if (r,c) == (0,0) or (r,c) == (7,0):
        if tile_info_at_coordinates(playerData,r,c+1) != False:
            return (True,True)
        return (True,False)
    elif (r,c) == (0,7) or (r,c) == (7,7):
        if tile_info_at_coordinates(playerData,r,c-1) != False:
            return (True,True)
        return (False,True)
    elif c-1 == 0:
        if tile_info_at_coordinates(playerData,r,c+1) != False:
            return (True,True)
        return (True,False)
    elif c+1 == 7:
        if tile_info_at_coordinates(playerData,r,c-1) != False:
            return (True,True)
        return (False,True)
    else:
        if tile_info_at_coordinates(playerData,r,c-1) != False:
            left = True
        else:
            left = False
        if tile_info_at_coordinates(playerData,r,c+1) != False:
            right = True
        else:
            right = False
        return(left,right)

def checktops(playerData,r,c,):
    """Function that checks the top and bottom of a tile to see if they are empty and returns 
    which are empty. 
    playerData -> playerData 
    r -> row of current tile
    c -> c of current tile
    returns a true or false for both the top and bottom tiles that it checks."""
    if (r,c) == (0,0) or (r,c) == (0,7):
        if tile_info_at_coordinates(playerData,r+1,c) != False:
            return (True,True)
        return (True,False)
    elif (r,c) == (7,0) or (r,c) == (7,7):
        if tile_info_at_coordinates(playerData,r-1,c) != False:
            return (True,True)
        return (False,True)
    elif r -1== 0:
        if tile_info_at_coordinates(playerData,r+1,c) != False:
            return (True,True)
        return (True,False)
    elif r +1== 7:
        if tile_info_at_coordinates(playerData,r-1,c) != False:
            return (True,True)
        return (False,True)
    elif r!= 0 and r != 7:
        if tile_info_at_coordinates(playerData,r-1,c) != False:
            top = True
        else:
            top = False
        if tile_info_at_coordinates(playerData,r+1,c) != False:
            bottom = True
        else:
            bottom = False
        return (top,bottom)
    
def random_strategy(playerData,validspots,emptyspot):
    """Randomly places a tile at a valid spot..
    playerData -> playerData
    validspots -> list of valid tile placements
    emptyspot -> list of emptytiles
    returns a move at a random tile."""
    if len(validspots) > 0:
        index = randrange(len(validspots))
    else:
        index = [0]
    playerId = playerData.playerId
    tileName = playerData.currentTile
    if len(validspots) == 0:
        position = emptyspot[0]
        rotation = 1
    else:
        position = validspots[index][0]
        rotation = validspots[index][2][0]
    move = PlayerMove(playerId, position, tileName, rotation)
    return move
                      
    
def edge_block_strategy(playerData,validspots):
    """Blocks oppenent on the edges.
    playerData - > playerData
    validspots -> list of valid spots
    Returns a move if it can block an oppenent returns false if it can't."""
    edgeblock = []
    edgemoves = []
    curtile = playerData.currentTile
    playerstations = playerData.ownedstations[playerData.playerId+1]
    enemystations  = []
    for i in range(1,33):
        if i not in playerstations:
            enemystations.append(int(i))
    enemyroutes = scan_board(playerData,enemystations)
    if curtile == 'a' or curtile == 'f':
        return False
    for r in range(len(playerData.board)):
        for c in range(len(playerData.board)):
            if r == 0 or r == 7 or c == 0 or c == 7:
                if tile_info_at_coordinates(playerData,r,c) != False:
                    edgeblock.append(((r,c),tile_info_at_coordinates(playerData,r,c)[0],tile_info_at_coordinates(playerData,r,c)[1]))
    if len(edgeblock) == 0:
        return False
    else:
        enemystart = []
        for key in enemyroutes:
            if enemyroutes[key] == 0:
                continue
            else:
                if enemyroutes[key][-1:] == ['connected']:
                    continue
                else:
                    enemystart.append(enemyroutes[key][-2])
        for i in enemystart:
            if i[0] == 0: 
                if checksides(playerData,i[0],i[1]) == (False,False):
                    if curtile != 'g':
                        if playerData.board[i[0]][i[1]].station.exitpos == 3:
                            if curtile == 'b':
                                edgemoves.append(((i[0],i[1]+1),curtile,(3)))
                            elif curtile == 'c':
                                edgemoves.append(((i[0],i[1]+1),curtile,(2,3)))
                            elif curtile == 'e':
                                edgemoves.append(((i[0],i[1]+1),curtile,(1)))
                            elif curtile == 'i':
                                edgemoves.append(((i[0],i[1]+1),curtile,(0,1,2,3)))
                            elif curtile == 'j':
                                edgemoves.append(((i[0],i[1]+1),curtile,(0,2)))
                        elif playerData.board[i[0]][i[1]].station.exitpos == 7:
                            if curtile == 'b':
                                edgemoves.append(((i[0],i[1]-1),curtile,(2)))
                            elif curtile == 'd':
                                edgemoves.append(((i[0],i[1]-1),curtile,(2,3)))
                            elif curtile == 'e':
                                edgemoves.append(((i[0],i[1]-1),curtile,(2)))
                            elif curtile == 'j':
                                edgemoves.append(((i[0],i[1]-1),curtile,(1,3)))
                            elif curtile == 'h':
                                edgemoves.append(((i[0],i[1]-1),curtile,(0,1,2,3)))
                elif checksides(playerData,i[0],i[1]) == (True,False): #right side
                    if curtile != 'g':
                        if playerData.board[i[0]][i[1]].station.exitpos == 3:
                            if curtile == 'b':
                                edgemoves.append(((i[0],i[1]+1),curtile,(3)))
                            elif curtile == 'c':
                                edgemoves.append(((i[0],i[1]+1),curtile,(2,3)))
                            elif curtile == 'e':
                                edgemoves.append(((i[0],i[1]+1),curtile,(1)))
                            elif curtile == 'i':
                                edgemoves.append(((i[0],i[1]+1),curtile,(0,1,2,3)))
                            elif curtile == 'j':
                                edgemoves.append(((i[0],i[1]+1),curtile,(0,2)))
                elif checksides(playerData,i[0],i[1]) == (False,True): # left side
                    if curtile != 'g':
                        if playerData.board[i[0]][i[1]].station.exitpos == 7:
                            if curtile == 'b':
                                edgemoves.append(((i[0],i[1]-1),curtile,(2)))
                            elif curtile == 'd':
                                edgemoves.append(((i[0],i[1]-1),curtile,(2,3)))
                            elif curtile == 'e':
                                edgemoves.append(((i[0],i[1]-1),curtile,(2)))
                            elif curtile == 'j':
                                edgemoves.append(((i[0],i[1]-1),curtile,(1,3)))
                            elif curtile == 'h':
                                edgemoves.append(((i[0],i[1]-1),curtile,(0,1,2,3)))
            if i[0] == 7: 
                if checksides(playerData,i[0],i[1]) == (False,False):
                    if curtile != 'g':
                        if playerData.board[i[0]][i[1]].station.exitpos == 3:
                            if curtile == 'b':
                                edgemoves.append(((i[0],i[1]+1),curtile,(0)))
                            elif curtile == 'd':
                                edgemoves.append(((i[0],i[1]+1),curtile,(0,1)))
                            elif curtile == 'e':
                                edgemoves.append(((i[0],i[1]+1),curtile,(0)))
                            elif curtile == 'h':
                                edgemoves.append(((i[0],i[1]+1),curtile,(0,1,2,3)))
                            elif curtile == 'j':
                                edgemoves.append(((i[0],i[1]+1),curtile,(1,3)))
                        elif playerData.board[i[0]][i[1]].station.exitpos == 7:
                            if curtile == 'b':
                                edgemoves.append(((i[0],i[1]-1),curtile,(1)))
                            elif curtile == 'c':
                                edgemoves.append(((i[0],i[1]-1),curtile,(0,1)))
                            elif curtile == 'e':
                                edgemoves.append(((i[0],i[1]-1),curtile,(3)))
                            elif curtile == 'i':
                                edgemoves.append(((i[0],i[1]-1),curtile,(0,1,2,3)))
                            elif curtile == 'j':
                                edgemoves.append(((i[0],i[1]-1),curtile,(0,2)))
                elif checksides(playerData,i[0],i[1]) == (True,False): #right side
                    if curtile != 'g':
                        if playerData.board[i[0]][i[1]].station.exitpos == 3:
                            if curtile == 'b':
                                edgemoves.append(((i[0],i[1]+1),curtile,(0)))
                            elif curtile == 'd':
                                edgemoves.append(((i[0],i[1]+1),curtile,(0,1)))
                            elif curtile == 'e':
                                edgemoves.append(((i[0],i[1]+1),curtile,(0)))
                            elif curtile == 'h':
                                edgemoves.append(((i[0],i[1]+1),curtile,(0,1,2,3)))
                            elif curtile == 'j':
                                edgemoves.append(((i[0],i[1]+1),curtile,(1,3)))
                elif checksides(playerData,i[0],i[1]) == (False,True): #left side
                    if curtile != 'g':
                        if playerData.board[i[0]][i[1]].station.exitpos == 7:
                            if curtile == 'b':
                                edgemoves.append(((i[0],i[1]-1),curtile,(1)))
                            elif curtile == 'c':
                                edgemoves.append(((i[0],i[1]-1),curtile,(0,1)))
                            elif curtile == 'e':
                                edgemoves.append(((i[0],i[1]-1),curtile,(3)))
                            elif curtile == 'i':
                                edgemoves.append(((i[0],i[1]-1),curtile,(0,1,2,3)))
                            elif curtile == 'j':
                                edgemoves.append(((i[0],i[1]-1),curtile,(0,2)))
            if i[1] == 0: 
                if checktops(playerData,i[0],i[1]) == (False,False):
                    if curtile != 'g':
                        if playerData.board[i[0]][i[1]].station.exitpos == 1: 
                            if curtile == 'b':
                                edgemoves.append(((i[0]-1,i[1]),curtile,(2)))
                            elif curtile == 'c':
                                edgemoves.append(((i[0]-1,i[1]),curtile,(1,2)))
                            elif curtile == 'e':
                                edgemoves.append(((i[0]-1,i[1]),curtile,(0)))
                            elif curtile == 'i':
                                edgemoves.append(((i[0]-1,i[1]),curtile,(0,1,2,3)))
                            elif curtile == 'j':
                                edgemoves.append(((i[0]-1,i[1]),curtile,(1,3)))
                        elif playerData.board[i[0]][i[1]].station.exitpos == 5:
                            if curtile == 'b':
                                edgemoves.append(((i[0]+1,i[1]),curtile,(1)))
                            elif curtile == 'd':
                                edgemoves.append(((i[0]+1,i[1]),curtile,(1,2)))
                            elif curtile == 'e':
                                edgemoves.append(((i[0]+1,i[1]),curtile,(1)))
                            elif curtile == 'h':
                                edgemoves.append(((i[0]+1,i[1]),curtile,(0,1,2,3)))
                            elif curtile == 'j':
                                edgemoves.append(((i[0]+1,i[1]),curtile,(0,2)))
                if checktops(playerData,i[0],i[1]) == (False,True): # top
                    if curtile != 'g':
                        if playerData.board[i[0]][i[1]].station.exitpos == 1:
                            if curtile == 'b':
                                edgemoves.append(((i[0]-1,i[1]),curtile,(2)))
                            elif curtile == 'c':
                                edgemoves.append(((i[0]-1,i[1]),curtile,(1,2)))
                            elif curtile == 'e':
                                edgemoves.append(((i[0]-1,i[1]),curtile,(0)))
                            elif curtile == 'i':
                                edgemoves.append(((i[0]-1,i[1]),curtile,(0,1,2,3)))
                            elif curtile == 'j':
                                edgemoves.append(((i[0]-1,i[1]),curtile,(1,3)))
                elif checktops(playerData,i[0],i[1]) == (True,False): #bottom 
                    if curtile != 'g':
                        if playerData.board[i[0]][i[1]].station.exitpos == 5:
                            if curtile == 'b':
                                edgemoves.append(((i[0]+1,i[1]),curtile,(1)))
                            elif curtile == 'd':
                                edgemoves.append(((i[0]+1,i[1]),curtile,(1,2)))
                            elif curtile == 'e':
                                edgemoves.append(((i[0]+1,i[1]),curtile,(1)))
                            elif curtile == 'h':
                                edgemoves.append(((i[0]+1,i[1]),curtile,(0,1,2,3)))
                            elif curtile == 'j':
                                edgemoves.append(((i[0]+1,i[1]),curtile,(0,2)))
            if i[1] == 7: 
                if checktops(playerData,i[0],i[1]) == (False,False):
                    if curtile != 'g':
                        if playerData.board[i[0]][i[1]].station.exitpos == 1:
                            if curtile == 'b':
                                edgemoves.append(((i[0]-1,i[1]),curtile,(3)))
                            elif curtile == 'd':
                                edgemoves.append(((i[0]-1,i[1]),curtile,(0,3)))
                            elif curtile == 'e':
                                edgemoves.append(((i[0]-1,i[1]),curtile,(3)))
                            elif curtile == 'h':
                                edgemoves.append(((i[0]-1,i[1]),curtile,(0,1,2,3)))
                            elif curtile == 'j':
                                edgemoves.append(((i[0]-1,i[1]),curtile,(0,2)))
                        elif playerData.board[i[0]][i[1]].station.exitpos == 5:
                            if curtile == 'b':
                                edgemoves.append(((i[0]+1,i[1]),curtile,(0)))
                            elif curtile == 'c':
                                edgemoves.append(((i[0]+1,i[1]),curtile,(0,3)))
                            elif curtile == 'e':
                                edgemoves.append(((i[0]+1,i[1]),curtile,(2)))
                            elif curtile == 'i':
                                edgemoves.append(((i[0]+1,i[1]),curtile,(0,1,2,3)))
                            elif curtile == 'j':
                                edgemoves.append(((i[0]+1,i[1]),curtile,(1,3)))
                elif checktops(playerData,i[0],i[1]) == (False,True): #top
                    if curtile != 'g':
                        if playerData.board[i[0]][i[1]].station.exitpos == 1:
                            if curtile == 'b':
                                edgemoves.append(((i[0]-1,i[1]),curtile,(3)))
                            elif curtile == 'd':
                                edgemoves.append(((i[0]-1,i[1]),curtile,(0,3)))
                            elif curtile == 'e':
                                edgemoves.append(((i[0]-1,i[1]),curtile,(3)))
                            elif curtile == 'h':
                                edgemoves.append(((i[0]-1,i[1]),curtile,(0,1,2,3)))
                            elif curtile == 'j':
                                edgemoves.append(((i[0]-1,i[1]),curtile,(0,2)))
                elif checktops(playerData,i[0],i[1]) == (True,False): #bottom
                    if curtile != 'g':
                        if playerData.board[i[0]][i[1]].station.exitpos == 5:
                            if curtile == 'b':
                                edgemoves.append(((i[0]+1,i[1]),curtile,(0)))
                            elif curtile == 'c':
                                edgemoves.append(((i[0]+1,i[1]),curtile,(0,3)))
                            elif curtile == 'e':
                                edgemoves.append(((i[0]+1,i[1]),curtile,(2)))
                            elif curtile == 'i':
                                edgemoves.append(((i[0]+1,i[1]),curtile,(0,1,2,3)))
                            elif curtile == 'j':
                                edgemoves.append(((i[0]+1,i[1]),curtile,(1,3)))
    if len(edgemoves) == 0:
        return False
    else:
        random = randrange(len(edgemoves))
        position = (edgemoves[random][0][0],edgemoves[random][0][1])
        if edgemoves[random][2] == 0:
            rotation = 0
        elif edgemoves[random][2] == 1:
            rotation = 1
        elif edgemoves[random][2] == 2:
            rotation = 2
        elif edgemoves[random][2] == 3:
            rotation = 3
        else:
            rotation = edgemoves[random][2][0]
        move = PlayerMove(playerData.playerId,position,curtile,rotation)
        return move
                        
def extend_strategy(playerData,validspots):
    """Strategy to extend the route if blocking is not possible.
    playerData -> playerData
    validspots -> lists of valid spots
    Extends the playerroute.
    Returns false if it cant make a move"""
    extendmoves = []
    curtile = playerData.currentTile
    if curtile == 'g':
        return False
    playerkeys = playerData.ownedstations[playerData.playerId+1]
    playerroutes = scan_board(playerData,playerkeys)
    nextmoves = []
    for key in playerroutes:
        if playerroutes[key] == 0:
            continue
        elif playerroutes[key][-1:] == ['connected']:
            continue
        else:
            nextmoves.append((playerroutes[key][-2]))
    for i in nextmoves:
        row = i[0]
        col = i[1]
        exitpos = playerData.board[row][col].station.exitpos
        if exitpos == 0 or exitpos == 1:
            if row == 0 or row == 1 or col == 0 or col == 7:
                continue
            elif tile_info_at_coordinates(playerData,row-1,col) == False:
                if curtile == 'a':
                    extendmoves.append(((row-1,col),curtile,(1,3)))
                elif curtile == 'c':
                    extendmoves.append(((row-1,col),curtile,(0,1,2)))
                elif curtile == 'd':
                    extendmoves.append(((row-1,col),curtile,(0,1,3)))
                elif curtile == 'e':
                    extendmoves.append(((row-1,col),curtile,(0,3)))
                else:
                    extendmoves.append(((row-1,col),curtile,(0,1,2,3)))
        elif exitpos == 2 or exitpos == 3:
            if row == 0 or row == 7 or col == 7 or col == 6:
                continue
            elif tile_info_at_coordinates(playerData,row,col+1) == False:
                if curtile == 'a':
                    extendmoves.append(((row,col+1),curtile,(0,2)))
                elif curtile == 'c':
                    extendmoves.append(((row,col+1),curtile,(1,2,3)))
                elif curtile == 'd':
                    extendmoves.append(((row,col+1),curtile,(0,1,2)))
                elif curtile == 'e':
                    extendmoves.append(((row,col+1),curtile,(0,1,)))
                else:
                    extendmoves.append(((row,col+1),curtile,(0,1,2,3)))
        elif exitpos == 4 or exitpos == 5:
            if row == 7 or col == 0 or col == 7 or row == 6:
                continue
            elif tile_info_at_coordinates(playerData,row+1,col) == False:
                if curtile == 'a':
                    extendmoves.append(((row+1,col),curtile,(1,3)))
                elif curtile == 'c':
                    extendmoves.append(((row+1,col),curtile,(0,2,3)))
                elif curtile == 'd':
                    extendmoves.append(((row+1,col),curtile,(1,2,3)))
                elif curtile == 'e':
                    extendmoves.append(((row+1,col),curtile,(1,2)))
                else:
                    extendmoves.append(((row+1,col),curtile,(0,1,2,3)))
        elif exitpos == 6 or exitpos == 7:
            if col == 0 or col == 1 or row == 0 or row == 7:
                continue
            elif tile_info_at_coordinates(playerData,row,col-1) == False:
                if curtile == 'a':
                    extendmoves.append(((row,col-1),curtile,(0,2)))
                elif curtile == 'c':
                    extendmoves.append(((row,col-1),curtile,(1,2,3)))
                elif curtile == 'd':
                    extendmoves.append(((row,col-1),curtile,(0,1,2)))
                elif curtile == 'e':
                    extendmoves.append(((row,col-1),curtile,(0,1,)))
                else:
                    extendmoves.append(((row,col-1),curtile,(0,1,2,3)))
    if len(extendmoves) == 0:
        return False
    else:
        random = randrange(len(extendmoves))
        position = (extendmoves[random][0])
        ranrotation = randrange(len(extendmoves[random][2]))
        rotation = extendmoves[random][2][ranrotation]
        move = PlayerMove(playerData.playerId,position,curtile,rotation)
        return move
                
        

def block_and_extend(playerData,validspots,emptyspot):
    """Best possible strategy blocks oppenent and extends route only on the edges.
    playerData -> playerData
    validspots -> list of valid spots
    Returns false if it can't make move"""
    curtile = playerData.currentTile 
    blockandextend = []
    playerkeys = playerData.ownedstations[playerData.playerId +1]
    enemystations = []
    for i in range(1,33):
        if i not in playerkeys:
            enemystations.append(int(i))
    enemyroutes = scan_board(playerData,enemystations)
    playerroutes = scan_board(playerData,playerkeys)
    keys = list(enemyroutes.copy().keys()) 
    es = keys[1]
    if len(emptyspot) == 60:
        if curtile == 'g':
            if es > 1 and es < 8:
                position = (0,es-1)
                move = PlayerMove(playerData.playerId,position,playerData.currentTile,0)
                return move
            elif es >9 and es < 16:
                position = (es-9,7) 
                move = PlayerMove(playerData.playerId,position,playerData.currentTile,0)
                return move
            elif es > 17 and es < 24:
                position = (7,abs(es-24))
                move = PlayerMove(playerData.playerId,position,playerData.currentTile,0)
                return move
            elif es > 25 and es < 32:
                position = (abs(es-32),0)
                move = PlayerMove(playerData.playerId,position,playerData.currentTile,0)
                return move
    else:
        lastenemypositions = []
        for key in enemyroutes:
            if enemyroutes[key] == 0:
                continue
            elif enemyroutes[key][-1:] == ['connected']:
                continue
            else:
                lastenemypositions.append((enemyroutes[key][-2]))
        for i in lastenemypositions:
            row = i[0]
            col = i[1]
            exitpos = playerData.board[row][col].station.exitpos
            if row == 0 and col != 7 and col != 0:
                if (row,col+1) != (0,7) and (row,col-1) != (0,0):
                    if exitpos == 4 or exitpos == 5:
                            if tile_info_at_coordinates(playerData,row+1,col) == False:
                                if curtile == 'a':
                                    blockandextend.append(((row+1,col),curtile,(0,2)))
                                elif curtile == 'c':
                                    blockandextend.append(((row+1,col),curtile,(1)))
                                elif curtile == 'd':
                                    blockandextend.append(((row+1,col),curtile,(0)))
                                elif curtile == 'e':
                                    blockandextend.append(((row+1,col),curtile,(0,3)))
                                elif curtile == 'g':
                                    blockandextend.append(((row+1,col),curtile,(0,1,2,3)))
                    elif exitpos == 7: 
                        if tile_info_at_coordinates(playerData,row,col-1) == False:
                            if curtile == 'b':
                                blockandextend.append(((row,col-1),curtile,(2)))
                            elif curtile == 'd':
                                blockandextend.append(((row,col-1),curtile,(3)))
                    elif exitpos == 3:
                        if tile_info_at_coordinates(playerData,row,col+1) == False:
                            if curtile == 'b':
                                blockandextend.append(((row,col+1),curtile,(3)))
                            elif curtile == 'a':
                                blockandextend.append(((row,col+1),curtile,(1,3)))
                            elif curtile == 'c':
                                blockandextend.append(((row,col+1),curtile,(2)))
            elif row == 7 and col != 0 and col != 7:
                if (row,col+1) != (7,7) and (row,col-1) != (7,0):
                    if exitpos == 0 or exitpos == 1:
                        if tile_info_at_coordinates(playerData,row-1,col) == False:
                            if curtile == 'a':
                                blockandextend.append(((row-1,col),curtile,(0,2)))
                            elif curtile == 'c':
                                blockandextend.append(((row-1,col),curtile,(3)))
                            elif curtile == 'd':
                                blockandextend.append(((row-1,col),curtile,(2)))
                            elif curtile == 'e':
                                blockandextend.append(((row-1,col),curtile,(1,2)))
                            elif curtile == 'g':
                                blockandextend.append(((row-1,col),curtile,(0,1,2,3)))
                    elif exitpos == 7:
                        if tile_info_at_coordinates(playerData,row,col-1) == False:
                            if curtile == 'b':
                                blockandextend.append(((row,col-1),curtile,(1)))
                            elif curtile == 'c':
                                blockandextend.append(((row,col-1),curtile,(0)))
                    elif exitpos == 3:
                        if tile_info_at_coordinates(playerData,row,col+1) == False:
                            if curtile == 'b':
                                blockandextend.append(((row,col+1),curtile,(0)))
                            elif curtile == 'd':
                                blockandextend.append(((row,col+1),curtile,(1)))
            elif col == 0 and row != 0 and row != 7:
                if (row+1,col) != (0,0) and (row-1,col) != (7,0):
                    if exitpos == 2 or exitpos == 3:
                        if tile_info_at_coordinates(playerData,row,col+1) == False:
                            if curtile == 'a':
                                blockandextend.append(((row,col+1),curtile,(1,3)))
                            elif curtile == 'c':
                                blockandextend.append(((row,col+1),curtile,(0)))
                            elif curtile == 'd':
                                blockandextend.append(((row,col+1),curtile,(3)))
                            elif curtile == 'e':
                                blockandextend.append(((row,col+1),curtile,(2,3)))
                            elif curtile == 'g':
                                blockandextend.append(((row,col+1),curtile,(0,1,2,3)))
                    elif exitpos == 0:
                        if tile_info_at_coordinates(playerData,row-1,col) == False:
                            if curtile == 'b':
                                blockandextend.append(((row-1,col),curtile,(1)))
                            elif curtile == 'd':
                                blockandextend.append(((row-1,col),curtile,(2)))
                    elif exitpos == 4:
                        if tile_info_at_coordinates(playerData,row+1,col) == False:
                            if curtile == 'b':
                                blockandextend.append(((row+1,col),curtile,(3)))
                            elif curtile == 'c':
                                blockandextend.append(((row+1,col),curtile,(3)))
            elif col == 7 and row != 0 and row != 7:
                if (row-1,col) != (0,7) and (row+1,col) != (7,7):
                    if exitpos == 7 or exitpos == 6:
                        if tile_info_at_coordinates(playerData,row,col-1) == False:
                            if curtile == 'a':
                                blockandextend.append(((row,col-1),curtile,(1,3)))
                            elif curtile == 'c':
                                blockandextend.append(((row,col-1),curtile,(2)))
                            elif curtile == 'd':
                                blockandextend.append(((row,col-1),curtile,(1)))
                            elif curtile == 'e':
                                blockandextend.append(((row,col-1),curtile,(0,1)))
                            elif curtile == 'g':
                                blockandextend.append(((row,col-1),curtile,(0,1,2,3)))
                    elif exitpos == 1:
                        if tile_info_at_coordinates(playerData,row-1,col) == False:
                            if curtile == 'b':
                                blockandextend.append(((row-1,col),curtile,(3)))
                            elif curtile == 'd':
                                blockandextend.append(((row-1,col),curtile,(0)))
                    elif exitpos == 5:
                        if tile_info_at_coordinates(playerData,row+1,col) == False:
                            if curtile == 'b':
                                blockandextend.append(((row+1,col),curtile,(0)))
                            elif curtile == 'c':
                                blockandextend.append(((row+1,col),curtile,(3)))
    if len(blockandextend) == 0:
        return False
    else:
        random = randrange(len(blockandextend))
        position = blockandextend[random][0]
        if blockandextend[random][2] == 0:
            rotation = 0
        elif blockandextend[random][2] == 1:
            rotation = 1
        elif blockandextend[random][2] == 2:
            rotation = 2
        elif blockandextend[random][2] == 3:
            rotation = 3
        else:
            ranrotation = randrange(len(blockandextend[random][2]))
            rotation = blockandextend[random][2][ranrotation]
        move = PlayerMove(playerData.playerId,position,curtile,rotation)
        return move

       
def move_info(playerData, playerMove, nextTile):
    """The engine calls this function to notify you of:
        -other players' moves
        -your and other players' next tiles
        
    The function is called with your player's data, as well as the valid move of
    the other player.  Your updated player data should be returned.
    
    Parameters:
        playerData - your player data, 
            which contains whatever you need to keep track of
        playerMove - the move of another player in the game, or None if own move
        nextTile - the next tile for the player specified in playerMove, 
                    or if playerMove is None, then own next tile
                    nextTile can be none if we're on the last move
    You return:
        playerData - your player data, 
            which contains whatever you need to keep track of
    """
    if playerMove == None:
        playerData.currentTile = nextTile
    else:
        playerData.board[playerMove.position[0]][playerMove.position[1]] = Tile(playerMove.tileName, playerMove.rotation, None, playerMove.position)
        playerData.logger.write("move_info() called")
    
    return playerData


################################# PART ONE FUNCTIONS #######################
# These functions are called by the engine during part 1 to verify your board 
# data structure
# If logging is enabled, the engine will tell you exactly which tests failed
# , if any

def tile_info_at_coordinates(playerData, row, column):
    """The engine calls this function during 
        part 1 to validate your board state.
    
    Parameters:
        playerData - your player data as always
        row - the tile row (0-7)
        column - the tile column (0-7)
    
    You return:
        tileName - the letter of the tile at the given coordinates (a-j), 
            or 'ps' if power station or None if no tile
        tileRotation - the rotation of the tile 
            (0 is north, 1 is east, 2 is south, 3 is west.
            If the tile is a power station, it should be 0.  
            If there is no tile, it should be None.
    """      
        
    Info = playerData.board[row][column]
    if Info == 0:
        return False
    elif Info != 0:
        tilename = Info.tileId
        tilerotation = Info.rotation
        return tilename, tilerotation


def route_complete(playerData, carId):
    """The engine calls this function 
        during part 1 to validate your route checking
    
    Parameters:
        playerData - your player data as always
        carId - the id of the car where the route starts (1-32)
        
    You return:
        isComplete - true or false depending on whether or not this car
             connects to another car or power station"""
   
    #gets the first starting tile in front of the current station
    if carId >= 0 and carId <= 8:
        entrance = playerData.board[0][carId -1]
        temppos = (0,carId-1)
        if entrance == 0:
            return False
        entrance.position = 0
        entrance.station.score = 0
    elif carId >=9 and carId <= 16:
        entrance = playerData.board[carId-9][7]
        temppos = (carId-9,7)
        if entrance == 0:
            return False
        entrance.position = 2
        entrance.station.score = 0
    elif carId >= 17 and carId <= 24:
        entrance = playerData.board[7][abs(carId-24)]
        temppos = (7,abs(carId-24))
        if entrance == 0:
            return False
        entrance.position = 4
        entrance.station.score = 0
    elif carId >= 25 and carId <= 32:
        entrance = playerData.board[abs(carId-32)][0]
        temppos = (abs(carId-32),0)
        if entrance == 0:
            return False
        entrance.position = 6
        entrance.station.score = 0
    entrance.station.stationId = carId
    firsttile = entrance   # firsttile is just used to record the score of every route so it can be referenced later
    firsttile.station.score += 1
    del firsttile.station.routepos[:]
    firsttile.station.routepos.append(temppos)
    if entrance is 0:
        firsttile.station.score = 0
        complete = False
    else:
        complete = checkcomplete(carId, entrance, playerData, firsttile)

    return complete

def checkcomplete(carId, entrance, playerData, firsttile):
    """Check complete traverses through the tiles by recursion.
    carId -> int
    entrance -> Tile object of current tile position
    playerData -> playerData as usual
    firsttile -> only used to score the tile score in the first tile"""
    
    
    if entrance.tileId is None:
        return  
     
    elif entrance.tileId == 'ps': #if at a powerstation double score 
        firsttile.station.score -= 1
        firsttile.station.routepos.append("connected")
        firsttile.station.score = firsttile.station.score * 2
        return True
    
    
    elif entrance.position == 0 or entrance.position == 1:
        exitpos = entrance.tracktiles[entrance.tileId][entrance.position][1] 
        entrance.station.exitpos = exitpos   #gets the exitpos of the tile
        newentrance = tilelocation(entrance, exitpos, playerData,firsttile) #gets the next tile depending on the exitpos
        if newentrance == True: # if it returns true it means that the tile is out of bounds so it is connected back to a station
            return True
        elif newentrance is 0:
            entrance.station.exitpos = exitpos #means route is incomplete
            return False 
        else:
            newentrance.position = newposition(exitpos) #entrance position into the next tile 
            firsttile.station.score += 1 #adds one to the firsttile score
        return checkcomplete(carId, newentrance, playerData,firsttile) 
        
        
    elif entrance.position == 2 or entrance.position == 3:
        exitpos = entrance.tracktiles[entrance.tileId][entrance.position][1]
        entrance.station.exitpos = exitpos
        newentrance = tilelocation(entrance,exitpos,playerData,firsttile)
        if newentrance == True:
            entrance.station.exitpos = exitpos
            return True
        elif newentrance is 0:
            return False
        else:
            newentrance.position = newposition(exitpos)
            firsttile.station.score += 1
        return checkcomplete(carId, newentrance, playerData,firsttile) 
        
        
    elif entrance.position == 4 or entrance.position == 5:
        exitpos = entrance.tracktiles[entrance.tileId][entrance.position][1]
        entrance.station.exitpos = exitpos
        newentrance = tilelocation(entrance, exitpos, playerData,firsttile)
        if newentrance == True:
            entrance.station.exitpos = exitpos
            return True
        elif newentrance is 0:
            return False
        else:
            newentrance.position = newposition(exitpos)
            firsttile.station.score += 1
        return checkcomplete(carId, newentrance, playerData,firsttile) 
        
        
    elif entrance.position == 6 or entrance.position == 7:
        exitpos = entrance.tracktiles[entrance.tileId][entrance.position][1]
        entrance.station.exitpos = exitpos
        newentrance = tilelocation(entrance, exitpos, playerData,firsttile)
        if newentrance == True:
            entrance.station.exitpos = exitpos
            return True
        elif newentrance is 0:
            return False
        else:
            newentrance.position = newposition(exitpos)
            firsttile.station.score += 1
        return checkcomplete(carId, newentrance, playerData,firsttile) 
    
    
def newposition(position):
    """Gets the exit position and returns the new starting position of the next tile.
    position -> int"""
    if position == 0:
        nt = 5
    elif position == 1:
        nt = 4
    elif position == 2:
        nt = 7
    elif position == 3:
        nt = 6
    elif position == 4:
        nt = 1
    elif position == 5:
        nt = 0
    elif position == 6:
        nt = 3
    elif position == 7:
        nt = 2
    return nt        
    
def tilelocation(entrance,exitpos,playerData,firsttile):
    """Moves the current tile to the next tile depending on the exit position.
    entrance -> current tile object
    playerData -> playerData as usual"""
    
    if exitpos == 0 or exitpos == 1:
        row = (int(entrance.boardlocation[0])) - 1
        col = (int(entrance.boardlocation[1]))
        if checkbounds(row,col) == True:
            entrance = True
            firsttile.station.routepos.append('connected')
            return entrance
        firsttile.station.routepos.append((row,col))
        entrance = playerData.board[row][col]
    elif exitpos == 2 or exitpos == 3:
        row = (int(entrance.boardlocation[0]))
        col = (int(entrance.boardlocation[1])) + 1
        if checkbounds(row,col) ==  True:
            entrance = True
            firsttile.station.routepos.append('connected')
            return entrance
        firsttile.station.routepos.append((row,col))
        entrance = playerData.board[row][col]
    elif exitpos == 4 or exitpos == 5:
        row = (int(entrance.boardlocation[0])) + 1
        col = (int(entrance.boardlocation[1]))
        if checkbounds(row,col) == True:
            entrance = True
            firsttile.station.routepos.append('connected')
            return entrance
        firsttile.station.routepos.append((row,col))
        entrance = playerData.board[row][col]
    elif exitpos == 6  or exitpos == 7:
        row = (int(entrance.boardlocation[0]))
        col = (int(entrance.boardlocation[1])) - 1
        if checkbounds(row,col) == True:
            entrance = True
            firsttile.station.routepos.append('connected')
            return entrance
        firsttile.station.routepos.append((row,col))
        entrance = playerData.board[row][col]
        
    return entrance

def checkbounds(row,col):
    """Checks to see if the next tile is out of bounds. If it is then it means that the route is 
    connected to another node.
    row -> int 
    col -> int"""
    if row <= -1 or col <= -1 or row >= 8 or col >= 8:
        return True
    else: 
        return False

def route_score(playerData, carId):
    """The engine calls this function 
        during route 1 to validate your route scoring
    
    Parameters:
        playerData - your player data as always
        carId - the id of the car where the route starts (1-32)
        
    You return:
        score - score is the length of the current route from the carId.
                if it reaches the power station, 
                the score is equal to twice the length.
    """
    route_complete(playerData,carId) #refreshes the board because the corner tiles would return different values if there not refreshed
    
    #gets the first starting tile so the score for that route can be referenced
    if carId >= 0 and carId <= 8:
        entrance = playerData.board[0][carId -1]
    elif carId >=9 and carId <= 16:
        entrance = playerData.board[carId-9][7]
    elif carId >= 17 and carId <= 24:
        entrance = playerData.board[7][abs(carId-24)]
    elif carId >= 25 and carId <= 32:
        entrance = playerData.board[abs(carId-32)][0]
        
    
    return entrance.station.score
   
    

def game_over(playerData, historyFileName = None):
    """The engine calls this function after the game is over 
        (regardless of whether or not you have been kicked out)
        Also prints out the scores of each player and the winners.

    You can use it for testing purposes or anything else you might need to do...
    
    Parameters:
        playerData - your player data as always       
        historyFileName - name of the current history file, 
            or None if not being used 
    """
    scores = dict()
    winners = []
    
    ownedstations = playerData.ownedstations

    for num in range(playerData.numPlayers):
        scores[num] = 0
        for val in ownedstations[num+1]:
                scores[num] += route_score(playerData,val)
        
    for n in range(playerData.numPlayers):
        winners.append(str(scores[n]))
        print ("The score for player %s is:  %s" ) % (n,scores[n])
    
    print("The winner(s) are: ")
    for i in range(len(winners)):
        winners[i] = int(winners[i])
    winners = max(winners)
    for i in range(playerData.numPlayers):
        if scores[i] == winners:
            print "Player", i
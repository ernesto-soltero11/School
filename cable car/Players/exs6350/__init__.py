from Model.interface import PlayerMove
from playerData import PlayerData
from Tile import *
from Station import *

"""
Cable Car: Student Computer Player

Complete these function stubs in order to implement your AI.
Author: Adam Oest (amo9149@rit.edu)
Author: Ernesto Soltero(exs6350@rit.edu)
Author: Obaleski Idemudia(oxi8927@rit.edu)
Author: Brian Holman(brh4711@rit.edu)
"""

def init(playerId, numPlayers, startTile, logger, arg = "None"):
    """The engine calls this function at the start of the game in order to:
        -tell you your player id (0 through 5)
        -tell you how many players there are (1 through 6)
        -tell you what your start tile is (a letter a through i)
        -give you an instance of the logger (use logger.write("str") 
            to log a message) (use of this is optional)
        -inform you of an additional argument passed 
            via the config file (use of this is optional)
        
    Parameters:
        playerId - your player id (0-5)
        numPlayers - the number of players in the game (1-6)
        startTile - the letter of your start tile (a-j)
        logger - and instance of the logger object
        arg - an extra argument as specified via the config file (optional)

    You return:
        playerData - your player data, which is any data structure
                     that contains whatever you need to keep track of.
                     Consider this your permanent state.
    """
    
    # Put your data in here.  
    # This will be permanently accessible by you in all functions.
    # It can be an object, list, or dictionary
    
    playerData = PlayerData(logger, playerId, startTile, numPlayers, 0)
    
    # initializes the power station 
    playerData.board[4][4] = Tile('ps', 0,None,(4,4))
    playerData.board[4][3] = Tile('ps', 0,None,(4,3))
    playerData.board[3][4] = Tile('ps', 0,None,(3,4))
    playerData.board[3][3] = Tile('ps', 0,None,(3,3))
    
    # This is how you write data to the log file
    playerData.logger.write("Player %s starting up" % playerId)
    
    # This is how you print out your data to standard output (not logged)
    print(playerData)
    
    return playerData

def move(playerData):  
    """The engine calls this function when it wants you to make a move.
    
    Parameters:
        playerData - your player data, 
            which contains whatever you need to keep track of
        
    You return:
        playerData - your player data, 
            which contains whatever you need to keep track of
        playerMove - your next move
    """
    
    playerData.logger.write("move() called")
    # Populate these values
    playerData.moved = False
    row = playerData.currentpos[0]
    col = playerData.currentpos[1]
    if col > 7:
        row += 1  
        playerData.currentpos[1] = 0
        playerData.currentpos[0] += 1
        col = 0
    if tile_info_at_coordinates(playerData,row,col) == ('ps',0): #skips the power station when encountered
        col += 2
        playerData.currentpos[1] += 2
    row,col = checkemptyspots(playerData,row,col)
    playerId = playerData.playerId
    position = row,col
    tileName = playerData.currentTile
    rotation = 0
    move = PlayerMove(playerId, position, tileName, rotation)
    isvalid(playerData,move,row,col)
    if playerData.moved != True:
        playerData.currentpos[1] += 1
    playerData.board[move.position[0]][move.position[1]] = Tile(move.tileName,move.rotation,None,move.position)
    return playerData, move

def checkemptyspots(playerData,row,col):
    """Function that sequentially updates the currenpos in the PlayerData class. If a tile is already at the next current spot
    it advances to a new spot until it finds a empty spot.
    PlayerData.currentpos-> updated to the next empty spot
    """
    if col > 7:
        row += 1
        col = 0
    if tile_info_at_coordinates(playerData,row,col) != False:
        playerData.currentpos[1] += 1
        return checkemptyspots(playerData,row,col+1)
    elif tile_info_at_coordinates(playerData,row,col) == False:
        return row,col
    
def findvalidspot(playerData,move,col,row):
    """If the tile is currently taken it finds a new spot to place the tile. Keeps recursing if there
    is a tile at the new spot to find the next empty position"
    PlayerData-> PlayerData
    move-> PlayerMove object
    col -> current col pos
    row->current row pos"""
    if col >= 7:
        col = 0
        row += 1
        move.rotation = 0
        playerData.moved = True
    if tile_info_at_coordinates(playerData,row,col) != False:
        move.rotation = 0
        playerData.moved = True
        col += 1
        return findvalidspot(playerData,move,col,row)
    elif tile_info_at_coordinates(playerData,row,col) == False:
        move.rotation = 0
        playerData.moved = True
        return col,row
    

def isvalid(playerData, move, row, col):
    """Checks the validity of every tile at the current spot. Also contains special case where last tile placed is 
    always valid will only rotate if the last tile is 'j'. Another special case is if 'g' is the first tile to be placed
    it will be valid at the first spot.
    playerData -> PlayerData
    move -> PlayerMove object
    row-> current row pos
    col-> current col pos"""
    if (row,col) == (7,7):
        if move.tileName == 'j':
            move.rotation += 1
        return PlayerMove(move.playerId,move.position,move.tileName,move.rotation)
    valid = ''
    if move.rotation > 3:
        col,row = findvalidspot(playerData,move,col+1,row)
        isvalid(playerData,move,row,col)
    if move.tileName == 'a':
        if (row,col) == (0,0) or (row,col) == (7,0) or (row,col) == (0,7) or (row,col) == (7,7):
            valid = False
            move.rotation += 4
        elif row == 0 or row == 7:
            if move.rotation == 0:
                valid = False
                move.rotation += 1        
    if move.tileName == 'b':
        if (row,col) == (0,7) or (row,col) == (7,0):
            if move.rotation == 0:
                valid = False
                move.rotation += 1
    if move.tileName == 'c':
        if (row,col) == (0,0) or (row,col) == (7,0) or (row,col) == (0,7) or (row,col) == (7,7):
            valid = False
            move.rotation += 4
        elif col == 0:
            if move.rotation == 0:
                move.rotation += 1
                valid = False
    if move.tileName == 'd':
        if (row,col) == (0,0) or (row,col) == (7,0) or (row,col) == (0,7) or (row,col) == (7,7):
            valid = False
            move.rotation += 4
        elif row == 0:
            if move.rotation == 0:
                valid = False
                move.rotation += 1
    if move.tileName == 'e':
        if (row,col) == (0,0) or (row,col) == (7,0) or (row,col) == (0,7) or (row,col) == (7,7):
                valid = False
                move.rotation += 4
        elif row == 0:
            if move.rotation == 0:
                valid = False
                move.rotation += 1
        elif col == 7:
            if move.rotation == 0:
                valid = False
                move.rotation += 2
    if move.tileName == 'f':
        pass
    if move.tileName == 'g':
        if (row,col) == (0,0):
            pass
        if (row,col) == (1,1):
            if tile_info_at_coordinates(playerData,row-1,col) == False:
                move.position = checkemptyspots(playerData,row,col)
                return PlayerMove(move.playerId,move.position,move.tileName,move.rotation)
        elif row == 0 or col == 0 or col == 7:
            valid = False
            move.rotation += 4
    if move.tileName == 'h':
        if (row,col) == (0,0) or (row,col) == (7,0) or (row,col) == (0,7) or (row,col) == (7,7):
            move.rotation += 4
            valid = False
    if move.tileName == 'i':
        if (row,col) == (0,0) or (row,col) == (7,0) or (row,col) == (0,7) or (row,col) == (7,7):
            move.rotation += 4
            valid = False
    if move.tileName == 'j':
        if (row,col) == (0,0) or (row,col) == (7,7):
            if move.rotation == 0:
                valid = False
                move.rotation += 1
    if valid == False:
        isvalid(playerData,move,row,col)
    if valid != False:
        move.position = row,col
        return PlayerMove(move.playerId,move.position,move.tileName,move.rotation)
    
def move_info(playerData, playerMove, nextTile):
    """The engine calls this function to notify you of:
        -other players' moves
        -your and other players' next tiles
        
    The function is called with your player's data, as well as the valid move of
    the other player.  Your updated player data should be returned.
    
    Parameters:
        playerData - your player data, 
            which contains whatever you need to keep track of
        playerMove - the move of another player in the game, or None if own move
        nextTile - the next tile for the player specified in playerMove, 
                    or if playerMove is None, then own next tile
                    nextTile can be none if we're on the last move
    You return:
        playerData - your player data, 
            which contains whatever you need to keep track of
    """
    if playerMove == None:
        playerData.currentTile = nextTile
    else:
        playerData.board[playerMove.position[0]][playerMove.position[1]] = Tile(playerMove.tileName, playerMove.rotation, None, playerMove.position)
        playerData.currentTile = nextTile
        playerData.logger.write("move_info() called")
    
    return playerData


################################# PART ONE FUNCTIONS #######################
# These functions are called by the engine during part 1 to verify your board 
# data structure
# If logging is enabled, the engine will tell you exactly which tests failed
# , if any

def tile_info_at_coordinates(playerData, row, column):
    """The engine calls this function during 
        part 1 to validate your board state.
    
    Parameters:
        playerData - your player data as always
        row - the tile row (0-7)
        column - the tile column (0-7)
    
    You return:
        tileName - the letter of the tile at the given coordinates (a-j), 
            or 'ps' if power station or None if no tile
        tileRotation - the rotation of the tile 
            (0 is north, 1 is east, 2 is south, 3 is west.
            If the tile is a power station, it should be 0.  
            If there is no tile, it should be None.
    """      
        
    Info = playerData.board[row][column]
    if Info == 0:
        return False
    elif Info != 0:
        tilename = Info.tileId
        tilerotation = Info.rotation
        return tilename, tilerotation


def route_complete(playerData, carId):
    """The engine calls this function 
        during part 1 to validate your route checking
    
    Parameters:
        playerData - your player data as always
        carId - the id of the car where the route starts (1-32)
        
    You return:
        isComplete - true or false depending on whether or not this car
             connects to another car or power station"""
   
    #gets the first starting tile in front of the current station
    if carId >= 0 and carId <= 8:
        entrance = playerData.board[0][carId -1]
        entrance.position = 0
        entrance.station.score = 0
    elif carId >=9 and carId <= 16:
        entrance = playerData.board[carId-9][7]
        entrance.position = 2
        entrance.station.score = 0
    elif carId >= 17 and carId <= 24:
        entrance = playerData.board[7][abs(carId-24)]
        entrance.position = 4
        entrance.station.score = 0
    elif carId >= 25 and carId <= 32:
        entrance = playerData.board[abs(carId-32)][0]
        entrance.position = 6
        entrance.station.score = 0
    entrance.station.stationId = carId
    firsttile = entrance   # firsttile is just used to record the score of every route so it can be referenced later
    firsttile.station.score += 1
    if entrance.tileId is None:
        firsttile.station.score = 0
        complete = False
    else:
        complete = checkcomplete(carId, entrance, playerData, firsttile)

    return complete

def checkcomplete(carId, entrance, playerData, firsttile):
    """Check complete traverses through the tiles by recursion.
    carId -> int
    entrance -> Tile object of current tile position
    playerData -> playerData as usual
    firsttile -> only used to score the tile score in the first tile"""
    
    if entrance.tileId is None:
        return  
     
    elif entrance.tileId == 'ps': #if at a powerstation double score 
        firsttile.station.score -= 1
        firsttile.station.score = firsttile.station.score * 2
        return True
    
    
    elif entrance.position == 0 or entrance.position == 1:
        exitpos = entrance.tracktiles[entrance.tileId][entrance.position][1]    #gets the exitpos of the tile
        newentrance = tilelocation(entrance, exitpos, playerData) #gets the next tile depending on the exitpos
        if newentrance == True: # if it returns true it means that the tile is out of bounds so it is connected back to a station
            return True
        elif newentrance.tileId is None: #means route is incomplete
            return False 
        else:
            newentrance.position = newposition(exitpos) #entrance position into the next tile 
            firsttile.station.score += 1 #adds one to the firsttile score
        return checkcomplete(carId, newentrance, playerData,firsttile) 
        
        
    elif entrance.position == 2 or entrance.position == 3:
        exitpos = entrance.tracktiles[entrance.tileId][entrance.position][1]
        newentrance = tilelocation(entrance,exitpos,playerData)
        if newentrance == True:
            return True
        elif newentrance.tileId is None:
            return False
        else:
            newentrance.position = newposition(exitpos)
            firsttile.station.score += 1
        return checkcomplete(carId, newentrance, playerData,firsttile) 
        
        
    elif entrance.position == 4 or entrance.position == 5:
        exitpos = entrance.tracktiles[entrance.tileId][entrance.position][1]
        newentrance = tilelocation(entrance, exitpos, playerData)
        if newentrance == True:
            return True
        elif newentrance.tileId is None:
            return False
        else:
            newentrance.position = newposition(exitpos)
            firsttile.station.score += 1
        return checkcomplete(carId, newentrance, playerData,firsttile) 
        
        
    elif entrance.position == 6 or entrance.position == 7:
        exitpos = entrance.tracktiles[entrance.tileId][entrance.position][1]
        newentrance = tilelocation(entrance, exitpos, playerData)
        if newentrance == True:
            return True
        elif newentrance.tileId is None:
            return False
        else:
            newentrance.position = newposition(exitpos)
            firsttile.station.score += 1
        return checkcomplete(carId, newentrance, playerData,firsttile) 
    
    
def newposition(position):
    """Gets the exit position and returns the new starting position of the next tile.
    position -> int"""
    if position == 0:
        nt = 5
    elif position == 1:
        nt = 4
    elif position == 2:
        nt = 7
    elif position == 3:
        nt = 6
    elif position == 4:
        nt = 1
    elif position == 5:
        nt = 0
    elif position == 6:
        nt = 3
    elif position == 7:
        nt = 2
    return nt        
    
def tilelocation(entrance,exitpos,playerData):
    """Moves the current tile to the next tile depending on the exit position.
    entrance -> current tile object
    playerData -> playerData as usual"""
    if exitpos == 0 or exitpos == 1:
        
        row = (int(entrance.boardlocation[0])) - 1
        col = (int(entrance.boardlocation[1]))
        if checkbounds(row,col) == True:
            entrance = True
            return entrance
        entrance = playerData.board[row][col]
    elif exitpos == 2 or exitpos == 3:
        row = (int(entrance.boardlocation[0]))
        col = (int(entrance.boardlocation[1])) + 1
        if checkbounds(row,col) ==  True:
            entrance = True
            return entrance
        entrance = playerData.board[row][col]
    elif exitpos == 4 or exitpos == 5:
        row = (int(entrance.boardlocation[0])) + 1
        col = (int(entrance.boardlocation[1]))
        if checkbounds(row,col) == True:
            entrance = True
            return entrance
        entrance = playerData.board[row][col]
    elif exitpos == 6  or exitpos == 7:
        row = (int(entrance.boardlocation[0]))
        col = (int(entrance.boardlocation[1])) - 1
        if checkbounds(row,col) == True:
            entrance = True
            return entrance
        entrance = playerData.board[row][col]
    
    return entrance

def checkbounds(row,col):
    """Checks to see if the next tile is out of bounds. If it is then it means that the route is 
    connected to another node.
    row -> int 
    col -> int"""
    if row <= -1 or col <= -1 or row >= 8 or col >= 8:
        return True
    else: 
        return False

def route_score(playerData, carId):
    """The engine calls this function 
        during route 1 to validate your route scoring
    
    Parameters:
        playerData - your player data as always
        carId - the id of the car where the route starts (1-32)
        
    You return:
        score - score is the length of the current route from the carId.
                if it reaches the power station, 
                the score is equal to twice the length.
    """
    route_complete(playerData,carId) #refreshes the board because the corner tiles would return different values if there not refreshed
    
    #gets the first starting tile so the score for that route can be referenced
    if carId >= 0 and carId <= 8:
        entrance = playerData.board[0][carId -1]
    elif carId >=9 and carId <= 16:
        entrance = playerData.board[carId-9][7]
    elif carId >= 17 and carId <= 24:
        entrance = playerData.board[7][abs(carId-24)]
    elif carId >= 25 and carId <= 32:
        entrance = playerData.board[abs(carId-32)][0]
        
    
    return entrance.station.score
   
    

def game_over(playerData, historyFileName = None):
    """The engine calls this function after the game is over 
        (regardless of whether or not you have been kicked out)

    You can use it for testing purposes or anything else you might need to do...
    
    Parameters:
        playerData - your player data as always       
        historyFileName - name of the current history file, 
            or None if not being used 
    """
    
    # Test things here, changing the function calls...
    print "History File: %s" % historyFileName
    print "If it says False below, you are doing something wrong"
    
    if historyFileName == "example_complete.data":
        print tile_info_at_coordinates(playerData, 3,7) == ('i', 0)
        print route_complete(playerData, 30) == True
        print route_complete(playerData, 26) == True
    elif historyFileName == "example_complete.data":
        print tile_info_at_coordinates(playerData, 5,7) == ('b', 0)
        print route_complete(playerData, 2) == True
    elif historyFileName == "example_incomplete2.data":
        print tile_info_at_coordinates(playerData, 4,3) == ('ps', 0)
        print route_complete(playerData, 25) == True
  
    

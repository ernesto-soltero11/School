"""
Cable Car: Student Computer Player

A sample class you may use to hold your state data
Author: Adam Oest (amo9149@rit.edu)
Author: Ernesto Soltero (exs6350)
Author: Brian Holman(brh4711@rit.edu)
Author: Obaleski Idemudia(oxi8927@rit.edu)
"""
from Tile import Tile


class PlayerData(object):
    """A sample class for your player data"""
    
    # Add other slots as needed
    __slots__ = ('logger', 'playerId', 'currentTile', 'numPlayers', 'board', 'score','stations','currentpos','moved')
    
    def __init__(self, logger, playerId, currentTile, numPlayers, score):
        """
        __init__: PlayerData * Engine.Logger * int * NoneType * int -> None
        Constructs and returns an instance of PlayerData.
            self - new instance
            logger - the engine logger
            playerId - my player ID (0-5)
            currentTile - my current hand tile (initially None)
            numPlayers - number of players in game (1-6)
            stations - Number of stations that the player owns
        """
        
        self.currentpos = [0,0]
        self.logger = logger
        self.playerId = playerId
        self.currentTile = currentTile
        self.numPlayers = numPlayers
        self.board = [[0 for row in range(8)] for col in range(8)]
        self.score = score
        self.moved = False
        # initialize any other slots you require here
        
    def __str__(self):
        """
        __str__: PlayerData -> string
        Returns a string representation of the PlayerData object.
            self - the PlayerData object
        """
        result = "PlayerData= " \
                    + "playerId: " + str(self.playerId) \
                    + ", currentTile: " + str(self.currentTile) \
                    + ", numPlayers:" + str(self.numPlayers) \
                    + ", score: " + str(self.score)
                
        # add any more string concatenation for your other slots here
                
        return result
    



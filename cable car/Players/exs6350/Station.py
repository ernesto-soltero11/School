"""
Author: Ernesto Soltero(exs6350@rit.edu
"""

class Station(object):
    """A station node that represents the score and id of the station"""
    
    __slots__ = ('stationId', 'score')
    
    def __init__(self, stationId, score):
        
        self.stationId = stationId
        self.score = score

        
    def __str__(self):
        result = "StationId: " + str(self.stationId)
        + "score: " + str(self.score)
        return result 
    

    

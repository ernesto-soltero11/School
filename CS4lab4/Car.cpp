// File: $Id: Car.cpp,v 1.4 2012/10/02 18:15:43 exs6350 Exp exs6350 $
//
// Author: Ernesto Soltero
// Revisions: $Log: Car.cpp,v $
// Revisions: Revision 1.4  2012/10/02 18:15:43  exs6350
// Revisions: updated
// Revisions:
// Revisions: Revision 1.3  2012/09/30 19:01:37  exs6350
// Revisions: initial finish
// Revisions:
// Revisions: Revision 1.2  2012/09/27 13:47:20  exs6350
// Revisions: added more code
// Revisions:
// Revisions: Revision 1.1  2012/09/27 13:15:42  exs6350
// Revisions: Initial revision
// Revisions:
// Description: Represents a car instance. Has a make, model, and a year. 

#include "Car.h"
#include <iostream>

using namespace std;

	
Car::Car(string &make,string &model, int year):ma(make),mod(model),yr(year){}

Car::Car(const Car &other):ma(other.make()),mod(other.model()),yr(other.year()){}

string Car::make() const{
	return ma;
}

string Car::model()const{
	return mod;
}

int Car::year()const{
	return yr;
}

void Car::display()const{
	cout << "Make = " << make() << ", Model = " << model() << ", Year = " << year() << endl;
}

// File: $Id: Garage.cpp,v 1.4 2012/10/02 18:41:43 exs6350 Exp exs6350 $
// Author: Ernesto Soltero
//
// Revisions: $Log: Garage.cpp,v $
// Revisions: Revision 1.4  2012/10/02 18:41:43  exs6350
// Revisions: changed some code
// Revisions:
// Revisions: Revision 1.3  2012/09/30 19:01:37  exs6350
// Revisions: initial finish
// Revisions:
// Revisions: Revision 1.2  2012/09/27 13:47:20  exs6350
// Revisions: added more code
// Revisions:
// Revisions: Revision 1.1  2012/09/27 13:15:42  exs6350
// Revisions: Initial revision
// Revisions:
// Description: The garage class is a vector that holds instances of Car classes. Is able to store and remove cars and check if it is full or not.

#include "Garage.h"
#include <iostream>
#include "Car.h"
#include <vector>
#include <assert.h>

using namespace std;

	
// Creates a vector and initializes the capacity.
Garage::Garage(string &name, int capacity):na(name){
	vector<Car *> garage(capacity);
	cap = capacity;
	}
	
// Destroys the instance of this class and everything it holds.
Garage::~Garage(){
}

// Returns the capacity of the vector.
int Garage::capacity()const{
	return cap;
}
	
// Displays the contents of the elements in the vector
void Garage::display()const{
	cout << "Garage: " << name() << endl;
	cout << "Currently holds " << size() << " cars." << endl;
	for(int i = 0; i < garage.size(); i++){
		garage[i].display();
		cout << endl;
	}
}
	
// Returns true if the vector contains no objects
bool Garage::empty()const{
	return garage.empty();
}
	
// Returns full if the vector is full
bool Garage::full()const{
	if(int(garage.size()) == capacity()){
		return true;
	}
	return false;
}

// Returns the name of the vector
string Garage::name()const{
	return na;
}
	
// Removes the first element of the vector.
void Garage::pop(){
	assert(int(garage.size()) != 0);
	garage.erase(garage.begin());
}

// Adds an element to the beginning of the vector.
void Garage::push(const Car &car){
	assert(full() == false);
	garage.insert(garage.begin(),car);
}

// Removes the element from the vector
bool Garage::remove(const Car &car){
	int oldsize = int(garage.size());
	assert(oldsize != 0);
	for(int i = 0;i < oldsize;i++){
		if(garage[i].make() == car.make() && garage[i].model() == car.model() && garage[i].year() == car.year()){
			garage.erase(garage.begin() + i);
			assert(int(garage.size()) < oldsize);
			return true; 
		}
		else{continue;}
	}
	return false;
}
	
// Returns the size of the elements in the vector
int Garage::size()const{
	return int(garage.size());
}


// Returns the top element of the vector.
Car Garage::top()const{
	assert(int(garage.size()) > 0);
	return garage.front();
}


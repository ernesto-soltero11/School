// File: $Id: Garage.h,v 1.4 2012/10/02 18:41:43 exs6350 Exp exs6350 $
//
// Author: Ernesto Soltero
// Revisions: $Log: Garage.h,v $
// Revisions: Revision 1.4  2012/10/02 18:41:43  exs6350
// Revisions: changed some code
// Revisions:
// Revisions: Revision 1.3  2012/09/30 19:01:37  exs6350
// Revisions: initial finish
// Revisions:
// Revisions: Revision 1.2  2012/09/27 13:47:20  exs6350
// Revisions: added more code
// Revisions:
// Revisions: Revision 1.1  2012/09/27 12:47:45  exs6350
// Revisions: Initial revision
// Revisions:
// Description: Contains all the prototype code for the Garage class.

#include <string>
#include "Car.h"
#include <vector>

using namespace std;

class Garage{
	
	public:
	//Constructs an instance of the garage with initialized variables.
	Garage(string &name, int capacity);

	// Destructor for the garage instance.
	~Garage();

	// Returns the capacity of the garage.
	int capacity()const;

	// Displays information about the instance.
	void display()const;

	// Returns true or false if the garage is empty or not.
	bool empty()const;

	// Returns true or false if the garage is full.
	bool full()const;

	// Returns the name of the garage.
	string name()const;

	// Pops off the first element off the stack.
	void pop();

	// Pushes the element on the stack.
	void push(const Car &car);

	// Removes the element off the stack.
	bool remove(const Car &car);

	// Returns the size of the garage.
	int size()const;

	// Returns the top of the garage
	Car top()const;

	private:
	string na;
	int cap;
	vector<Car> garage; 

};

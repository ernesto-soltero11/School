// File: $Id: Car.h,v 1.3 2012/10/02 18:15:43 exs6350 Exp exs6350 $
//
// Author: Ernesto Soltero
// Revisions: $Log: Car.h,v $
// Revisions: Revision 1.3  2012/10/02 18:15:43  exs6350
// Revisions: updated
// Revisions:
// Revisions: Revision 1.2  2012/09/30 19:01:37  exs6350
// Revisions: initial finish
// Revisions:
// Revisions: Revision 1.1  2012/09/27 12:47:45  exs6350
// Revisions: Initial revision
// Revisions:
// Description: The header that contains the prototypes for all the
// functions of the Car class.
#ifndef CAR_H_
#define CAR_H_

#include <string>

using namespace std;

class Car{

public:
	// Creates a Car object and initializes the constants.
	Car(string &make, string &model, int year);

	// Makes a copy of the supplied instance.
	Car(const Car &other);

	// Displays information about the class.
	void display()const;
	
	// Returns the make of the car.
	string make()const;

	// Returns the model of the car.
	string model()const;

	//Returns the year of the car.
	int year()const;

// Data members
private:
	string ma;
	string mod;
	int yr;
};
#endif

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;

/**
 * The client proxy communicates with the server and uses the model to perform box and ui 
 * functions
 * @author Ernesto Soltero (exs6350@rit.edu)
 * 
 */
public class ClientProxy implements Proxy
{
	
//Hidden data members
	private DatagramSocket socket;
	private ClientModelClone controller;
	private InetAddress host;
	private int port;
	private int playerNumber;
	private String playerName;
	private InetAddress clientAddress;
	private int serverPort;
	private String[] names = {"",""};
	private int[] scores = {0,0};
	private byte[] buffer;

	/**
	 * Construct a new client proxy.
	 * @param host - The host name of the server
	 * @param hostport - The port that the host is on
	 * @param clientAdd - The address of the client
	 * @param port - The port of the client
	 * @param player - The name of the player
	 *
	 */
	
	public ClientProxy(String host, int hostport, String clientAdd, int port, String player) 
	{
		try {
			this.port = port;
			this.socket = new DatagramSocket(port);
			this.host = InetAddress.getByName(host);
			this.serverPort = hostport;
			this.clientAddress = InetAddress.getByName(clientAdd);
			this.scores = new int[] { 0, 0 };
			this.playerName = player;
		}
		catch(Exception e){
			System.err.println("Error connecting to the socket.");
		}
	}

	/**
	 * Sets the client model
	 * @param m - The model to set to
	 *   
	 */
	public void setMVC(ClientModelClone m)
	{
		this.controller = m;
		new ReaderThread().start();
	}

	/**
	 * Sends the request to join the server
	 * @param playerName - The name of the player
	 *         
	 */
	public void join(String playerName) 
	{
		String mes = "join " + playerName;
		System.out.println("client sent: " + mes);
		buffer = new byte[mes.length()];
		buffer = mes.getBytes();
		DatagramPacket packet = new DatagramPacket(buffer, buffer.length, host, serverPort);
		try {
			socket.send(packet);
		} catch (IOException e) {
			System.out.println("Error sending the packet.");
		}
	}

	/**
	 * Sends a request to flip the tile
	 * @param x - The location of the tile               
	 *
	 */
	public void flipTile(int x)
	{
		String mes = "flip " + x;
		System.out.println("client sent: " + mes);
		buffer = new byte[mes.length()];
		buffer = mes.getBytes();
		DatagramPacket packet = new DatagramPacket(buffer, buffer.length, host, serverPort);
		try {
			socket.send(packet);
		} catch (IOException e) {
			System.out.println("Error sending the packet.");
		}
	}
	
	/**
	 * Sends a request to roll the dice
	 *
	 */
	public void roll()
	{
		String mes = "roll";
		System.out.println("client sent: " + mes);
		buffer = new byte[mes.length()];
		buffer = mes.getBytes();
		DatagramPacket packet = new DatagramPacket(buffer, buffer.length, host, serverPort);
		try {
			socket.send(packet);
		} catch (IOException e) {
			System.out.println("Error sending the packet.");
		}
	}

	/**
	 * Sends a request to finish the turn
	 *
	 */
	public void done()
	{
		String mes = "done";
		System.out.println("client sent: " + mes);
		buffer = new byte[mes.length()];
		buffer = mes.getBytes();
		DatagramPacket packet = new DatagramPacket(buffer, buffer.length, host, serverPort);
		try {
			socket.send(packet);
		} catch (IOException e) {
			System.out.println("Error sending the packet.");
		}
	}

	/**
	 * Sends a request to end the session 
	 *
	 */
	public void quit()
	{
		String mes = "quit";
		System.out.println("client sent: " + mes);
		buffer = new byte[mes.length()];
		buffer = mes.getBytes();
		DatagramPacket packet = new DatagramPacket(buffer, buffer.length, host, serverPort);
		try {
			socket.send(packet);
		} catch (IOException e) {
			System.out.println("Error sending the packet.");
		}
	}

	/**
	 * Class ReaderThread receives messages from the network, decodes them, and
	 * invokes the proper methods to process them using the model controller.
	 *
	 */
	private class ReaderThread extends Thread
	{
		public void run()
		{
			
			boolean running = true;
			while (running)
			{
				byte[] buffer = new byte[32];
				DatagramPacket packet = new DatagramPacket(buffer, buffer.length);
				try {
					socket.receive(packet);
				} catch (IOException e1) {
					System.out.println("Error recieveing the packet.");
				}
				//This packet is not for us!
				if(packet.getPort() != serverPort & packet.getAddress() != host){
					System.out.println("This packet is not for us!");
					continue;
				}
				String temp = new String(packet.getData(), 0, packet.getLength());
				buffer = new byte[packet.getLength()];
				String[] recieve = temp.split(" ");
				if (recieve != null){
					if(recieve[0].equals("connected")){
						if(recieve[1].equals(playerName)){
							playerNumber = 1;
						}
						else{playerNumber = 2;}
						names[0] = recieve[1];
						names[1] = recieve[2];
						controller.setMessage(recieve[1] + " -- " + recieve[2]);
					}
					else if(recieve[0].equals("turn")){
						controller.reset();
						if(Integer.valueOf(recieve[1]).equals(playerNumber)){
							controller.enableButtons(true);
						}
						else{
							controller.enableButtons(false);
						}
					}
					else if(recieve[0].equals("flip")){
						int pos = Integer.valueOf(recieve[1]);
						controller.flipTile(pos);
					}
					else if(recieve[0].equals("dice")){
						int x = Integer.valueOf(recieve[1]);
						int y = Integer.valueOf(recieve[2]);
						controller.setDie(0, x);
						controller.setDie(1, y);
					}
					else if(recieve[0].equals("score")){
						int player = Integer.valueOf(recieve[1]);
						int score = Integer.valueOf(recieve[2]);
						System.out.println("player number: " + playerNumber);
						if(playerNumber == player){
							scores[player-1] = score;
							controller.setMessage(names[0] + " " + scores[0] + " -- " + names[1] + " " + scores[1]);
							controller.reset();
							controller.enableButtons(false);
						}
						else{
							scores[player -1] = score;
							controller.setMessage(names[0] + " " + scores[0] + " -- " + names[1] + " " + scores[1]);
							controller.reset();
							controller.enableButtons(true);
						}
					}
					else if(recieve[0].equals("win")){
						if(recieve[1].equals("0")){
							controller.setMessage(names[0] + " " + scores[0] + " -- " + names[1] + " " + scores[1] + " Tie!");
						}
						else{
							int winner = Integer.valueOf(recieve[1]);
							controller.setMessage(names[0] + " " + scores[0] + " -- " + names[1] + " " + scores[1] +" "+ names[winner-1] + " wins!");
						}
						scores[0] = 0;
						scores[1] = 0;
						controller.reset();
					}
					else if(recieve[0].equals("quit")){
						try{
							socket.close();
						}
						catch(Exception e){
							System.err.println("Error closing the socket.");
						}
						running = false;
						controller.quit();
					}
				}

			}
		}
	}
}

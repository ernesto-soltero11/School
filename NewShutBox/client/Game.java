import java.util.BitSet;
/**
 * The shut the box logic that keeps track of the game and its states.
 * @author Ernesto Soltero (exs6350@rit.edu)
 */
public class Game {

	private BitSet tiles;
	private int[] dice = {1,1};
	
	/**
	 * Constructor for shut the box with a default tile start value of 9 tiles.
	 */
	public Game() {
		this.tiles = new BitSet(9);
		reset();
	}
	
	/**
	 * Sets the tile to the stage. True for up false for down.
	 * @param i - The index to change the tile
	 */
	public void flipTile(int i){
		this.tiles.flip(i-1);
	}
	
	/**
	 * Resets all the tiles to a up position
	 */
	public void reset(){
		this.tiles.set(0, 9);
	}
	
	/**
	 * Returns the tile at the i index.
	 * @param i - The index of the tile to return 
	 * @return The state of the tile
	 */
	public boolean getTile(int i){
		return this.tiles.get(i-1);
	}
	
	/**
	 * Sets the dice to the specified value
	 * @param index - The index of the dice to change
	 * @param val - The value of the dice to change
	 */
	public void setDie(int index, int val){
		this.dice[index] = val;
	}
	
	/**
	 * Returns the die at the index specified.
	 * @return The value of the dice.
	 */
	public int getDie(int i){
		return this.dice[i];
	}
	
	public static void main (String args[]){
		
		Game game = new Game();
		ClientProxy proxy = new ClientProxy(args[0], Integer.valueOf(args[1]), args[2], Integer.valueOf(args[3]), args[4]);
		ShutTheBoxUI ui = new ShutTheBoxUI(proxy);
		ClientModelClone clone = new ClientModelClone(game, ui);
		proxy.join(args[4]);
	}
	
}

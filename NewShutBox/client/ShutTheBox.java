/**
 * Starts the client gui and sets the proxy and clone for it.
 * @author Ernesto Soltero (exs6350@rit.edu)
 *
 */
public class ShutTheBox {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		if(args.length < 4 || args.length > 4){
			System.err.println("Usage: java ShutTheBox <shost> <sport> <chost> <cport> <playername>");
		}
		Game game = new Game();
		ClientProxy proxy = new ClientProxy(args[0], Integer.valueOf(args[1]),args[2],Integer.valueOf(args[3]),args[4]);
		ShutTheBoxUI ui = new ShutTheBoxUI(proxy);
		ClientModelClone clone = new ClientModelClone(game,ui);
		proxy.setMVC(clone);
		proxy.join(args[4]);
	}

}

/**
 * The model controller of the client. Controls the game logic and the updates the gui.
 * @author Ernesto Soltero (exs6350@rit.edu)
 *
 */
public class ClientModelClone implements ClientModel {

	private static Game box;
	private static ShutTheBoxUI view;
	
	/**
	 * Constructor for the controller.
	 * @param game - The game session to use
	 * @param ui - The gui of the client
	 */
	public ClientModelClone(Game game, ShutTheBoxUI ui) {
		this.box = game;
		this.view = ui;
	}
	
	/**
	 * Flips the specified tile on the board and game.
	 * @param int - The index of the tiel to change
	 */
	public void flipTile(int index) {
		box.flipTile(index);
		view.flipTile(index);
	}

	/**
	 * Resets the game session and the gui.
	 */
	public void reset() {
		view.reset();
		box.reset();
	}

	/**
	 * Sets the die at the index to the value.
	 * @param index - The index of the dice to set (1 or 2).
	 * @param val - The value of the dice to set (1-6)
	 */
	public void setDie(int index, int val) {
		box.setDie(index, val);
		view.setDie(index, val);
	}

	
	/**
	 * Sets the message on the UI.
	 * @param m - The message to display
	 */
	public void setMessage(String m){
		view.setMessage(m);
	}

	/**
	 * Quits the client and shuts down the connection.
	 */
	public void quit() {
		view.quit();
	}

	/**
	 * Enables or disables the buttons in the GUI
	 * @param enable - True to enable false otherwise
	 */
	public void enableButtons(boolean enable) {
		view.enableButtons(enable);	
	}
}

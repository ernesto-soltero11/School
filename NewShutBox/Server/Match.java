import java.net.InetAddress;
/**
 * 
 * A match object holds the information of one client. This includes their address, port number,
 * and the their player name.
 * @author Ernesto Soltero (exs6350@rit.edu)
 *
 */
public class Match{
	private int port;
	private InetAddress clientAddress;
	private String player;
	
	/**
	 * Constructor for the Match object
	 * @param port - The port the client is on
	 * @param add - The address of the client
	 * @param player - The player name
	 */
	public Match(int port, InetAddress add, String player){
		this.port = port;
		this.clientAddress = add;
		this.player = player;
	}
	
	/**
	 * Returns the port number this client is on
	 * @return int - the port number of the client
	 */
	public int returnPort(){
		return this.port;
	}
	
	/**
	 * Returns the address the client is on
	 * @return InetAddress - The address of the client
	 */
	public InetAddress returnAddress(){
		return this.clientAddress;
	}
	
	/**
	 * Returns the player name of this player
	 * @return String - The name of this client
	 */
	public String returnName(){
		return this.player;
	}
}
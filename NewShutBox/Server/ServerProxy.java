import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.util.Random;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

/**
 * This class is the proxy for the server. It is not multi-threaded in the sense 
 * that every session is a thread. Rather every packet is processed in a new thread 
 * so preference is given to making new sessions and  
 * @author Ernesto Soltero (exs6350@rit.edu)
 *
 */
public class ServerProxy {

	private int serverPort;
	private InetAddress serverAddress;
	private DatagramSocket serverSocket;
	private Executor threadPool;
	
	/**
	 * The constructor for the server proxy
	 * @param serv - the port number of the server
	 * @param add - The address of the server
	 * @param socket - The socket the server is using
	 */
	public ServerProxy(int serv, InetAddress add, DatagramSocket socket){
		this.serverPort = serv;
		this.serverAddress = add;
		this.serverSocket = socket;
		this.threadPool = Executors.newCachedThreadPool();
	}
	
	/**
	 * Process the score for the current player and send it to both clients.
	 * Resets the model after the game is finished and resets the view after 
	 * evry player.
	 * @param clone - The game session model to use
	 * @param player - The current player turn player 1 or 2
	 */
	synchronized public void done(ServerModelClone clone, int player) {
		int temp = 0;
		for(int i = 1; i < 10; ++i){
			if(clone.getGame().getTile(i)){
				temp += i;
			}
		}
		//Score player one or two
		clone.setScore(player, temp);
		String score;
		if(player == 1){
			score = "1 " + clone.getScore(player);
		}
		else{score = "2 " + clone.getScore(player);}
		String message = "score " + score;
		System.out.println("Server sent: " + message);
		byte[] buffer = new byte[message.length()];
		buffer = message.getBytes();
		DatagramPacket packet = new DatagramPacket(buffer, buffer.length, clone.returnOne().returnAddress(), clone.returnOne().returnPort());
		try {
			serverSocket.send(packet);
			packet.setAddress(clone.returnTwo().returnAddress());
			packet.setPort(clone.returnTwo().returnPort());
			serverSocket.send(packet);
		} catch (IOException e) {
			System.err.println("Unable to send packet.");
		}
		if(player == 1){
			message = "turn 2";
			byte[] buff = new byte[message.length()];
			buff = message.getBytes();
			packet.setData(buff);
			try {
				serverSocket.send(packet);
				packet.setAddress(clone.returnOne().returnAddress());
				packet.setPort(clone.returnOne().returnPort());
				serverSocket.send(packet);
				clone.reset();
			} catch (IOException e) {
				System.err.println("Unable to send packet.");
			}
		}
		//Reset if player two is done and send winner
		else{
			int winner = 0;
			if(clone.getScore(1) > clone.getScore(2)){
				winner = 2;
			}
			else if(clone.getScore(1) < clone.getScore(2)){
				winner = 1;
			}
			message = "win " + winner;
			System.out.println("Server sent: " + message);
			byte[] buff = new byte[message.length()];
			buff = message.getBytes();
			packet.setData(buff);
			packet.setAddress(clone.returnTwo().returnAddress());
			packet.setPort(clone.returnTwo().returnPort());
			try {
				serverSocket.send(packet);
			    packet.setAddress(clone.returnOne().returnAddress());
			    packet.setPort(clone.returnOne().returnPort());
			    serverSocket.send(packet);
			    clone.reset();
			    message = "turn 1";
			    byte[] bu = new byte[message.length()];
			    bu = message.getBytes();
			    packet.setData(bu);
			    serverSocket.send(packet);
			    packet.setAddress(clone.returnTwo().returnAddress());
			    packet.setPort(clone.returnTwo().returnPort());
			    serverSocket.send(packet);
			}
			catch(IOException e){
				System.err.println("Error sending packet.");
			}
		}
	}

	/**
	 * Process quit for a client. When one client disconnects the other is also 
	 * disconnected.
	 * @param clone - The game session model to use
	 */
	synchronized public void quit(ServerModelClone clone) {
		String mes = "quit";
		System.out.println("Server sent: " + mes);
		byte[] buffer = new byte[mes.length()];
		buffer = mes.getBytes();
		DatagramPacket packet = new DatagramPacket(buffer, buffer.length, clone.returnOne().returnAddress(), clone.returnOne().returnPort());
		try {
			serverSocket.send(packet);
			packet.setAddress(clone.returnTwo().returnAddress());
			packet.setPort(clone.returnTwo().returnPort());
			serverSocket.send(packet);
		} catch (IOException e) {
			System.err.println("Unable to send packet.");
		}
		clone.quit();
	}

	/**
	 * Flips the tile in the model and sends it to both clients to update their gui. 
	 * @param clone - The game session model to use
	 */
	synchronized public void flipTile(int num, ServerModelClone clone) {
		String mes = "flip " + num;
		System.out.println("Server sent: " + mes);
		byte[] buffer = new byte[mes.length()];
		buffer = mes.getBytes();
		DatagramPacket packet = new DatagramPacket(buffer, buffer.length, clone.returnOne().returnAddress(), clone.returnOne().returnPort());
		try {
			serverSocket.send(packet);
			packet.setAddress(clone.returnTwo().returnAddress());
			packet.setPort(clone.returnTwo().returnPort());
			serverSocket.send(packet);
		} catch (IOException e) {
			System.err.println("Unable to send the packet.");
		}
	}

	/**
	 * Rolls the dice and sends the rolled values to both clients to update their gui.
	 * @param clone - The game session model to use 
	 */
	synchronized public void roll(ServerModelClone clone) {
		Random rand = new Random();
		int x = rand.nextInt(6) + 1;
		int y = rand.nextInt(6) + 1;
		clone.setDie(0, x);
		clone.setDie(1, y);
		String mes = "dice " + x + " " + y;
		System.out.println("Server sent: " + mes);
		byte[] buffer = new byte[mes.length()];
		buffer = mes.getBytes();
		DatagramPacket packet = new DatagramPacket(buffer, buffer.length, clone.returnOne().returnAddress(), clone.returnOne().returnPort());
		try {
			serverSocket.send(packet);
			packet.setAddress(clone.returnTwo().returnAddress());
			packet.setPort(clone.returnTwo().returnPort());
			serverSocket.send(packet);
		} catch (IOException e) {
			System.err.println("Unable to send the packet.");
		}
	}

	/**
	 * Process the packet request in a new thread
	 * @param packet - The packet of received data
	 * @param clone - The model to use
	 */
	public void process(DatagramPacket packet, ServerModelClone clone){
		ServerThread p = new ServerThread(packet, clone);
		threadPool.execute(p);
	}
	
	/**
	 * The server thread that processes the incoming client request.
	 * Takes the packet data and calls the appropriate method to process the request.
	 * @author Ernesto Soltero (exs6350@rit.edu)
	 *
	 */
	private class ServerThread implements Runnable{
		
		private DatagramPacket packet;
		private ServerModelClone model;
		
		public ServerThread(DatagramPacket packet, ServerModelClone clone){
			this.packet = packet;
			this.model = clone;
		}
		
		/**
		 * Runs the server thread to process the incoming request.
		 */
		public void run() {
			String in = new String(packet.getData(), 0, packet.getLength());
			String[] receive = in.split(" ");
			if(receive[0].equals("flip")){
				int index = Integer.valueOf(receive[1]);
				model.flipTile(index);
				flipTile(index, model);
			}
			else if(receive[0].equals("done")){
				int player = 0;
				if(packet.getPort() == model.returnOne().returnPort() && packet.getAddress().equals(model.returnOne().returnAddress())){
					player = 1;
				}
				else{player = 2;}
				done(model, player);
			}
			else if(receive[0].equals("quit")){
				quit(model);
				model.quit();
			}
			else if(receive[0].equals("roll")){
				roll(model);
			}
		}
	}
}

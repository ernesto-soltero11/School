import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.util.ArrayDeque;
import java.util.ArrayList;
/**
 * Starts the server and takes incoming requests. Takes incoming clients and 
 * adds them to a queue. When there are enough clients matches them together so 
 * they can play. Takes incoming packets and sends them to the proxy to be processed.
 * @author Ernesto Soltero (exs6350@rit.edu)
 *
 */
public class ShutTheBoxServer{

	private DatagramSocket socket;
	private InetAddress serverAddress;
	private ServerProxy serverProxy;
	private ArrayDeque<Match> waitList = new ArrayDeque<Match>();
	private ArrayList<ServerModelClone> storage = new ArrayList<ServerModelClone>();
	
	/**
	 * Sets up the server and listen to new clients connecting.
	 * @param host - The host to connect to
	 * @param port - The port number the host is listening to
	 */
	public ShutTheBoxServer(String host, int port) {
		try {
			this.serverAddress = InetAddress.getByName(host);
			this.socket = new DatagramSocket(port, serverAddress);
			this.serverProxy = new ServerProxy(port, serverAddress, socket);
		} catch (IOException e) {
			System.out.println("Error binding the socket.");
		}
	}
	
	/**
	 * Runs the server listening to requests.
	 */
	public void run(){
		while(true){
			byte[] buff = new byte[32];
			DatagramPacket packet = new DatagramPacket(buff, buff.length);
			try {
				socket.receive(packet);
				String in = new String(packet.getData(), 0, packet.getLength());
				String[] temp = in.split(" ");
				String join = "join";
				
				//Adds the user to the queue
				if(temp[0].equals(join)){
					System.out.println("Recieved join request from: " + packet.getSocketAddress());
					Match match = new Match(packet.getPort(), packet.getAddress(), temp[1]);
					waitList.add(match);
				}
				
				//Process the packet and find the current session in storage
				else{
					ServerModelClone clone = null;
					if(storage.size() > 0){
						for(ServerModelClone k: storage){
							if(((k.returnOne().returnAddress()).equals(packet.getAddress()) && k.returnOne().returnPort() == packet.getPort())){
								clone = k;
								break;
							}
							else if(((k.returnTwo().returnAddress().equals(packet.getAddress())) && k.returnTwo().returnPort() == packet.getPort())){
								clone = k;
								break;
							}
						}
					serverProxy.process(packet, clone);
					}
				}
				//Create a new match when there's at least two clients in the queue
				if(waitList.size() >= 2){
					Match playerOne = waitList.remove();
					Match playerTwo = waitList.remove();
					ServerModelClone clone = new ServerModelClone(playerOne, playerTwo, new Game());
					System.out.println("Starting a new session player1: " + clone.returnOne().returnName()+ " player2: " 
							+ clone.returnTwo().returnName());
					String message = "connected " + clone.returnOne().returnName() + " " + clone.returnTwo().returnName();
					System.out.println("Sent: " + message);
					byte[] buffer = new byte[message.length()];
					buffer = message.getBytes();
					DatagramPacket freshPacket = new DatagramPacket(buffer, buffer.length, clone.returnOne().returnAddress(), 
							clone.returnOne().returnPort());
					socket.send(freshPacket);
					freshPacket.setAddress(clone.returnTwo().returnAddress());
					freshPacket.setPort(clone.returnTwo().returnPort());
					socket.send(freshPacket);
					message = "turn 1";
					buffer = message.getBytes();
					freshPacket.setData(buffer);
					socket.send(freshPacket);
					freshPacket.setAddress(clone.returnOne().returnAddress());
					freshPacket.setPort(clone.returnOne().returnPort());
					socket.send(freshPacket);
					storage.add(clone);
				}
			} catch (IOException e) {
				System.err.println("Error receiving the packet.");
			}
		}
	}

	/**
	 * Start the server.
	 * @param args - The host address and the port to listen to.
	 */
	public static void main(String args[]){
		if(args.length != 2){
			System.out.println("Usage: java ShutTheBoxServer <host> <port> ");
			System.exit(1);
		}
		ShutTheBoxServer server = new ShutTheBoxServer(args[0], Integer.valueOf(args[1]));
		System.out.println("Server is running!");
		server.run();
		}
}

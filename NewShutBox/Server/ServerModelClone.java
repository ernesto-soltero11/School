/**
 * The server model clone. A new clone is made for every session and holds
 * the two clients and game information.
 * @author Ernesto Soltero (exs6350@rit.edu)
 *
 */
public class ServerModelClone implements ServerModel {

	private Match playerOne;
	private Match playerTwo;
	private Game box;
	private int[] score = {0,0};
	
	/**
	 * Constructor for the server model.
	 * @param playerone - The first player
	 * @param playertwo - The second player
	 * @param game - The game model for this session
	 */
	public ServerModelClone(Match playerone, Match playertwo, Game game) {
		this.playerOne = playerone;
		this.playerTwo = playertwo;
		this.box = game;
	}

	/**
	 * Returns the game currently being used.
	 * @return box - The game holding the state of the tiles
	 */
	public Game getGame(){
		return this.box;
	}
	
	/**
	 * returns the score of the specified player
	 * @param playerNum - The player to get the score of
	 * @return int - The score of this player
	 */
	public int getScore(int playerNum){
		System.out.println("Player get score: " + playerNum);
		return score[playerNum-1];
	}
	
	/**
	 * Sets the score of the specified player
	 * @param player - The player to set the score of
	 * @param score - The score to set
	 */
	public void setScore(int player, int score){
		System.out.println("Player set score: " + player);
		this.score[player-1] = score;
	}
	
	/**
	 * Returns the first player object
	 * @return Match - The first player information
	 */
	public Match returnOne() {
		return this.playerOne;
	}

	/**
	 * Returns the second player object
	 * @return Match - Teh second player information
	 */
	public Match returnTwo() {
		return this.playerTwo;
	}

	/**
	 * Flips the tile in the game model
	 * @param index - The index of the tile to flip (1-9)
	 */
	public void flipTile(int index) {
		box.flipTile(index);
	}

	/**
	 * Resets the game view and the game model (only tiles)
	 */
	public void reset() {
		box.reset();
	}

	/**
	 * Sets the dice to the specified value
	 * @param i - The index of the dice to set
	 * @param val - The value to change the dice to 
	 */
	public void setDie(int i, int val) {
		box.setDie(i, val);
	}

	/**
	 * Quits the session destroys this game when logging off the clients
	 */
	public void quit() {
		System.gc();
	}
}

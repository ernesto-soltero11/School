
/**
 * The interface that the server model implements. It has all the functions to 
 * process every type of request
 * @author Ernesto Soltero (exs6350@rit.edu)
 *
 */
public interface ServerModel {

	/**
	 * Returns playerone
	 * @return The playerOne match object
	 */
	public Match returnOne();
	
	/**
	 * Returns playertwo
	 * @return The playerTwo match object
	 */
	public Match returnTwo();
	
	/**
	 * Flips the tile in the game.
	 * @param index - The index of the tile to flip
	 */
	public void flipTile(int index);
	
	/**
	 * Resets the game
	 */
	public void reset();
	
	/**
	 * Sets the die to the specified value.
	 * @param i - The index of the dice to set
	 * @param val - The new value of the dice to set
	 */
	public void setDie(int i, int val);
	
	/**
	 * Shuts down the session for these two clients
	 */
	public void quit();
}

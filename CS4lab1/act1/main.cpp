// File:     $Id: main.cpp,v 1.1 2005/09/12 16:41:51 cs4 Exp cs4 $
// Author    Sean Strout
// Description:  main program for the printit program
// Revision History:
// 	$Log: main.cpp,v $
// 	Revision 1.1  2005/09/12 16:41:51  cs4
// 	Initial revision
//

// prototype for printit function
void printit(const char *str);

int main() {
	printit("Hello world!");
}

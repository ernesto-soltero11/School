CS4 Makefile Lab - Activity 1
Name: <YOUR NAME HERE>

1. List the command/s that you typed on the command line to compile the
program 'main.cpp' into the object file 'main.o':


<YOUR ANSWER HERE>


2. List the command/s that you typed on the command line to link all
the object files and create the executable 'printit':


<YOUR ANSWER HERE>


3. What was the output when 'printit' was run?


<YOUR ANSWER HERE>

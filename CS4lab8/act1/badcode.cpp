// File:     $Id: badcode.cpp,v 1.3 2011/10/18 23:36:00 cs4 Exp $
// Author    Joe Geigel
// Contributor    Ben Steele
// Description:  main program for activity 1 of the memory management lab. 
// Revision History:
// 	$Log: badcode.cpp,v $
// 	Revision 1.3  2011/10/18 23:36:00  cs4
// 	ubuntu update (bks)
//
// 	Revision 1.2  2011/10/18 22:00:56  cs4
// 	updated for ubuntu includes.
//
// 	Revision 1.1  2011/10/18 01:55:38  cs4
// 	Initial revision
//

#include <iostream>
#include <cstring>

using namespace std;

/// This simple program purposely generates memory related errors
/// to illustrate use of gdb in catching such errors.

//
// This function purposely leaks memory.
//
void memoryLeaker( int n ) {
  int *x = new int [n];
  delete x;
}


/// This function purposely deletes a pointer twice.
/// The function also deletes this 'data table' pointer incorrectly.
///
void doubleDeleter() {
  int *x = new int[1000];
  int *y = x;

  delete x;
}


// 
// This function purposely writes beyond the bounds of an array.
//
void boundsViolator( const char *s ) {
  char *shortString = new char[28];

  strcpy( shortString, s );

  delete shortString;
}


//
// A function that purposely deletes memory that was not dynamically
// allocated.
//
void memoryTrasher( int *nonDynamicArray ) {
  delete nonDynamicArray;
}


//
// A program that purposely does bad things to memory
//
int main( int argc, char * const argv[] ) {

  // Let's leak some memory
  memoryLeaker( 1000 );

  // Then delete something twice
  doubleDeleter();

  // Pay no attention to array bounds
  boundsViolator( "This string is just too long" );

  // Delete some memory that hasn't been newed
  int *anArray = new int[10];
  memoryTrasher( anArray );


  cout << "Look!  No core dumps!" << endl;
}

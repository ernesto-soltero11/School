// File:         $Id: Time.cpp,v 1.1 2005/10/16 17:42:36 cs4 Exp $
// Author:       Joe Geigel
// Description:  Simple class for storing the time of day
// Revisions:
//              $Log: Time.cpp,v $
//              Revision 1.1  2005/10/16 17:42:36  cs4
//              Initial revision
//
//              Revision 1.1  2005/10/16 16:33:51  cs4
//              Initial revision
//


#include "Time.h"

//
// Name:        (constructor)
//

Time::Time (int h, int m, string tod) : hour(h), min (m), timeofday(tod)
{
  // Value check 
  if ((h < 1) || ( h > 12) || (m < 0) || ( m > 59) || 
      ((tod != "am") && (tod != "pm"))) {
    hour = 12;
    min = 1;
    timeofday = "am";
  }
}


//
// Name:        (copy constructor)
//
Time::Time (const Time &T){
    hour = T.hour;
    min = T.min;
    timeofday = T.timeofday;
}

//
// Name:        getHour
//
int Time::getHour()
{
  return hour;
}


//
// Name:        getHour
//
int Time::getMinute()
{
  return min;
}

//
// Name:        getTOD
//

string Time::getTOD()
{
  return timeofday;
}

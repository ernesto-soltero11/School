// File:         $Id: Song.cpp,v 1.2 2005/10/16 18:42:15 cs4 Exp $
// Description:  Class that represents a song
// Revisions:
//              $Log: Song.cpp,v $
//              Revision 1.2  2005/10/16 18:42:15  cs4
//              fixed title/artist parameter switch in constructor (jmg)
//
//              Revision 1.1  2005/10/16 17:42:36  cs4
//              Initial revision
//
//


#include "Song.h"
#include "Time.h"
#include <iostream> 

using namespace std;

//
// Name:        (default constructor)
//
Song::Song() : artist ("none"), title ("none"), last_played (0) 
{}

//
// Name:        (constructor)
//
Song::Song (string t, string a) : artist (a), title (t), last_played (0) 
{}


//
// Name:        (destructor)
//
Song::~Song()
{
	if (last_played != 0){ delete last_played;}
}

//
//Name: operator=
//Makes this Song object equal to the passed in song object.
Song& Song::operator=(const Song &s){
     if(this != &s){
         artist = s.artist;
	 title = s.title;
	
	 Time *t = s.last_played;;
	 cout << t->getTOD() << t->getHour() << t->getMinute() << endl;
	 //If s doesn't have a time object then delete this
	 //songs time object and make it equal to NULL
	 if(s.last_played == 0){
	     if(last_played != 0){
	         delete last_played;
		 last_played = 0;
	     }
	     else{last_played = 0; }
	 }
	 //else if s has a time object copy it
	 else{
	      
	      //If this song has a time object delete it before 
	      //copying the s time object
	      if(last_played != 0){
	          delete last_played;
		  last_played = new Time(*s.last_played);
	      }

	      //else just copy the time object of s
	      else{last_played = new Time(*s.last_played);}
	 }
     }
     return *this;
}

//
// Name:        operator==
//
bool Song::operator== (const Song &S)
{
  // Two songs are equal if their titles and artists are equal
  return ((title == S.title) && (artist == S.artist));
}


//
// Name:        getArtist
//
string Song::getArtist()
{
  return artist;
}

//
// Name:        getTitle
//
string Song::getTitle()
{
  return title;
}



//
// Name:        play
//
void Song::play (Time *T)
{
    //If song has never been played add time object
    if(last_played == 0){
        last_played = T;
    }

    //If song has already been played before delete old time
    //object for memory management issues. Then add the new
    //time object.
    else{
        delete last_played;
	last_played = T;
    }
}


//
// Name:        getLastPlayed
//
Time *Song::getLastPlayed ()
{
	return last_played;
}




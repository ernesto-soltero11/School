// File:     $Id: printsongs.cpp,v 1.6 2011/10/19 18:20:01 cs4 Exp $
// Author    Joe Geigel
// Description:  main program for the memory management lab. 
// Revision History:
// 	$Log: printsongs.cpp,v $
// 	Revision 1.6  2011/10/19 18:20:01  cs4
// 	replaced exit with return. fixed usage message. added arg check.
//
// 	Revision 1.5  2010/10/22 22:43:15  cs4
// 	added <cdstdlib> for building on linux machines
//
// 	Revision 1.4  2006/10/23 14:16:39  cs4
// 	fixed usage message (again)
//
// 	Revision 1.3  2006/10/23 14:15:41  cs4
// 	fixed usage message - sps
//
// 	Revision 1.2  2005/10/20 16:32:21  cs4
// 	added n as commandline arg (jmg)
//
// 	Revision 1.1  2005/10/16 17:42:36  cs4
// 	Initial revision
//
//


#include <iostream>
#include <sstream>
#include <cstdlib>
#include "Song.h"
#include "SongList.h"

using namespace std;

// This simple program will manage a list of songs played by a radio station.
// After adding songs to a playlist, the last song played, and the time
// it was played will be printed out.
//
// usage: printsongs n
//
int main (int argc, char * const argv[]) {

  // Commandline check
  if (argc != 2) {
    cerr << "usage: printsongs n" << endl;
    return(1);
  }

  // Convert commandline argument to int
  istringstream arg1 (argv[1]);
  int n;
  arg1 >> n;
  if (!arg1) {
    cerr << "usage: printsongs n" << endl;   
    cerr << "n must be an integer" << endl;
    return(1);
  }  
  
  // check for positive input value
  if ( n < 1 ) {
    cerr << "n must be positive" << endl;
    return(2);
  }

  // Create a song list
  //Adding 11 songs into the list
  SongList songs(10);
	
  // Play some songs
  songs.play (Song ("Only Time Will Tell", "Asia"), 
              new Time (10,12,"am"));
  songs.play (Song ("You'll Think of Me", "Keith Urban"), 
              new Time (10,20,"am"));
  songs.play (Song ("What I Like About You", "The Romantics"), 
              new Time (10,25,"am"));
  songs.play (Song ("Stairway to Heaven", "Led Zepplin"), 
              new Time (10,30,"am"));
  songs.play (Song ("Roundabout", "Yes"), 
              new Time (10,45,"am"));
  songs.play (Song ("I Do the Rock", "Tim Curry"), 
              new Time (10,55,"am"));
  songs.play (Song ("Better Days", "Goo Goo Dolls"), 
              new Time (11,02,"am"));
  songs.play (Song ("There's No Business Like Show Business", "Ethel Merman"),
              new Time (11,30,"am"));
  songs.play (Song ("What I Like About You", "The Romantics"), 
              new Time (11,35,"am"));
  songs.play (Song ("Twist and Shout", "The Beatles"), 
              new Time (11,39,"am"));
  songs.play (Song ("Taxi", "Harry Chapin"), 
              new Time (12,12,"pm"));
	
	
  // get the nth song played
  Song s = songs.getNthSong (n);
  Song defaultSong;
	
  // Print out the last time it was played
  if (!(s == defaultSong) ) {
    Time *T = s.getLastPlayed();
    cout << "Song #" << n << " is " << s.getTitle() << " by " 
         << s.getArtist() << ".  Last played at " << T->getHour() << ":" 
         << T->getMinute() << " " << T->getTOD() << endl;
  }

  return 0;
}

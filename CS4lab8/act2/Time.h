// File:         $Id: Time.h,v 1.1 2005/10/16 17:42:36 cs4 Exp $
// Author        Joe Geigel
// Description:  Simple class for storing the time of day
// Revision History:
//      $Log: Time.h,v $
//      Revision 1.1  2005/10/16 17:42:36  cs4
//      Initial revision
//
//


#ifndef TIME_H_DEFINED
#define TIME_H_DEFINED

#include <string>
 
using namespace std;


/***************************************************************
 *
 * Simple class for storing the time of day.  For a more comprehensive
 * treatment of time, please see 'man -s 3c ctime'
 * 
 * @author       Joe Geigel
 **************************************************************/

class Time {
 private:

  /**
   *  The hour 
   */
  int hour;

  /**
   * The minute
   */
  int min;

  /**
   * am or pm
   */
  string timeofday;
	
 public:

  /** 
   * Constructor: Creates a Time object with a given hour, minute, and time
   * of day.  If any of the values are incorrect, the resultant time object
   * will be initialized to 12:01 am.
   * 
   * @param h The hour
   * @param m The minute
   * @param tod The time of day (either "am" or "pm")
   */
  Time (int h, int m, string tod);

  /**
   * Copy Constructor: Creats a tiem object, initilaizing it with data from
   * another Time object
   *
   * @param T the object from which to get initial data
   */
  Time (const Time &T);

  /**
   * getHour: returns the hour
   *
   */
  int getHour();

  /**
   * getMinute: returns the minute
   */
  int getMinute();

  /**
   * getTOD:  returns the time of day ("am" or "pm")
   *
   */
  string getTOD();
};

#endif

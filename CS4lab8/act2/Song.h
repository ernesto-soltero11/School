// File:         $Id: Song.h,v 1.2 2011/10/19 18:25:04 cs4 Exp $
// Author        Joe Geigel
// Description:  Class that represents a song
// Revision History:
//      $Log: Song.h,v $
//      Revision 1.2  2011/10/19 18:25:04  cs4
//      fixed field declaration order.
//
//      Revision 1.1  2005/10/16 17:42:36  cs4
//      Initial revision
//
//


#ifndef SONG_H_DEFINED
#define SONG_H_DEFINED

#include <string>
class Time;
 
using namespace std;


/***************************************************************
 *
 * This class represents a Song that is played by a radio 
 * station.
 * 
 * @author       Joe Geigel
 **************************************************************/

class Song {
 private:

  /**
   * The recording artist
   */
  string artist;

  /**
   *  The title of the song
   */
  string title;

  /**
   * The time last played.  Will be equal to NULL if the song has not
   * yet been played.
   *
   * Note that the class takes ownership of this pointer.
   */
  Time *last_played;
	
 public:

  /**
   * Default construtor: Will initialize title and artist to "none"
   * and set last_played to NULL
   */
  Song();

  /** 
   * Constructor: Creates a song with a given title and artist.
   * 
   * @param t The title of the song
   * @param a The artist
   */
  Song (string t, string a);

  /**
   * Destructor
   */
  ~Song();
	
  /**
  * operator=: sets this song object equal to the passed in song
  *
  *@param song: The song to copy.
  */
  Song &operator=(const Song &s);

  /**
   * operator== : Indicates whether two songs should be considered equal
   *
   * @param S Reference to song being compared with
   */
  bool operator== (const Song &S);

  /**
   * getArtist - Returns the artist
   */
  string getArtist();

  /**
   * getTitle - Returns the title
   */
  string getTitle();

  /*
   * play: Indicates that the song has been played.  The time passed in 
   * will be recorded by the object as the last time played.
   *
   * @param T the time at which the song was played.
   */
  void play (Time *T);

  /**
   * getLastPlayed:  Returns the last time the song was played or NULL if 
   * the song has never been played.
   */
  Time *getLastPlayed();
};


#endif

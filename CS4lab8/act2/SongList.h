// File:         $Id: SongList.h,v 1.1 2005/10/16 17:42:36 cs4 Exp $
// Author        Joe Geigel
// Description:  Class that represents a playlist, i.e. a list of songs
//               played.
// Revision History:
//      $Log: SongList.h,v $
//      Revision 1.1  2005/10/16 17:42:36  cs4
//      Initial revision
//
//


#ifndef SONGLIST_H_DEFINED
#define SONGLIST_H_DEFINED

#include "Song.h"
#include "Time.h"
 
/***************************************************************
 *
 * This class represents a list of song that is played by a radio 
 * station.
 * 
 * @author       Joe Geigel
 **************************************************************/

class SongList {
private:

  /**
   * Maximum number of songs that can be stored in the list
   */
  int maxsongs;
  
  /** 
   * An array holding the songs that have been played
   */
  Song *theSongs;

  /**
   * The current number of songs in the list
   */
  int nsongs;
	
public:
  /** 
   * Constructor: Creates an empty songlist that can contain up to a 
   * maximum number of songs.
   * 
   * @param n The maximum number of songs that can be held in the list
   */
  SongList (int n);

  /**
   * Destructor
   */
  ~SongList();

  /**
   * playSong:  Indicate that a song has been played at a given time.  If 
   * the song is already in the list, it's latest play time will be updated. 
   * If not in the list, the song will be added to the list, if possible.
   * In this case, if the list is full, no song is added and an error message
   * will be outputted.
   *
   * @param S the song that was played
   * @param T the time at which the song was played.
   */
  void play (Song S, Time *T);
	

  /**
   * getNthSong:  Returns the nth song in the list.  If n is
   * outside the bounds of the list, an error message is printed and a 
   * "default song" is returned.
   *
   * @param n Which song is requested
   */
  Song getNthSong (int n);
};

#endif



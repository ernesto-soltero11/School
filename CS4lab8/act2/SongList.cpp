// File:         $Id: SongList.cpp,v 1.3 2011/10/19 21:03:26 cs4 Exp $
// Author:       Joe Geigel
// Description:  Class that represents a list of songs
// Revisions:
//              $Log: SongList.cpp,v $
//              Revision 1.3  2011/10/19 21:03:26  cs4
//              removed unneeded break
//
//              Revision 1.2  2008/10/27 18:52:02  cs4
//              fixed an indexing problem - sps
//
//              Revision 1.1  2005/10/16 17:42:36  cs4
//              Initial revision
//
//

#include "SongList.h"
#include <iostream>

using namespace std;

//
// Name:        (default constructor)
//
SongList::SongList (int n) : maxsongs (n),nsongs(0)
{
    //can't initialize an array of objects in a initilizer list	
    theSongs = new Song[n]; 
}


//
// Name:        destructor
//
SongList::~SongList ()
{
  delete[] theSongs;
}

//
// Name:        play
//
void SongList::play (Song S, Time *T)
{
  
  // check to see if the song is in the list
  bool found = false;
  int songIdx;
  for (songIdx=0; !found && (songIdx < nsongs) ; songIdx++) {
    
    if ( S.getArtist() == theSongs[songIdx].getArtist() && S.getTitle() == theSongs[songIdx].getTitle()){
        
	//Update the song if it is already in the list
	theSongs[songIdx] = S;
	found = true;
    }	
  }
	
  // If the song is not in the list, add it if you can
  if (!found) {
    if (nsongs < maxsongs) {
      // Add the song to the array
      theSongs[nsongs] = S;
  
      // Indicate the index of the song just added
      songIdx = nsongs;

      // Increment the number of songs currently in the list
      nsongs++;

      // mark that the song in question is now in the array
      found = true;
    }
    else {
      // Print an error message
      cerr << "Song list is full.  Sorry!" << endl;
    }
  }

  // If the song in question is in the list, update it's last play time
  if (found) theSongs[songIdx].play (T);
}
      
  
//
// Name:        getNthSong
//
Song SongList::getNthSong (int n)
{

  Song S;
  
  //Need to start at index 0!
  // Song 1 is index 0
  int next = n-1;

  // check to see if n is in bounds 
  if (next < nsongs) {
    // We'll need the nth song from the array
    S = theSongs[next];
  }
  else {
    // Print an error message
    cerr << "There is no song " << n << " in the list. " << endl;
  }

  return S;

}




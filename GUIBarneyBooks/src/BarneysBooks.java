/**
 * BarneysBooks.java
 * 
 * Version:
 * $Id: BarneysBooks.java,v 1.4 2012-05-02 03:54:15 exs6350 Exp $
 * 
 * Revisions:
 * $Log: BarneysBooks.java,v $
 * Revision 1.4  2012-05-02 03:54:15  exs6350
 * Added JList and Listener to track changes.
 * Also prints out selected title to appropiate
 * JTextFields.
 *
 * Revision 1.3  2012-05-02 01:58:53  exs6350
 * added a button listener
 *
 * Revision 1.2  2012-05-01 00:48:55  exs6350
 * Added more code
 *
 * Revision 1.1  2012-04-27 02:21:37  exs6350
 * Initial
 *
 * 
 */
import javax.swing.*;
import javax.swing.event.*;
import java.awt.*;
import java.awt.event.*;
import java.util.ArrayList;
import java.util.Vector;
/**
 * The GUI for the store class. Shows a list of all books that you can buy or 
 * rent and allows you to search in multiple formats.
 * @author Ernesto Soltero(exs6350@rit.edu)
 *
 */
public class BarneysBooks{

	private JRadioButton Title = new JRadioButton("Title");
	private JRadioButton Author = new JRadioButton("Author");
	private JRadioButton Media = new JRadioButton("Media Format");
	private JTextField searchbar = new JTextField();
	private JButton searchbutton = new JButton("search");
	private JButton Buy = new JButton("Buy");
	private JButton Rent = new JButton("Rent");
	private JTextField t = new JTextField();
	private JTextField a = new JTextField();
	private JTextField m = new JTextField();
	private JTextField p = new JTextField();
	private JTextField buyInfo = new JTextField();
	private JTextField content = new JTextField();
	private JList list = new JList();
	JScrollPane scrollPane = new JScrollPane(list);
	private Store store;
	private ArrayList<Book> books;
	private Book tempinfo;
	
	/**
	 * Constructor that initializes the gui. 
	 * @param A store object
	 */
	public BarneysBooks(Store store){
		this.store = store;
		JFrame window = new JFrame("Barneys Books");
		
		//Button Listener for all the buttons
		ButtonListener listener = new ButtonListener();
		searchbutton.addActionListener(listener);
		Buy.addActionListener(listener);
		Rent.addActionListener(listener);
		
		//border layout
		window.setLayout(new BorderLayout(0,15));
		
		//North 
		JPanel pannorth = new JPanel(new BorderLayout(0,10));
		
		//North north panel
		JTextArea textarea = new JTextArea("Barney's Books 'n Bytes" + "\n");
		textarea.setEditable(false);
		textarea.setFont(new Font("Dialog", Font.BOLD, 32));
		pannorth.add(textarea,BorderLayout.NORTH);
		
		//North Center panel
		FlowLayout buttonflow = new FlowLayout();
		buttonflow.setHgap(70);
		JPanel pannorthcenter = new JPanel(buttonflow);
		ButtonGroup searchbuttons = new ButtonGroup();
		searchbuttons.add(Title);
		searchbuttons.add(Author);
		searchbuttons.add(Media);
		pannorthcenter.add(Title);
		pannorthcenter.add(Author);
		pannorthcenter.add(Media);
		pannorth.add(pannorthcenter,BorderLayout.CENTER);
		
		//North South Panel
		JPanel pannorthsouth = new JPanel(new BorderLayout(0,10));
		pannorthsouth.add(searchbar,BorderLayout.NORTH);
		pannorthsouth.add(searchbutton,BorderLayout.SOUTH);
		pannorth.add(pannorthsouth,BorderLayout.SOUTH);
		
		//Add the North JPanel
		window.add(pannorth,BorderLayout.NORTH);
		
		//Center
		content.setEnabled(false);
		
		//Add the center scroll pane
		window.add(scrollPane,BorderLayout.CENTER);
		
		//South panel
		JPanel southPanel = new JPanel();
		southPanel.setLayout(new BorderLayout());
		
		//South Panel North
		JPanel southNorth = new JPanel(new BorderLayout(40,0));
		JPanel w = new JPanel(new GridLayout(0,1,20,20));
		JPanel e = new JPanel(new GridLayout(0,1,20,20));
		t.setEnabled(false);
		a.setEnabled(false);
		m.setEnabled(false);
		p.setEnabled(false);
		w.add(new JLabel("Title:"));
		e.add(t);
		w.add(new JLabel("Author:"));
		e.add(a);
		w.add(new JLabel("Media Format:"));
		e.add(m);
		w.add(new JLabel("Price:"));
		e.add(p);
		southNorth.add(w,BorderLayout.WEST);
		southNorth.add(e,BorderLayout.CENTER);
		southPanel.add(southNorth,BorderLayout.NORTH);
		
		//South Panel Center
		FlowLayout flow = new FlowLayout();
		flow.setHgap(40);
		flow.setVgap(30);
		JPanel southCenter = new JPanel(flow);
		Buy.setEnabled(false);
		Rent.setEnabled(false);
		southCenter.add(Buy);
		southCenter.add(Rent);
		southPanel.add(southCenter,BorderLayout.CENTER);
		
		//South Panel South 
		buyInfo.setEnabled(false);
		southPanel.add(buyInfo,BorderLayout.SOUTH);
		
		//Add the south JPanel
		window.add(southPanel,BorderLayout.SOUTH);
	
		
		//All other settings for the entire gui
		window.setSize(650,650);
		window.setLocation(100,100);
		window.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		window.setVisible(true);
		
		// This is when an element is selected from the list of books.
		if(bookVector.size() > 0){
		list.setListData(bookVector);}
		list.addListSelectionListener(new ListSelectionListener(){
			//This is the action when an element in the list is selected.
			public void valueChanged(ListSelectionEvent e){
				int sel = list.getSelectedIndex();
				if(sel != -1){
					for(Book i:books){
						if(i.getTitle().equals(bookVector.get(sel))){
							tempinfo = i;
						}
					}
					String cost = new Double(tempinfo.getCost()).toString();
					t.setText(tempinfo.getTitle());
					a.setText(tempinfo.getAuthor());
					m.setText(tempinfo.getMedia());
					p.setText(cost);
				}
				if(tempinfo.isForSale() == true){
					Buy.setEnabled(true);
				}
				else{Rent.setEnabled(true);}
			}
		});
	}
	
	/**
	 * 
	 * The buttonListener class. Also uses a Jlist to get the element that
	 * is selected by the user and displays the information in the related
	 * text boxes.
	 * @author Ernesto
	 *
	 */
	class ButtonListener implements ActionListener{
		
		/**
		 * @param The action performed.
		 * Searches for the text and displays it in the text boxes.
		 * 
		 */
		public void actionPerformed(ActionEvent event){
			if(event.getSource().equals(searchbutton)){
				String text = searchbar.getText();
				if(text == ""){
					return;
				}
				if(Title.isSelected() == true){
					ArrayList<Book> books.add(store.listMatching(text,store.TITLE_SEARCH));
				}
				else if(Author.isSelected() == true){
					ArrayList<Book> books.add(store.listMatching(text,store.AUTHOR_SEARCH));
				}
				else if(Media.isSelected() == true){
					ArrayList<Book> books.add(store.listMatching(text, store.MEDIA_SEARCH));
				}
			else if(event.getSource().equals(Buy)){
				buyInfo.setText("");
				buyInfo.setText("You have bought " + tempinfo.getTitle() + " for " + tempinfo.getCost());
				}
			else if(event.getSource().equals(Rent)){
				buyInfo.setText("");
				buyInfo.setText("You have rented " + tempinfo.getTitle() + " for " + store.rentalPrice);
				}
			}
		}
	}
	
	/**
	 * @param args
	 */
	public static void main(String[] args) throws java.io.FileNotFoundException{
		Store store = new Store();
		store.fillInventory(args[0]);
		new BarneysBooks(store);

	}

}
